#pragma once
#include<fstream>
#define SLog(arg, ...) CLog::getInstance().Log((wchar_t *)arg, __VA_ARGS__);
#define NLog(arg)	   CLog::getInstance().NetworkLog((wchar_t *)arg);


class CLog : public Singleton<CLog>
{
public:

	CLog() {
		wstring name = L"LogFile.log";
		mFile.imbue(std::locale("kor"));
		mFile.open(name, std::ios::out | std::ios::trunc);
		if (mFile.fail()) {
			printf("! 에러발생\n");
		}
		if (mFile.bad()) {
			printf("! LogFile error .... file open Failed.\n");
		}
	}
	virtual ~CLog()
	{
		mFile.close();
		mFile.clear();

		wstring beforeName = L"LogFile.log";
		wstring  closeFilename = {};
		closeFilename += CLOCK.nowTime((wchar_t *)L"_%Y%m%d-%H%M%S.log");
		// LogFile name 변경 함수
		_wrename(beforeName.c_str(), closeFilename.c_str());
	}

	void Log(wchar_t * inPtr, ...)
	{
		std::array<wchar_t, 1024> buffer = {};
		wstring temp = {};
		va_list vs;

		va_start(vs, inPtr);
		vswprintf(buffer.data(), inPtr, vs);
		temp += buffer.data();
		temp += L"\n";
		this->StoreDataLog(temp.c_str());
		va_end(vs);
	}
	void NetworkLog(wchar_t * inWstr)
	{
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL, WSAGetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf, 0, NULL);
		Log((wchar_t *)L"[%s] %s\n", inWstr, (LPCTSTR)lpMsgBuf);
		LocalFree(lpMsgBuf);
		exit(1);
	}
	void StoreDataLog(const array<wchar_t, 1024> & inData)
	{

	}
	void StoreDataLog(const wchar_t * inPtr)
	{
		wprintf(L"%s", inPtr);
		mFile << inPtr;
		mFile.flush();
	}
private:
	wfstream mFile;
};


#define Assert(str) DumpAssert::Execute(__FUNCTION__, __LINE__, (wchar_t *)str)

class DumpAssert
{
public:
	DumpAssert() {}
	virtual ~DumpAssert() {}
	static void Execute(string inFileName, int inClassLine, wchar_t * inStr) {
		SLog(L"[AssertDump][Method_Name: %S ], [ Edit_Line : %d] [message : %s]", inFileName.c_str(), inClassLine, inStr);
	}
};