#pragma once
class DirectImage
{
public:
	DirectImage();
	DirectImage(const wchar_t * inFileName);
	virtual ~DirectImage();

	void Draw(const D2D1_RECT_F & inPosition) {
		DirectGraphics::getInstance().GetRenderTarget()->DrawBitmap(
			mBmp,
			inPosition,
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			D2D1::RectF(0.0f, 0.0f, mBmp->GetSize().width, mBmp->GetSize().height)
		);
		D2D1_TAG a;
		D2D1_TAG b;
		auto retval = DirectGraphics::getInstance().GetRenderTarget()->Flush(&a, &b);
		if (retval != S_OK)
			return;
	}
	void Draw(const D2D1_RECT_F & inSheetRect, const D2D1_RECT_F & inFieldPosition) {
		DirectGraphics::getInstance().GetRenderTarget()->DrawBitmap(
			mBmp,
			inFieldPosition,
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			inSheetRect
		);
	}
	void Draw(const D2D1_RECT_F & inPosition, float inAlpha) {
		DirectGraphics::getInstance().GetRenderTarget()->DrawBitmap(
			mBmp,
			inPosition,
			inAlpha,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			D2D1::RectF(0.0f, 0.0f, mBmp->GetSize().width, mBmp->GetSize().height)
		);
		D2D1_TAG a;
		D2D1_TAG b;
		auto retval = DirectGraphics::getInstance().GetRenderTarget()->Flush(&a, &b);
		if (retval != S_OK)
			return;
	}
private:
	ID2D1Bitmap * mBmp;
};