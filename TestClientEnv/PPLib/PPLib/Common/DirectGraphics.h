#pragma once

#include<DWrite.h>
#include<string>


class DirectGraphics : public Singleton<DirectGraphics>
{
public:
	DirectGraphics() {}
	virtual ~DirectGraphics();
	bool Initialize(HWND & inWindowsHandle);

	void BeginDraw() { mRenderTarget->BeginDraw(); }
	void EndDraw() { 
		HRESULT hr = mRenderTarget->EndDraw(); 
	}

	void ClearScreen(float r, float g, float b);
	void DrawCircle(float x, float y, float radius, float r, float g, float b, float a);
	void DrawRectangle();
	void DrawRectangle(const D2D1::ColorF inColor, const D2D1_RECT_F rect)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(inColor, &brush);

		// mRenderTarget->DrawRectangle(rect, brush, 5.0f, 0);
		mRenderTarget->FillRectangle(rect, brush);
		brush->Release();
	}
/*
	void DrawMap()
	{
		DrawWorld(12, 12);
	}

	void DrawMap(const XMFLOAT2& inPosition, const StageEnum & inCurrentStageEnum)
	{
		World::getInstance().DrawField(inPosition, inCurrentStageEnum);
	}

	void DrawMap(const XMFLOAT2& inPosition)
	{
		DrawWorld(inPosition);
		DrawMinimap(inPosition);
	}

	void DrawWorld(const XMFLOAT2& inPosition)
	{
		const int len = 50;
		float deltaX = 0.f;
		float deltaY = 0.f;
		int widStart = 0;
		int widEnd = 0;
		int higStart = 0;
		int higEnd = 0;
		if (inPosition.x < 300)
		{
			widStart = 0;
			widEnd = (((int)inPosition.x + 300) / len) + 1 > maxLength ? maxLength : (((int)inPosition.x + 300) / len) + 1;
			deltaX = 300 - inPosition.x;
		}
		else if (inPosition.x >= 300)
		{
			widStart = ((int)inPosition.x - 300) / len;
			if (widStart > 0) --widStart;
			widEnd = ((((int)inPosition.x + 300) / len) + 1) > maxLength ? maxLength : ((((int)inPosition.x + 300) / len) + 1);
			deltaX = 300 - inPosition.x;
		}
		if (inPosition.y < 300)
		{
			higStart = 0;
			higEnd = (((int)inPosition.y + 300) / len) + 1 > maxLength ? maxLength : (((int)inPosition.y + 300) / len) + 1;
			deltaY = 300 - inPosition.y;
		}
		if (inPosition.y >= 300)
		{
			higStart = ((int)inPosition.y - 300) / len;
			if (higStart > 0) --higStart;
			higEnd = (((int)inPosition.y + 300) / len) + 1 > maxLength ? maxLength : (((int)inPosition.y + 300) / len) + 1;
			deltaY = 300 - inPosition.y;
		}
		auto pos = inPosition;
		char mm[maxLength][maxLength] = {
			{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' }
		};



		wstring str;
		int fps = Timer::getInstance().GetFrameRate();
		wchar_t  ch[150] = L"";
		_stprintf_s(ch, L" FPS : %d currrent Location [x : %.2f] [y : %.2f] wid [start : %d end : %d ]hig [s : %d end : %d ] ",
			fps, pos.x, pos.y, widStart, widEnd, higStart, higEnd);
		str = ch;

		SetWstring(str);

		ID2D1SolidColorBrush * brush;
		ID2D1SolidColorBrush * brush2;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(86, 185, 0, 1), &brush2);

		for (auto higth = higStart; higth < higEnd; ++higth)
		{
			for (auto wid = widStart; wid < widEnd; ++wid)
			{
				int deltaleft = (wid * len) + (int)deltaX;
				int deltaup = (higth * len) + (int)deltaY;
				int deltaright = ((wid + 1) * len) + (int)deltaX;
				int deltadown = ((higth + 1)* len) + (int)deltaY;
				if ((min(max(deltaleft, 0), 600) == deltaleft && min(max(deltaup, 0), 600) == deltaup) ||
					(min(max(deltaleft, 0), 600) == deltaleft && min(max(deltadown, 0), 600) == deltadown) ||
					(min(max(deltaright, 0), 600) == deltaright && min(max(deltaup, 0), 600) == deltaup) ||
					(min(max(deltaright, 0), 600) == deltaright && min(max(deltadown, 0), 600) == deltadown))
				{
					if (mm[higth][wid] == 'a') {
						mRenderTarget->DrawRectangle(
							D2D1::RectF(
							(float)deltaleft,			// Left
								(float)deltaup,		// UP
								(float)deltaright,	// RIGHT
								(float)deltadown),	// DOWN
							brush,
							1.0f,
							0);
					}
					else if (mm[higth][wid] == 'b')
					{
						mRenderTarget->FillRectangle(D2D1::RectF(
							(float)deltaleft,			// Left
							(float)deltaup,		// UP
							(float)deltaright,	// RIGHT
							(float)deltadown),	// DOWN
							brush2);
						//mRenderTarget->DrawRectangle(
						//	D2D1::RectF(
						//		deltaleft,			// Left
						//		deltaup,		// UP
						//		deltaright,	// RIGHT
						//		deltadown),	// DOWN
						//	brush2,
						//	1.0f,
						//	0);
					}

				}
			}
		}
		brush->Release();
		brush2->Release();
	}
	void DrawMinimap(const XMFLOAT2& inPosition)
	{
		float deltaX = 0.f;
		float deltaY = 0.f;
		int widStart = 0;
		int widEnd = 0;
		int higStart = 0;
		int higEnd = 0;
		// TODO :해당 라인에 미니맵이 같이 이동하는 원인이 있을 것이라 추정됨
		if (inPosition.x < 300)
		{
			widStart = 0;
			widEnd = (((int)inPosition.x + 300) / 50) + 1 > 20 ? 20 : (((int)inPosition.x + 300) / 50) + 1;
			deltaX = 300 - inPosition.x;
		}
		else if (inPosition.x >= 300)
		{
			widStart = ((int)inPosition.x - 300) / 50;
			if (widStart > 0) --widStart;
			widEnd = ((((int)inPosition.x + 300) / 50) + 1) > 20 ? 20 : ((((int)inPosition.x + 300) / 50) + 1);
			deltaX = 300 - inPosition.x;
		}
		if (inPosition.y < 300)
		{
			higStart = 0;
			higEnd = (((int)inPosition.y + 300) / 50) + 1 > 20 ? 20 : (((int)inPosition.y + 300) / 50) + 1;
			deltaY = 300 - inPosition.y;
		}
		if (inPosition.y >= 300)
		{
			higStart = ((int)inPosition.y - 300) / 50;
			if (higStart > 0) --higStart;
			higEnd = (((int)inPosition.y + 300) / 50) + 1 > 20 ? 20 : (((int)inPosition.y + 300) / 50) + 1;
			deltaY = 300 - inPosition.y;
		}
		auto pos = inPosition;
		char mm[20][20] = {
			{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'b', 'b', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' },
		{ 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' ,'a' }
		};

		const int len = 8;


		ID2D1SolidColorBrush * brush;
		ID2D1SolidColorBrush * brush2;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(86.F, 185.F, 0.F, 0.3F), &brush2);

		for (auto higth = higStart; higth < higEnd; ++higth)
		{
			for (auto wid = widStart; wid < widEnd; ++wid)
			{
				int deltaleft = (wid * len) + (int)deltaX;
				int deltaup = (higth * len) + (int)deltaY;
				int deltaright = ((wid + 1) * len) + (int)deltaX;
				int deltadown = ((higth + 1)* len) + (int)deltaY;
				if ((min(max(deltaleft, 0), 600) == deltaleft && min(max(deltaup, 0), 600) == deltaup) ||
					(min(max(deltaleft, 0), 600) == deltaleft && min(max(deltadown, 0), 600) == deltadown) ||
					(min(max(deltaright, 0), 600) == deltaright && min(max(deltaup, 0), 600) == deltaup) ||
					(min(max(deltaright, 0), 600) == deltaright && min(max(deltadown, 0), 600) == deltadown))
				{
					if (mm[higth][wid] == 'a') {
						mRenderTarget->DrawRectangle(
							D2D1::RectF(
							(float)deltaleft,			// Left
								(float)deltaup,		// UP
								(float)deltaright,	// RIGHT
								(float)deltadown),	// DOWN
							brush,
							0.3f,
							0);
					}
					else if (mm[higth][wid] == 'b')
					{
						mRenderTarget->FillRectangle(D2D1::RectF(
							(float)deltaleft,			// Left
							(float)deltaup,		// UP
							(float)deltaright,	// RIGHT
							(float)deltadown),	// DOWN
							brush2);
					}

				}
			}
		}
		brush->Release();
		brush2->Release();

	}
	void DrawWorld(int inWidth, int inHigth)
	{
		const int len = 30;
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);

		for (auto higth = 0; higth < inHigth; ++higth)
		{
			for (auto wid = 0; wid < inWidth; ++wid)
			{
				mRenderTarget->DrawRectangle(
					D2D1::RectF((float)(wid * len), (float)(higth * len), (float)((wid + 1) * len),
					(float)((higth + 1)* len)), brush, 1.0f, 0);
			}
		}
		brush->Release();
	}

	void DrawPlayer()
	{
		const int len = 30;
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);

		mRenderTarget->DrawEllipse(D2D1::Ellipse(
			D2D1::Point2F(300, 300), 15, 15), brush);
		brush->Release();


	}
	*/
	HRESULT initialized_Text()
	{
		HRESULT hr;
		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,
			__uuidof(mWriteFactory), (IUnknown**)&mWriteFactory);
		if (FAILED(hr))
			return hr;
		hr = mWriteFactory->CreateTextFormat(
			L"맑은고딕",
			0,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			15,
			L"ko",
			&mWriteTextFormat
		);
		return hr;
	}

	void DrawTextW(wstring inText, D2D1_RECT_F rect)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1, 1, 1, 1), &brush);
		mRenderTarget->DrawTextW(inText.c_str(), (uint32_t)inText.length(), mWriteTextFormat, rect, brush);

		brush->Release();
	}
	void DrawTextW(wstring inText, D2D1_RECT_F rect, float alpha)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1, 1, 1, alpha), &brush);
		mRenderTarget->DrawTextW(inText.c_str(), (uint32_t)inText.length(), mWriteTextFormat, rect, brush);

		brush->Release();
	}
	void DrawChatText(const wstring & inStr, const D2D1_RECT_F & inPosition, const D2D1_COLOR_F inColor)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(inColor, &brush);
		mRenderTarget->DrawTextW(inStr.c_str(), (uint32_t)inStr.length(), mWriteTextFormat,
			inPosition, brush);
		brush->Release();
	}

	void DrawFillRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor);
	

	void ConstTextDraw(const wstring & inStr, D2D1_RECT_F inRect, int inFontSize, DWRITE_TEXT_ALIGNMENT inAlignment = DWRITE_TEXT_ALIGNMENT_LEADING)
	{
		wstring datss = inStr;
		IDWriteTextLayout *textLayout = 0;
		ID2D1SolidColorBrush * brush;

		HRESULT hr = mWriteFactory->CreateTextLayout(
			datss.c_str(),
			datss.length(),
			mWriteTextFormat,
			inRect.right - inRect.left,
			inRect.bottom - inRect.top,
			&textLayout
		);
		D2D1_POINT_2F startPoint;
		startPoint.x = inRect.left;
		startPoint.y = inRect.top;
		hr = mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);
		DWRITE_TEXT_RANGE range;
		range.startPosition = 0;
		range.length = datss.length();
		hr = textLayout->SetTextAlignment(inAlignment);
		hr = textLayout->SetFontSize((float)inFontSize, range);
		mRenderTarget->DrawTextLayout(startPoint, textLayout, brush, D2D1_DRAW_TEXT_OPTIONS_NO_SNAP);
		// mRenderTarget->DrawTextW(inStr.c_str(), inStr.length(),			mWriteTextFormat, inRect,brush);
		brush->Release();
		textLayout->Release();
	}

	void ConstTextDraw(const wstring & inStr, D2D1_RECT_F inRect, int inFontSize, const D2D1_COLOR_F inColor ,DWRITE_TEXT_ALIGNMENT inAlignment = DWRITE_TEXT_ALIGNMENT_LEADING)
	{
		wstring datss = inStr;
		IDWriteTextLayout *textLayout = 0;
		ID2D1SolidColorBrush * brush;

		HRESULT hr = mWriteFactory->CreateTextLayout(
			datss.c_str(),
			datss.length(),
			mWriteTextFormat,
			inRect.right - inRect.left,
			inRect.bottom - inRect.top,
			&textLayout
		);
		D2D1_POINT_2F startPoint;
		startPoint.x = inRect.left;
		startPoint.y = inRect.top;
		hr = mRenderTarget->CreateSolidColorBrush(inColor, &brush);
		DWRITE_TEXT_RANGE range;
		range.startPosition = 0;
		range.length = datss.length();
		hr = textLayout->SetTextAlignment(inAlignment);
		hr = textLayout->SetFontSize((float)inFontSize, range);
		mRenderTarget->DrawTextLayout(startPoint, textLayout, brush, D2D1_DRAW_TEXT_OPTIONS_NO_SNAP);
		// mRenderTarget->DrawTextW(inStr.c_str(), inStr.length(),			mWriteTextFormat, inRect,brush);
		brush->Release();
		textLayout->Release();
	}
	ID2D1RenderTarget * GetRenderTarget() { return mRenderTarget; }

	void DrawRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect);

	void DrawFillRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect);
	void DrawRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor);

private:
	ID2D1Factory * mFactory;
	IDWriteFactory * mWriteFactory = nullptr;
	IDWriteTextFormat * mWriteTextFormat = nullptr;
	ID2D1HwndRenderTarget * mRenderTarget;
};

