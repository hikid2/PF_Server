#include "CommonHeader.h"
#include "DirectGraphics.h"


DirectGraphics::~DirectGraphics()
{
	CoUninitialize();
	if (mWriteTextFormat)
		mWriteTextFormat->Release();
	if (mWriteFactory)
		mWriteFactory->Release();
	if (mFactory)
		mFactory->Release();
	if (mRenderTarget)
		mRenderTarget->Release();
}

bool DirectGraphics::Initialize(HWND & inWindowsHandle)
{
	mFactory = nullptr;
	mRenderTarget = nullptr;

	if (S_OK != CoInitializeEx(NULL, COINIT_APARTMENTTHREADED))
		return false;
	auto retval = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &mFactory);
	if (retval != S_OK)
		return false;

	RECT rect;
	GetClientRect(inWindowsHandle, &rect);

	retval = mFactory->CreateHwndRenderTarget(
		D2D1::RenderTargetProperties(),
		D2D1::HwndRenderTargetProperties(inWindowsHandle, D2D1::SizeU(rect.right, rect.bottom), D2D1_PRESENT_OPTIONS_IMMEDIATELY),
		&mRenderTarget);
	if (retval != S_OK)
		return false;
	retval = initialized_Text();
	if (retval != S_OK)
		return false;

	return true;
}

void DirectGraphics::ClearScreen(float r, float g, float b)
{
	mRenderTarget->Clear(D2D1::ColorF(r, g, b));
}

void DirectGraphics::DrawCircle(float x, float y, float radius, float r, float g, float b, float a)
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(r, g, b, a), &brush);

	mRenderTarget->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(x, y), radius, radius), brush, 3.0f);
	brush->Release();
}

void DirectGraphics::DrawRectangle()
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);

	mRenderTarget->DrawRectangle(D2D1::RectF(50.0f, 50.0f, 200.0f, 200.0f), brush, 5.0f, 0);
	brush->Release();
}


void DirectGraphics::DrawRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect)
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(inColor, &brush);

	mRenderTarget->DrawRectangle(inRect, brush, 5.0f, 0);
	brush->Release();
}

void DirectGraphics::DrawFillRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect)
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(inColor, &brush);

	mRenderTarget->FillRectangle(inRect, brush);
	
	brush->Release();
}

void DirectGraphics::DrawRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor)
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(inColor, &brush);
	mRenderTarget->DrawRoundedRectangle(rect, brush);
	brush->Release();
}

void DirectGraphics::DrawFillRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor)
{
	ID2D1SolidColorBrush * brush;
	mRenderTarget->CreateSolidColorBrush(inColor, &brush);
	mRenderTarget->FillRoundedRectangle(rect, brush);
	brush->Release();
}
