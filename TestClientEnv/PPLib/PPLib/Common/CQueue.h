#pragma once

template<class T>
class CQueue
{
public:
	CQueue() {}
	~CQueue() {}
	void Enqueue(T & Data)
	{
		mLock.Lock();
		mQueue.emplace(std::move(Data));
		// mQueue.push(std::move(Data));
		mLock.UnLock();
	}
	template<class T>
	bool Try_Dequeue(T & outData)
	{
		mLock.Lock();
		if (!mQueue.empty()) {
			outData = std::move(mQueue.front());
			mQueue.pop();
			mLock.UnLock();
			return true;
		}
		mLock.UnLock();
		return false;
	}
	const size_t GetSize() {
		mLock.Lock();
		size_t size = mQueue.size();
		mLock.UnLock();
		return size;
	}

private:
	std::queue<T> mQueue;
	BackOffSpinLock mLock;
};

