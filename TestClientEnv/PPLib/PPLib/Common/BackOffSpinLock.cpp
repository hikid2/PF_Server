#include"CommonHeader.h"
#include"BackOffSpinLock.h"
void BackOffSpinLock::Lock() {
	while (true) {
		while (mState) {}
		if (!TestAndSet(true))
			return;
		else
			InterruptedException();

	}
}

void BackOffSpinLock::UnLock() {
	while (!TestAndSet(false)) {}
}
