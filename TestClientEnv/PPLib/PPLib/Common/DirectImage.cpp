#include "CommonHeader.h"
#include "DirectImage.h"


DirectImage::DirectImage()
{
}

DirectImage::DirectImage(const wchar_t * inFileName)
{
	mBmp = NULL;
	HRESULT hr;	// HRSULT for check err
	IWICImagingFactory * wicFactory = nullptr;
	IWICBitmapDecoder * decoderPtr = nullptr;
	IWICBitmapFrameDecode * wicFrame = nullptr;
	IWICFormatConverter * wicConverter = nullptr;
	try
	{
		hr = CoCreateInstance(
			CLSID_WICImagingFactory,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IWICImagingFactory,
			(LPVOID*)&wicFactory);
		if (hr != S_OK)
			throw exception("Error 1");
		// wicFactory 에서  파일을 Read한다
		hr = wicFactory->CreateDecoderFromFilename(
			inFileName,
			NULL,
			GENERIC_READ,
			WICDecodeMetadataCacheOnLoad,
			&decoderPtr);
		// Frame을 Read 한다.
		if (hr != S_OK)
			throw exception("Error 2");
		hr = decoderPtr->GetFrame(0, &wicFrame);
		if (hr != S_OK)
			throw exception("Error 3");
		// Create a Converter

		hr = wicFactory->CreateFormatConverter(&wicConverter);
		if (hr != S_OK)
			throw exception("Error 4");

		hr = wicConverter->Initialize(
			wicFrame,
			GUID_WICPixelFormat32bppPBGRA,
			WICBitmapDitherTypeNone,
			NULL,
			0.0,
			WICBitmapPaletteTypeCustom);
		if (hr != S_OK)
			throw exception("Error 5");
		hr = DirectGraphics::getInstance().GetRenderTarget()->CreateBitmapFromWicBitmap(
			wicConverter,
			NULL,
			&mBmp);
		if (hr != S_OK)
			throw exception("Error 6");
	}
	catch (const std::exception& exp)
	{
		cout << exp.what();
	}

	if (wicFactory) wicFactory->Release();
	if (decoderPtr)  decoderPtr->Release();
	if (wicConverter) wicConverter->Release();
	if (wicFrame) wicFrame->Release();
}


DirectImage::~DirectImage()
{
	if (mBmp) mBmp->Release();
}
