#pragma once

class BackOffSpinLock
{
public:
	BackOffSpinLock(int & inMin, int & inMax)
		: mMinDelay(inMin), mMaxDelay(inMax), mLimit(inMax), mState(false)
	{}
	BackOffSpinLock()
		: mMinDelay(2), mMaxDelay(60), mLimit(60), mState(false)
	{}
	void InterruptedException()
	{
		int delay = rand() % mLimit + 1;
		if (mLimit<mMaxDelay)
			mLimit = 2 * mLimit;
		Sleep(delay);
	}


	void Lock();
	void UnLock();
private:
	int				mMinDelay;
	int				mMaxDelay;


	int				mLimit;
	volatile long	mState = false;


	long TestAndSet(long instate) { return _InterlockedExchange(&mState, instate); }
};