#pragma once

extern bool loadConfig(JsonValue_t * inRoot, const char * inFileName);
extern bool loadConfig(JsonValue_t * inRoot);
class IDirectFrameWork
{
public:
	IDirectFrameWork(){}

	virtual ~IDirectFrameWork() {}

	void SetHwnd(HWND * inPtr) { mHwnd = inPtr; }

	virtual void Update() = 0;

	virtual void ExecutePacket(Packet * inPacketPtr) = 0;
	virtual void ExecutePacket(ReadStream & inPacketPtr) = 0;

	virtual void InputKeyBoardMessage(const WPARAM & inWParam) = 0;
	virtual void InputKeyBoardMessage(const LPARAM & inWParam, HWND & inHwnd) = 0;

private:
	HWND * mHwnd;
};