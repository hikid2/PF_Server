#pragma once

class GameObject
{
public:
	GameObject(){}
	GameObject(int & inLocation, wstring & inName) : mLocation(inLocation), mName(inName){}
	virtual ~GameObject(){}
	virtual void Draw() = 0;
protected:
	int		 mLocation;
	wstring  mName;
};

class Item : public GameObject
{
public:


private:
	byte mCurrentField;
};

class Character : public GameObject
{
public:
	Character() : GameObject() {}
	Character(int & inLocation, wstring & inName) : GameObject(inLocation, inName){}
	virtual ~Character(){ }
	void SetDirection(XMFLOAT2 & inDir) {}
	void SetHp(uint32_t inHp) { mHp = inHp; }
	void SetMp(uint32_t inMp) { mMp = inMp; }
	void SetGold(uint32_t inGold) { mGold = inGold; }
	void SetPosition(XMFLOAT2 & inPos) { mPosition = inPos; }
	void SetVelocity(uint32_t & inVelocity) { mVelocity = inVelocity; }
	void SetCurrnetExp(uint32_t inExp){}
	void SetNextLevelExp(uint32_t inExp){}
	void SetLevel(uint32_t inExp){}
	virtual void Draw() {}
protected:
	uint32_t mNetID;
	XMFLOAT2 mLookVector;
	XMFLOAT2 mPosition;
	uint32_t mHp;
	uint32_t mMp;
	uint32_t mGold;
	uint32_t mVelocity;
	uint32_t mCurrnetExp;
	uint32_t mNextLevelExp;
	uint32_t mLevel;
};


class Warrior : public Character
{
public:
	Warrior() : Character(){}
	virtual ~Warrior() {}
	virtual void Move() = 0;
	virtual void Attact() = 0;
	virtual void Update() = 0;
private:

	
};
class WarriorClient : public Warrior
{
public:


private:

};

class WarriorServer : public Warrior
{
public:
	WarriorServer(): Warrior(){}
	void Move()
	{

	}
	void Attact() 
	{

	}
private:

};

class Wizard : public Character
{

};
class WizardClient : public Wizard
{

};
class WizardServer : public Wizard
{

};

class Elf : public Character
{
	
};

class ElfClient : public Elf
{

};
class ElfServer : public Elf
{

};

class Monster : public GameObject
{

};
