#pragma once

#include "targetver.h"






#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.


#pragma comment(lib, "D2D1.lib")
#pragma comment(lib, "dwrite.lib")
#pragma comment(lib, "Windowscodecs.lib")

#pragma warning(disable:4996)
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"imm32.lib")

#include<wincodec.h>
#include<atomic>
#include<WinSock2.h>
#include<D3D10_1.h>
//
#include<DirectXMath.h>
using namespace DirectX;
//
//
#include <windows.h>
#include<imm.h>
#include<thread>
#include<DirectXCollision.h>
#include<WS2tcpip.h>
#include<mutex>
#include<queue>


// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
#include <iostream>
using namespace std;

#include "json\json.h"

typedef Json::Value JsonValue_t;
typedef Json::Reader JsonReader_t;


// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>
#include<array>
#include<d2d1.h>


#undef	SAFE_DELETE
#define SAFEDELETE(obj)						\
{												\
	if ((obj)) delete(obj);		    			\
    (obj) = 0L;									\
}

const int SocketMaxCnt = 3;
const int IOBufferSize = 10240;
const int PacketBufferSize = 10240;

#include"DirectTools\IPanelTool.h"
#include"DirectTools\PanelTextBoard.h"
#include"DirectTools\ICamera.h"
#include"DirectTools\IGameLevel.h"



#include"Common\BackOffSpinLock.h"
#include"Common\CQueue.h"

#include"Common\Singleton.h"
#include"Common\Timer.h"
#include"Common\Clock.h"
#include"Common\Log.h"
#include"Common\Stream.h"
#include"Common\DirectGraphics.h"
#include"DirectTools\TileMap.h"
#include"Common\DirectImage.h"
#include"Common\GameObject.h"
#include"Network\Packet.h"
#include"Network\Session.h"
#include"Network\PacketType.h"
#include"Network\PacketAnalyzer.h"
#include"Network\EventSelectNetwork.h"
#include"Common\IDirectFrameWork.h"