#pragma once
class IPanelTools
{
public:
	IPanelTools() = default;
	virtual ~IPanelTools() = default;
	virtual bool IdentifyFocus(const POINT & inPoint) = 0;
	virtual const D2D1_RECT_F & GetPosition()const = 0;
};