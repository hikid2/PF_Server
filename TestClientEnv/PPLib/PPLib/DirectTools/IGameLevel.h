#pragma once


/*
레벨
특징 : 오브젝트와 해당 레벨의 데이터와 기능을 관리
로그인레벨
- 패널데이터
-- 버튼 (좌표) buttond 2d1_RectF ,버튼이름 string
-- 텍스트 출력데이터 위치, string
-- 입력보드 출력데이터 위치, string
-- 포커스 파악기능 Function()
인게임 레벨
- 오브젝트 오브젝트 매니저
- 인벤토리 인벤토리 데이터
- 채팅출력데이터
- 포커스 파악기능
*/


class IGameLevel
{
public:
	IGameLevel() {}
	virtual ~IGameLevel() {}
	virtual void Initialize() = 0;
	virtual void Render() = 0;
	virtual bool Update(const uint32_t & inDeltaTime) = 0;
	virtual const uint32_t GetLevelName() = 0;
	virtual void MouseButtonUp(const POINT & inPoint) = 0;
	virtual void MouseButtonDown(const POINT & inPoint) = 0;
	virtual bool GetInputFocus(D2TextBoard *& outPtr) = 0;

	virtual void InsertMessage(wstring & inText) = 0;
	virtual void KeyBoardIdentify(const WPARAM & inWParam) = 0;
	virtual void KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd) = 0;
	
};

