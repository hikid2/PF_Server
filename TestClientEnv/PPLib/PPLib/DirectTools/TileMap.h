#pragma once

class TiledMap
{
public:
	TiledMap() { mBmp = nullptr;  mMaxRow = 0; }
	TiledMap(const wchar_t * inFileName, uint32_t inFirstGid) :mFileName(inFileName), mFirstGid(inFirstGid) { this->Initialized(inFileName); }
	~TiledMap() {
		if (mBmp != nullptr)
		{
			mBmp->Release();
			mBmp = nullptr;
		}

	}
	void Initialized(const wchar_t * inFileName) {
		if (S_OK != CoInitializeEx(NULL, COINIT_MULTITHREADED))
			return;
		mBmp = nullptr;
		HRESULT hr;	// HRSULT for check err
		IWICImagingFactory * wicFactory = nullptr;
		hr = CoCreateInstance(
			CLSID_WICImagingFactory,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IWICImagingFactory,
			(LPVOID*)&wicFactory);
		if (hr != S_OK) { return; }
		IWICBitmapDecoder * decoderPtr = nullptr;
		// wicFactory 에서  파일을 Read한다
		hr = wicFactory->CreateDecoderFromFilename(
			inFileName,
			NULL,
			GENERIC_READ,
			WICDecodeMetadataCacheOnLoad,
			&decoderPtr);
		// Frame을 Read 한다.
		IWICBitmapFrameDecode * wicFrame = nullptr;
		hr = decoderPtr->GetFrame(0, &wicFrame);

		// Create a Converter
		IWICFormatConverter * wicConverter = nullptr;
		hr = wicFactory->CreateFormatConverter(&wicConverter);

		hr = wicConverter->Initialize(
			wicFrame,
			GUID_WICPixelFormat32bppPBGRA,
			WICBitmapDitherTypeNone,
			NULL,
			0.0,
			WICBitmapPaletteTypeCustom);

		hr = DirectGraphics::getInstance().GetRenderTarget()->CreateBitmapFromWicBitmap(
			wicConverter,
			NULL,
			&mBmp);

		if (wicFactory) wicFactory->Release();
		if (decoderPtr)  decoderPtr->Release();
		if (wicConverter) wicConverter->Release();
		if (wicFrame) wicFrame->Release();
		if (mBmp != nullptr) {
			float width = mBmp->GetSize().width;
			mMaxRow = (uint32_t)floor(width / 32);
			auto tt = floor(mBmp->GetSize().width / 32) * floor(mBmp->GetSize().height / 32);
			mLastGid = ((uint32_t)tt - 1) + mFirstGid;
		}
		CoUninitialize();
	}
	void Draw(const D2D1_RECT_F & inPosition, const uint32_t inCount) {
		uint32_t bitCalculate = (inCount & ~0xE0000000);
		uint32_t deltaCount = bitCalculate - mFirstGid;
		uint32_t outColumn = deltaCount / mMaxRow;
		uint32_t outRow = deltaCount % mMaxRow;
		// 90 degrees
		if ((inCount & 0xA0000000) == 0xA0000000) {
			DirectGraphics::getInstance().GetRenderTarget()->SetTransform(
				D2D1::Matrix3x2F::Rotation(90.0f,
					D2D1::Point2F(inPosition.left + 25,
						inPosition.top + 25)));

		}
		// 180 degrees
		else if ((inCount & 0xC0000000) == 0xC0000000) {
			DirectGraphics::getInstance().GetRenderTarget()->SetTransform(
				D2D1::Matrix3x2F::Rotation(180.0f,
					D2D1::Point2F(inPosition.left + 25,
						inPosition.top + 25)));

		}
		// 270 degrees
		else if ((inCount & 0x60000000) == 0x60000000) {
			DirectGraphics::getInstance().GetRenderTarget()->SetTransform(
				D2D1::Matrix3x2F::Rotation(270.0f,
					D2D1::Point2F(inPosition.left + 25,
						inPosition.top + 25)));
		}
		D2D1_RECT_F ImageTileArea = D2D1::RectF((float)32 * outRow, (float)32 * outColumn, (float)32 * (outRow + 1), (float)32 * (outColumn + 1));
		this->Draw(ImageTileArea, inPosition);
		DirectGraphics::getInstance().GetRenderTarget()->SetTransform(D2D1::Matrix3x2F::Identity());
	}
	bool IsValidID(const uint32_t & inGid) {
		uint32_t bitCalculate = (inGid & ~0xE0000000);
		bool retval = max(min(mLastGid, bitCalculate), mFirstGid) == bitCalculate ? true : false;
		return retval;
	}
private:
	void Draw(const D2D1_RECT_F & inTileArea, const D2D1_RECT_F & inPosition) {
		DirectGraphics::getInstance().GetRenderTarget()->DrawBitmap(
			mBmp,
			inPosition,
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			inTileArea
		);
	}
	ID2D1Bitmap * mBmp;
	uint32_t	mFirstGid;
	uint32_t	mLastGid;
	uint32_t	mMaxRow;

	wstring		mFileName;
};

