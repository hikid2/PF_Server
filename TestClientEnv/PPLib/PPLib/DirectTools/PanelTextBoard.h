#pragma once
class D2TextBoard : public IPanelTools
{
public:
	D2TextBoard() {}
	D2TextBoard(D2D1_RECT_F inPosition);
	virtual ~D2TextBoard();
	virtual bool IdentifyFocus(const POINT & inPoint);
	virtual const D2D1_RECT_F & GetPosition() const { return mPosition; }
	virtual wstring & GetText() { return mInputText; }
	D2TextBoard(D2TextBoard && other)
		: mPosition(other.mPosition), mInputText(move(other.mInputText))
	{
		other.mPosition = {};
		other.mInputText.clear();
	}
	D2TextBoard & operator= (D2TextBoard && other) {
		if (this != &other)
		{
			mPosition = other.mPosition;
			mInputText = other.mInputText;
			other.mPosition = {};
			other.mInputText.clear();
			return *this;
		}
	}
	void SetPosition(const D2D1_RECT_F & inData) {
		mPosition = inData;
	}
	void Clear() { mInputText.clear(); }
private:
	D2D1_RECT_F mPosition;
	wstring		mInputText;
};