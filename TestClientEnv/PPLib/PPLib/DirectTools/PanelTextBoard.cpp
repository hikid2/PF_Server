#include "CommonHeader.h"
#include "PanelTextBoard.h"

D2TextBoard::D2TextBoard(D2D1_RECT_F inPosition)
	: mPosition(inPosition), mInputText()
{
}

D2TextBoard::~D2TextBoard() {}

/**
@return : bool (true = Focus Hit, false  = Focus Miss)

@param1 : Current Mouse Point Reference

@brief : 텍스트보드의 크기와 마우스 Point의 범위가 겹치는지 확인

@warning : 미검증
*/
bool D2TextBoard::IdentifyFocus(const POINT & inPoint)
{
	if ((this->mPosition.left < inPoint.x) && (this->mPosition.right > inPoint.x) &&
		(this->mPosition.top < inPoint.y) && (this->mPosition.bottom > inPoint.y)) {
		return true;
	}
	return false;
}