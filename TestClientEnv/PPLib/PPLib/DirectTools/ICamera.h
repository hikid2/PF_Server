#pragma once
class GameObject;
class ICamera
{
public:
	virtual void Draw(GameObject * inObject) = 0;
	virtual void PutPivot(GameObject * inObjectPtr) = 0;

};
