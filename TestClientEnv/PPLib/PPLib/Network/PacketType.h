#pragma once


//enum PacketType : uint32_t
//{
//	PK_CTS_LOGIN = 10000,	// 클라이언트에서 서버로 로그인을 요청한다.
//	PK_CTS_JOIN,			// 클라이언트에서 서버로 회원가입을 요청한다
//	PK_CTS_REGISTCHARACTER, // 클라이언트->서버 캐릭터 생성을 요청한다.
//	PK_CTS_DeleteCharacter, // 클라이언트 -> 서버 캐릭터 삭제를 요청한다.
//	PK_CTS_EnterInWorld,	// 클라이언트->서버로 게임월드 입장 요청
//	PK_CTS_NotifyChatData,	// Local Client에서 타인에게 전송할 채팅 데이터를 서버에게 전송
//	PK_CTS_GameWorldINPUTData, // 게임 월드속 Local Client의 입력데이터를 서버로 전송.
//	PK_CTS_INPUT,
//	PK_Test_STC_Init,
//	PK_Test_STC_PutObject,
//	PK_Test_STC_Remove,
//	PK_Test_STC_Move,
//
//
//	PK_STC_LOGINSUCCESS = 20000,	// 서버->클라이언트 로그인 성공
//	PK_STC_LOGINFAIL,				// 서버->클라이언트 로그인 실패
//	PK_STC_JOINSUCCESS,				// 서버->클라이언트 회원가입 성공
//	PK_STC_JOINFAIL,				// 서버->클라이언트 회원가입 실패
//	PK_STC_RegistCharacterSuccesss, // 서버->클라이언트 캐릭터 생성 성공
//	PK_STC_RegistCharacterFail,		// 서버->클라이언트 캐릭터 생성 실패.
//	PK_STC_InitializeWorldData,		// 서버->클라이언트 월드 정보 초기화 패킷 전송?
//	PK_STC_BroadCastChatData,		// 서버->클라이언트 A플레이어의 채팅 내용을 일정 범위의 다른 유저에게 전달한다.
//};

class Packet;

class PacketFactory 
{
public:
	PacketFactory() {}
	virtual ~PacketFactory() {}

	virtual bool GetPacket(const uint32_t & inPacketType, Packet *& outPacketPtr) = 0;
};
