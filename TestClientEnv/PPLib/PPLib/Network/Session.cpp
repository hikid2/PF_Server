#include"CommonHeader.h"
#include"Session.h"

Session::Session()
{
	RecvStandby();
}

Session::~Session()
{
	closesocket(mSocket);
}

bool Session::Initialize(const string inServerIp, const int inPort)
{
	mSocket = WSASocket(AF_INET, SOCK_STREAM, NULL, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (mSocket == INVALID_SOCKET)
	{
		Assert(L"WSASocket method failed");
		return false;
	}

	mServerAddress.sin_family = AF_INET;
	inet_pton(AF_INET, inServerIp.c_str(), &mServerAddress.sin_addr);
	mServerAddress.sin_port = htons(inPort);
	if (!this->Connect())
	{
		Assert(L"Connect() method failed");
		return false;
	}
	return true;
}

const int Session::Send(WriteStream & inData, uint32_t & inPacketSize, size_t & inSendOffset)
{
	int retval = ::send(this->mSocket, inData.GetData() + inSendOffset, inPacketSize, 0);
	return retval;
}

/**
@return : void

@param1 : 송신을 희망하는 Data

@brief : 송신을 희망하는 Data를 체크하여 송신함수 호출

@warning :
*/
void Session::SendPacket(WriteStream & inData)
{
	uint32_t packetSize = inData.GetOffset();
	size_t sendOffset = 0;
	while (true)
	{
		int retval = Send(inData, packetSize, sendOffset);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				NLog(L"Send Error");
				closesocket(mSocket);
				return;
			}
			mIoData[LWRITE].WriteData(inData, packetSize);
			break;
		}
		if (retval < packetSize) {
			sendOffset = retval;
			packetSize -= retval;
			continue;
		}
		break;
	}
}

void Session::SendPacket(Packet * inPacket)
{
	WriteStream streamData;
	inPacket->Write(streamData);
	streamData.WriteSize();
	uint32_t packetSize = streamData.GetOffset();
	size_t sendOffset = 0;
	while (true)
	{
		int retval = Send(streamData, packetSize, sendOffset);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				NLog(L"Send Error");
				closesocket(mSocket);
				return;
			}
			mIoData[LWRITE].WriteData(streamData, packetSize);
			break;
		}
		if (retval < packetSize) {
			sendOffset = retval;
			packetSize -= retval;
			continue;
		}
		break;
	}
}

void Session::SendPacket(unsigned char * packet)
{
	int len = 0; //= packet[0];
	memcpy_s(&len, sizeof(int), packet, sizeof(int));
	int offset = 0;
	while (true)
	{
		int retval = ::send(this->mSocket, (const char  *)(packet + offset), len, 0);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				NLog(L"Send Error");
				closesocket(mSocket);
				return;
			}
			break;
		}
		if (retval < len) {
			offset = retval;
			len -= retval;
			continue;
		}
		break;
	}
}

const int Session::Send()
{
	if (mIoData[LWRITE].GetTotalByte() - mIoData[LWRITE].GetCurrentIOByte() == 0)
	{

	}
	int retval = ::send(
		this->mSocket,
		mIoData[LWRITE].GetBuffer() + mIoData[LWRITE].GetCurrentIOByte(),
		mIoData[LWRITE].GetTotalByte() - mIoData[LWRITE].GetCurrentIOByte(),
		0);
	return retval;
}

const int Session::Recv()
{
	int retval = ::recv(this->mSocket, mIoData[LREAD].GetBuffer() + mIoData[LREAD].GetCurrentIOByte(),
		mIoData[LREAD].GetTotalByte() - mIoData[LREAD].GetCurrentIOByte(),
		0);
	return retval;
}

void Session::ConnectProcess()
{
	SLog(L"ConnectProcess!!!!!!");
	if (WSAEventSelect(mSocket, mEventHandle, FD_WRITE | FD_READ | FD_CLOSE) == SOCKET_ERROR)
	{
		SLog(L"ConnectProcess NO!!!!!!!!!");
	}
}

bool Session::OnSend()
{
	while (true)
	{
		int retval = this->Send();
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				NLog(L"Send Error");
				closesocket(mSocket);
			}
			return false;
		}
		if (mIoData[LWRITE].CompareSendSize(retval)) {
			SendTaskClear();
			break;
		}
	}
	return true;
}

bool Session::onRecv()
{
	while (true)
	{
		int retval = this->Recv();
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				NLog(L"Recv Method Error");
				closesocket(mSocket);
				return false;
			}
			return false;
		}
		if (ProcessPacketCheck(retval)) {
			//PacketAnalize(outReadStream);
		}

	}
	return true;
}


/**
@return : (true : 헤더에 적재된 사이즈와 패킷사이즈가 일치, false : 헤더에 적재된 사이즈와 패킷사이즈가 불일치)

@param 1 : Recv()함수의 Return PackctSize

@brief : 패킷에더에 적재된 패킷의 이상적크기와 실제 받은 패킷의 크기를 Compare하고 넘어갔을경우, 적을경우 처리

@warning :
*/
bool Session::ProcessPacketCheck(int & inRecvSize)
{
	
	uint32_t headerSize = 0;
	while (true)
	{
		size_t offset = 0;
		ReadStream readStream;
		memcpy_s(&headerSize, sizeof(uint32_t), mIoData[IO_Enum::LREAD].GetBuffer(), sizeof(byte));
		offset += sizeof(byte);

		// 덜 받은경우 처리는? 더 받을수 있게 준비를 하자
		if (inRecvSize < headerSize) {
			mIoData[IO_Enum::LREAD].SetTotalByte(headerSize);
			mIoData[IO_Enum::LREAD].SetCurrentIOByte(inRecvSize);
			return false;
		}
		// 많이 받은 경우는? 짤라서 선처리 후 나머지를 체크하자
		else if (inRecvSize > headerSize)
		{
			readStream.set(mIoData[IO_Enum::LREAD].GetBuffer() + offset, headerSize - offset);
			PacketAnalyzer::getInstance().ExecuteAnalyze(readStream);

			size_t reorderSize = inRecvSize - headerSize;
			// Execute AttemptReorder 
			memcpy_s(mIoData[IO_Enum::LREAD].GetBuffer(),
				reorderSize,
				mIoData[IO_Enum::LREAD].GetBuffer() + headerSize,
				reorderSize
			);
			memset(mIoData[IO_Enum::LREAD].GetBuffer() + reorderSize, NULL, headerSize);
			inRecvSize = reorderSize;
			continue;
		}
		readStream.set(mIoData[IO_Enum::LREAD].GetBuffer() + offset, headerSize - offset);
		PacketAnalyzer::getInstance().ExecuteAnalyze(readStream);
		this->RecvStandby();
		return true;
	}
}

void Session::RecvStandby()
{
	mIoData[IO_Enum::LREAD].ClearBuffer();
	mIoData[IO_Enum::LREAD].SetTotalByte(PacketBufferSize);
	mIoData[IO_Enum::LREAD].SetCurrentIOByte(0);
	mIoData[IO_Enum::LREAD].SetOffset(0);

}

inline bool Session::Connect()
{
	mEventHandle = WSACreateEvent();
	if (WSAEventSelect(mSocket, mEventHandle, FD_CONNECT | FD_CLOSE) == SOCKET_ERROR)
	{
		return false;
	}

	int retval = WSAConnect(mSocket, (SOCKADDR*)&mServerAddress, sizeof(mServerAddress), NULL, NULL, NULL, NULL);
	if (GetLastError() != WSAEWOULDBLOCK && retval == INVALID_SOCKET)
	{
		auto aaa = GetLastError();
		return false;
	}


	// 6. WSAEventSelect()
	// The return value is zero if the application's specification of 
	// the network events and the associated event object was successful. 
	// Otherwise, the value SOCKET_ERROR is returned, and a specific error 
	// number can be retrieved by calling WSAGetLastError.
	return true;
}

void IoData::WriteData(WriteStream & inWriteStream, const size_t & inStreamOffset)
{
	if (!BufferWriteCheck(inWriteStream.GetOffset())) {
		mTempDataQueue.push(std::move(inWriteStream));
		SLog(L"mTempDataQueue keepping");
		return;
	}
	// Lock,Lock();
	atomic_thread_fence(memory_order_seq_cst);
	memcpy_s(
		this->mBuffer.data() + mOffset,
		IOBufferSize - mOffset,
		inWriteStream.GetData(),
		inStreamOffset);
	mTotalByte += inStreamOffset;
	mOffset += inStreamOffset;
	// Lock.Unlock();
}

/**
@return : bool(true : 데이터를 IoBuffer에 작성가능, false : IoBuffer에 작성 불가능)

@param1 : 작성하고자 하는 Data의 size

@brief : 현재 IOBuffer에 inDataSize만큼 작성이 가능한지를 확인

@warning : None
*/
bool IoData::BufferWriteCheck(const size_t & inDataSize)
{
	if (mOffset + inDataSize < IOBufferSize) {
		return true;
	}
	return false;
}
