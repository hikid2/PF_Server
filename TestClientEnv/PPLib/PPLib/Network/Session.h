#pragma once
#include<unordered_map>
#include<queue>

typedef enum IO_ENUM
{
	LWRITE,
	LREAD,
	LMAX
}IO_Enum;

class IoData
{
private:
	typedef std::array<char, IOBufferSize> IO_Buffer;
public:
	IoData() : mCurrentIOByte(0), mTotalByte(0), mOffset(0) { mBuffer.assign(NULL); }

	~IoData() {}

	char * GetBuffer() { return mBuffer.data(); }

	const volatile size_t & GetCurrentIOByte() const { return mCurrentIOByte; }

	const volatile size_t & GetTotalByte() const { return mTotalByte; }

	void SetTotalByte(const size_t & inSize) { mTotalByte = inSize; }

	void ClearBuffer() { mBuffer.assign(NULL); }

	void WriteData(WriteStream & inWriteStream, const size_t & inStreamOffset);

	bool CompareSendSize(const int & inSendBytes)
	{
		// 보내려고 한 데이터와 Send의 SendBytes의 대조필요.
		if (inSendBytes < mTotalByte) {
			// 적은 데이터를 보냈다.
			// 그럼 나머지 를 더 보내야 한다.
			mCurrentIOByte += inSendBytes;
			return false;
		}
		return true;
	}

	void SetOffset(const size_t & inSize) { mOffset = inSize; }
	void SetCurrentIOByte(const size_t & inSize) { mCurrentIOByte = inSize; }
	// const volatile size_t & GetOffset() { return mOffset; }
private:
	bool BufferWriteCheck(const size_t & inDataSize);
	// 현재 보내진 바이트
	volatile size_t		mCurrentIOByte;
	volatile size_t		mOffset = 0;
	// mBuffer에 들어간 총 바이트
	volatile size_t		mTotalByte;
	IO_Buffer	mBuffer;
	std::queue<WriteStream> mTempDataQueue;
};

class Session
{
public:
	Session();
	virtual ~Session();


	bool Initialize(const string inServerIp, const int inPort);

	void SendPacket(WriteStream & inData);

	void SendPacket(Packet * inPacket);
	
	void SendPacket(unsigned char *packet);

	const int Send();

	const int Send(WriteStream & inData, uint32_t & inPacketSize, size_t & inSendOffset);

	const int Recv();

	const WSAEVENT & GetWSAEvent() const { return mEventHandle; }

	SOCKET & GetSocket() { return mSocket; }

	void ConnectProcess();

	bool OnSend();

	bool onRecv();

	bool ProcessPacketCheck(int & inRecvSize);

	void RecvStandby();

private:

	void SendTaskClear()
	{
		this->mIoData[LWRITE].ClearBuffer();
		this->mIoData[LWRITE].SetOffset(0);
		this->mIoData[LWRITE].SetTotalByte(0);
		this->mIoData[LWRITE].SetCurrentIOByte(0);
	}
	bool Connect();

	SOCKET									mSocket;
	SOCKADDR_IN								mServerAddress;
	std::array<IoData, IO_Enum::LMAX>		mIoData;
	WSAEVENT								mEventHandle;
	int64_t									mOid;
	INT8									mNetworkIndex = 0;
};

