#pragma once
class PacketAnalyzer : public Singleton<PacketAnalyzer>
{
public:
	PacketAnalyzer() = default;
	virtual ~PacketAnalyzer()
	{
		SAFEDELETE(mFactoryPtr)
	}
	void InsertFactory(PacketFactory * inPtr)
	{
		mFactoryPtr = inPtr;
	}
	void ExecuteAnalyze(ReadStream & inReadStream);
	PacketFactory * mFactoryPtr = nullptr;
};