#include"CommonHeader.h"
#include "EventSelectNetwork.h"


bool ReadFromFile(const char * inFile, string & inReadBuffer)
{
	ifstream file(inFile);
	if (!file.is_open()) {
		printf("! File err.have not Config file\n");
		return false;
	}
	file.seekg(0, ios::end);
	int size = (int)file.tellg();
	file.seekg(0, ios::beg);
	inReadBuffer.resize(size);
	file.read(&inReadBuffer[0], size);
	file.close();
	return true;
}


bool loadConfig(JsonValue_t * inRoot, const char * inFileName)
{
	string readBuffer;
	string tempName = ".\\";
	tempName.append(inFileName);
	tempName.append(".json");

	if (!ReadFromFile(tempName.c_str(), readBuffer)) {
		return false;
	}
	JsonReader_t reader;
	if (!reader.parse(readBuffer, *inRoot)) {
		printf("!! Json Parse Err. !!\n");
		return false;
	}
	return true;
}

bool loadConfig(JsonValue_t * root)
{
	string readBuffer;
	if (!ReadFromFile(".\\config.json", readBuffer)) {
		return false;
	}
	JsonReader_t reader;
	if (!reader.parse(readBuffer, *root)) {
		printf("!! Json Parse Err. !!\n");
		return false;
	}
	return true;
}

extern bool Shutdown;

EventSelectNetwork::~EventSelectNetwork() 
{ 
	if (mServerSession != nullptr)
	{
		delete mServerSession;
		mServerSession = nullptr;
	}

	CleanUp(); 
}

bool EventSelectNetwork::run()
{
	int Port = 0;
	string Ip = "";
	// 1. WSAStartup()
	if (WSAStartup(MAKEWORD(2, 2), &mWsaData) != 0)
	{
		SLog(L"WSAStartup() mehtod is failed...");
		return false;
	}
	Readconfig(Ip, Port);
	mServerSession = new Session();
	if (!mServerSession->Initialize(Ip, Port))
	{
		Assert(L"Initialize fail");
		return false;
	}
	if (!EventSelectProcess())
	{
		SLog(L"EventSelectProcess return false client Exit");
		return false;
	}
	return true;
}



bool WINAPI EventSelectNetwork::EventSelectProcess()
{
	int retEventIndex = 0;
	while (!Shutdown)
	{
		// Read, Write, COnnect, Close Event Manage 
		WSANETWORKEVENTS Events;
		Session * sessionPtr = GetServerSession();
		if (sessionPtr == nullptr) {
			return false;
		}
		auto eventHandle = sessionPtr->GetWSAEvent();
		retEventIndex = WSAWaitForMultipleEvents(1, &eventHandle, false, WSA_INFINITE, false);
		if (WSA_WAIT_FAILED == retEventIndex || WSA_WAIT_TIMEOUT == retEventIndex) continue;
		else
		{
			auto ServerSockekt = sessionPtr->GetSocket();
			retEventIndex = retEventIndex - WSA_WAIT_EVENT_0;
			int retval = WSAEnumNetworkEvents(ServerSockekt, eventHandle, &Events);
			if (retval == SOCKET_ERROR) {
				SLog(L"Error");
				closesocket(ServerSockekt);
			}
			if (Events.lNetworkEvents & FD_READ)
			{
				if (Events.iErrorCode[FD_READ_BIT] != 0)
				{
					NLog(L"FD_READ Error");
				}
				else
				{
					sessionPtr->onRecv();
				}
			}
			if (Events.lNetworkEvents & FD_WRITE)
			{
				if (Events.iErrorCode[FD_WRITE_BIT] != 0)
				{
					NLog(L"FD_Write Error");
				}
				else
				{
					sessionPtr->OnSend();
				}
			}
			else if (Events.lNetworkEvents & FD_CLOSE)
			{
				if (Events.iErrorCode[FD_CLOSE_BIT] != 0)
				{
					NLog(L"FD_CLOSE Error");
					this->CleanUp();
					return false;
				}
				else {
					this->CleanUp();
				}
			}
			else if (Events.lNetworkEvents & FD_CONNECT)
			{
				if (Events.iErrorCode[FD_CONNECT_BIT] != 0)
				{
					NLog(L"FD_CONNECT Error");
					this->CleanUp();
					return false;

				}
				sessionPtr->ConnectProcess();
			}
			/*switch (Events.lNetworkEvents)
			{
			case FD_READ:
				if (Events.iErrorCode[FD_READ_BIT] != 0)
				{
					NLog(L"FD_READ Error");
				}
				else
				{
					sessionPtr->onRecv();
				}
				break;
			case FD_WRITE:
				if (Events.iErrorCode[FD_WRITE_BIT] != 0)
				{
					NLog(L"FD_Write Error");
				}
				else
				{
					sessionPtr->OnSend();
				}
				break;
			case FD_CLOSE:
				if (Events.iErrorCode[FD_CLOSE_BIT] != 0)
				{
					NLog(L"FD_CLOSE Error");
					this->CleanUp();
					return false;
				}
				else {
					this->CleanUp();
				}
				break;
			case FD_CONNECT:
				if (Events.iErrorCode[FD_CONNECT_BIT] != 0)
				{
					NLog(L"FD_CONNECT Error");
					this->CleanUp();
					return false;

				}
				sessionPtr->ConnectProcess();
				break;
			default:
				break;
			}*/
		}
	}
	return true;
}

void EventSelectNetwork::CleanUp()
{
	if (mServerSession != nullptr) {
		SAFEDELETE(mServerSession);
		WSACleanup();
	}
}

inline Session * EventSelectNetwork::GetServerSession() { return mServerSession; }

inline bool EventSelectNetwork::Readconfig(string & inIp, int & inPort) {
	JsonValue_t root;
	if (loadConfig(&root, "Config")) {
		if (!root.get("Server", "").isObject()) return false;
		inIp = root.get("Server", "").get("IP", "").asString();
		inPort = root.get("Server", "").get("PORT", "").asInt();
		return true;
	}
	return false;
}
