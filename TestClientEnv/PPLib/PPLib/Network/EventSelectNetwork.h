#pragma once
extern bool loadConfig(JsonValue_t * inRoot, const char * inFileName);
extern bool loadConfig(JsonValue_t * inRoot);
class EventSelectNetwork : public Singleton<EventSelectNetwork>
{
public:
	EventSelectNetwork() { }
	virtual ~EventSelectNetwork();
	bool run();
	bool EventSelectProcess();
	void CleanUp();
	Session * GetServerSession();

	CQueue<ReadStream>						mPacketJobQueue;
private:
	bool Readconfig(string & inIp, int & inPort);

	WSADATA mWsaData;
	int mPortNum;
	Session * mServerSession;
};