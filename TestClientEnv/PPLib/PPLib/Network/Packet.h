#pragma once
class Packet
{
public:
	virtual BYTE GetPacketType() = 0;
	virtual void Write(WriteStream & inStream) { }
	virtual void Read(ReadStream & inStream) { }
};