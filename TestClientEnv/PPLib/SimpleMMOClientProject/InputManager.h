#pragma once

#define MAXBOARDLENGTH 100
class InputManager : public Singleton<InputManager>
{
public:
	InputManager() : mOffset(0), mBoardState(false){}
	void Imm_Write(const TCHAR inCh, D2TextBoard * inTextData);
	void Imm_Write(const TCHAR inCh, wstring & inTextData);
	void Imm_Composition(const LPARAM & inlParam, D2TextBoard * inTextData, HWND & hwnd);
	void Imm_Composition(const LPARAM & inlParam, wstring & inTextData, HWND & hwnd);
	void GetKeyBoardData(int & outKeydata, IGameLevel * inGameLevelPtr)
	{
		static UCHAR keyBuffer[256];
		// 일정 시간마다 1번씩 체크.
		if (GetKeyboardState(keyBuffer))
		{
			if (keyBuffer[VK_RETURN] & 0x80)
			{

			}
			else if (keyBuffer['1'] & 0x80) {
				
				
			}
			else if (keyBuffer['2'] & 0x80) {
				
			}
			else if (keyBuffer['3'] & 0x80) {
				
			}
			else if (keyBuffer['A'] & 0x80) {
				outKeydata += 1 << 4;
			}
			else if (keyBuffer[VK_UP] & 0x80)
			{
				outKeydata += 1 << 0;
				// PlayerManager::getInstance().SetLocalAvatarDIrection(EUP);
			}
			else if (keyBuffer[VK_DOWN] & 0x80)
			{
				outKeydata += 1 << 1;
				// PlayerManager::getInstance().SetLocalAvatarDIrection(EDOWN);
			}
			else if (keyBuffer[VK_LEFT] & 0x80)
			{
				// PlayerManager::getInstance().SetLocalAvatarDIrection(ELEFT);
				outKeydata += 1 << 2;
			}
			else if (keyBuffer[VK_RIGHT] & 0x80)
			{
				// PlayerManager::getInstance().SetLocalAvatarDIrection(ERIGHT);
				outKeydata += 1 << 3;
			}
		}
	}
private:
	uint32_t			 mOffset;
	bool				 mBoardState = false;
};
