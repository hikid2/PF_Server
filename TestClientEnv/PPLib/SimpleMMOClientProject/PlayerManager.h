#pragma once
extern bool loadConfig(JsonValue_t * inRoot, const char * inFileName);
extern bool loadConfig(JsonValue_t * inRoot);
enum AvatarClass : byte
{
	E_Warrior = 1,
	E_Wizerd,
	E_Elf,
	M_mushroom,
	M_SLIME_Green,
	M_SLIME_Orange,
	M_SLIME_SKY,
	M_SLIME_RED,
	M_LITTLE_SKELETON,
	M_LITTLE_Goblin,
	M_LITTLE_Golem,
	M_BAT,
	M_FIRESKELETONHEAD,
};
#ifdef DEBUG
struct Avatar
{
	int id;
	wstring name;
	XMFLOAT2 pos;
	byte dir;
	byte class_type;
	uint32_t mLevel;
	uint32_t mCurrentHP;
	uint32_t mCurrentMP;
	uint32_t mCurrentExp;
	uint32_t mMaxHP;
	uint32_t mMaxMP;
	uint32_t mNextExp;
};
#endif // DEBUG

enum DIrection
{
	ERIGHT,
	ELEFT,
	EDOWN,
	EUP,
};
class Avatar
{
public:
	Avatar() { SetLookVector(0, 1); }
	~Avatar() {}
	Avatar(const Avatar & instance) 
		: mObjectID(instance.mObjectID), mName(instance.mName), mLevel(instance.mLevel), mClassType(instance.mClassType),
		mCurrHP(instance.mCurrHP), mCurrMP(instance.mCurrMP), mMaxHP(instance.mMaxHP)
		,mCurrExp(instance.mCurrExp), mNeedExp(instance.mNeedExp), mNowField(instance.mNowField), 
		str(instance.str),
		dex(instance.dex),
		ints(instance.ints),
		luck(instance.luck)
	{
	
		
		mPoint = instance.mPoint;
		mHasGold = instance.mHasGold;
		str = instance.str;
		dex = instance.dex;
		ints = instance.ints;
		luck = instance.luck;
		mLookVector = instance.mLookVector;
	}
	void Clear() {}

	const float & GetAvatarPositionX() { return mPoint.x; }

	const float & GetAvatarPositionY() { return mPoint.y; }

	void SetAvatarPosition(int32_t inPx, int32_t inPy)
	{
		mPoint.x = inPx;
		mPoint.y = inPy;
	}
	void SetAvatarPosition(XMFLOAT2 inPoint)
	{
		mPoint.x = inPoint.x;
		mPoint.y = inPoint.y;
	}
	const byte & GetClassType() { return mClassType; }
	const BYTE & GetNowField() { return mNowField; }
	const uint32_t & GetHp() { return mCurrHP; }
	void SetLv(const int & inLV) { mLevel = inLV; }
	void SetGold(const int & inGold) { mHasGold = inGold; }
	void SetAbility(const int & inStr, const int & inDex, const int & inInts, const int & inLuck)
	{
		dex = inDex;
		ints = inInts;
		luck = inLuck;
		str = inStr;
	}
	void SetNowField(const int & inValue) { mNowField = inValue; }
	void SetClassType(const byte & inValue) { mClassType = inValue; }
	void SetCurrHP(const int & inValue) { mCurrHP = inValue; }
	void SetCurrMP(const int & inValue) { mCurrMP = inValue; }
	void SetMaxHP(const int & inValue) { mMaxHP = inValue; }
	void SetMaxMP(const int & inValue) { mMaxMP = inValue; }
	void SetCurExp(const int64_t  inValue) { mCurrExp = inValue; }
	void SetNeedExp(const int64_t inValue) { mNeedExp = inValue; }
	void SetName(const wstring & inValue) { mName = inValue; }


	const int & GetLv() { return mLevel; }
	const int & GetGold() { return mHasGold; }
	const int & GetCurrHP() { return mCurrHP; }
	const int & GetCurrMP() { return mCurrMP; }
	const int & GetMaxHP() { return mMaxHP; }
	const int & GetMaxMP() { return mMaxMP; }
	const int64_t & GetCurExp() { return mCurrExp; }
	const int64_t & GetNeedExp() { return mNeedExp ; }
	const wstring & GetName() { return mName; }
	XMFLOAT2 & GetPoint() { return mPoint; }
	void SetID(int inValue) { mObjectID = inValue; }
	const int & GetID() { return mObjectID; }
	void SetLookVector(int inHorizontal, int inVertical) 
	{ 
		auto dumyDirections = XMVectorSet(inHorizontal, inVertical, 0, 0);
		XMStoreFloat2(&mLookVector, dumyDirections);
	}
	int GetDIrction()
	{
		/// Right
		if (mLookVector.x == 1) { return ERIGHT; }
		/// Left
		else if (mLookVector.x == -1) { return ELEFT; }
		/// Down
		else if (mLookVector.y == 1) { return EDOWN; }
		/// Up
		else if (mLookVector.y == -1) { return EUP; }
	}
	XMFLOAT2 & GetLookVector() { return mLookVector; }

	void LevelUp(sc_packet_notify_Levelup packet) 
	{
		mLevel = packet.mLv;
		mMaxHP = packet.mMaxHP;
		mMaxMP= packet.mMaxMP;
		mNeedExp = packet.mNeedExp;
		mCurrExp= packet.mNowExp;
		mCurrHP = packet.mNowHP;
		mCurrMP = packet.mNowMP;
	}
private:
	int			mObjectID;
	wstring		mName;
	int			mLevel;
	byte		mClassType;
	uint32_t	mCurrHP;
	uint32_t	mCurrMP;
	uint32_t	mMaxHP;
	uint32_t	mMaxMP;
	int64_t		mCurrExp;
	int64_t		mNeedExp;
	int		mNowField;
	XMFLOAT2	mPoint;
	int			mHasGold;
	int			str;
	int			dex;
	int			ints;
	int			luck;

	XMFLOAT2	mLookVector;
};
struct AvatarImage
{
	DirectImage * ImageData;
	string CharacterName;
	int wid;
	int hig;
	array<vector<pair<int, int>>, 4> mSheetIndex;


	vector<pair<int, int>> left;
	vector<pair<int, int>> rig;
	vector<pair<int, int>> up;
	vector<pair<int, int>> down;
};

enum ENUM_PlayerState
{
	UNREADY,
	READY,
	DEAD,
};
class PlayerManager : public Singleton<PlayerManager>
{
public:
	PlayerManager() : mOwnID(-1) , mOwnPlayerState(UNREADY){ Initialize(); }
	virtual ~PlayerManager()
	{
		for (auto & iter : mAvatarImages)
		{
			if (iter.second.ImageData != nullptr) {
				delete iter.second.ImageData;
				iter.second.ImageData = nullptr;
			}

		}
		mAvatarImages.clear();
		for (auto & iter : mAvatars)
		{
			SAFEDELETE(iter.second);
		}
	}
	void Initialize()
	{
		mNameToAvatarEnum["Elf"] = AvatarClass::E_Elf;
		mNameToAvatarEnum["Magician"] = AvatarClass::E_Wizerd;
		mNameToAvatarEnum["Warrior"] = AvatarClass::E_Warrior;
		mNameToAvatarEnum["M_mushroom"] = AvatarClass::M_mushroom;
		mNameToAvatarEnum["M_SLIME_Green"] = AvatarClass::M_SLIME_Green;
		mNameToAvatarEnum["M_SLIME_Orange"] = AvatarClass::M_SLIME_Orange;
		mNameToAvatarEnum["M_SLIME_SKY"] = AvatarClass::M_SLIME_SKY;
		mNameToAvatarEnum["M_SLIME_RED"] = AvatarClass::M_SLIME_RED;
		mNameToAvatarEnum["M_LITTLE_SKELETON"] = AvatarClass::M_LITTLE_SKELETON;
		mNameToAvatarEnum["M_LITTLE_Goblin"] = AvatarClass::M_LITTLE_Goblin;
		mNameToAvatarEnum["M_LITTLE_Golem"] = AvatarClass::M_LITTLE_Golem;
		mNameToAvatarEnum["M_BAT"] = AvatarClass::M_BAT;
		mNameToAvatarEnum["M_FIRESKELETONHEAD"] = AvatarClass::M_FIRESKELETONHEAD;
		JsonValue_t root;
		loadConfig(&root, "CharacterScriptData");
		auto objects = root.get("CharacterScriptData", "");
		for (auto iter : objects)
		{
			AvatarImage tmpimage = AvatarImage();
			wstring fileNameW;
			tmpimage.CharacterName = iter.get("name", "").asString();

			string filenameS = iter.get("Filename", "").asString();
			fileNameW.resize(filenameS.size() + 1);
			StrConvA2W(&filenameS.front(), &fileNameW.front(), filenameS.size() + 1);
			tmpimage.ImageData = new DirectImage(fileNameW.data());
			int framemax = 3;
			tmpimage.wid = iter.get("wid", "").asInt();
			tmpimage.hig = iter.get("hig", "").asInt();
			int row = 0;
			int col = 1;
			for (int frame = 0; frame < framemax; ++frame)
			{
				// righr
				tmpimage.mSheetIndex[ERIGHT].push_back(make_pair(iter.get("dir", "").get("right", "")[frame][row].asInt(),
					iter.get("dir", "").get("right", "")[frame][col].asInt()));
				// left
				tmpimage.mSheetIndex[ELEFT].push_back(make_pair(iter.get("dir", "").get("left", "")[frame][row].asInt(),
					iter.get("dir", "").get("left", "")[frame][col].asInt()));
				// down
				tmpimage.mSheetIndex[EDOWN].push_back(make_pair(iter.get("dir", "").get("down", "")[frame][row].asInt(),
					iter.get("dir", "").get("down", "")[frame][col].asInt()));
				// up
				tmpimage.mSheetIndex[EUP].push_back(make_pair(iter.get("dir", "").get("up", "")[frame][row].asInt(),
					iter.get("dir", "").get("up", "")[frame][col].asInt()));




				///*tmpimage.down.push_back(make_pair(
				//	iter.get("dir", "").get("down", "")[frame][row].asInt(),
				//	iter.get("dir", "").get("down", "")[frame][col].asInt())
				//);
				//tmpimage.up.push_back(make_pair(
				//	iter.get("dir", "").get("up", "")[frame][row].asInt(),
				//	iter.get("dir", "").get("up", "")[frame][col].asInt())
				//);
				//tmpimage.left.push_back(make_pair(
				//	iter.get("dir", "").get("left", "")[frame][row].asInt(),
				//	iter.get("dir", "").get("left", "")[frame][col].asInt())
				//);
				//tmpimage.rig.push_back(make_pair(
				//	iter.get("dir", "").get("right", "")[frame][row].asInt(),
				//	iter.get("dir", "").get("right", "")[frame][col].asInt())*/
				//);
			}
			mAvatarImages.emplace(make_pair(mNameToAvatarEnum[tmpimage.CharacterName.data()], tmpimage));
		}
		loadConfig(&root, "MonsterScriptData");
		auto mon_objs = root.get("MonsterList", "");
		for (auto iter : mon_objs)
		{
			AvatarImage tmpimage = AvatarImage();
			wstring fileNameW;
			tmpimage.CharacterName = iter.get("name", "").asString();

			string filenameS = iter.get("Filename", "").asString();
			fileNameW.resize(filenameS.size() + 1);
			StrConvA2W(&filenameS.front(), &fileNameW.front(), filenameS.size() + 1);
			tmpimage.ImageData = new DirectImage(fileNameW.data());
			int framemax = iter.get("frame", "").asInt();
			int row = 0;
			int col = 1;
			tmpimage.wid = iter.get("wid", "").asInt();
			tmpimage.hig = iter.get("hig", "").asInt();
			for (int frame = 0; frame < framemax; ++frame)
			{
				// RIGHT
				tmpimage.mSheetIndex[ERIGHT].push_back(make_pair(iter.get("dir", "").get("down", "")[frame][row].asInt(),
					iter.get("dir", "").get("down", "")[frame][col].asInt()));
				// left
				tmpimage.mSheetIndex[ELEFT].push_back(make_pair(iter.get("dir", "").get("left", "")[frame][row].asInt(),
					iter.get("dir", "").get("left", "")[frame][col].asInt()));
				// down
				tmpimage.mSheetIndex[EDOWN].push_back(make_pair(iter.get("dir", "").get("down", "")[frame][row].asInt(),
					iter.get("dir", "").get("down", "")[frame][col].asInt()));
				// up
				tmpimage.mSheetIndex[EUP].push_back(make_pair(iter.get("dir", "").get("up", "")[frame][row].asInt(),
					iter.get("dir", "").get("up", "")[frame][col].asInt()));
			}
			mAvatarImages.emplace(make_pair(mNameToAvatarEnum[tmpimage.CharacterName.data()], tmpimage));
		}


	}
	void PutPlayer(int & inID, byte & inClasstype, XMFLOAT2 inPos, uint32_t & inNowHP,
		uint32_t & inMaxHP, uint32_t & inNowMP, uint32_t & inMaxMP, int & inLevel, 
		wstring &inName, bool & isNPC, int & inHorizontal, int & inVertial,bool isOwn = false)
	{
		if (isOwn)
		{
			mOwnID = inID;
		}
		mAvatars[inID] = new Avatar();
		mAvatars[inID]->SetID(inID);
		mAvatars[inID]->SetName(inName);
		mAvatars[inID]->SetClassType(inClasstype);
		mAvatars[inID]->SetAvatarPosition(inPos);
		mAvatars[inID]->SetCurrHP(inNowHP);
		mAvatars[inID]->SetMaxHP(inMaxHP);
		mAvatars[inID]->SetMaxMP(inMaxMP);
		mAvatars[inID]->SetCurrMP(inNowMP);
		mAvatars[inID]->SetLv(inLevel);
		mAvatars[inID]->SetLookVector(inHorizontal, inVertial);
		

	}
	void RemovePlayer(int inID);

	void AvatarMove(int & inID, int inPointx, int inPointy, int inHorizontal, int inVertical);

	bool GetOwnXmfloat2(XMFLOAT2 & outData)
	{
		if (mOwnPlayerState == ENUM_PlayerState::UNREADY)
			return false;
		if (mOwnID == -1)
			return false;
		outData = mAvatars[static_cast<int>(mOwnID)]->GetPoint();
		return true;
	}

	void Draw()
	{
		const float mHorizonal = 400.f;
		const float mVertical = 400.f;

		for (auto iter : mAvatars)
		{
			if (iter.second == nullptr) continue;
			byte class_type = iter.second->GetClassType();
			float cameraLocationX = (iter.second->GetAvatarPositionX() - mAvatars[static_cast<int>(mOwnID)]->GetAvatarPositionX()) * 50.f + mHorizonal;
			float cameraLocationY = (iter.second->GetAvatarPositionY() - mAvatars[static_cast<int>(mOwnID)]->GetAvatarPositionY()) * 50.f + mVertical;

			auto dir = iter.second->GetDIrction();
			auto animationFrame = 0;

			/// Object Image Draw()
			mAvatarImages[class_type].ImageData->Draw(D2D1::RectF(
				mAvatarImages[class_type].mSheetIndex[dir][animationFrame].second * mAvatarImages[class_type].wid,
				mAvatarImages[class_type].mSheetIndex[dir][animationFrame].first * mAvatarImages[class_type].hig,
				(mAvatarImages[class_type].mSheetIndex[dir][animationFrame].second + 1) * mAvatarImages[class_type].wid,
				(mAvatarImages[class_type].mSheetIndex[dir][animationFrame].first + 1) * mAvatarImages[class_type].hig),
				D2D1::RectF(cameraLocationX - 25, cameraLocationY - 25, cameraLocationX + 25, cameraLocationY + 25));
			/// Object Name Draw()
			DirectGraphics::getInstance().ConstTextDraw(iter.second->GetName(),
				D2D1::RectF(cameraLocationX - 45, cameraLocationY - 40, cameraLocationX + 45, cameraLocationY),15, DWRITE_TEXT_ALIGNMENT_CENTER);
			/// TODO : Object HP Show! NPC��!
			if (iter.first >= 10000)
			{
				// HP Gauge Rounded Bar
				DirectGraphics::getInstance().DrawRoundedRectangle(D2D1::RoundedRect(
					D2D1::RectF(cameraLocationX - 30, cameraLocationY + 35, cameraLocationX + 30, cameraLocationY + 45), 2, 2), D2D1::ColorF(D2D1::ColorF::White, 1));
				// HP Gauge Fill Bar
				auto percentHP = (float)iter.second->GetCurrHP() / (float)iter.second->GetMaxHP();
				DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::OrangeRed, 0.7),
					D2D1::RectF(cameraLocationX - 30, cameraLocationY + 35, ((cameraLocationX - 30) + (60 * percentHP)), cameraLocationY + 45));
				// HP integer Text
				/*DirectGraphics::getInstance().DrawTextW(mVariable_HP,
				D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 1, mBackBoard.right, mTextPaddingTop * 2), 0.7);*/
			}
		}
	}

	void SetFieldAvatar(sc_packet_wrap_field * inPacket)
	{
		if (mAvatars[inPacket->mAvatarID] != nullptr) {
			auto dumyPointXY = XMVectorSet(inPacket->mPointx, inPacket->mPointy, 0, 0);
			XMStoreFloat2(&mAvatars[inPacket->mAvatarID]->GetPoint(), dumyPointXY);
		}
		mAvatars[inPacket->mAvatarID]->SetNowField(inPacket->mWarpField);
	}
	Avatar *& GetOwnAvatar()
	{
		return mAvatars[mOwnID];
	}
	void SetControllAvatar(const sc_packet_enterworld_Succ & inPacket)
	{
		mOwnID = inPacket.mObjectID;
		mAvatars[mOwnID] = new Avatar();
		mAvatars[mOwnID]->SetID(inPacket.mObjectID);
		mAvatars[mOwnID]->SetName(inPacket.mName);
		mAvatars[mOwnID]->SetClassType(inPacket.mClassType);
		mAvatars[mOwnID]->SetAvatarPosition(inPacket.mX, inPacket.mY);
		mAvatars[mOwnID]->SetCurrHP(inPacket.mCurrHP);

		mAvatars[mOwnID]->SetNeedExp(inPacket.mNeedExp);
		mAvatars[mOwnID]->SetMaxHP(inPacket.mMaxHP);
		mAvatars[mOwnID]->SetMaxMP(inPacket.mMaxMP);
		mAvatars[mOwnID]->SetCurExp(inPacket.mCurrExp);
		mAvatars[mOwnID]->SetCurrMP(inPacket.mCurrMP);
		mAvatars[mOwnID]->SetLv(inPacket.mLevel);
		mAvatars[mOwnID]->SetNowField(inPacket.mNowField);
		mAvatars[mOwnID]->SetGold(inPacket.mHasGold);
		mAvatars[mOwnID]->SetAbility(inPacket.str, inPacket.dex, inPacket.ints, inPacket.luck);
		mAvatars[mOwnID]->SetLookVector(inPacket.mHorizontal, inPacket.mVertial);
		mOwnPlayerState = ENUM_PlayerState::READY;

	}
	void SetPlayerState(const ENUM_PlayerState inState)
	{
		mOwnPlayerState = inState;
	}
	const ENUM_PlayerState & GetPlayerState() { return mOwnPlayerState; }
	void UpdateHp(sc_packet_update_avatarhp * packet)
	{
		mAvatars[packet->id]->SetCurrHP(packet->hp);
		if (packet->id == mOwnID && packet->hp == 0)
		{
			SetPlayerState(ENUM_PlayerState::DEAD);
		}
	}
private:
	int mOwnID;
	ICamera * mCamera;
	map<int, Avatar *> mAvatars;

	map<byte, AvatarImage> mAvatarImages;
	map<string, byte> mNameToAvatarEnum;

	ENUM_PlayerState mOwnPlayerState;
};
static PlayerManager playerManager;