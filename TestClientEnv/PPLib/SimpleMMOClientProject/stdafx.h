// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.




#pragma comment(lib, "D2D1.lib")
#pragma comment(lib, "dwrite.lib")
#pragma comment(lib, "Windowscodecs.lib")

#pragma warning(disable:4996)
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"imm32.lib")

#include<wincodec.h>
#include<atomic>
#include<WinSock2.h>
#include<D3D10_1.h>
//
#include<DirectXMath.h>
using namespace DirectX;
//
//
#include <windows.h>
#include<imm.h>
#include<thread>
#include<DirectXCollision.h>
#include<WS2tcpip.h>
#include<mutex>
#include<queue>



// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
#include<iostream>
using namespace std;

#include "json\json.h"

typedef Json::Value JsonValue_t;
typedef Json::Reader JsonReader_t;


#undef	SAFE_DELETE
#define SAFEDELETE(obj)						\
{												\
	if ((obj)) delete(obj);		    			\
    (obj) = 0L;									\
}

//-------------------------------------------------------------------//
//문자열 변환   멀티바이트  -> 유니코드  or 유니코드 ->  멀티바이트
inline void StrConvA2T(CHAR *src, TCHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
#endif
}

inline void StrConvT2A(TCHAR *src, CHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
#endif
}

inline void StrConvA2W(CHAR *src, WCHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
}
inline void StrConvW2A(WCHAR *src, CHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
}


// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>
#include<array>
#include<d2d1.h>
#include<list>
#include<chrono>
#include<fstream>
#include<unordered_map>
#include <ctime>
using namespace std::chrono;

const int SocketMaxCnt = 3;
const int IOBufferSize = 10240;
const int PacketBufferSize = 10240;
#include"Singleton.h"
#include"ItemBox.h"
#include"IPanelTool.h"
#include"PanelTextBoard.h"
#include"Stream.h"
#include"SelectAvatarData.h"

#include"IGameLevel.h"
#include"ICamera.h"
#include"Defines.h"
#include"MainLevel.h"
#include"LoginLevel.h"
#include"JoinLevel.h"
#include"SelectLevel.h"



#include"BackOffSpinLock.h"
#include"CQueue.h"

#include"Clock.h"
#include"Timer.h"
#include"Log.h"

#include"DirectGraphics.h"
#include"TileMap.h"
#include"DirectImage.h"
#include"LoginPanel.h"
#include"Packet.h"
#include"PacketImpl.h"
#include"Session.h"
#include"PacketType.h"
#include"PacketFactoryImpl.h"
#include"PacketAnalyzer.h"
#include"EventSelectNetwork.h"
#include"IDirectFrameWork.h"
#include"CollisionBox.h"
#include"World.h"
#include"PlayerManager.h"


#include"InputManager.h"
#include"UIManager.h"
#include"DirectFrameWork.h"