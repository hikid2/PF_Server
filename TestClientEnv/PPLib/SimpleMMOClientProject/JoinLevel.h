#pragma once
class DirectImage;
class UITools;
class ColorBoxTool;
class TextTool;
class TextBoardTool;
class ButtonTool;
class MessageBoxTool;
class JoinLevel : public IGameLevel
{
public:
	JoinLevel();
	virtual void Initialize();
	virtual void Render();
	virtual bool Update(const uint32_t & inDeltaTime) { return true; }
	virtual const uint32_t GetLevelName() { return LevelEnum::Login; }
	virtual void MouseButtonUp(const POINT & inPoint);
	virtual void MouseButtonDown(const POINT & inPoint);
	virtual bool GetInputFocus(D2TextBoard *& outPtr) { return false; }

	virtual void InsertMessage(wstring & inText);
	virtual void KeyBoardIdentify(const WPARAM & inWParam);
	virtual void KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd);

	DirectImage * mTitleBackGround;
private:
	list<MessageBoxTool> mMessages;
	int32_t mSavedMouseDown_UI_ID;
	unordered_map<uint32_t, UITools*> mJoinUIs;
};