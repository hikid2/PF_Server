#include "stdafx.h"
enum ENUM_JOIN_UI_NAME
{
	JoinBoardColorBox = 'JBC',
	VariableID,
	VariablePW,
	VariableRePW,
	VariableEmail,
	ConstTextID,
	ConstTextPW,
	ConstTextRePW,
	ConstTextEmail,
	SubmitButton,
	BackButton,
};

JoinLevel::JoinLevel() : mTitleBackGround(new DirectImage(L"resource\\Background.jpg"))
{
	mJoinUIs[JoinBoardColorBox] = UIManager::getInstance().CreateTools<ColorBoxTool>();

	mJoinUIs[VariableID] = UIManager::getInstance().CreateTools<TextBoardTool>();
	mJoinUIs[VariablePW] = UIManager::getInstance().CreateTools<TextBoardTool>();
	mJoinUIs[VariableRePW] = UIManager::getInstance().CreateTools<TextBoardTool>();
	mJoinUIs[VariableEmail] = UIManager::getInstance().CreateTools<TextBoardTool>();

	mJoinUIs[ConstTextID] = UIManager::getInstance().CreateTools<TextTool>();
	mJoinUIs[ConstTextPW] = UIManager::getInstance().CreateTools<TextTool>();
	mJoinUIs[ConstTextRePW] = UIManager::getInstance().CreateTools<TextTool>();
	mJoinUIs[ConstTextEmail] = UIManager::getInstance().CreateTools<TextTool>();

	mJoinUIs[SubmitButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mJoinUIs[BackButton] = UIManager::getInstance().CreateTools<ButtonTool>();

	mJoinUIs[JoinBoardColorBox]->Initialize(false, true, false);

	mJoinUIs[VariableID]->Initialize(false, true, true);
	mJoinUIs[VariablePW]->Initialize(false, true, true);
	mJoinUIs[VariableRePW]->Initialize(false, true, true);
	mJoinUIs[VariableEmail]->Initialize(false, true, true);

	mJoinUIs[ConstTextID]->Initialize(false, true, false);
	mJoinUIs[ConstTextPW]->Initialize(false, true, false);
	mJoinUIs[ConstTextRePW]->Initialize(false, true, false);
	mJoinUIs[ConstTextEmail]->Initialize(false, true, false);

	mJoinUIs[SubmitButton]->Initialize(false, true, true);
	mJoinUIs[BackButton]->Initialize(false, true, true);

	reinterpret_cast<ColorBoxTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::JoinBoardColorBox])->Initialize(
		D2D1::RectF(400.F - 150, 350.F, 400 + 150.F, 620.F), D2D1::ColorF(0.96F, 0.925F, 0.505F, 0.2F));

	reinterpret_cast<TextTool*>(mJoinUIs[ConstTextID])->Initialize(L"ID      :  ", D2D1::RectF(400.F - 110, 400.F, 400.F +20, 410.F));
	reinterpret_cast<TextTool*>(mJoinUIs[ConstTextPW])->Initialize(L"PW    : ", D2D1::RectF(400.F - 110, 430.F, 400.F + 20, 440.F));
	reinterpret_cast<TextTool*>(mJoinUIs[ConstTextRePW])->Initialize(L"RePW  : ", D2D1::RectF(400.F - 110, 460.F, 400.F + 70, 470.F));
	reinterpret_cast<TextTool*>(mJoinUIs[ConstTextEmail])->Initialize(L"Email : ", D2D1::RectF(400.F - 110, 490.F, 400.F + 70, 500.F));

	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableID])->Initialize(
		D2D1::RectF(400.F - 20, 410.F, 400.F + 120, 430.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariablePW])->Initialize(
		D2D1::RectF(400.F - 20, 440.F, 400.F + 120, 460.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableRePW])->Initialize(
		D2D1::RectF(400.F - 20, 470.F, 400.F + 120, 490.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableEmail])->Initialize(
		D2D1::RectF(400.F - 20, 500.F, 400.F + 120, 520.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<ButtonTool*>(mJoinUIs[BackButton])->Initialize(L"resource\\Back_off.png",
		D2D1::RectF(400.F - 110, 530.F, 400.F + 10, 590.F));
	reinterpret_cast<ButtonTool*>(mJoinUIs[SubmitButton])->Initialize(L"resource\\Request_off.png",
		D2D1::RectF(400.F + 10, 530.F, 400.F + 120, 590.F));

}

void JoinLevel::Initialize()
{
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableID])->TextClear();
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableRePW])->TextClear();
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariableEmail])->TextClear();
	reinterpret_cast<TextBoardTool*>(mJoinUIs[VariablePW])->TextClear();
}

void JoinLevel::Render()
{
	DirectGraphics::getInstance().BeginDraw();
	DirectGraphics::getInstance().ClearScreen(1.f, 1.f, 1.0f);
	mTitleBackGround->Draw(D2D1::RectF(0, 0, 800, 800));
	for (auto iter : mJoinUIs)
	{
		iter.second->Draw();
	}

	for (auto iter : mMessages)
	{
		iter.Draw();
	}
	DirectGraphics::getInstance().EndDraw();
}

void JoinLevel::MouseButtonUp(const POINT & inPoint)
{
	if (!mMessages.empty()) {
		if (mMessages.back().PointInIntersectionCheckup(inPoint)) {
			if (mSavedMouseDown_UI_ID == MessageBoxs) {
				mMessages.pop_back();
				return;
			}
		}
	}

	for (auto iter : mJoinUIs)
	{
		if (!iter.second->IsInputMode()) continue;
		if (iter.second->PointInIntersectionCheckup(inPoint))
		{
			if (mSavedMouseDown_UI_ID == (int)iter.first)
			{
				switch (mSavedMouseDown_UI_ID)
				{
				case ENUM_JOIN_UI_NAME::SubmitButton:
				{
					auto SubmitExecute = [=]() {
						auto varID = reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableID])->getText();
						auto varPW = reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariablePW])->getText();
						auto varRePW = reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableRePW])->getText();
						auto varEmail = reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableEmail])->getText();
						if (varID.empty()) {
							cout << "아이디가 비어져있다." << endl;
							return;
						}
						if (varPW.empty()) {
							cout << "비밀번호가 비어져 있다." << endl;
							return;
						}
						if (varRePW.empty())
						{
							cout << "비밀번호 확인 부분이 비어져 있다." << endl;
							return;
						}
						if (varEmail.empty()) {
							cout << "이메일 부분이 비어져 있다." << endl;
							return;
						}
						if (!varRePW._Equal(varPW))
						{
							cout << "비밀번호가 일치하지 않습니다." << endl;
							return;
						}

						cs_packet_Joinreq packet;
						packet.mEmail = varEmail;
						packet.mId = varID;
						packet.mPw = varPW;
						
						EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);
					};
					SubmitExecute();
				}
				break;
				case ENUM_JOIN_UI_NAME::BackButton:
				{
					DirectFrameWork::getInstance().ChangeLevel(Login);
				}
				break;
				default:
					break;
				}
			}
			else
			{
				mSavedMouseDown_UI_ID = -1;
			}
		}
	}


}

void JoinLevel::MouseButtonDown(const POINT & inPoint)
{
	if (!mMessages.empty())
	{
		if (mMessages.back().PointInIntersectionCheckup(inPoint)) {
			mSavedMouseDown_UI_ID = MessageBoxs;
			return;
		}
	}
	for (auto iter : mJoinUIs)
	{
		if (!iter.second->IsInputMode()) continue;
		if (iter.second->PointInIntersectionCheckup(inPoint))
		{
			mSavedMouseDown_UI_ID = (int)iter.first;
			return;
		}
	}
}

void JoinLevel::InsertMessage(wstring & inText) { mMessages.push_back(MessageBoxTool(inText)); }

void JoinLevel::KeyBoardIdentify(const WPARAM & inWParam)
{
	auto immWrite = [](const WPARAM & inWParam, wstring & inwstr) {
		InputManager::getInstance().Imm_Write((TCHAR)inWParam, inwstr);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;

	switch (mSavedMouseDown_UI_ID)
	{
	case ENUM_JOIN_UI_NAME::VariableID:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mJoinUIs[mSavedMouseDown_UI_ID])->getText());
		break;
	case ENUM_JOIN_UI_NAME::VariablePW:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mJoinUIs[mSavedMouseDown_UI_ID])->getText());
		break;
	case ENUM_JOIN_UI_NAME::VariableRePW:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mJoinUIs[mSavedMouseDown_UI_ID])->getText());
		break;
	case ENUM_JOIN_UI_NAME::VariableEmail:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mJoinUIs[mSavedMouseDown_UI_ID])->getText());
		break;
	}
}

void JoinLevel::KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd)
{
	auto immcomposition = [](const LPARAM & inLParam, wstring & inwstr, HWND & inHwnd) {
		InputManager::getInstance().Imm_Composition(inLParam, inwstr, inHwnd);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;



	switch (mSavedMouseDown_UI_ID)
	{
	case ENUM_JOIN_UI_NAME::VariableID:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableID])->getText(),
			inHwnd);
		break;
	case ENUM_JOIN_UI_NAME::VariablePW:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariablePW])->getText(),
			inHwnd);
		break;
	case ENUM_JOIN_UI_NAME::VariableRePW:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableRePW])->getText(),
			inHwnd);
		break;
	case ENUM_JOIN_UI_NAME::VariableEmail:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mJoinUIs[ENUM_JOIN_UI_NAME::VariableEmail])->getText(),
			inHwnd);
		break;
	}


}
