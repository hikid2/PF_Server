#pragma once
class MiniPlayerInfoBoard;
class ChattingBoard;
class InventoryBoard;
class MainLevel : public IGameLevel
{
public:
	MainLevel();
	// IGameLevel을(를) 통해 상속됨
	virtual ~MainLevel();
	virtual void Initialize();

	virtual void Render() override;

	virtual bool Update(const uint32_t & inDeltaTime) override;

	virtual const uint32_t GetLevelName() override;

	virtual void MouseButtonUp(const POINT & inPoint) override;

	virtual void MouseButtonDown(const POINT & inPoint) override;

	virtual bool GetInputFocus(D2TextBoard *& outPtr) override;

	virtual void InsertMessage(wstring & inText) override;
	virtual void KeyBoardIdentify(const WPARAM & inWParam);
	virtual void KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd);
	void InsertBroadCastChatText(const wstring & inText);
	
	void DeadMessage()
	{

	}
private:
	volatile bool IsActiveInputBoard;
	uint32_t mAccumlateTime = 1000;
	MiniPlayerInfoBoard * mMiniBoard;
	ChattingBoard * mChatBoard;
	InventoryBoard * mInventory;
};



/*

PK_LOGIN_OK:

PK_LOGIN_FAIL
	size + ErrCode;
PK_Change_Field
	size + Code + BeforeField + ID + NextField
PK_PutObject
	size + Code + ID + name + Pos + LookVector + ........
PK_RemoveObject
	size + Code + ID

PK_State_Change
	size + ID + HP + exp + Level


*/