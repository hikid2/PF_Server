#pragma once

extern bool loadConfig(JsonValue_t * inRoot, const char * inFileName);
extern bool loadConfig(JsonValue_t * inRoot);

extern RECT rect;


#define maxLength 200
#define IsRange(Value, MinValue, MaxValue) (min(max(Value, MinValue), MaxValue) == Value)



class IWorld
{
public:
	class Monster;
	class Character;
	class Packet;

	typedef enum RENDERINGLAYERENUM : uint32_t
	{
		ENUM_BACKGROUND,
		ENUM_CollisionImage,
		ENUM_MAX,
	}RenderingLayerEnum;

	typedef unordered_map<uint32_t, Monster *>		MonsterHashMap;
	typedef unordered_map<uint32_t, Character *>	CharacterHashMap;
	typedef array<array<uint32_t, 200>, 200>		LocationArray;
	typedef array<array<byte, 200>, 200>		CollisionArray;
	typedef std::array<LocationArray, RenderingLayerEnum::ENUM_MAX> FieldArray;



	IWorld() {}
	virtual ~IWorld() { }
	virtual void Initialize() = 0;
	virtual bool PutMonster(uint32_t & inMonsterID, uint32_t MonsterdType, Packet * inPacketPtr) = 0;
	virtual bool PutCharacter(uint32_t & inCharcterID, uint32_t MonsterdType, Packet * inPacketPtr) = 0;
	virtual bool DeleteMonster(uint32_t & inMonsterID) = 0;
	virtual bool DeleteCharacter(uint32_t & inCharacterID) = 0;
	virtual void DrawField(const XMFLOAT2& inPosition) = 0;

	template<class T>
	auto GetDrawLambda()
	{
		auto a = [&](const XMFLOAT2& inPosition, FieldArray & mFieldData, T & mTileImageData) {
			const int len = 50;
			float deltaRangeX, deltaRangeY = 0.f;
			int widStart, widEnd, higStart, higEnd = 0;
			auto ReadPositionX = inPosition.x * 50;
			auto ReadPositionY = inPosition.y * 50;
			ViewRangeWrite(ReadPositionX, widStart, widEnd, deltaRangeX);
			ViewRangeWrite(ReadPositionY, higStart, higEnd, deltaRangeY);
			for (auto higth = higStart; higth < higEnd; ++higth)
			{
				for (auto wid = widStart; wid < widEnd; ++wid)
				{
					float deltaleft = (wid * len) + deltaRangeX;
					int deltaup = (higth * len) + deltaRangeY;
					int deltaright = ((wid + 1) * len) + deltaRangeX;
					int deltadown = ((higth + 1)* len) + deltaRangeY;

					uint32_t currentFieldData = mFieldData[RenderingLayerEnum::ENUM_BACKGROUND][higth][wid];
					uint32_t currCollisionImage = mFieldData[RenderingLayerEnum::ENUM_CollisionImage][higth][wid];

					if ((IsRange(deltaleft, 0, rect.right) && IsRange(deltaup, 0, rect.right)) ||
						(IsRange(deltaleft, 0, rect.right) && IsRange(deltadown, 0, rect.right)) ||
						(IsRange(deltaright, 0, rect.right) && IsRange(deltaup, 0, rect.right)) ||
						(IsRange(deltaright, 0, rect.right) && IsRange(deltadown, 0, rect.right)))
					{
						if (currentFieldData != 0)
						{
							for (auto iter : mTileImageData)
							{
								if (!iter->IsValidID(currentFieldData))
									continue;
								iter->Draw(D2D1::RectF(deltaleft, deltaup, deltaright, deltadown), currentFieldData);
								break;

							}
						}
						if (currCollisionImage != 0)
						{
							for (auto iter : mTileImageData)
							{
								if (!iter->IsValidID(currCollisionImage))
									continue;
								iter->Draw(D2D1::RectF(deltaleft, deltaup, deltaright, deltadown), currCollisionImage);
								break;

							}
						}

					}
				}
			}
		};
		return a;
	}
protected:
	void ViewRangeWrite(const float & inPos, int & outStart, int & outEnd, float & outDeltaRange)
	{
		const int len = 50;
		if (inPos < rect.right / 2)
		{
			outStart = 0;
			outEnd = ((inPos + rect.right / 2) / len) + 1 > maxLength ? maxLength : ((inPos + rect.right / 2) / len) + 1;
			outDeltaRange = rect.right / 2 - inPos;
		}
		else if (inPos >= rect.right / 2)
		{
			outStart = (inPos - rect.right / 2) / len;
			if (outStart > 0) --outStart;
			outEnd = (((inPos + rect.right / 2) / len) + 1) > maxLength ? maxLength : (((inPos + rect.right / 2) / len) + 1);
			outDeltaRange = rect.right / 2 - inPos;
		}
		outDeltaRange = ceil(outDeltaRange);
	}
	void LoadTile(const JsonValue_t & inJson, FieldArray & mFieldData)
	{
		JsonValue_t tileData = inJson[0].get("data", "");
		for (auto column = 0; column < 200; ++column) {
			for (auto row = 0; row < 200; ++row) {
				mFieldData[RenderingLayerEnum::ENUM_BACKGROUND][column][row] = tileData[(column * 200) + row].asUInt();
			}
		}
		JsonValue_t colision = inJson[1].get("data", "");
		for (auto column = 0; column < 200; ++column) {
			for (auto row = 0; row < 200; ++row) {
				mFieldData[RenderingLayerEnum::ENUM_CollisionImage][column][row] = colision[(column * 200) + row].asUInt();
			}
		}
	}
	void LoadColition(JsonValue_t & inJson, vector<CollisionBox> & mFieldData)
	{
		size_t collisionCnt = inJson.size();
		mFieldData.resize(collisionCnt);
		for (int index = 0; index < collisionCnt; ++index)
		{
			int xPos = inJson[index].get("x", "").asInt();
			int yPos = inJson[index].get("y", "").asInt();
			int height = inJson[index].get("height", "").asInt();
			int width = inJson[index].get("width", "").asInt();

			mFieldData.emplace_back(D2D1::Point2F(xPos, yPos), height, width);
		}
	}

	template<class T>
	void LoadTileImage(const JsonValue_t & inJson, T & inContainer)
	{
		for (uint32_t index = 0; index < inJson.size(); ++index)
		{
			uint32_t firstGid = inJson[index].get("firstgid", 0).asUInt();
			string route = inJson[index].get("source", "").asString();
			size_t routesize = route.size() + 1;
			wstring Wroute;
			Wroute.resize(routesize);
			StrConvA2W(&route.front(), &Wroute.front(), routesize);
			inContainer[index] = new TiledMap(Wroute.c_str(), firstGid);
		}

	}
};

class StartTownField : public IWorld
{
public:
	StartTownField() : IWorld() {}
	virtual ~StartTownField()
	{
		for (auto & iter : mTileImageData)
			SAFEDELETE(iter);
	}
	virtual void Initialize()
	{
		JsonValue_t root;
		if (!loadConfig(&root, "MStructData"))  return;

		const JsonValue_t & layers = root["layers"];
		LoadTile(layers, mFieldData);

		const JsonValue_t & tilesets = root["tilesets"];
		LoadTileImage<array<TiledMap *, 11>>(tilesets, mTileImageData);

		Json::Value colisions = layers[2].get("objects", "");
		LoadColition(colisions, mCollisionBoxContainer);

	}
	virtual bool PutMonster(uint32_t & inMonsterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}

	virtual bool PutCharacter(uint32_t & inCharcterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}
	virtual bool DeleteMonster(uint32_t & inMonsterID)
	{
		return false;
	}
	virtual bool DeleteCharacter(uint32_t & inCharacterID)
	{
		return false;
	}
	virtual void DrawField(const XMFLOAT2& inPosition)
	{
		auto Lambda = GetDrawLambda<array<TiledMap *, 11>>();
		Lambda(inPosition, mFieldData, mTileImageData);
	}
private:
	MonsterHashMap mMonsterContainer;
	CharacterHashMap mCharacterContainer;
	FieldArray mFieldData;
	array<TiledMap *, 11> mTileImageData;
	vector<CollisionBox> mCollisionBoxContainer;
};

class DesertField : public IWorld
{
public:
	DesertField() : IWorld() {}

	virtual ~DesertField()
	{
		for (auto & iter : mTileImageData)
			SAFEDELETE(iter);
	}
	virtual void Initialize()
	{
		JsonValue_t root;
		if (!loadConfig(&root, "MStructData2"))	return;
		const JsonValue_t & layers = root["layers"];
		LoadTile(layers, mFieldData);

		const JsonValue_t & tilesets = root["tilesets"];
		LoadTileImage<array<TiledMap *, 5>>(tilesets, mTileImageData);

		Json::Value colisions = layers[2].get("objects", "");
		LoadColition(colisions, mCollisionBoxContainer);
	}

	virtual bool PutMonster(uint32_t & inMonsterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}

	virtual bool PutCharacter(uint32_t & inCharcterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}
	virtual bool DeleteMonster(uint32_t & inMonsterID) { return false; }
	virtual bool DeleteCharacter(uint32_t & inCharacterID) { return false; }
	virtual void DrawField(const XMFLOAT2& inPosition)
	{
		auto Lambda = GetDrawLambda<array<TiledMap *, 5>>();
		Lambda(inPosition, mFieldData, mTileImageData);
	}
private:
	MonsterHashMap mMonsterContainer;
	CharacterHashMap mCharacterContainer;
	FieldArray mFieldData;
	vector<CollisionBox> mCollisionBoxContainer;
	array<TiledMap *, 5> mTileImageData;
};

class InsDan : public IWorld
{
public:
	InsDan() {}
	virtual ~InsDan()
	{
		for (auto & iter : mTileImageData)
			SAFEDELETE(iter);
	}
	virtual void Initialize()
	{
		JsonValue_t root;
		if (!loadConfig(&root, "Instance"))	return;
		const JsonValue_t & layers = root["layers"];
		LoadTile(layers, mFieldData);

		const JsonValue_t & tilesets = root["tilesets"];
		LoadTileImage<array<TiledMap *, 4>>(tilesets, mTileImageData);

		Json::Value colisions = layers[2].get("objects", "");
		LoadColition(colisions, mCollisionBoxContainer);

	}

	virtual bool PutMonster(uint32_t & inMonsterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}

	virtual bool PutCharacter(uint32_t & inCharcterID, uint32_t MonsterdType, Packet * inPacketPtr)
	{
		return false;
	}
	virtual bool DeleteMonster(uint32_t & inMonsterID) { return false; }
	virtual bool DeleteCharacter(uint32_t & inCharacterID) { return false; }
	virtual void DrawField(const XMFLOAT2& inPosition)
	{
		auto Lambda = GetDrawLambda<array<TiledMap *, 4>>();
		Lambda(inPosition, mFieldData, mTileImageData);
	}
private:
	MonsterHashMap mMonsterContainer;
	CharacterHashMap mCharacterContainer;
	FieldArray mFieldData;
	array<TiledMap *, 4> mTileImageData;
	vector<CollisionBox> mCollisionBoxContainer;
};

class WorldManager : public Singleton<WorldManager>
{
public:
	WorldManager() { Init(); }
	void Init()
	{
		auto t1 = new thread{ &StartTownField::Initialize, &mStartTownField };
		auto t2 = new thread{ &DesertField::Initialize, &mDesertField };
		auto t3 = new thread{ &InsDan::Initialize, &mInsDan };
		t1->join();
		t2->join();
		t3->join();
		SAFEDELETE(t1);
		SAFEDELETE(t2);
		SAFEDELETE(t3);
	}
	void DrawField(const XMFLOAT2& inPosition)
	{
		mCurrnetWorldPtr->DrawField(inPosition);
	}
	void ChangeWorld(const byte & inWorldEnum)
	{
		switch (inWorldEnum)
		{
		case WorldEnum::Desert_Field:
			mCurrnetWorldPtr = &mDesertField;
			break;
		case WorldEnum::Ins_Dan:
			mCurrnetWorldPtr = &mInsDan;
			break;
		case WorldEnum::Start_Town:
			mCurrnetWorldPtr = &mStartTownField;
			break;
		default:
			break;
		}
	}
private:
	StartTownField  mStartTownField;
	DesertField		mDesertField;
	InsDan			mInsDan;
	IWorld *		mCurrnetWorldPtr;
};