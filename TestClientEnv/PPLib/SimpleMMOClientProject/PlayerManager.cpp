#include "stdafx.h"

void PlayerManager::RemovePlayer(int inID)
{
	if (mAvatars.count(inID) > 0)
		SAFEDELETE(mAvatars[inID]);
}

void PlayerManager::AvatarMove(int & inID, int inPointx, int inPointy, int inHorizontal, int inVertical)
{
	if (mAvatars[inID] != nullptr) {
		auto dumyPointXY = XMVectorSet(inPointx, inPointy, 0, 0);
		XMStoreFloat2(&mAvatars[inID]->GetPoint(), dumyPointXY);
		auto dumyDirections = XMVectorSet(inHorizontal, inVertical, 0, 0);
		XMStoreFloat2(&mAvatars[inID]->GetLookVector(), dumyDirections);
	}
}
