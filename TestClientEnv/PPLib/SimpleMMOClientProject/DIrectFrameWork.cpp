#include "stdafx.h"

void DirectFrameWork::Update()
{
	Timer::getInstance().Tick(60);
	mGameLevelPtr->Update(0);
}

void DirectFrameWork::Draw()
{
	mGameLevelPtr->Render();
}

void DirectFrameWork::ExecutePacket(Packet * inPacketPtr)
{

}

void DirectFrameWork::InputKeyBoardMessage(const WPARAM & inWParam)
{
	if(mGameLevelPtr != nullptr) 
		mGameLevelPtr->KeyBoardIdentify(inWParam);
}

void DirectFrameWork::InputKeyBoardMessage(const LPARAM & inLParam, HWND & inHwnd)
{
	if (mGameLevelPtr != nullptr) 
		mGameLevelPtr->KeyBoardIdentify(inLParam, inHwnd);
}
