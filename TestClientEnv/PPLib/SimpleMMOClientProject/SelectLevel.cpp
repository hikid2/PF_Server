#include"stdafx.h"
#include"SelectLevel.h"
enum ENUM_Select_UI_NAME
{
	SelectBoardColorBox = 'bcb',	/*컬러보드*/
	AvatarWindowsFir,					/**/
	AvatarWindowsSec,					/**/
	AvatarWindowsThr,					/**/
	DeleteButton,
	NewBuutton,
	EnterWorldButton,
	LeftArrowButton,
	RightArrowButton,
	ENUM_Select_UI_NAME_MAX,
};
enum ENUM_CreateAvatar_NAME
{
	BoardColorBox = 'Bcb',
	AvatarImages,
	AvatarConstName,
	AvatarName,
	AvatarClassWarrior,
	AvatarClassWizerd,
	AvatarClassElf,
	AvatarCreateButton,
	AvatarBackButton,
	ENUM_CreateAvatar_NAME_MAX,
};
SelectLevel::SelectLevel()
	: mTitleBackGround(new DirectImage(L"resource\\Background.jpg"))
{
	mSelectStateID = OP_Select;
	mSavedMouseDown_UI_ID = -1;

	SetupToSelect();
	SetupToCretae();
}

inline SelectLevel::~SelectLevel() 
{
	delete mTitleBackGround;
	mTitleBackGround = nullptr;

}

void SelectLevel::Initialize()
{


}

void SelectLevel::Render()
{
	DirectGraphics::getInstance().BeginDraw();
	DirectGraphics::getInstance().ClearScreen(1.f, 1.f, 1.0f);
	mTitleBackGround->Draw(D2D1::RectF(0, 0, 800, 800));
	// TODO : Write Draw Codes!
	switch (mSelectStateID)
	{
	case OP_Select:
		mSelectUIs[SelectBoardColorBox]->Draw();
		mSelectUIs[AvatarWindowsFir]->Draw();
		mSelectUIs[AvatarWindowsSec]->Draw();
		mSelectUIs[AvatarWindowsThr]->Draw();
		mSelectUIs[DeleteButton]->Draw();
		mSelectUIs[NewBuutton]->Draw();
		mSelectUIs[EnterWorldButton]->Draw();
		mSelectUIs[LeftArrowButton]->Draw();
		mSelectUIs[RightArrowButton]->Draw();
		break;
	case OP_Create:
		mSelectUIs[SelectBoardColorBox]->Draw();
		mSelectUIs[AvatarWindowsFir]->Draw();
		mSelectUIs[AvatarWindowsSec]->Draw();
		mSelectUIs[AvatarWindowsThr]->Draw();
		mSelectUIs[DeleteButton]->Draw();
		mSelectUIs[NewBuutton]->Draw();
		mSelectUIs[EnterWorldButton]->Draw();
		mSelectUIs[LeftArrowButton]->Draw();
		mSelectUIs[RightArrowButton]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::BoardColorBox]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarConstName]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWarrior]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWizerd]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassElf]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarName]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarImages]->Draw();

		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarBackButton]->Draw();
		mSelectUIs[ENUM_CreateAvatar_NAME::AvatarCreateButton]->Draw();
		break;
	}
	for (auto iter : mMessages)
	{
		iter.Draw();
	}
	DirectGraphics::getInstance().EndDraw();
}

bool SelectLevel::Update(const uint32_t & inDeltaTime)
{
	if (IsUpdate)
	{
		reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsFir])->SetAvatarData(mAvatarLists[mSelectPage * 3]);
		reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsSec])->SetAvatarData(mAvatarLists[mSelectPage * 3 + 1]);
		reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsThr])->SetAvatarData(mAvatarLists[mSelectPage * 3 + 2]);
		IsUpdate = false;
	}

	return true;
}

void SelectLevel::UpdateAvatarList(ReadStream & inStream, int eventType)
{
	int listCount = 0;
	inStream.Read(&listCount);
	for (auto index = 0; index < listCount; ++index)
	{
		if (listCount > 9)
			break;
		mAvatarLists[index].Read(inStream);
	}
	IsUpdate = true;
}

void SelectLevel::MouseButtonUp(const POINT & inPoint)
{
	if (!mMessages.empty()) {
		if (mMessages.back().PointInIntersectionCheckup(inPoint)) {
			if (mSavedMouseDown_UI_ID == MessageBoxs) {
				mMessages.pop_back();
				return;
			}
		}
	}
	switch (mSelectStateID)
	{
	case OP_Select:
		for (int index = ENUM_Select_UI_NAME::SelectBoardColorBox; index < ENUM_Select_UI_NAME_MAX; ++index)
		{
			if (!mSelectUIs[index]->IsInputMode())
				continue;
			if (mSelectUIs[index]->PointInIntersectionCheckup(inPoint))
			{
				if (mSavedMouseDown_UI_ID == index)
				{
					switch (mSavedMouseDown_UI_ID)
					{
					case ENUM_Select_UI_NAME::AvatarWindowsFir:
						mSelectUIs[AvatarWindowsFir]->SetActive(true);
						mSelectUIs[AvatarWindowsSec]->SetActive(false);
						mSelectUIs[AvatarWindowsThr]->SetActive(false);
						return;
					case ENUM_Select_UI_NAME::AvatarWindowsSec:
						mSelectUIs[AvatarWindowsFir]->SetActive(false);
						mSelectUIs[AvatarWindowsSec]->SetActive(true);
						mSelectUIs[AvatarWindowsThr]->SetActive(false);
						return;
					case ENUM_Select_UI_NAME::AvatarWindowsThr:
						mSelectUIs[AvatarWindowsFir]->SetActive(false);
						mSelectUIs[AvatarWindowsSec]->SetActive(false);
						mSelectUIs[AvatarWindowsThr]->SetActive(true);
						return;
					case ENUM_Select_UI_NAME::DeleteButton:
						mSelectStateID = OP_Delete;
						return;
					case ENUM_Select_UI_NAME::EnterWorldButton:
					{
						cs_packet_enterworld_req packet;
						if (mSelectUIs[AvatarWindowsFir]->IsActive())
						{
							packet.mAvatarId = mAvatarLists[mSelectPage * 3].avatar_id;
						}
						else if (mSelectUIs[AvatarWindowsSec]->IsActive())
						{
							packet.mAvatarId = mAvatarLists[mSelectPage * 3 + 1].avatar_id;

						}
						else if (mSelectUIs[AvatarWindowsThr]->IsActive())
						{
							packet.mAvatarId = mAvatarLists[mSelectPage * 3 + 2].avatar_id;
						}
						else
						{
							wstring message = L"캐릭터가 선택되지 않았습니다.";
							InsertMessage(message);
							return;
						}
						EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);

					}
					return;
					case ENUM_Select_UI_NAME::NewBuutton:
						mSelectStateID = OP_Create;
						return;
					case ENUM_Select_UI_NAME::LeftArrowButton:
						if (mSelectPage > 0) {
							mSelectPage--;
							IsUpdate = true;
						}
						return;
					case ENUM_Select_UI_NAME::RightArrowButton:
						if (mSelectPage < 2) {
							mSelectUIs[AvatarWindowsFir]->SetActive(false);
							mSelectUIs[AvatarWindowsSec]->SetActive(false);
							mSelectUIs[AvatarWindowsThr]->SetActive(false);
							mSelectPage++;
							IsUpdate = true;
						}
						return;
					}
				}
				else
				{
					mSavedMouseDown_UI_ID = -1;
					return;
				}
			}
		}
		mSavedMouseDown_UI_ID = -1;
		return;
		break;
	case OP_Create:
		for (int index = ENUM_CreateAvatar_NAME::BoardColorBox; index < ENUM_CreateAvatar_NAME_MAX; ++index)
		{
			if (!mSelectUIs[index]->IsInputMode())
				continue;
			if (mSelectUIs[index]->PointInIntersectionCheckup(inPoint))
			{
				if (mSavedMouseDown_UI_ID == index)
				{
					switch (mSavedMouseDown_UI_ID)
					{
					case ENUM_CreateAvatar_NAME::AvatarClassWarrior:
						mSelectUIs[AvatarClassWarrior]->SetActive(true);
						mSelectUIs[AvatarClassWizerd]->SetActive(false);
						mSelectUIs[AvatarClassElf]->SetActive(false);
						reinterpret_cast<ImageTool*>(mSelectUIs[AvatarImages])->Initialize(L"resource\\Warrior.png");
						return;
					case ENUM_CreateAvatar_NAME::AvatarClassWizerd:
						mSelectUIs[AvatarClassWarrior]->SetActive(false);
						mSelectUIs[AvatarClassWizerd]->SetActive(true);
						mSelectUIs[AvatarClassElf]->SetActive(false);
						reinterpret_cast<ImageTool*>(mSelectUIs[AvatarImages])->Initialize(L"resource\\magician.png");
						return;
					case ENUM_CreateAvatar_NAME::AvatarClassElf:
						mSelectUIs[AvatarClassWarrior]->SetActive(false);
						mSelectUIs[AvatarClassWizerd]->SetActive(false);
						mSelectUIs[AvatarClassElf]->SetActive(true);
						reinterpret_cast<ImageTool*>(mSelectUIs[AvatarImages])->Initialize(L"resource\\Elf.png");
						return;
					case ENUM_CreateAvatar_NAME::AvatarCreateButton:
					{
						/*
							아바타 이름이 잘 적혀 있는지? 정도?
						*/
						auto avatarName = reinterpret_cast<TextBoardTool*>(mSelectUIs[AvatarName])->getText();
						if (avatarName.empty())
						{
							wstring message = L"캐릭터 이름이 비어져 있습니다.";
							InsertMessage(message);
							return;
						}
						if (avatarName.length() > 12)
						{
							wstring message = L"캐릭터이름은 12글자 이내만 가능합니다.";
							InsertMessage(message);
							return;
						}
						cs_packet_create_avatar_req packet;
						packet.mAvatarName = avatarName;
						if (mSelectUIs[AvatarClassWarrior]->IsActive()) { packet.mClassType = E_Warrior; }
						else if (mSelectUIs[AvatarClassWizerd]->IsActive()) { packet.mClassType = E_Wizerd; }
						else { packet.mClassType = E_Elf; }
						EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);
						return;
					}
					return;
					case ENUM_CreateAvatar_NAME::AvatarBackButton:
					{
						// 이전데이터는 지워야한다.
						reinterpret_cast<TextBoardTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarName])->TextClear();
						mSelectStateID = OP_Select;
					}
					return;
					default:
						break;
					}
					return;
				}
				else
				{
					mSavedMouseDown_UI_ID = -1;
					return;
				}
			}
		}
		mSavedMouseDown_UI_ID = -1;
		break;
	}
}

void SelectLevel::MouseButtonDown(const POINT & inPoint)
{
	if (!mMessages.empty())
	{
		if (mMessages.back().PointInIntersectionCheckup(inPoint))
		{
			mSavedMouseDown_UI_ID = MessageBoxs;
			return;
		}
	}
	switch (mSelectStateID)
	{
	case OP_Select:
		for (int index = ENUM_Select_UI_NAME::SelectBoardColorBox; index < ENUM_Select_UI_NAME_MAX; ++index)
		{
			if (!mSelectUIs[index]->IsInputMode())
				continue;
			if (mSelectUIs[index]->PointInIntersectionCheckup(inPoint))
			{
				if (mSavedMouseDown_UI_ID != -1 && index != ENUM_Select_UI_NAME::EnterWorldButton)
				{
					switch (mSavedMouseDown_UI_ID)
					{
					case ENUM_Select_UI_NAME::AvatarWindowsFir:
					case ENUM_Select_UI_NAME::AvatarWindowsSec:
					case ENUM_Select_UI_NAME::AvatarWindowsThr:
						mSelectUIs[AvatarWindowsFir]->SetActive(false);
						mSelectUIs[AvatarWindowsSec]->SetActive(false);
						mSelectUIs[AvatarWindowsThr]->SetActive(false);
						break;
					}
				}
				mSavedMouseDown_UI_ID = index;
				return;
			}
		}
		if (mSavedMouseDown_UI_ID != -1)
		{
			mSavedMouseDown_UI_ID = -1;
		}
		break;
	case OP_Create:
		for (int index = ENUM_CreateAvatar_NAME::BoardColorBox; index < ENUM_CreateAvatar_NAME_MAX; ++index)
		{
			if (!mSelectUIs[index]->IsInputMode())
				continue;
			if (mSelectUIs[index]->PointInIntersectionCheckup(inPoint))
			{
				if (mSavedMouseDown_UI_ID != -1)
				{
					/*switch (mSavedMouseDown_UI_ID)
					{
					case ENUM_CreateAvatar_NAME::AvatarClassWarrior:
					case ENUM_CreateAvatar_NAME::AvatarClassElf:
					case ENUM_CreateAvatar_NAME::AvatarClassWizerd:
						mSelectUIs[mSavedMouseDown_UI_ID]->SetActive(false);
						break;
					}*/
				}
				mSavedMouseDown_UI_ID = index;
				return;
			}
		}
		break;
	}

	/*for (auto iter : mSelectUIs)
	{
		if (!iter.second->IsInputMode()) continue;
		if (iter.second->PointInIntersectionCheckup(inPoint))
		{
			mSavedMouseDown_UI_ID = (int)iter.first;
			return;
		}
	}*/

}

void SelectLevel::InsertMessage(wstring & inText) { mMessages.push_back(MessageBoxTool(inText)); }

void SelectLevel::KeyBoardIdentify(const WPARAM & inWParam)
{
	auto immWrite = [](const WPARAM & inWParam, wstring & inwstr) {
		InputManager::getInstance().Imm_Write((TCHAR)inWParam, inwstr);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;

	switch (mSelectStateID)
	{
	case OP_Create:
		if (mSavedMouseDown_UI_ID == ENUM_CreateAvatar_NAME::AvatarName)
		{
			immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarName])->getText());
		}
		break;
	case OP_Delete:

		break;
	}
}

void SelectLevel::KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd)
{
	auto immcomposition = [](const LPARAM & inLParam, wstring & inwstr, HWND & inHwnd) {
		InputManager::getInstance().Imm_Composition(inLParam, inwstr, inHwnd);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;



	switch (mSelectStateID)
	{
	case OP_Create:
		if (mSavedMouseDown_UI_ID == ENUM_CreateAvatar_NAME::AvatarName)
		{
			immcomposition(inLParam,
				reinterpret_cast<TextBoardTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarName])->getText(),
				inHwnd);
		}
		break;
	case OP_Delete:

		break;
	}
}

inline void SelectLevel::SetupToSelect()
{
	// Select
	mSelectUIs[ENUM_Select_UI_NAME::LeftArrowButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mSelectUIs[ENUM_Select_UI_NAME::RightArrowButton] = UIManager::getInstance().CreateTools<ButtonTool>();




	mSelectUIs[SelectBoardColorBox] = UIManager::getInstance().CreateTools<ColorBoxTool>();
	mSelectUIs[AvatarWindowsFir] = UIManager::getInstance().CreateTools<SelectAvatarWindows>();
	mSelectUIs[AvatarWindowsSec] = UIManager::getInstance().CreateTools<SelectAvatarWindows>();
	mSelectUIs[AvatarWindowsThr] = UIManager::getInstance().CreateTools<SelectAvatarWindows>();
	mSelectUIs[DeleteButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mSelectUIs[NewBuutton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mSelectUIs[EnterWorldButton] = UIManager::getInstance().CreateTools<ButtonTool>();

	reinterpret_cast<ButtonTool*>(mSelectUIs[ENUM_Select_UI_NAME::LeftArrowButton])->Initialize(
		L"resource\\SelectLevel_LeftArrowOn.png",
		D2D1::RectF(40, 500, 40 + 70, 570));
	reinterpret_cast<ButtonTool*>(mSelectUIs[ENUM_Select_UI_NAME::RightArrowButton])->Initialize(
		L"resource\\SelectLevel_RightArrowOn.png",
		D2D1::RectF(760 - 70, 500, 760, 570));
	mSelectUIs[ENUM_Select_UI_NAME::LeftArrowButton]->Initialize(false, true, true);
	mSelectUIs[ENUM_Select_UI_NAME::RightArrowButton]->Initialize(false, true, true);

	mSelectUIs[SelectBoardColorBox]->Initialize(false, true, false);
	mSelectUIs[AvatarWindowsFir]->Initialize(false, true, true);
	mSelectUIs[AvatarWindowsSec]->Initialize(false, true, true);
	mSelectUIs[AvatarWindowsThr]->Initialize(false, true, true);
	mSelectUIs[DeleteButton]->Initialize(false, true, true);
	mSelectUIs[NewBuutton]->Initialize(false, true, true);
	mSelectUIs[EnterWorldButton]->Initialize(false, true, true);

	reinterpret_cast<ColorBoxTool*>(mSelectUIs[SelectBoardColorBox])->Initialize(
		D2D1::RectF(0, 100.F, 800, 690.F), D2D1::ColorF(D2D1::ColorF::SkyBlue, 0.8));

	reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsFir])->Initialize(0);
	reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsSec])->Initialize(1);
	reinterpret_cast<SelectAvatarWindows*>(mSelectUIs[AvatarWindowsThr])->Initialize(2);

	reinterpret_cast<ButtonTool*>(mSelectUIs[NewBuutton])->Initialize(L"resource\\SelectLevel_NewOff.png",
		D2D1::RectF(570, 630 - 55, 570 + 120, 630));
	reinterpret_cast<ButtonTool*>(mSelectUIs[DeleteButton])->Initialize(L"resource\\SelectLevel_deleteOff.png",
		D2D1::RectF(90, 630 - 55, 210, 630));
	reinterpret_cast<ButtonTool*>(mSelectUIs[EnterWorldButton])->Initialize(L"resource\\SelectLevel_WorldEntryOFF.png",
		D2D1::RectF(330, 630 - 55, 330 + 120, 630));
}

inline void SelectLevel::SetupToCretae()
{
	// Create
	mSelectUIs[BoardColorBox] = UIManager::getInstance().CreateTools<ColorBoxTool>();
	reinterpret_cast<ColorBoxTool*>(mSelectUIs[BoardColorBox])->Initialize(
		D2D1::RectF(300, 180.F, 500, 500.F), D2D1::ColorF(D2D1::ColorF::MintCream, 1));

	mSelectUIs[AvatarImages] = UIManager::getInstance().CreateTools<ImageTool>();

	mSelectUIs[AvatarImages]->SetToolsRect(D2D1::RectF(333, 200, 467, 345));
	reinterpret_cast<ImageTool*>(mSelectUIs[AvatarImages])->SetSheetRect(D2D1::RectF(0, 0, 32, 32));

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWarrior] = UIManager::getInstance().CreateTools<TextButtonSwitchTool>();
	reinterpret_cast<TextButtonSwitchTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWarrior])->Initialize(
		L"전사", D2D1::RectF(308.75, 350, 308.75 + 55, 375), D2D1::RectF(10, 0, 10, 0),
		D2D1::ColorF(D2D1::ColorF::LightSkyBlue), D2D1::ColorF(D2D1::ColorF::Yellow, 0.95), 17);

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWizerd] = UIManager::getInstance().CreateTools<TextButtonSwitchTool>();
	reinterpret_cast<TextButtonSwitchTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWizerd])->Initialize(
		L"마법사", D2D1::RectF(372.5, 350, 427.5, 375), D2D1::RectF(3, 0, 3, 0),
		D2D1::ColorF(D2D1::ColorF::LightSkyBlue), D2D1::ColorF(D2D1::ColorF::Yellow, 0.95), 17);

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassElf] = UIManager::getInstance().CreateTools<TextButtonSwitchTool>();
	reinterpret_cast<TextButtonSwitchTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassElf])->Initialize(
		L"엘프", D2D1::RectF(436.25, 350, 491.25, 375), D2D1::RectF(10, 0, 10, 0),
		D2D1::ColorF(D2D1::ColorF::LightSkyBlue), D2D1::ColorF(D2D1::ColorF::Yellow, 0.95), 17);

	mSelectUIs[AvatarConstName] = UIManager::getInstance().CreateTools<TextTool>();
	reinterpret_cast<TextTool*>(mSelectUIs[AvatarConstName])->Initialize(L"캐릭터 명 : ",
		D2D1::RectF(305, 380, 350, 390), 17);
	mSelectUIs[AvatarName] = UIManager::getInstance().CreateTools<TextBoardTool>();
	reinterpret_cast<TextBoardTool*>(mSelectUIs[AvatarName])->Initialize(
		D2D1::RectF(390, 382.5, 494, 400), D2D1::ColorF(D2D1::ColorF::LightGray));

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarBackButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	reinterpret_cast<ButtonTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarBackButton])->Initialize(
		L"resource\\Backoff.png", D2D1::RectF(305, 420, 397.5, 480));

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarCreateButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	reinterpret_cast<ButtonTool*>(mSelectUIs[ENUM_CreateAvatar_NAME::AvatarCreateButton])->Initialize(
		L"resource\\CreateOff.png", D2D1::RectF(402.5, 420, 495, 480));


	mSelectUIs[BoardColorBox]->Initialize(false, true, false);
	mSelectUIs[AvatarImages]->Initialize(false, true, false);

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWarrior]->Initialize(true, true, true);
	reinterpret_cast<ImageTool*>(mSelectUIs[AvatarImages])->Initialize(L"resource\\Warrior.png");

	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassWizerd]->Initialize(false, true, true);
	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarClassElf]->Initialize(false, true, true);
	mSelectUIs[AvatarConstName]->Initialize(false, true, false);
	mSelectUIs[AvatarName]->Initialize(false, true, true);
	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarBackButton]->Initialize(false, true, true);
	mSelectUIs[ENUM_CreateAvatar_NAME::AvatarCreateButton]->Initialize(false, true, true);
}
