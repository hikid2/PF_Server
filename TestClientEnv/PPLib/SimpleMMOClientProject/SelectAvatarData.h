#pragma once
class SelectAvatarData
{
public:
	SelectAvatarData() { clear(); }
	~SelectAvatarData(){}
	void Write(WriteStream & inStream)
	{
		inStream.Write(avatar_id);
		inStream.Write(classType);
		inStream.Write(name);
		inStream.Write(level);
		inStream.Write(fieldID);
	}
	void Read(ReadStream & inStream)
	{
		inStream.Read(&avatar_id);
		inStream.Read(&classType);
		inStream.Read(&name);
		inStream.Read(&level);
		inStream.Read(&fieldID);
	}
	void clear() { 
		avatar_id = -1;
		classType = 0;
		name = L"";
		level = 0;
		fieldID = -1;
	}
	int					avatar_id;
	byte				classType;
	wstring				name;
	int					level;
	int					fieldID;
};
