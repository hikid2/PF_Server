#pragma once
class DirectFrameWork : public IDirectFrameWork, public Singleton<DirectFrameWork>
{
public:
	// IDirectFrameWork��(��) ���� ��ӵ�
	bool Initialize(HWND & hwnd)
	{
		if (!DirectGraphics::getInstance().Initialize(hwnd))
			return false;
		UIManager::getInstance().Initialize();
		WorldManager::getInstance();
		LevelEnum;
		mGameLevelContainer[LevelEnum::Login] = new LoginLevel();
		mGameLevelContainer[LevelEnum::Join] = new JoinLevel();
		mGameLevelContainer[LevelEnum::Select] = new SelectLevel();
		mGameLevelContainer[LevelEnum::Main] = new MainLevel();
		// mGameLevelPtr = new MainLevel();
		mGameLevelPtr = mGameLevelContainer[LevelEnum::Login];
		return true;
	}
	virtual ~DirectFrameWork()
	{
		mGameLevelPtr = nullptr;
		for (auto iter : mGameLevelContainer)
			SAFEDELETE(iter);
	}
	void Execute()
	{
		PacketUpdate();
		Update();
		Draw();
	}
	void PacketUpdate()
	{
		auto packetCnt = EventSelectNetwork::getInstance().mPacketJobQueue.GetSize();
		for (auto index = 0; index < packetCnt; ++index)
		{
			ReadStream packetPtr;
			EventSelectNetwork::getInstance().mPacketJobQueue.Try_Dequeue(packetPtr);
			ExecutePacket(packetPtr);
		}
	}
	virtual void Update() override;
	void Draw();
	virtual void ExecutePacket(Packet * inPacketPtr) override;
	virtual void ExecutePacket(ReadStream & inPacketPtr)
	{
		byte type;
		inPacketPtr.Read(&type);
		if (type == PacketType::SC_MOVE_PLAYER)
		{
			sc_packet_move packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().AvatarMove(packet.avatarindex, packet.mPointx, packet.mPointy, packet.mHorizontal, packet.mVertical);
		}
		else if (type == SC_LOGIN_OK)
		{
			int id;
			int x;
			int y;
			byte class_type;
			uint32_t hp;
			uint32_t max_hp;
			uint32_t curr_mp;
			uint32_t max_mp;
			uint32_t curr_exp;
			uint32_t next_exp;
			uint32_t level;
			inPacketPtr.Read(&id);
			inPacketPtr.Read(&x);
			inPacketPtr.Read(&y);
			inPacketPtr.Read(&class_type);
			inPacketPtr.Read(&hp);

			inPacketPtr.Read(&max_hp);
			inPacketPtr.Read(&curr_mp);
			inPacketPtr.Read(&max_mp);
			inPacketPtr.Read(&curr_exp);
			inPacketPtr.Read(&next_exp);
			inPacketPtr.Read(&level);



		}
		else if (type == SC_REMOVE_PLAYER)
		{
			int id;
			inPacketPtr.Read(&id);
			PlayerManager::getInstance().RemovePlayer(id);
		}
		else if (type == SC_PUT_PLAYER)
		{
			sc_packet_put_player packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().PutPlayer(packet.avatarindex, packet.mClasstype, XMFLOAT2(packet.mPosX, packet.mPosY),
				packet.mCurrHP, packet.mMaxHP, packet.mCurrMP, packet.mMaxMP, packet.mLv, packet.mName, packet.isNPC, packet.mHorizontal, packet.mVertial,false);
		}
		else if (type == SC_JOIN_SUCC)
		{
			wstring messages = L"ȸ������ �Ϸ�";
			mGameLevelPtr->InsertMessage(messages);
		}
		else if (type == SC_JOIN_FAIL)
		{
			wstring messages = L"ȸ������ ����";
			mGameLevelPtr->InsertMessage(messages);
		}
		else if (type == SC_LOGIN_SUCC)
		{
			ChangeLevel(LevelEnum::Select);
		}
		else if (type == SC_LOGIN_FAIL)
		{
			wstring messages = L"�α��� ����";
			mGameLevelPtr->InsertMessage(messages);
		}
		else if(type == SC_AVATAR_LIST)
		{
			//SelectAvatarData data;
			//data.Read(inPacketPtr);
			reinterpret_cast<SelectLevel*>(mGameLevelPtr)->UpdateAvatarList(inPacketPtr, SC_AVATAR_LIST);
		}
		else if (type == SC_CREATE_AVATAR_RES)
		{
			sc_packet_create_avatar_res pk;
			pk.Read(inPacketPtr);
			if(pk.mResult == 1)
				reinterpret_cast<SelectLevel*>(mGameLevelPtr)->SetLevelState(0);
			mGameLevelPtr->InsertMessage(pk.mMessage);
		}
		else if (type == SC_ENTERWORLD_Succ)
		{
			sc_packet_enterworld_Succ packet;
			packet.Read(inPacketPtr);
			ChangeLevel(LevelEnum::Main);
			WorldManager::getInstance().ChangeWorld(packet.mNowField);
			PlayerManager::getInstance().SetControllAvatar(packet);
		}
		else if (type == SC_UPDATE_AVATARHP)
		{
			sc_packet_update_avatarhp packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().UpdateHp(&packet);
		}
		else if (type == SC_Notify_LevelUp)
		{
			sc_packet_notify_Levelup packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().GetOwnAvatar()->LevelUp(packet);
		}
		else if (type == SC_UPDATE_Avatar_Exp)
		{
			sc_packet_update_avatar_exp packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().GetOwnAvatar()->SetCurExp(packet.mNowExp);
		}
		else if (type == SC_Broad_Chat)
		{
			sc_packet_broadcast_chat packet;
			packet.Read(inPacketPtr);
			reinterpret_cast<MainLevel *>(mGameLevelPtr)->InsertBroadCastChatText(packet.mMessage);
		}
		else if (type == SC_Warp_Field)
		{
			sc_packet_wrap_field packet;
			packet.Read(inPacketPtr);
			WorldManager::getInstance().ChangeWorld(packet.mWarpField);
			PlayerManager::getInstance().SetFieldAvatar(&packet);
		}
		else if (type == SC_Respawn_Avatar)
		{
			sc_packet_respawn_avatar packet;
			packet.Read(inPacketPtr);
			WorldManager::getInstance().ChangeWorld(packet.mWarpFieldId);
			PlayerManager::getInstance().GetOwnAvatar()->SetCurrHP(packet.mNowHP);
			PlayerManager::getInstance().GetOwnAvatar()->SetCurrMP(packet.mNowMP);
			PlayerManager::getInstance().GetOwnAvatar()->SetAvatarPosition(packet.mPointx, packet.mPointy);
			PlayerManager::getInstance().GetOwnAvatar()->SetNowField(packet.mWarpFieldId);
			PlayerManager::getInstance().SetPlayerState(ENUM_PlayerState::READY);
		}
		else if (type == SC_Update_Gold)
		{
			sc_packet_update_avatar_Gold packet;
			packet.Read(inPacketPtr);
			PlayerManager::getInstance().GetOwnAvatar()->SetGold(packet.mNowGold);
			wstring message = L"[system -> ��� ȹ��] �� ��� =  ";
			message += to_wstring(packet.mNowGold);
			reinterpret_cast<MainLevel *>(mGameLevelPtr)->InsertBroadCastChatText(message);
		}
	}
	virtual void InputKeyBoardMessage(const WPARAM & inWParam) override;
	virtual void InputKeyBoardMessage(const LPARAM & inLParam, HWND & inHwnd);
	void WINAPI InputMouseMessage(UINT inMessage, HWND & hwnd)
	{
		switch (inMessage)
		{
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		{
			POINT point;
			SetCapture(hwnd);
			GetCursorPos(&point);
			ScreenToClient(hwnd, &point);
			mGameLevelPtr->MouseButtonDown(point);
			uint32_t outTools = 0;
			// UIManager::getInstance().IsMouseDownToUI(mGameLevelPtr->GetLevelName(), point, outTools);
		}
		break;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			if (GetCapture() == hwnd)
			{
				POINT point;
				GetCursorPos(&point);
				ScreenToClient(hwnd, &point);
				mGameLevelPtr->MouseButtonUp(point);
				// mLevels[mCurrentLevel->GetLevelName()]->MouseButtonUp(point);
				ReleaseCapture();
			}
			break;

		default:
			break;
		}
	}
	void ChangeLevel(int inLevenEnum)
	{
		mGameLevelPtr->Initialize();
		mGameLevelPtr = mGameLevelContainer[inLevenEnum];
	}
private:
	array<IGameLevel *, 4> mGameLevelContainer;
	IGameLevel * mGameLevelPtr;
};