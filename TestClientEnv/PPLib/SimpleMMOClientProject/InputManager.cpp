#include "stdafx.h"

void InputManager::Imm_Write(const TCHAR inCh, D2TextBoard * inTextData)
{

	auto & textData = inTextData->GetText();
	if (inCh == '\r') {
		// SendPacket(); or WritePacket();

		textData.clear();
		return;
	}
	if (inCh == '\b')
	{
		if (textData.size() > 0) {
			textData.pop_back();
		}
		return;
	}
	if (textData.size() < MAXBOARDLENGTH) {
		textData.push_back(inCh);
	}
}

void InputManager::Imm_Write(const TCHAR inCh, wstring & inTextData)
{
	if (inCh == '\r') {

		inTextData.clear();
		return;
	}
	if (inCh == '\b')
	{
		if (inTextData.size() > 0) {
			inTextData.pop_back();
		}
		return;
	}

	if (inTextData.size() < MAXBOARDLENGTH) {
		inTextData.push_back(inCh);
	}
}

void InputManager::Imm_Composition(const LPARAM & inlParam, D2TextBoard * inTextData, HWND & hwnd)
{
	HIMC himc = ImmGetContext(hwnd);
	auto & buffer = inTextData->GetText();

	if (inlParam & GCS_COMPSTR) {

		LONG length = ImmGetCompositionString(himc, GCS_COMPSTR, NULL, 0);

		if (mOffset > 0) buffer.pop_back();
		if (length != 0) {
			wchar_t wCh = { NULL };
			ImmGetCompositionStringW(himc, GCS_COMPSTR, &wCh, (DWORD)length);
			buffer.push_back(wCh);
			mOffset = length;
		}
		else
			mOffset = 0;
	}
	else if (inlParam & GCS_RESULTSTR) {
		LONG length = ImmGetCompositionString(himc, GCS_RESULTSTR, NULL, 0);
		buffer.pop_back();
		wchar_t wCh = { NULL };
		ImmGetCompositionString(himc, GCS_RESULTSTR, &wCh, length);
		buffer.push_back(wCh);
		mOffset = 0;
	}


	ImmReleaseContext(hwnd, himc);
}

void InputManager::Imm_Composition(const LPARAM & inlParam, wstring & inTextData, HWND & hwnd)
{
	HIMC himc = ImmGetContext(hwnd);

	if (inlParam & GCS_COMPSTR) {

		LONG length = ImmGetCompositionString(himc, GCS_COMPSTR, NULL, 0);

		if (mOffset > 0) inTextData.pop_back();
		if (length != 0) {
			wchar_t wCh = { NULL };
			ImmGetCompositionStringW(himc, GCS_COMPSTR, &wCh, (DWORD)length);
			inTextData.push_back(wCh);
			mOffset = length;
		}
		else
		{
			mOffset = 0;

		}
	}
	else if (inlParam & GCS_RESULTSTR) {
		LONG length = ImmGetCompositionString(himc, GCS_RESULTSTR, NULL, 0);
		inTextData.pop_back();
		wchar_t wCh = { NULL };
		ImmGetCompositionString(himc, GCS_RESULTSTR, &wCh, length);
		inTextData.push_back(wCh);
		mOffset = 0;
	}


	ImmReleaseContext(hwnd, himc);
}
