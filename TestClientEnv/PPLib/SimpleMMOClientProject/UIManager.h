#pragma once
#include<functional>
#include<typeinfo>
typedef enum UITOOLSTYPEENUM
{
	Button = 1,
	ColorBox,
	Text,
	TextBoard,
	TextButtonSwitch,
	Images,
	AvatarWindows,
	MessageBoxs,
	UITool_Max,
}UIToolsTypeEnum;
/*
	1. 시야에 보여야 하는가? -> bool visible;
	2. 입력이 가능한 오브젝트인가? bool inputable;
	3. 입력중인 오브젝트인가? -> active?

*/
class UITools
{
public:
	UITools() {}
	UITools(uint32_t inID, D2D_RECT_F inToolsRect, bool inInputAvailable, bool inActive = true)
		: mToolsID(inID), mToolsRect(inToolsRect), mActive(inActive), mInputMode(inInputAvailable)
	{ }
	virtual ~UITools() {}
	bool IsActive() { return mActive; }
	bool IsInputAvailable() { return mInputMode; }
	bool PointInIntersectionCheckup(const POINT & inPoint) {
		if ((this->mToolsRect.left < inPoint.x) && (this->mToolsRect.right > inPoint.x) &&
			(this->mToolsRect.top < inPoint.y) && (this->mToolsRect.bottom > inPoint.y)) {
			return true;
		}
		return false;
	}
	const uint32_t & GetToolsID() {
		return mToolsID;
	}
	void SetToolsID(uint32_t inToolsID) { mToolsID = inToolsID; }
	void SetToolsRect(D2D1_RECT_F inRect) { mToolsRect = inRect; }
	virtual D2D1_RECT_F & GetUIToolsRect() { return mToolsRect; }
	virtual UIToolsTypeEnum GetType() = 0;
	virtual void Initialize(JsonValue_t & inValue) = 0;
	virtual void Draw() = 0;
	void InitRect(JsonValue_t & inValue)
	{
		auto rect = inValue.get("RECT", "");
		mToolsRect = D2D1::RectF(rect[0].asFloat(), rect[1].asFloat(), rect[2].asFloat(), rect[3].asFloat());
	}

	virtual void SetActive(bool inActive) { mActive = inActive; }
	void SetInputMode(bool inMode) { mInputMode = inMode; }
	bool IsInputMode() { return mInputMode; }
	void SetVisible(bool inValue) { mVisible = inValue; }
	bool IsVisible() { return mVisible; }
	void Initialize(bool inActive, bool inVisible, bool inInputMode)
	{
		mVisible = inVisible;
		mActive = inActive;
		mInputMode = inInputMode;
	}
protected:
	uint32_t mToolsID;				/*Tool ID*/
	bool mVisible = false;			/*시야에 보이는가*/
	bool mActive = false;			/*활성화 유무*/
	bool mInputMode = false;		/*입력이 가능한 UI인가*/
	D2D1_RECT_F mToolsRect = {};
};

class ColorBoxTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::ColorBox };
	static UITools * CreateInstance() { return new ColorBoxTool(); }
	virtual UIToolsTypeEnum GetType()
	{
		return UITOOLSTYPEENUM::ColorBox;
	}
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);
		auto colorElement = inValue.get("Color", "");
		mColor =
			D2D1::ColorF(colorElement[0].asFloat(), colorElement[1].asFloat(), colorElement[2].asFloat(), colorElement[3].asFloat());

	}
	void Initialize(D2D1_RECT_F inRect, D2D_COLOR_F inColor)
	{
		mToolsRect = inRect;
		mColor = inColor;

	}
	void SetColor(D2D_COLOR_F inColor)
	{
		mColor = inColor;
	}
	virtual void Draw()
	{
		DirectGraphics::getInstance().DrawFillRectangle(mColor, mToolsRect);
	}
private:
	D2D_COLOR_F mColor;
};

class ButtonTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::Button };
	ButtonTool() : UITools() {}
	ButtonTool(uint32_t inID, D2D_RECT_F inToolsRect, bool inInputAvailable, bool inActive = true)
		: UITools(inID, inToolsRect, inInputAvailable, inActive)
	{

	}
	virtual ~ButtonTool() { mButtonImage.reset(); }
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);

		auto path = inValue.get("FilePath", "").asString();
		array<WCHAR, 100> widePath{};
		StrConvA2W(&path.front(), &widePath.front(), path.size() + 1);
		AttachImage(widePath.data());

	}
	void Initialize(const wchar_t * inFilePath, D2D1_RECT_F inRect)
	{
		mToolsRect = inRect;
		mButtonImage = make_shared<DirectImage>(inFilePath);
	}
	virtual void Draw() { (*mButtonImage).Draw(mToolsRect); }
	virtual UIToolsTypeEnum GetType() { return UIToolsTypeEnum::Button; }
	static UITools * CreateInstance() { return new ButtonTool(); }
private:
	shared_ptr<DirectImage> mButtonImage;

	void AttachImage(wstring inFileName) { mButtonImage = make_shared<DirectImage>(inFileName.data()); }
};

class TextTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::Text };
	TextTool() : UITools() , mText(), mFontSize(15) {}
	TextTool(uint32_t inID, D2D_RECT_F inToolsRect, bool inInputAvailable, bool inActive = true)
		: UITools(inID, inToolsRect, inInputAvailable, inActive)
	{}
	virtual void Draw() {

		DirectGraphics::getInstance().ConstTextDraw(mText, mToolsRect, mFontSize);

		//DirectGraphics::getInstance().DrawChatText(mText, mToolsRect);
	}
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);
		string textData = inValue.get("Text", "").asString();
		array<WCHAR, 100> wideText{};
		StrConvA2W(&textData.front(), &wideText.front(), textData.size() + 1);
		mText = wideText.data();
	}
	void Initialize(const wchar_t * inText, int inFontSize = 21)
	{
		mFontSize = inFontSize;
		mText = inText;
		// mToolsRect = inRect;
	}
	void Initialize(const wchar_t * inText, D2D1_RECT_F inRect, int inFontSize = 21)
	{
		mFontSize = inFontSize;
		mText = inText;
		mToolsRect = inRect;
	}
	static UITools * CreateInstance() { return new TextTool(); }
	virtual UIToolsTypeEnum GetType() { return UIToolsTypeEnum::Text; }
	void SetText(const wchar_t * inValue) { mText = inValue; }
private:
	int mFontSize;
	wstring mText;
};

class TextBoardTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::TextBoard };
	TextBoardTool() : UITools() {}
	TextBoardTool(uint32_t inID, D2D_RECT_F inToolsRect, bool inInputAvailable, D2D_COLOR_F inColorF, bool inActive = true)
		: UITools(inID, inToolsRect, inInputAvailable, inActive), mBackColor(inColorF)
	{}
	virtual void Draw()
	{
		DirectGraphics::getInstance().DrawFillRectangle(mBackColor, mToolsRect);
		DirectGraphics::getInstance().DrawChatText(mText, mToolsRect,mFontColor);
	}
	void Initialize(D2D1_RECT_F inRect, D2D_COLOR_F inBackColor, D2D_COLOR_F inFontColor = D2D1::ColorF(D2D1::ColorF::Black))
	{
		mToolsRect = inRect;
		mBackColor = inBackColor;
		mFontColor = inFontColor;
	}
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);
	}
	static UITools * CreateInstance() { return new TextBoardTool(); }
	virtual UIToolsTypeEnum GetType() { return UIToolsTypeEnum::TextBoard; }
	wstring & getText() { return mText; }
	void TextClear() { mText.clear(); }
private:
	D2D_COLOR_F mBackColor;
	D2D_COLOR_F mFontColor;
	wstring mText;
};

class MiniPlayerInfoBoard
{
public:
	MiniPlayerInfoBoard() {}
	~MiniPlayerInfoBoard() {}
	void Initialize()
	{
		mTextHP = L"HP : ";
		mTextMP = L"MP : ";
		mTextLv = L"LV : ";
		mTextExp = L"Exp ";
		mBackBoard = D2D1::RectF(800 - 250, 0, 800, 80);
		mTextPaddingLeft = (mBackBoard.right - mBackBoard.left) / 3;
		mTextPaddingTop = (mBackBoard.bottom - mBackBoard.top) / 4;
		mRoundedPaddingLeft = mBackBoard.left + (mTextPaddingLeft + 40);
		mRoundedPaddingLeftExp = mBackBoard.left + 40;
	}
	void Update(Avatar * inOwn)
	{
		if (inOwn == nullptr)
			return;
		mTextID = L"이름 : " + inOwn->GetName();
		wchar_t level_to_wchar[20];
		wsprintf(level_to_wchar, L"LV : %d", inOwn->GetLv());
		mTextLv = level_to_wchar;

		mPercentExp = (float)inOwn->GetCurExp() / (float)inOwn->GetNeedExp();

		wchar_t hp_to_wchar[20];
		mPercentHP = (float)inOwn->GetCurrHP() / (float)inOwn->GetMaxHP();
		int iNowHp = inOwn->GetCurrHP();
		int iMaxHp = inOwn->GetMaxHP();
		wsprintf(hp_to_wchar, L"%d/%d", iNowHp, iMaxHp);
		mVariable_HP = hp_to_wchar;

		wchar_t mp_to_wchar[20];
		mPercentMP = (float)inOwn->GetCurrMP() / (float)inOwn->GetMaxMP();
		wsprintf(mp_to_wchar, L"%d/%d", inOwn->GetCurrMP(), inOwn->GetMaxMP());
		mVariable_MP = mp_to_wchar;

		wchar_t exp_to_wchar[30];
		float f_exp = (float)inOwn->GetCurExp() / (float)inOwn->GetNeedExp();
		_stprintf_s(exp_to_wchar, L"%.2f%%(%d/%d)", f_exp * 100, inOwn->GetCurExp(), inOwn->GetNeedExp());
		mVariable_Exp = exp_to_wchar;
	}
	void Draw()
	{
		// BackBoard
		DirectGraphics::getInstance().DrawRectangle(D2D1::ColorF(D2D1::ColorF::Black, 0.7), mBackBoard);
		// ID Text
		DirectGraphics::getInstance().DrawTextW(mTextID,
			D2D1::RectF(mBackBoard.left + mTextPaddingLeft, 0, mBackBoard.right, mTextPaddingTop * 1));
		// HP Text
		DirectGraphics::getInstance().DrawTextW(mTextHP,
			D2D1::RectF(mBackBoard.left + mTextPaddingLeft, mTextPaddingTop, mBackBoard.right, mTextPaddingTop * 2));
		// MP Text
		DirectGraphics::getInstance().DrawTextW(mTextMP,
			D2D1::RectF(mBackBoard.left + mTextPaddingLeft, mTextPaddingTop * 2, mBackBoard.right, mTextPaddingTop * 3));
		// LV Text
		DirectGraphics::getInstance().DrawTextW(mTextLv,
			D2D1::RectF(mBackBoard.left + 10, mBackBoard.top, mBackBoard.right, mTextPaddingTop * 1));
		// Exp Text
		DirectGraphics::getInstance().DrawTextW(mTextExp,
			D2D1::RectF(mBackBoard.left + 10, mTextPaddingTop * 3, mBackBoard.right, mTextPaddingTop * 4));


		// HP Gauge Rounded Bar
		DirectGraphics::getInstance().DrawRoundedRectangle(D2D1::RoundedRect(
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 1 + 2, mRoundedPaddingLeft + mGauge, mTextPaddingTop * 2 - 2),
			2, 2),
			D2D1::ColorF(D2D1::ColorF::White, 1));
		// HP Gauge Fill Bar
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::OrangeRed, 0.7),
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 1 + 3, (mRoundedPaddingLeft + (mGauge * mPercentHP)), mTextPaddingTop * 2 - 3));


		// HP integer Text
		DirectGraphics::getInstance().DrawTextW(mVariable_HP,
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 1, mBackBoard.right, mTextPaddingTop * 2), 0.7);
		// MP Gauge Rounded Bar
		DirectGraphics::getInstance().DrawRoundedRectangle(D2D1::RoundedRect(
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 2 + 2, mRoundedPaddingLeft + mGauge, mTextPaddingTop * 3 - 2),
			2, 2), D2D1::ColorF(D2D1::ColorF::White, 1));
		// MP Gauge Fill Bar
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::Blue, 0.8),
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 2 + 3, (mRoundedPaddingLeft + (mGauge * mPercentMP)), mTextPaddingTop * 3 - 2));
		// MP integer Text
		DirectGraphics::getInstance().DrawTextW(mVariable_MP,
			D2D1::RectF(mRoundedPaddingLeft, mTextPaddingTop * 2, mBackBoard.right, mTextPaddingTop * 3), 0.7);
		// Exp Gauge Rounded Bar
		DirectGraphics::getInstance().DrawRoundedRectangle(D2D1::RoundedRect(
			D2D1::RectF(mRoundedPaddingLeftExp, mTextPaddingTop * 3 + 2, mRoundedPaddingLeftExp + mExpGauge, mTextPaddingTop * 4 - 2),
			2, 2), D2D1::ColorF(D2D1::ColorF::White, 1));
		// Exp Gauge Fill Bar
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::Yellow, 0.8),
			D2D1::RectF(mRoundedPaddingLeftExp, mTextPaddingTop * 3 + 3, (mRoundedPaddingLeftExp + (mExpGauge * mPercentExp)), mTextPaddingTop * 4 - 3));
		// Exp integer Text
		DirectGraphics::getInstance().DrawTextW(mVariable_Exp,
			D2D1::RectF(mRoundedPaddingLeft - 80, mTextPaddingTop * 3, mBackBoard.right, mTextPaddingTop * 4), 0.7);
	}

private:
	D2D1_RECT_F mBackBoard;
	wstring mTextID;
	wstring mTextHP;
	wstring mTextMP;
	wstring mTextLv;
	wstring mTextExp;

	wstring mVariable_HP;
	wstring mVariable_MP;
	wstring mVariable_Exp;
	float mTextPaddingLeft;
	float mTextPaddingTop;
	float mRoundedPaddingLeft;
	const int mGauge = 120;
	const float mExpGauge = mGauge + 85;

	float mPercentHP;
	float mPercentMP;
	float mPercentExp;
	float mRoundedPaddingLeftExp;
};
class ChattingBoard
{
public:
	ChattingBoard() {}
	void Initialize(D2D1_RECT_F inRect)
	{
		mBackColorBox.Initialize(inRect, D2D1::ColorF(D2D1::ColorF::LightGray, 0.7));
		mInputBoard.Initialize(D2D1::RectF(inRect.left, 770, inRect.right, 800), D2D1::ColorF(D2D1::ColorF::Black), D2D1::ColorF(D2D1::ColorF::White));
	}
	void Draw(bool IsinputMode)
	{
		atomic_thread_fence(memory_order_seq_cst);
		mBackColorBox.Draw();
		int cnt = 0;
		for (auto iter : mChattingMessage)
		{
			iter.SetToolsRect(D2D1::RectF(0.f, 770 - (cnt + 1) * 30, 400.f, 770 + cnt * 30));
			iter.Draw();
			cnt = cnt + 1;
		}
		if (IsinputMode)
			mInputBoard.Draw();
	}
	wstring & GetInputBoardText() 
	{
		return mInputBoard.getText();

	}
	void ClearInputBoard() { mInputBoard.TextClear(); }
	void InsertChattingMessage(const wstring & inText)
	{
		TextTool text;
		text.SetText(inText.c_str());
		mChattingMessage.push_front(text);
		if (mChattingMessage.size() > 5)
			mChattingMessage.pop_back();
	}
private:
	deque<TextTool> mChattingMessage;
	ColorBoxTool mBackColorBox;
	TextBoardTool mInputBoard;

};


class ImageTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::Images };
	ImageTool() : UITools(), mBmp(nullptr), mSheetRect(D2D1::RectF()){}
	virtual ~ImageTool() { mBmp->Release(); }
	static UITools * CreateInstance() { return new ImageTool(); }
	virtual UIToolsTypeEnum GetType() { return UIToolsTypeEnum::Images; }
	virtual void Initialize(JsonValue_t & inValue) {}
	void Initialize(const wchar_t * inFileName)
	{
		if (mBmp != nullptr) mBmp->Release();
		HRESULT hr;	// HRSULT for check err
		IWICImagingFactory * wicFactory = nullptr;
		IWICBitmapDecoder * decoderPtr = nullptr;
		IWICBitmapFrameDecode * wicFrame = nullptr;
		IWICFormatConverter * wicConverter = nullptr;
		try
		{
			hr = CoCreateInstance(
				CLSID_WICImagingFactory,
				NULL,
				CLSCTX_INPROC_SERVER,
				IID_IWICImagingFactory,
				(LPVOID*)&wicFactory);
			if (hr != S_OK)
				throw exception("Error 1");
			// wicFactory 에서  파일을 Read한다
			hr = wicFactory->CreateDecoderFromFilename(
				inFileName,
				NULL,
				GENERIC_READ,
				WICDecodeMetadataCacheOnLoad,
				&decoderPtr);
			// Frame을 Read 한다.
			if (hr != S_OK)
				throw exception("Error 2");
			hr = decoderPtr->GetFrame(0, &wicFrame);
			if (hr != S_OK)
				throw exception("Error 3");
			// Create a Converter

			hr = wicFactory->CreateFormatConverter(&wicConverter);
			if (hr != S_OK)
				throw exception("Error 4");

			hr = wicConverter->Initialize(
				wicFrame,
				GUID_WICPixelFormat32bppPBGRA,
				WICBitmapDitherTypeNone,
				NULL,
				0.0,
				WICBitmapPaletteTypeCustom);
			if (hr != S_OK)
				throw exception("Error 5");
			hr = DirectGraphics::getInstance().GetRenderTarget()->CreateBitmapFromWicBitmap(
				wicConverter,
				NULL,
				&mBmp);
			if (hr != S_OK)
				throw exception("Error 6");
		}
		catch (const std::exception& exp)
		{
			cout << exp.what();
		}

		if (wicFactory) wicFactory->Release();
		if (decoderPtr)  decoderPtr->Release();
		if (wicConverter) wicConverter->Release();
		if (wicFrame) wicFrame->Release();
	}
	void SetSheetRect(D2D1_RECT_F inSheetRect) { mSheetRect = inSheetRect; }
	virtual void Draw() {
		if (mBmp == nullptr)
			return;
		DirectGraphics::getInstance().GetRenderTarget()->DrawBitmap(
			mBmp,
			mToolsRect,
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			mSheetRect
		);
	}
private:
	ID2D1Bitmap * mBmp;
	D2D1_RECT_F mSheetRect;
};

enum Enum_InventoryPage
{
	Equips,
	consume,
	others,
	Max_InventoryPage,
};
class InventoryBoard
{
public:
	InventoryBoard() 
		: mInventoryImage(new DirectImage(L"resource\\inventoryTest01.png")), IsVisible(false)
	{
		
	}
	~InventoryBoard()
	{
		delete mInventoryImage;
		mInventoryImage = nullptr;
	}
	void Initialize()
	{

	}
	void Draw()
	{
		if (IsVisible)
			mInventoryImage->Draw(D2D1::RectF(500.f, 175.f, 800.f, 625.f), 0.8f);
	}
	bool IsVisible;
	DirectImage * mInventoryImage;
	typedef array<array<int, 5>, 7> PageSlot;
	array<PageSlot, Max_InventoryPage> mSloatItem;
};
class SelectAvatarWindows : public UITools
{
public:
	SelectAvatarWindows() {}
	virtual ~SelectAvatarWindows() {}
	static UITools * CreateInstance() { return new SelectAvatarWindows(); }
	virtual UIToolsTypeEnum GetType() { return UIToolsTypeEnum::AvatarWindows; }
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);
	}
	void Initialize(int inIndex)
	{
		float left = 20 + (inIndex * 260);
		float right = left + 240;
		float top = 150;
		float bot = top + 320;

		mToolsRect = D2D1::RectF(left, top, right, bot);
		mAvatarImage.SetSheetRect(D2D1::RectF(0, 0, 32, 32));
		mAvatarImage.SetToolsRect(D2D1::RectF(left + 70, top + 50, left + 170, top + 150));

		mConstAvatarClassName.Initialize(L"직업 : ",
			D2D1::RectF(left + 10, top + 275, left + 200, top + 280), 19);

		mBackColorBox.Initialize(mToolsRect, D2D1::ColorF(D2D1::ColorF::LightYellow, 0.7));

		mConstAvatarName.Initialize(L"캐릭터 명 : ",
			D2D1::RectF(left + 10, top + 200, left + 200, top + 210), 19);

		mConstLevel.Initialize(L"레벨 : ",
			D2D1::RectF(left + 10, top + 225, left + 200, top + 230), 19);

		mConstFieldName.Initialize(L"위치 : ",
			D2D1::RectF(left + 10, top + 250, left + 200, top + 260), 19);

		mAvatarName.SetToolsRect(D2D1::RectF(left + 110, top + 200, left + 230, top + 210));
		mLevel.SetToolsRect(D2D1::RectF(left + 60, top + 225, left + 80, top + 230));
		mFieldName.SetToolsRect(D2D1::RectF(left + 60, top + 250, left + 180, top + 260));
		mAvatarClassName.SetToolsRect(D2D1::RectF(left + 65, top + 275, left + 180, top + 280));

	}
	void SetAvatarClass(byte inAvatarClass)
	{
		switch (inAvatarClass)
		{
		case E_Warrior:
			IsContain = true;
			mAvatarImage.Initialize(L"resource\\Warrior.png");
			mAvatarClassName.Initialize(L"전사", 19);
			break;
		case E_Wizerd:
			IsContain = true;
			mAvatarImage.Initialize(L"resource\\magician.png");
			mAvatarClassName.Initialize(L"마법사", 19);
			break;
		case E_Elf:
			IsContain = true;
			mAvatarImage.Initialize(L"resource\\Elf.png");
			mAvatarClassName.Initialize(L"요정", 19);
			break;

		default:
			IsContain = false;
			break;
		}

	}
	void SetField(const int & inFIeldID)
	{
		switch (inFIeldID)
		{
		case WorldEnum::Start_Town:
			mFieldName.Initialize(L"시작의 섬", 19);
			break;
		case WorldEnum::Desert_Field:
			mFieldName.Initialize(L"사막 스테이지", 19);
			break;
		case WorldEnum::Ins_Dan:
			mFieldName.Initialize(L"던전 스테이지", 19);
			break;
		default:
			mFieldName.Initialize(L"noTitle", 19);
			break;
		}
	}
	void SetAvatarData(SelectAvatarData & inData)
	{
		if (inData.avatar_id == -1) {
			Clear();
			return;
		}
		wchar_t levels[10];
		_itow(inData.level, levels, 10);
		mLevel.Initialize(levels, 19);
		SetAvatarClass(inData.classType);
		mAvatarName.Initialize(inData.name.c_str(), 19);
		SetField(inData.fieldID);

	}
	virtual void SetActive(bool inActive)
	{
		if (!IsContain)
			return;
		if (inActive)
		{
			mBackColorBox.SetColor(D2D1::ColorF(D2D1::ColorF::Yellow, 0.89));
		}
		else
		{
			mBackColorBox.SetColor(D2D1::ColorF(D2D1::ColorF::LightYellow, 0.7));
		}
		mActive = inActive;
	}
	virtual void Draw()
	{
		mBackColorBox.Draw();
		mConstAvatarName.Draw();
		mConstLevel.Draw();
		mConstFieldName.Draw();
		mConstAvatarClassName.Draw();
		if (IsContain)
		{
			mAvatarImage.Draw();
			mAvatarName.Draw();
			mLevel.Draw();
			mFieldName.Draw();
			mAvatarClassName.Draw();
		}
	}
	void Clear()
	{
		IsContain = false;
		mAvatarName.SetText(L"");
		mLevel.SetText(L"");
		mFieldName.SetText(L"");
		mAvatarClassName.SetText(L"");
	}
private:
	bool IsContain = false;
	ColorBoxTool mBackColorBox;
	ImageTool mAvatarImage;

	TextTool mConstAvatarName;
	TextTool mConstLevel;
	TextTool mConstFieldName;
	TextTool mConstAvatarClassName;

	TextTool mAvatarName;
	TextTool mLevel;
	TextTool mFieldName;
	TextTool mAvatarClassName;
};
class TextButtonSwitchTool : public UITools
{
public:
	enum { UCLASSID = UITOOLSTYPEENUM::TextButtonSwitch };
	TextButtonSwitchTool() : UITools() {}
	static UITools * CreateInstance() { return new TextButtonSwitchTool(); }
	virtual UIToolsTypeEnum GetType() { return UITOOLSTYPEENUM::TextButtonSwitch; }
	virtual void Initialize(JsonValue_t & inValue)
	{
		InitRect(inValue);
		string textData = inValue.get("Text", "").asString();
		array<WCHAR, 100> wideText{};
		StrConvA2W(&textData.front(), &wideText.front(), textData.size() + 1);
		mText = wideText.data();
	}
	void Initialize(const wchar_t * inText, const D2D_RECT_F inEdgeRect, D2D_RECT_F inTextAlign, D2D_COLOR_F inOffColor, D2D_COLOR_F inOnColor, int inFontSize)
	{
		mFontSize = inFontSize;
		mText = inText;
		mToolsRect = inEdgeRect;
		mTextRect = D2D1::RectF(mToolsRect.left + inTextAlign.left,
			mToolsRect.top + inTextAlign.top,
			mToolsRect.right + inTextAlign.right,
			mToolsRect.bottom + inTextAlign.bottom);
		mOffColor = inOffColor;
		mOnColor = inOnColor;
	}
	void Draw()
	{
		if (mActive)
		{
			// DirectGraphics::getInstance().DrawFillRectangle(mOnColor, mEdgeRect);
			DirectGraphics::getInstance().DrawFillRoundedRectangle(D2D1::RoundedRect(mToolsRect, 0.4, 0.4), mOnColor);
			DirectGraphics::getInstance().ConstTextDraw(mText, mTextRect, mFontSize);
		}
		else
		{
			// DirectGraphics::getInstance().DrawFillRectangle(mOffColor, mEdgeRect);
			DirectGraphics::getInstance().DrawFillRoundedRectangle(D2D1::RoundedRect(mToolsRect, 0.4, 0.4), mOffColor);
			DirectGraphics::getInstance().ConstTextDraw(mText, mTextRect, mFontSize);
		}
	}
	virtual void SetActive(bool inActive) {
		mActive = inActive;
	}
private:
	int mFontSize;
	wstring		mText;
	D2D_RECT_F  mTextRect;

	D2D_COLOR_F mOffColor;
	D2D_COLOR_F mOnColor;
};
class MessageBoxTool : public UITools
{
public:
	MessageBoxTool(wstring & Message)
	{
		mMessage = Message;
		BoxRect = D2D1::RectF(300, 300, 500, 500);
		mToolsRect = D2D1::RectF(400 - 25, 450, 400 + 10, 480);
	}
	void Draw()
	{
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::Snow),
			BoxRect);
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::MintCream),
			mToolsRect);
		DirectGraphics::getInstance().ConstTextDraw(mMessage, D2D1::RectF(310, 340, 470, 450), 18);
		DirectGraphics::getInstance().ConstTextDraw(L"확인", mToolsRect, 14);
	}
	virtual UIToolsTypeEnum GetType() { return UITOOLSTYPEENUM::TextButtonSwitch; }
	virtual void Initialize(JsonValue_t & inValue)
	{
	}
private:
	wstring mMessage;
	D2D_RECT_F BoxRect;
	D2D_RECT_F OKButtonRect;
};
class UI
{
public:
	UI() { mUIToolsContainer.reserve(1); }
	~UI()
	{
		for (auto & iter : mUIToolsContainer)
		{
			iter = nullptr;
		}
	}
	void Draw()
	{
		for (auto iter : mUIToolsContainer)
		{
			if (iter->IsVisible())
				iter->Draw();
		}
	}
	bool Find(const uint32_t & inID, shared_ptr<UITools> & outPointer) {
		auto retval = std::find_if(mUIToolsContainer.begin(), mUIToolsContainer.end(), [=](shared_ptr<UITools> & item) {
			return item->GetToolsID() == inID;
		});
		if (retval != mUIToolsContainer.end())
		{
			outPointer = *retval;
			return true;
		}
		return false;
	}
	bool IntersectionCheck(const POINT & inPoint, uint32_t & outToolsID)
	{
		for (auto iter : mUIToolsContainer)
		{
			if (!iter->IsInputMode())
				continue;
			if (iter->PointInIntersectionCheckup(inPoint)) {
				outToolsID = iter->GetToolsID();
				return true;
			}
		}
		return false;
	}
	void Insert(UITools * inTool)
	{
		mUIToolsContainer.push_back(shared_ptr<UITools>(inTool));
	}
private:
	vector<shared_ptr<UITools>> mUIToolsContainer;
};
extern bool loadConfig(JsonValue_t * inRoot, const char * inFileName);

class UIManager : public Singleton<UIManager>
{
	typedef function<UITools *()> NewInstance;
public:
	template <class T>
	void RegistCreateMethods()
	{
		mFactoryMethodContainer[typeid(T).name()] = T::CreateInstance;
	}
	void Initialize()
	{
		UIManager::getInstance().RegistCreateMethods<ButtonTool>();
		UIManager::getInstance().RegistCreateMethods<TextTool>();
		UIManager::getInstance().RegistCreateMethods<TextBoardTool>();
		UIManager::getInstance().RegistCreateMethods<ColorBoxTool>();
		UIManager::getInstance().RegistCreateMethods<ImageTool>();
		UIManager::getInstance().RegistCreateMethods<SelectAvatarWindows>();
		UIManager::getInstance().RegistCreateMethods<TextButtonSwitchTool>();
	}

	/*bool IsMouseDownToUI(int inLevelID, const POINT & inPoint, uint32_t & outToolsID)
	{
		if (mUIContainer[inLevelID].IntersectionCheck(inPoint, outToolsID))
		{
			return true;
		}
		return false;
	}
	bool IsMouseUpToUI(int inID, const POINT & inPoint, uint32_t & inMouseUpToolsID)
	{
		if (mUIContainer[inID].IntersectionCheck(inPoint, inMouseUpToolsID))
		{
			return true;
		}
		return false;
	}*/
	//void SetUIActiveOn(int inLevelID, const uint32_t & inToolsID)
	//{
	//	shared_ptr<UITools> valuePtr;
	//	if (mUIContainer[inLevelID].Find(inToolsID, valuePtr))
	//	{
	//		valuePtr->SetActive(true);
	//	}
	//}
	//bool GetUIActiveOn(int inLevelID, const uint32_t & inToolsID)
	//{
	//	shared_ptr<UITools> valuePtr;
	//	if (mUIContainer[inLevelID].Find(inToolsID, valuePtr))
	//	{
	//		return valuePtr->IsActive();
	//	}
	//	return false;
	//}
	template<class T>
	bool CreateTools(int inLevelIndex, T *& outUIPtr)
	{
		if (mFactoryMethodContainer.find(typeid(T).name()) != mFactoryMethodContainer.end()) {
			NewInstance method = mFactoryMethodContainer[typeid(T).name()];
			outUIPtr = reinterpret_cast<T*>(method());
			outUIPtr->SetToolsID(mToolsCnt);
			mUIContainer[inLevelIndex].Insert(outUIPtr);
			++mToolsCnt;
			return true;
		}
		return false;
	}
	template<class T>
	T * CreateTools()
	{
		if (mFactoryMethodContainer.find(typeid(T).name()) != mFactoryMethodContainer.end()) {
			NewInstance method = mFactoryMethodContainer[typeid(T).name()];
			auto outptr = reinterpret_cast<T*>(method());
			outptr->SetToolsID(mToolsCnt);
			++mToolsCnt;
			return outptr;
		}
	}
private:
	// unordered_map<int, UI> mUIContainer;
	unordered_map<string, NewInstance> mFactoryMethodContainer;
	uint32_t mToolsCnt = 0;
};

