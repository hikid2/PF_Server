#pragma once

class PacketFactoryImpl : public PacketFactory
{
public:
	PacketFactoryImpl() : PacketFactory(){}
	virtual ~PacketFactoryImpl() {}
	virtual bool GetPacket(const uint32_t & inPacketType, Packet *& outPacketPtr)
	{
		switch (inPacketType)
		{
		case PacketType::SC_INPUT:
			break;
		case PacketType::SC_LOGIN_FAIL:
			break; ;
		case PacketType::SC_LOGIN_OK:
			break;
		case PacketType::SC_PUT_PLAYER:
			break;
		case PacketType::SC_REMOVE_PLAYER:
			break;
		default:
			break;
		}
		return false;
	}
};