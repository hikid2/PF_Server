#pragma once
///**
//@class		D2Text
//@date		2018.??.??
//@author		HanSaem
//@brief		텍스트 메시지 데이터
//@warning	Input 출력은
//*/
//class D2Text : public IPanelTools
//{
//public:
//	D2Text() {}
//	D2Text(D2D1_RECT_F inPosition, wstring inText, bool inVisible = true) 
//		: mPosition(inPosition), mVisible(inVisible), mOutText(inText)
//	{}
//	virtual ~D2Text(){}
//	virtual bool IdentifyFocus(const POINT & inPoint) { return false; }
//	virtual const D2D1_RECT_F & GetPosition() const { return mPosition; }
//	virtual wstring & GetText() { return mOutText; }
//	D2Text(D2Text && other) { *this = std::move(other); }
//	D2Text & operator=(D2Text && other)
//	{
//		if (this != &other)
//		{
//			mPosition = other.mPosition;
//			mVisible = other.mVisible;
//			mOutText = std::move(other.mOutText);
//
//			other.mPosition = D2D1::RectF();
//			other.mVisible = false;
//		}
//		return *this;
//	}
//private:
//	D2D1_RECT_F mPosition;
//	bool		mVisible;
//	wstring		mOutText;
//};
//
//class LoginPanel
//{
//public:
//	typedef enum {
//		LoginState,
//		JoinState
//	}LoginPanelState;
//	LoginPanel() 
//		: mIDTitle(D2D1::RectF(200, 340, 260, 380), L"ID"), 
//		mPWTitle(D2D1::RectF(200, 410, 280, 440), L"Password"), 
//		mState(LoginState), mBackgroundImage(L"resource\\Background.jpg"){}
//	void Draw() {
//		mBackgroundImage.Draw(D2D1::RectF(0, 0, 600, 600));
//	}
//private:
//	LoginPanelState mState;			/*로그인 상태*/
//	
//	DirectImage mBackgroundImage;	/*배경 이미지*/
//	D2Text		mIDTitle;			/*아이디 제목*/
//	D2Text		mPWTitle;			/*패스위드 제목*/
//
//	/*
//		id
//		pw
//		colorBoard
//		titleBackground;
//	*/
//};