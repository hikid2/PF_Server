#include "stdafx.h"
enum ENUM_LOGIN_UI_NAME
{
	LoginBoardColorBox = 'bcb',
	VariableID,
	VariablePW,
	ConstTextID,
	ConstTextPW,
	JoinButton,
	LoginButton,
	ExitButton,
};
LoginLevel::LoginLevel()
	: mTitleBackGround(new DirectImage(L"resource\\Background.jpg"))
{
	mSavedMouseDown_UI_ID = -1;
	mLoginUIs[LoginBoardColorBox] = UIManager::getInstance().CreateTools<ColorBoxTool>();
	mLoginUIs[VariableID] = UIManager::getInstance().CreateTools<TextBoardTool>();
	mLoginUIs[VariablePW] = UIManager::getInstance().CreateTools<TextBoardTool>();
	mLoginUIs[ConstTextID] = UIManager::getInstance().CreateTools<TextTool>();
	mLoginUIs[ConstTextPW] = UIManager::getInstance().CreateTools<TextTool>();
	mLoginUIs[JoinButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mLoginUIs[LoginButton] = UIManager::getInstance().CreateTools<ButtonTool>();
	mLoginUIs[ExitButton] = UIManager::getInstance().CreateTools<ButtonTool>();

	mLoginUIs[LoginBoardColorBox]->Initialize(false, true, false);
	mLoginUIs[ConstTextID]->Initialize(false, true, false);
	mLoginUIs[ConstTextPW]->Initialize(false, true, false);
	mLoginUIs[JoinButton]->Initialize(false, true, true);
	mLoginUIs[LoginButton]->Initialize(false, true, true);
	mLoginUIs[ExitButton]->Initialize(false, true, true);
	mLoginUIs[VariablePW]->Initialize(false, true, true);
	mLoginUIs[VariableID]->Initialize(false, true, true);

	// mLoginBoardColorBox->Initialize(D2D1::RectF(400.F - 140, 500.F, 400 + 140.F, 720.F), D2D1::ColorF(0.96F, 0.925F, 0.505F, 0.2F));
	reinterpret_cast<ColorBoxTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::LoginBoardColorBox])->Initialize(D2D1::RectF(400.F - 140, 500.F, 400 + 140.F, 720.F), D2D1::ColorF(0.96F, 0.925F, 0.505F, 0.2F));

	// mConstTextID->Initialize(L"ID : ", D2D1::RectF(400.F - 80, 550.F, 400.F - 70, 560.F));
	reinterpret_cast<TextTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::ConstTextID])->Initialize(L"ID : ", D2D1::RectF(400.F - 94, 550.F, 400.F - 50, 560.F));

	// mConstTextPW->Initialize(L"PW : ", D2D1::RectF(400.F - 94, 585.F, 400.F - 70, 595.F));
	reinterpret_cast<TextTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::ConstTextPW])->Initialize(L"PW : ", D2D1::RectF(400.F - 94, 585.F, 400.F - 50, 595.F));

	// mVariableID->Initialize(D2D1::RectF(400.F - 30, 560.F, 500.F, 585.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariableID])->Initialize(D2D1::RectF(400.F - 30, 560.F, 500.F, 585.F), D2D1::ColorF(D2D1::ColorF::LightGray));

	// mVariablePW->Initialize(D2D1::RectF(400.F - 30, 595.F, 500.F, 620.F), D2D1::ColorF(D2D1::ColorF::LightGray));
	reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariablePW])->Initialize(D2D1::RectF(400.F - 30, 595.F, 500.F, 620.F), D2D1::ColorF(D2D1::ColorF::LightGray));

	// mJoinButton->Initialize(L"resource\\Join_off.png", D2D1::RectF(400.F - 95, 630, 400.F - 5, 630 + 60));
	reinterpret_cast<ButtonTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::JoinButton])->Initialize(L"resource\\Join_off.png", D2D1::RectF(400.F - 95, 630, 400.F - 5, 630 + 60));

	// mLoginButton->Initialize(L"resource\\Login_off.png", D2D1::RectF(400.F + 5, 630, 400.F + 95, 630 + 60));
	reinterpret_cast<ButtonTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::LoginButton])->Initialize(L"resource\\Login_off.png", D2D1::RectF(400.F + 5, 630, 400.F + 95, 630 + 60));

	// mExitButton->Initialize(L"resource\\Exit_off.png", D2D1::RectF(760 - 90 + 5, 10, 760, 10 + 60));
	reinterpret_cast<ButtonTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::ExitButton])->Initialize(L"resource\\Exit_off.png", D2D1::RectF(760 - 90 + 5, 10, 760, 10 + 60));

}

LoginLevel::~LoginLevel() { delete mTitleBackGround; mTitleBackGround = nullptr; }

void LoginLevel::Initialize()
{
	reinterpret_cast<TextBoardTool*>(mLoginUIs[VariableID])->TextClear();
	reinterpret_cast<TextBoardTool*>(mLoginUIs[VariablePW])->TextClear();
}

void LoginLevel::Render() {
	DirectGraphics::getInstance().BeginDraw();
	DirectGraphics::getInstance().ClearScreen(1.f, 1.f, 1.0f);
	mTitleBackGround->Draw(D2D1::RectF(0, 0, 800, 800));
	for (auto iter : mLoginUIs)
	{
		if (iter.second->IsVisible()) iter.second->Draw();
	}
	for (auto iter : mMessages)
	{
		iter.Draw();
	}
	DirectGraphics::getInstance().EndDraw();
}

void LoginLevel::MouseButtonUp(const POINT & inPoint)
{
	if (!mMessages.empty()) {
		if (mMessages.back().PointInIntersectionCheckup(inPoint)){
			if (mSavedMouseDown_UI_ID == MessageBoxs){
				mMessages.pop_back();
				return;
			}
		}
	}

	for (auto iter : mLoginUIs)
	{
		if (!iter.second->IsInputMode()) continue;
		if (iter.second->PointInIntersectionCheckup(inPoint))
		{
			if (mSavedMouseDown_UI_ID == (int)iter.first)
			{
				switch (mSavedMouseDown_UI_ID)
				{
				case ENUM_LOGIN_UI_NAME::LoginButton:
				{
					auto LoginExecute = [=]() {
						auto varID = reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariableID]);
						auto varPW = reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariablePW]);
						if (varID->getText().empty()) {
							wstring mess = L"아이디가 비어져있다.";
							InsertMessage(mess);
							return;
						}
						if (varPW->getText().empty()) {
							
							wstring mess = L"비밀번호가 비어져 있다.";
							InsertMessage(mess);
							return;
						}

						cs_packet_Login_req packet;
						packet.mID = varID->getText();
						packet.mPW= varPW->getText();
						EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);
					};
					LoginExecute();
				}
				break;
				case ENUM_LOGIN_UI_NAME::ExitButton:
				{
					auto ExtButton = []() {

					};
				}
				break;
				case ENUM_LOGIN_UI_NAME::JoinButton:
				{
					auto JoinButton = []() {
						DirectFrameWork::getInstance().ChangeLevel(Join);
					};
					JoinButton();
				}
				break;
				default:
					break;
				}
			}
			else
			{
				mSavedMouseDown_UI_ID = -1;
			}
		}
	}


	/*if (UIManager::getInstance().IsMouseUpToUI(GetLevelName(), inPoint, mouse_up_id))
	{
		if (mSavedMouseDown_UI_ID == (int)mouse_up_id)
		{
			UIManager::getInstance().SetUIActiveOn(LevelEnum::Login, mouse_up_id);
		}
	}*/

}

void LoginLevel::MouseButtonDown(const POINT & inPoint)
{
	if (!mMessages.empty())
	{
		if (mMessages.back().PointInIntersectionCheckup(inPoint))
		{
			mSavedMouseDown_UI_ID = MessageBoxs;
			return;
		}
	}
	for (auto iter : mLoginUIs)
	{
		if (!iter.second->IsInputMode()) continue;
		if (iter.second->PointInIntersectionCheckup(inPoint))
		{
			mSavedMouseDown_UI_ID = (int)iter.first;
			return;
		}
	}

	/*if (UIManager::getInstance().IsMouseDownToUI(GetLevelName(), inPoint, mouse_down_id))
	{
		mSavedMouseDown_UI_ID = (int)mouse_down_id;
	}*/
}

void LoginLevel::InsertMessage(wstring & inText)
{
	mMessages.push_back(MessageBoxTool(inText));
}

void LoginLevel::KeyBoardIdentify(const WPARAM & inWParam)
{
	/*
		1. active가 되어있고
		2. 키 입력이 가능한 UI여야 가능하다.
	*/
	auto immWrite = [](const WPARAM & inWParam, wstring & inwstr) {
		InputManager::getInstance().Imm_Write((TCHAR)inWParam, inwstr);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;

	switch (mSavedMouseDown_UI_ID)
	{
	case ENUM_LOGIN_UI_NAME::VariableID:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariableID])->getText());
		break;
	case ENUM_LOGIN_UI_NAME::VariablePW:
		immWrite(inWParam, reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariablePW])->getText());
		break;
	}
}

void LoginLevel::KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd)
{
	auto immcomposition = [](const LPARAM & inLParam, wstring & inwstr, HWND & inHwnd) {
		InputManager::getInstance().Imm_Composition(inLParam, inwstr, inHwnd);
	};
	if (this->mSavedMouseDown_UI_ID == -1)
		return;



	switch (mSavedMouseDown_UI_ID)
	{
	case ENUM_LOGIN_UI_NAME::VariableID:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariableID])->getText(),
			inHwnd);
		break;
	case ENUM_LOGIN_UI_NAME::VariablePW:
		immcomposition(inLParam,
			reinterpret_cast<TextBoardTool*>(mLoginUIs[ENUM_LOGIN_UI_NAME::VariablePW])->getText(),
			inHwnd);
		break;
	}


	/*if (UIManager::getInstance().GetUIActiveOn(GetLevelName(), mSavedMouseDown_UI_ID))
	{
		if (mSavedMouseDown_UI_ID == 'VID')
		{
			TextBoardTool * ptrs;
			UIManager::getInstance().Find(0, mSavedMouseDown_UI_ID, ptrs);
			InputManager::getInstance().Imm_Write((TCHAR)inLParam, ptrs);
			ptrs->getText();
		}
	}*/
}
