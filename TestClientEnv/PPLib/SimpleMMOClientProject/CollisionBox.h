#pragma once

class CollisionBox
{
public:
	CollisionBox() {}
	CollisionBox(const D2D1_POINT_2F & inPoint, const double & inHeight, const double & inWidth)
		: mPoint(inPoint), mHeight(inHeight), mWidth(inWidth) {}
	~CollisionBox() {}
	CollisionBox(CollisionBox && other)
	{
		*this = std::move(other);
	}
	CollisionBox & operator=(CollisionBox && other)
	{
		if (this != &other)
		{
			mCid = std::move(other.mCid);
			mPoint = std::move(other.mPoint);
			mHeight = std::move(other.mHeight);
			mWidth = std::move(other.mWidth);
		}
		return *this;
	}
private:
	uint32_t		mCid;
	D2D1_POINT_2F	mPoint;
	double			mHeight;
	double			mWidth;

};

