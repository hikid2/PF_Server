#pragma once
class DirectImage;
class UITools;
class ColorBoxTool;
class TextTool;
class TextBoardTool;
class ButtonTool;

class SelectLevel : public IGameLevel
{
public:
	enum
	{
		OP_Select,
		OP_Create,
		OP_Delete,
	};
	SelectLevel();
	virtual ~SelectLevel();
	virtual void Initialize();
	virtual void Render();
	virtual bool Update(const uint32_t & inDeltaTime);
	void UpdateAvatarList(ReadStream & inStream, int eventType);
	virtual const uint32_t GetLevelName() { return LevelEnum::Login; }
	virtual void MouseButtonUp(const POINT & inPoint);
	virtual void MouseButtonDown(const POINT & inPoint);
	virtual bool GetInputFocus(D2TextBoard *& outPtr) { return false; }

	virtual void InsertMessage(wstring & inText);
	virtual void KeyBoardIdentify(const WPARAM & inWParam);
	virtual void KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd);
	void SetLevelState(int inValue)
	{
		mSelectStateID = inValue;
	}

	DirectImage * mTitleBackGround;
private:
	list<MessageBoxTool> mMessages;
	int mSelectStateID;
	bool IsUpdate = false;
	int32_t mSavedMouseDown_UI_ID; /*MouseDown*/
	unordered_map<uint32_t, UITools *> mSelectUIs;
	array<SelectAvatarData, 9> mAvatarLists;
	int32_t mAvatarCount;
	byte mSelectPage = 0;

	void SetupToSelect();

	void SetupToCretae();
};