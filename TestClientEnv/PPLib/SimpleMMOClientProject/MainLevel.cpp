#include "stdafx.h"
MainLevel::MainLevel() 
	: IsActiveInputBoard(false), mMiniBoard(new MiniPlayerInfoBoard()), mChatBoard(new ChattingBoard()),
	mInventory(new InventoryBoard())
{ 
	Initialize(); 
}

// IGameLevel을(를) 통해 상속됨

MainLevel::~MainLevel() { delete mMiniBoard; }
void MainLevel::Initialize()
{
	WorldManager::getInstance().ChangeWorld(WorldEnum::Start_Town);
	mChatBoard->Initialize(D2D1::RectF(0.f, 620, 400.f, 800.f));
	mMiniBoard->Initialize(); // Initialize
}

void MainLevel::Render()
{
	auto DrawNormalState = [&]() {
		XMFLOAT2 tmpFLoat2{ 0,0 };
		/// GetOwnXmfloat2에서 false를 반환하면 아직 유저의 정보가 정확히 기입이 안된것이다. 그리면안된다.
		if (PlayerManager::getInstance().GetOwnXmfloat2(tmpFLoat2))
			WorldManager::getInstance().DrawField(tmpFLoat2);
		// TODO : Loading screen show;
		// TODO : dead 알림 Screen show; -> 5초뒤 부활 이런거 보여주면 적당할것 같다.

		mMiniBoard->Draw();
		PlayerManager::getInstance().Draw();
		mChatBoard->Draw(IsActiveInputBoard);
		mInventory->Draw();
	};
	auto DrawDeadState = [&]() {
		XMFLOAT2 tmpFLoat2{ 0,0 };
		/// GetOwnXmfloat2에서 false를 반환하면 아직 유저의 정보가 정확히 기입이 안된것이다. 그리면안된다.
		if (PlayerManager::getInstance().GetOwnXmfloat2(tmpFLoat2))
			WorldManager::getInstance().DrawField(tmpFLoat2);
		mMiniBoard->Draw();
		PlayerManager::getInstance().Draw();
		mChatBoard->Draw(IsActiveInputBoard);
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::DarkGray, 0.88F), D2D1::RectF(0,0,800.F,800.F));
		DirectGraphics::getInstance().ConstTextDraw(L"당신은 죽었습니다. 5초뒤 마을에서 부활합니다.", D2D1::RectF(200, 300, 600, 600),24, D2D1::ColorF(D2D1::ColorF::Red, 0.99), DWRITE_TEXT_ALIGNMENT_CENTER);

	};
	auto DrawUnreadyState = [&]() {
		DirectGraphics::getInstance().DrawFillRectangle(D2D1::ColorF(D2D1::ColorF::DarkGray, 0.88F), D2D1::RectF(0, 0, 800.F, 800.F));
		DirectGraphics::getInstance().ConstTextDraw(L"로딩 중 입니다..", D2D1::RectF(200, 300, 600, 600), 24, D2D1::ColorF(D2D1::ColorF::LightYellow, 0.99), DWRITE_TEXT_ALIGNMENT_CENTER);
	
	};
	DirectGraphics::getInstance().BeginDraw();
	DirectGraphics::getInstance().ClearScreen(1.f, 1.f, 1.0f);
	auto ownPlayerState = PlayerManager::getInstance().GetPlayerState();
	/// 그려야 하는 순서가 다르기때문에 구분이 필요.
	switch (ownPlayerState)
	{
	case ENUM_PlayerState::READY:
		DrawNormalState();
		break;
	case ENUM_PlayerState::UNREADY:
		DrawUnreadyState();
		break;
	case ENUM_PlayerState::DEAD:
		DrawDeadState();
		break;
	}
	//XMFLOAT2 tmpFLoat2{0,0};
	///// GetOwnXmfloat2에서 false를 반환하면 아직 유저의 정보가 정확히 기입이 안된것이다. 그리면안된다.
	//if(PlayerManager::getInstance().GetOwnXmfloat2(tmpFLoat2))
	//	WorldManager::getInstance().DrawField(tmpFLoat2);
	//	// TODO : Loading screen show;
	//	// TODO : dead 알림 Screen show; -> 5초뒤 부활 이런거 보여주면 적당할것 같다.

	//mMiniBoard->Draw();
	//PlayerManager::getInstance().Draw();
	//mChatBoard->Draw(IsActiveInputBoard);
	//mInventory->Draw();
	DirectGraphics::getInstance().EndDraw();
}

bool MainLevel::Update(const uint32_t & inDeltaTime)
{
	mMiniBoard->Update(PlayerManager::getInstance().GetOwnAvatar());
	if (IsActiveInputBoard == false)
	{
		const uint32_t deltaTime = (uint32_t)(Timer::getInstance().GetTimeElapsed() * 1000);
		if (mAccumlateTime + deltaTime > 1000)
		{
			int retKeydata = 0;
			InputManager::getInstance().GetKeyBoardData(retKeydata, this);
			if (retKeydata != 0) {
				mAccumlateTime = (mAccumlateTime + deltaTime) - 1000;
				sc_packet_input packet;
				packet.key = retKeydata;
				EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);
			}
			
		}
		else
		{
			mAccumlateTime = mAccumlateTime + deltaTime;
		}
	}
	else
	{
		mAccumlateTime = 1000;
	}
	
	return false;
}

const uint32_t MainLevel::GetLevelName()
{
	return LevelEnum::Main;
}

void MainLevel::MouseButtonUp(const POINT & inPoint)
{
}

void MainLevel::MouseButtonDown(const POINT & inPoint)
{
}

bool MainLevel::GetInputFocus(D2TextBoard *& outPtr)
{
	return false;
}

void MainLevel::InsertMessage(wstring & inText)
{
}

void MainLevel::KeyBoardIdentify(const WPARAM & inWParam)
{
	auto immWrite = [](const WPARAM & inWParam, wstring & inwstr) {
		InputManager::getInstance().Imm_Write((TCHAR)inWParam, inwstr);
	};

	if (IsActiveInputBoard)
	{
		if (inWParam == '\r')
		{
			/*
			TODO : 지금까지 입력된 데이터를 SendPacket해야한다.
			SendPacket을 한뒤에 버퍼를 비우고 맨 처음 상태로 되돌아 가야한다.
			*/
			cs_packet_chat_req packet;
			packet.mMessage = mChatBoard->GetInputBoardText();
			EventSelectNetwork::getInstance().GetServerSession()->SendPacket(&packet);
			mChatBoard->ClearInputBoard();
			IsActiveInputBoard = false;
		}
		else
		{
			immWrite(inWParam, mChatBoard->GetInputBoardText());
		}
	}
	else
	{
		if (inWParam == '\r')
		{
			// TODO : 확인 취소 버튼이 있을 경우 Enter를 치면 확인 버튼을 누르게 끔 유도!
			IsActiveInputBoard = true;
		}
		else if (inWParam == L'i')
		{
			if (mInventory->IsVisible)
				mInventory->IsVisible = false;
			else
				mInventory->IsVisible = true;
		}
	}
}

void MainLevel::KeyBoardIdentify(const LPARAM & inLParam, HWND & inHwnd)
{
	auto immcomposition = [](const LPARAM & inLParam, wstring & inwstr, HWND & inHwnd) {
		InputManager::getInstance().Imm_Composition(inLParam, inwstr, inHwnd);
	};
	immcomposition(inLParam, mChatBoard->GetInputBoardText(), inHwnd);

}

void MainLevel::InsertBroadCastChatText(const wstring & inText)
{
	mChatBoard->InsertChattingMessage(inText);
}
