#pragma once

class SessionManager : public Singleton<SessionManager>
{
	// TODO : 함수기능 추가 필요.
public:
	SessionManager(){}

	virtual ~SessionManager()
	{
		mSessionContainer;
	}	

	bool CreateNewSession(SOCKET & inSocket, Session *& outSessionPtr);

	bool GetSession(const uint64_t & inSessionID, Session *& outSessionPtr);

	void CloseSession(const uint64_t & inValue);
	typedef tbb::concurrent_hash_map<uint64_t, Session *> SessionContainer_t;
private:
	/// unordered_map<uint64_t, Session *> mSessionContainer;
	SessionContainer_t mSessionContainer;
	static uint64_t mStaticCount;
};
