-- Create a new table called 'AvatarAbility' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarAbility', 'U') IS NOT NULL
DROP TABLE dbo.AvatarAbility
GO

-- Create a new table called 'KillMonsterQuestStatus' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.KillMonsterQuestStatus', 'U') IS NOT NULL
DROP TABLE dbo.KillMonsterQuestStatus
GO

-- Create a new table called 'DeliveryQuestStatus' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.DeliveryQuestStatus', 'U') IS NOT NULL
DROP TABLE dbo.DeliveryQuestStatus
GO

-- Create a new table called 'Regist_Item_auction' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Regist_Item_auction', 'U') IS NOT NULL
DROP TABLE dbo.Regist_Item_auction
GO

-- Create a new table called 'AvatarEquips_Information' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarEquips_Information', 'U') IS NOT NULL
DROP TABLE dbo.AvatarEquips_Information
GO

-- Create a new table called 'AvatarEquipInventory' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarEquipInventory', 'U') IS NOT NULL
DROP TABLE dbo.AvatarEquipInventory
GO

-- Create a new table called 'AvatarConsumeInventory' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarConsumeInventory', 'U') IS NOT NULL
DROP TABLE dbo.AvatarConsumeInventory
GO


-- Create a new table called 'AvatarOthersInventory' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarOthersInventory', 'U') IS NOT NULL
DROP TABLE dbo.AvatarOthersInventory
GO


-- Create a new table called 'Login_out_Info' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Login_out_Info', 'U') IS NOT NULL
DROP TABLE dbo.Login_out_Info
GO


-- Create a new table called 'CollectionQuestStatus' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.CollectionQuestStatus', 'U') IS NOT NULL
DROP TABLE dbo.CollectionQuestStatus
GO


-- Create a new table called 'AvatarLocation' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarLocation', 'U') IS NOT NULL
DROP TABLE dbo.AvatarLocation
GO

-- Create a new table called 'Avatar' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Avatar', 'U') IS NOT NULL
DROP TABLE dbo.Avatar
GO


-- Create a new table called 'User' in schema 'dbo'
-- Drop the table if it already exists

IF OBJECT_ID('dbo.Users', 'U') IS NOT NULL
DROP TABLE dbo.Users
GO


-- Create a new table called 'WearingItem' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.WearingItem', 'U') IS NOT NULL
DROP TABLE dbo.WearingItem
GO




-- Create the table in the specified schema

CREATE TABLE dbo.Users
(
    UserId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, -- primary key column
    IdVar [NVARCHAR](50) NOT NULL,  -- 실제아이디
    PwVar [NVARCHAR](50) NOT NULL,  -- 실제 비밀번호
    EmailVar [NVARCHAR](50) NOT NULL    -- 실제비밀번호
);
GO


-- Create a new table called 'AvatarClass' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.AvatarClass', 'U') IS NOT NULL
DROP TABLE dbo.AvatarClass
GO


-- Create a new table called 'DropProbability' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.DropProbability', 'U') IS NOT NULL
DROP TABLE dbo.DropProbability
GO
-- Create a new table called 'NPCShop' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.NPCShop', 'U') IS NOT NULL
DROP TABLE dbo.NPCShop
GO

-- Create a new table called 'CollectionQuest' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.CollectionQuest', 'U') IS NOT NULL
DROP TABLE dbo.CollectionQuest
GO



-- Create a new table called 'DefenceEquipAbility' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.DefenceEquipAbility', 'U') IS NOT NULL
DROP TABLE dbo.DefenceEquipAbility
GO

-- Create a new table called 'ConsumeItem' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.ConsumeItem', 'U') IS NOT NULL
DROP TABLE dbo.ConsumeItem
GO


-- Create a new table called 'WeaponAbility' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.WeaponAbility', 'U') IS NOT NULL
DROP TABLE dbo.WeaponAbility
GO
-- Create a new table called 'SpawnItemWeapon_Information' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.SpawnItemWeapon_Information', 'U') IS NOT NULL
DROP TABLE dbo.SpawnItemWeapon_Information
GO
-- Create a new table called 'SpawnItemDefence_Information' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.SpawnItemDefence_Information', 'U') IS NOT NULL
DROP TABLE dbo.SpawnItemDefence_Information
GO

-- Create a new table called 'DeliveryQuest' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.DeliveryQuest', 'U') IS NOT NULL
DROP TABLE dbo.DeliveryQuest
GO

-- Create a new table called 'KillMonsterQuest' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.KillMonsterQuest', 'U') IS NOT NULL
DROP TABLE dbo.KillMonsterQuest
GO

-- Create a new table called 'Quest' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Quest', 'U') IS NOT NULL
DROP TABLE dbo.Quest
GO

-- Create a new table called 'NPC' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.NPC', 'U') IS NOT NULL
DROP TABLE dbo.NPC
GO

-- Create a new table called 'SpawnItemData' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.SpawnItemData', 'U') IS NOT NULL
DROP TABLE dbo.SpawnItemData
GO


-- Create a new table called 'Item' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Item', 'U') IS NOT NULL
DROP TABLE dbo.Item
GO

-- Create a new table called 'ItemType' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.ItemType', 'U') IS NOT NULL
DROP TABLE dbo.ItemType
GO

-- Create the table in the specified schema
CREATE TABLE dbo.AvatarClass
(
    ClassType TINYINT NOT NULL PRIMARY KEY, -- primary key column
    ClassName [NVARCHAR](50) NOT NULL   -- 직업이름
);
GO

Insert INTO dbo.AvatarClass(ClassType, ClassName)
VALUES (1, N'전사'), (2, N'마법사'), (3, N'엘프')
go





-- Create the table in the specified schema
CREATE TABLE dbo.Avatar
(
    AvatarId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,                      -- primary key column
    AvatarName [NVARCHAR](50) NOT NULL UNIQUE,              -- 아바타 이름
    ClassType TINYINT NOT NULL,                             -- 직업 [FK] dbo.AvatarClass
    AvatarLevel INT NOT NULL DEFAULT 1,                     -- 아바타 레벨
    AvatarGold INT NOT NULL DEFAULT 0,                      -- 보유머니
    UserId INT NOT NULL ,                                   -- 유저아이디 FK
    AvatarCurExp INT NOT NULL DEFAULT 0,                    -- 현재 경험치
    CONSTRAINT FK_Avatar_ClassType FOREIGN KEY (ClassType)  -- FK 직업
    REFERENCES dbo.AvatarClass(ClassType) ON UPDATE CASCADE, 
    CONSTRAINT FK_Avatar_UserID FOREIGN KEY (UserId) 
    REFERENCES dbo.Users(UserId) ON DELETE CASCADE,         -- FK UserId
    CONSTRAINT CK_Avatar_Gold CHECK(AvatarGold >= 0),       -- Check (gold)
    CONSTRAINT CK_Avatar_Level CHECK(AvatarLevel >= 1 and AvatarLevel < 200), -- Check( level)
    CONSTRAINT CK_Avatar_Exp CHECK(AvatarCurExp >= 0)       -- Check(Exp )
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.AvatarAbility
(
    AvatarId INT NOT NULL PRIMARY KEY, -- primary key column
    Str int NOT NULL CHECK(0 < Str),
    Dex int  NOT NULL CHECK(0 < Dex),
    Ints int NOT NULL CHECK(0 < Ints),
    Luck int NOT NULL CHECK(0 < Luck),
    Max_hp bigint NOT NULL CHECK(0 < Max_hp),
    Curr_Hp bigint NOT NULL,
    Max_Mp bigint NOT null CHECK(0 < Max_Mp),
    Curr_Mp bigint not null ,
    CONSTRAINT FK_AvatarAbility 
    FOREIGN KEY (AvatarID) 
    REFERENCES dbo.Avatar(AvatarId),
    CONSTRAINT CK_HP CHECK(0 <= Curr_Hp and Curr_Hp <= Max_hp),
    CONSTRAINT CK_MP CHECK(0 <= Curr_mp and Curr_Mp <= Max_Mp)

);
GO



-- Create the table in the specified schema
CREATE TABLE dbo.AvatarLocation
(
    AvatarId INT NOT NULL PRIMARY KEY, -- primary key column
    AvatarField INT NOT NULL,
    Pos_X INT NOT NULL,
    Pos_Y INT NOT NULL,
    CONSTRAINT FK_AvatarLocation_AvatarId foreign key (AvatarId) REFERENCES dbo.Avatar(avatarId)
);
GO
-- 좌표정보, 체력 및 능력 테이블 만들어야함.



-- Create the table in the specified schema
CREATE TABLE dbo.Login_out_Info
(
    Login_out_InfoId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,  -- primary key column
    AvatarId int NOT NULL,                               -- 아바타 아이디 [FK]
    LoginTime datetime NOT NULL,                         -- 로그인 시간
    LogoutTime dateTIme NULL,                   -- 로그아웃 시간
    CONSTRAINT FK_LoginOutInfo_AvatarId 
    FOREIGN KEY (AvatarId) 
    REFERENCES dbo.Avatar(AvatarId) 
    ON DELETE CASCADE                          -- FK AvatarID Delete cascade
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.ItemType
(
    ItemTypeId INT NOT NULL PRIMARY KEY, -- primary key column
    TypeName [NVARCHAR](50) NOT NULL, --타입명
);
GO
BULK insert dbo.ItemType FROM N'C:\sqls\test\ItemType.csv'
with (
fieldterminator = ',' , -- 컬럼 구분 문자열
rowterminator = '\n' , -- 행 구분 문자열
firstrow = 2 -- 시작 행의 Index를 지정
)
go



-- Create the table in the specified schema
CREATE TABLE dbo.Item
(
    ItemId INT NOT NULL PRIMARY KEY,        -- primary key column
    ItemName [NVARCHAR](50) NOT NULL,       -- 아이템 이름
    SalePrice BIGINT NOT NULL DEFAULT 0,    -- 판매 가격
    ItemType INT NOT NULL,                  -- 아이템 타입 [FK]
    MaxItemCount TINYINT NOT NULL DEFAULT 1, -- 한슬롯에 가질 수있는 최대수량
    CONSTRAINT FK_Item_ItemType 
    FOREIGN KEY (ItemType) 
    REFERENCES dbo.ItemType(ItemTypeId)
    ON UPDATE CASCADE
    ON DELETE CASCADE                       -- FK ItemType Cascade update and Delete
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.WearingItem
(
    ItemId INT NOT NULL PRIMARY KEY,        -- primary key column
    ClassType TINYINT NOT NULL,             -- 착용가능 직업 [FK]
    RequireLevel int NOT NULL DEFAULT 1,    -- 착용가능 레벨 
    CONSTRAINT FK_WearingItem_ClassType 
    FOREIGN KEY (ClassType) 
    REFERENCES dbo.AvatarClass(ClassType)
    ON DELETE CASCADE,                      --
    CONSTRAINT FK_WearingItem_ItemId 
    FOREIGN KEY (ItemId) 
    REFERENCES dbo.Item(ItemId)
    ON DELETE CASCADE
    ON UPDATE CASCADE,                       --
    CONSTRAINT CK_WearingItem_RequireLevel 
    CHECK (RequireLevel > 0)    -- 
);
GO


-- Create a new table called 'EffectCodeTable' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.EffectCodeTable', 'U') IS NOT NULL
DROP TABLE dbo.EffectCodeTable
GO
-- Create the table in the specified schema
CREATE TABLE dbo.EffectCodeTable
(
    EffectCode INT NOT NULL PRIMARY KEY, -- primary key column
    Body [NVARCHAR](50) NOT NULL
);
GO




-- Create the table in the specified schema
CREATE TABLE dbo.ConsumeItem
(
    ConsumeItemId INT NOT NULL PRIMARY KEY, -- primary key column
    EffectCode INT NOT NULL, -- 효과 코드[FK]
    EffectValue INT NOT NULL, -- 효과 값
    CONSTRAINT FK_ConsumeItem_EffectCode 
    FOREIGN KEY (EffectCode) 
    REFERENCES dbo.EffectCodeTable(EffectCode), --

    CONSTRAINT FK_ConsumeItem_ConsumeItemId 
    FOREIGN KEY (ConsumeItemId)
    REFERENCES dbo.Item(ItemId),    --

    CONSTRAINT CK_ConsumeItem_EffectValue 
    CHECK(EffectValue > 0)  --
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.WeaponAbility
(
    ItemId INT NOT NULL PRIMARY KEY,                -- primary key column
    MinPhyDamage INT NOT NULL,                      -- 최소물리공격력
    MinMagDamage INT NOT NULL,                      -- 최소마법공격력
    MinCriProbability FLOAT NOT NULL,               -- 최소크리티컬확률
    MinCriDamage FLOAT NOT NULL,                    -- 최소크리티컬데미지
    AttackSpeed FLOAT NOT NULL,                     -- 공격속도
    MaxPhyDamage INT NOT NULL,                      -- 최대물리공격력
    MaxMagDamage INT NOT NULL,                      -- 최대마법공격력
    MaxCriProbability FLOAT NOT NULL,               -- 최대크리티컬확률
    MaxCriDamage FLOAT NOT NULL,                    -- 최대크리티컬데미지

    CONSTRAINT FK_WeaponAbility_ItemId
    FOREIGN KEY (ItemId)
    REFERENCES dbo.Item(ItemId)
    ON DELETE CASCADE,                              --
    CONSTRAINT CK_WeaponAbility_PhyDamage 
    CHECK(MinPhyDamage <= MaxPhyDamage and MinPhyDamage >= 0),            --
    CONSTRAINT CK_WeaponAbility_MagDamage 
    CHECK(MinMagDamage <= MaxMagDamage and MinMagDamage >= 0),            --
    CONSTRAINT CK_WeaponAbility_CriProbability 
    CHECK(MinCriProbability <= MaxCriProbability and MinCriProbability >= 0),  --
    CONSTRAINT CK_WeaponAbility_CriDamage 
    CHECK(MinCriDamage <= MaxCriDamage and MinCriDamage >= 0)             --
);
GO

-- Create the table in the specified schema
CREATE TABLE dbo.DefenceEquipAbility
(
    ItemId INT NOT NULL PRIMARY KEY, -- primary key column
    MinPhyDefense int NOT NULL,  -- 최소물리방어력
    MaxPhyDefense int NOT NULL,  -- 최대물리방어력
    CONSTRAINT FK_DefenceEquipAbility_ItemId
    FOREIGN KEY (ItemId)
    REFERENCES dbo.Item(ItemId)
    ON DELETE CASCADE,              -- FK ItemId
    CONSTRAINT CK_DefenceEquipAbility_PhyDef 
    CHECK(MinPhyDefense <= MaxPhyDefense and MinPhyDefense >= 0)  -- Check phyDef
);
GO


-- Create a new table called 'Monster' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Monster', 'U') IS NOT NULL
DROP TABLE dbo.Monster
GO

-- Create a new table called 'MonsterType' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.MonsterType', 'U') IS NOT NULL
DROP TABLE dbo.MonsterType
GO
-- Create the table in the specified schema
CREATE TABLE dbo.MonsterType
(
    MonsterTypeId TINYINT NOT NULL PRIMARY KEY, -- primary key column
    TypeName [NVARCHAR](50) NOT NULL,
);
GO

-- Create a new table called 'WorldType' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.WorldType', 'U') IS NOT NULL
DROP TABLE dbo.WorldType
GO
-- Create the table in the specified schema
CREATE TABLE dbo.WorldType
(
    WorldTypeId INT NOT NULL PRIMARY KEY, -- primary key column
    body [NVARCHAR](50) UNIQUE NOT NULL ,
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.Monster
(
    MonsterId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, -- primary key column
    MonsterName [NVARCHAR](50) NOT NULL,            --  몬스터 이름
    MinDamage int NOT NULL,                         --  몬스터 최소 데미지
    MaxDamage int NOT NULL,                         --  몬스터 최대 데미지
    HP INT NOT NULL,                                --  몬스터 체력
    SpawnWorld INT,                                 -- 출현장소
    MonsterTypeId TINYINT NOT NULL,                     -- 몬스터 성향
    MonsterLevel INT NOT NULL,                      -- 몬스터 레벨
    DropExp INT NOT NULL,
    AttackSpeed FLOAT NOT NULL,
    CONSTRAINT FK_Monster_MonsterType
    FOREIGN key (MonsterTypeId)
    REFERENCES dbo.MonsterType(MonsterTypeId)
    ON UPDATE CASCADE,                              --
    CONSTRAINT FK_Monster_SpawnWorld
    FOREIGN KEY (SpawnWorld)
    REFERENCES dbo.WorldType(WorldTypeId)
    ON UPDATE CASCADE
    ON DELETE SET NULL,                             --
    CONSTRAINT CK_Monster_HP 
    CHECK(HP >= 0),                                 --
    CONSTRAINT CK_Monster_Damage 
    CHECK(MinDamage <= MaxDamage AND MinDamage >= 0),    --
    CONSTRAINT CK_Monster_MonsterLevel 
    CHECK(MonsterLevel > 0)                         --    
);
GO

-- Create the table in the specified schema
CREATE TABLE dbo.DropProbability
(
    DropProbabilityId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, -- primary key column
    MonsterId INT NOT NULL,  --
    ItemId INT NOT NULL, --
    Probability FLOAT NOT NULL,    --
    CONSTRAINT FK_DropProbability_MonsterId
    FOREIGN KEY (MonsterId)
    REFERENCES dbo.Monster(MonsterId),    -- 
    CONSTRAINT FK_DropProbability_ItemId
    FOREIGN KEY (ItemId)
    REFERENCES dbo.Item(ItemId),    --
    CONSTRAINT CK_DropProbability_Probability 
    CHECK(Probability > 0)  --
);
GO



-- Create the table in the specified schema
CREATE TABLE dbo.NPC
(
    NPCId INT NOT NULL PRIMARY KEY, -- primary key column
    NPCName [NVARCHAR](50) NOT NULL UNIQUE,    --
    NPCLocationX [INT]NOT NULL, --  
    NPCLocationY [INT]NOT NULL, --
    NPCSpawnWorld INT NOT NULL, --
    CONSTRAINT FK_NPC_NPCSpawnWorld 
    FOREIGN KEY (NPCSpawnWorld)
    REFERENCES dbo.WorldType(WorldTypeId),  --
    CONSTRAINT CK_NPCLocations 
    CHECK(NPCLocationX >= 0 and NPCLocationY >= 0)  --
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.Quest
(
    QuestId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, -- primary key column
    QuestTitle [NVARCHAR](50) NOT NULL UNIQUE,
    QuestBody [NVARCHAR](200) NOT NULL,
    NpcId INT NOT NULL FOREIGN KEY REFERENCES dbo.NPC(NpcId),
    ActiveLevel INT NOT NULL CHECK(ActiveLevel > 0),
);
GO




-- Create the table in the specified schema
CREATE TABLE dbo.NPCShop
(
    NPCShopId INT NOT NULL PRIMARY KEY, -- primary key column
    NPCId INT NOT NULL FOREIGN KEY REFERENCES dbo.NPC(NPCId),
    ItemId INT NOT NULL FOREIGN KEY REFERENCES dbo.Item(ItemId),
    PurchasePrice BIGINT NOT NULL CHECK(PurchasePrice > 0)
);
GO

-- Create the table in the specified schema
CREATE TABLE dbo.KillMonsterQuest
(
    KillMonsterQuestId INT NOT NULL PRIMARY KEY, -- primary key column
    MonsterId INT NOT NULL FOREIGN KEY REFERENCES dbo.Monster(MonsterId),   --
    ClearToMonsterCount INT NOT NULL CHECK(ClearToMonsterCount >= 5),   --
    CONSTRAINT KF_KillMonsterQuest_Id
    FOREIGN KEY (KillMonsterQuestId)
    REFERENCES dbo.Quest(QuestId),  -- 
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.DeliveryQuest
(
    DeliveryQuestId INT NOT NULL PRIMARY KEY, -- primary key column
    Sender INT NOT NULL FOREIGN KEY REFERENCES dbo.NPC(NPCId),
    Reciver INT NOT NULL FOREIGN KEY REFERENCES dbo.NPC(NPCId),
    CONSTRAINT FK_DeliveryQuest_DeliveryQuestId 
    FOREIGN KEY (DeliveryQuestId)
    REFERENCES dbo.Quest(QuestId),
    CONSTRAINT CK_NotEqulSender_Reciver
    CHECK(Sender <> Reciver)
);
GO



-- Create the table in the specified schema
CREATE TABLE dbo.CollectionQuest
(
    CollectionQuestId INT NOT NULL PRIMARY KEY, -- primary key column
    ItemId INT NOT NULL FOREIGN KEY REFERENCES dbo.Item(ItemId),
    RequireCount int NOT NULL CHECK(RequireCount >= 5),
    CONSTRAINT FK_CollectionQuest_CollectionQuestId 
    FOREIGN KEY (CollectionQuestId)
    REFERENCES dbo.Quest(QuestId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.CollectionQuestStatus
(
    QuestId INT NOT NULL FOREIGN KEY REFERENCES dbo.Quest(QuestId), -- primary key column
    AvatarId INT NOT NULL FOREIGN KEY REFERENCES dbo.Avatar(AvatarId),
    CollectingCount INT NOT NULL CHECK(CollectingCount >= 0),
    IsSucc BIT NOT NULL DEFAULT(0),
    QuestAcceptTime DATETIME NOT NULL,
    PRIMARY KEY(QuestId, AvatarId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.KillMonsterQuestStatus
(
    QuestId INT NOT NULL FOREIGN KEY REFERENCES dbo.Quest(QuestId), -- primary key column
    AvatarId INT NOT NULL FOREIGN key REFERENCES dbo.Avatar(AvatarId),
    KillMonsterCount INT NOT NULL CHECK(KillMonsterCount >= 0),
    IsSucc bit NOT NULL DEFAULT(0),
    QuestAcceptTime DATETIME NOT NULL,
    PRIMARY KEY(QUestId, AvatarId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.DeliveryQuestStatus
(
    QuestId INT NOT NULL FOREIGN key REFERENCES dbo.Quest(QUestId), -- primary key column
    AvatarId int not null FOREIGN key REFERENCES dbo.Avatar(AvatarId),
    IsSucc bit not null DEFAULT(0),
    HasItem bit NOT NULL DEFAULT(0),
    QuestAcceptTime DATETIME NOT NULL,
    PRIMARY KEY(QUestId, AvatarId)
);
GO




-- Create a new table called 'LevelUpExp' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.LevelUpExp', 'U') IS NOT NULL
DROP TABLE dbo.LevelUpExp
GO
-- Create the table in the specified schema
CREATE TABLE dbo.LevelUpExp
(
    LevelNO INT NOT NULL PRIMARY KEY, -- primary key column
    NeedExp BIGINT NOT NULL Check(NeedExp > 0),
    HP INT NOT NULL,
    MP INT NOT NULL
);
GO
BULK insert dbo.LevelUpExp FROM N'C:\sqls\test\LevelupTable.csv'
with (
fieldterminator = ',' , -- 컬럼 구분 문자열
rowterminator = '\n' , -- 행 구분 문자열
firstrow = 2 -- 시작 행의 Index를 지정
)
go

-- Create the table in the specified schema
CREATE TABLE dbo.SpawnItemData
(
    SpawnItemId BIGINT NOT NULL PRIMARY KEY, -- primary key column
    ItemCode INT NOT NULL FOREIGN KEY REFERENCES dbo.Item(ItemId),
    SpawnTime DATETIME NOT NULL,
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.SpawnItemWeapon_Information
(
    SpawnItemId BIGINT NOT NULL PRIMARY KEY, -- primary key column
    Real_PhyDamage int NOT NULL CHECK(Real_PhyDamage >= 0),
    Real_MagDamage int NOT NULL CHECK(Real_MagDamage>= 0),
    Real_CriticalProbability FLOAT NOT NULL CHECK(Real_CriticalProbability>= 0),
    ReaL_CriticalDamamge FLOAT NOT NULL CHECK(ReaL_CriticalDamamge >= 0),
    Real_ReinforceCount TINYINT NOT NULL CHECK(Real_ReinforceCount >= 0),
    CONSTRAINT FK_SpawnItemWeapon_Information_SpawnItemId
    FOREIGN KEY (SpawnItemId)
    REFERENCES dbo.SpawnItemData(SpawnItemId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.Regist_Item_auction
(
    AuctionNo INT NOT NULL PRIMARY KEY, -- primary key column
    RegistedAvatarId INT NOT NULL FOREIGN KEY REFERENCES dbo.Avatar(AvatarId),
    Deadline DATETIME NOT NULL,
    SpawnItemId BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    RegistedTime DATETIME NOT NULL,
    ItemCount TINYINT NOT NULL CHECK(ItemCount > 0),
    SalePrice BIGINT NOT NULL CHECK(SalePrice > 0),
    IsSaleSucc BIT NOT NULL DEFAULT(0),
    PurchaseAvatarId INT FOREIGN KEY REFERENCES dbo.Avatar(AvatarId)
);
GO



-- Create the table in the specified schema
CREATE TABLE dbo.SpawnItemDefence_Information
(
    SpawnItemId BIGINT NOT NULL PRIMARY KEY, -- primary key column
    Real_PhyDefence INT NOT NULL CHECK(Real_PhyDefence >= 0),
    Real_MagDefence INT NOT NULL CHECK(Real_MagDefence >= 0),
    Real_ReinforceCount TINYINT NOT NULL CHECK(Real_ReinforceCount >= 0),
    CONSTRAINT FK_SpawnItemDefence_Information_SpawnItemId
    FOREIGN KEY (SpawnItemId)
    REFERENCES dbo.SpawnItemData(SpawnItemId)
);
GO

-- Create the table in the specified schema
CREATE TABLE dbo.AvatarEquips_Information
(
    AvatarId INT NOT NULL PRIMARY KEY, -- primary key column
    EquipHead BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    EquipBody BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    EquipHand BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    EquipFoot BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    EquipWeapon BIGINT FOREIGN KEY REFERENCES dbo.SpawnItemData(SpawnItemId),
    CONSTRAINT FK_EquipsInformation_AvatarId
    FOREIGN KEY (AvatarId)
    REFERENCES dbo.Avatar(AvatarId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.AvatarEquipInventory
(
    AvatarId INT NOT NULL FOREIGN key REFERENCES dbo.Avatar(AvatarId), -- primary key column
    BagIndex TINYINT NOT NULL,
    SpawnItemId BIGINT NOT NULL FOREIGN key REFERENCES dbo.SpawnItemData(SpawnItemId),
    PRIMARY KEY (AvatarId, BagIndex)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.AvatarConsumeInventory
(
    AvatarId INT NOT NULL, -- primary key column
    BagIndex int NOT NULL ,
    ItemId INT NOT NULL FOREIGN KEY REFERENCES dbo.Item(ItemId),
    Counts TINYINT NOT NULL CHECK(Counts >= 0),
    PRIMARY KEY(AvatarId, BagIndex),
    CONSTRAINT FK_AvatarConsumeInventory_AvatarId
    FOREIGN KEY (AvatarId)
    REFERENCES dbo.Avatar(AvatarId)
);
GO


-- Create the table in the specified schema
CREATE TABLE dbo.AvatarOthersInventory
(
    AvatarId INT NOT NULL, -- primary key column
    BagIndex int NOT NULL ,
    ItemId INT NOT NULL FOREIGN KEY REFERENCES dbo.Item(ItemId),
    Counts TINYINT NOT NULL CHECK(Counts >= 0),
    PRIMARY KEY(AvatarId, BagIndex),
    CONSTRAINT FK_AvatarOthersInventory_AvatarId
    FOREIGN KEY (AvatarId)
    REFERENCES dbo.Avatar(AvatarId)
);
GO
