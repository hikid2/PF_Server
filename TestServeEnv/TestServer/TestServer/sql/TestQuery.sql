-- DECLARE @JOINID NVARCHAR(50)
-- DECLARE @JOINPW NVARCHAR(50)
-- DECLARE @JOINEMAIL NVARCHAR(50)

-- DECLARE @RETVAL INT

-- DECLARE @MESSAGE NVARCHAR(50)

-- SET @JOINID = N'a'
-- SET @JOINPW = N'a'
-- SET @JOINEMAIL = N'@naver.com'

-- EXECUTE dbo.proc_Join @JOINID, @JOINPW, @JOINEMAIL, @RETVAL OUTPUT, @MESSAGE OUTPUT

-- select @RETVAL, @MESSAGE
-- GO


-- DECLARE @TID NVARCHAR(50);
-- DECLARE @TPW NVARCHAR(50);
-- DECLARE @retval INT;
-- DECLARE @resultUID INT;

-- set @TID = N't'
-- set @TPW = N't'

-- EXECUTE dbo.proc_Login @TID , @TPW , @retval OUTPUT , @resultUID OUTPUT
-- select @retval , @resultUID


-- DECLARE @inClassType  TINYINT    
-- DECLARE @inAvatarName [NVARCHAR](50)       
-- DECLARE @outRetCode   INT

-- set @inClassType  = 1
-- set @inAvatarName  = N'테스트 전사2'


-- EXECUTE dbo.SP_CreateAvatars @inClassType, @inAvatarName, @resultUID, @outRetCode OUTPUT
-- select @outRetCode

-- set @inClassType  = 2
-- set @inAvatarName  = N'테스트 마법사'


-- EXECUTE dbo.SP_CreateAvatars @inClassType, @inAvatarName, @resultUID, @outRetCode OUTPUT
-- select @outRetCode

-- DECLARE @retvals int


-- EXECUTE dbo.sp_ShowMyAvatars @resultUID, @retvals
-- GO

-- DECLARE @avatarid INT
-- set @avatarid = 6
-- -- example to execute the stored procedure we just created
-- EXECUTE dbo.SP_EnnterIngame @avatarid
-- GO
-- create NONCLUSTERED INDEX IX_

-- CREATE PROCEDURE dbo.SP_AvatarLevelUp

DECLARE @outLevel INT
DECLARE @outCurrExp INT
DECLARE @outNeedExp INT
DECLARE @outCurrHP INT
DECLARE @outCurrMP INT
DECLARE @outMaxHp INT
DECLARE @outMaxMP INT
DECLARE @outvalue INT
execute dbo.SP_AvatarLevelUp 2, 53,  @outLevel output , @outCurrExp output, @outNeedExp output, @outCurrHP output, @outCurrMP output, @outMaxHp output, @outMaxMP output, @outvalue output
select @outLevel,@outCurrExp,@outNeedExp,@outCurrHP,@outCurrMP,@outMaxHp,@outMaxMP
go