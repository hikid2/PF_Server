-- Create a new stored procedure called 'proc_Login' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'proc_Login'
)
DROP PROCEDURE dbo.proc_Login
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.proc_Login
    @id NVARCHAR(50),
    /*입력 아이디*/
    @password NVARCHAR(50),
    /*입력 패스워드*/
    @OutretCode INT OUTPUT,
    /*반환값[1 = 로그인 성공 , 0 = 로그인 실패]*/
    @OutUserId INT OUTPUT
/*반환유저 ID*/
AS
-- body of the stored procedure
SET NOCOUNT ON;

DECLARE @sameCount int
set @sameCount = (SELECT COUNT(*)
FROM dbo.Users as u
WHERE @id = u.IdVar and @password = u.PwVar)
IF @sameCount = 1 BEGIN
    SET @OutretCode = 1
    SET @OutUserId = (SELECT u.UserId
    FROM dbo.Users as u
    WHERE u.IdVar = @id)
END
    ELSE BEGIN
    SET @OutretCode = -1
END
GO
-- example to execute the stored procedure we just created
-- DECLARE @TID NVARCHAR(50);
-- DECLARE @TPW NVARCHAR(50);
-- DECLARE @retval INT;
-- DECLARE @resultUID INT;

-- EXECUTE dbo.proc_Login @TID , @TPW , @retval, @resultUID
-- GO





-- Create a new stored procedure called 'proc_Join' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'proc_Join'
)
DROP PROCEDURE dbo.proc_Join
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.proc_Join
    @joinid NVARCHAR(50),
    @joinpw NVARCHAR(50),
    @join_email NVARCHAR(50),
    @result int OUTPUT,
    @message NVARCHAR(50) OUTPUT
AS
set NOCOUNT ON
-- 중복된 아이디 확인
DECLARE @sameIdCount int;
SET @sameIdCount = (SELECT COUNT(*)
FROM dbo.Users as u
WHERE u.IdVar = @joinid)
IF @sameIdCount = 0 BEGIN
    BEGIN TRY
        BEGIN TRAN
        INSERT INTO dbo.Users
        (IdVar, PwVar, EmailVar)
    VALUES(@joinid, @joinpw, @join_email)
        SET @result = 1
        SET @message = N'JOIN SUCC'
        COMMIT TRAN
        return
        END TRY
        BEGIN CATCH 
        SET @result = (select ERROR_NUMBER() AS ErrorNumber)
        SET @message = (select ERROR_MESSAGE() AS ErrorMessage)
        ROLLBACK TRAN
        END CATCH
END
    ELSE BEGIN
    SET @result = -1
    SET @message = N'같은 아이디가 이미 존재한다'
END
GO
-- example to execute the stored procedure we just created
-- DECLARE @JOINID NVARCHAR(50)
-- DECLARE @JOINPW NVARCHAR(50)
-- DECLARE @JOINEMAIL NVARCHAR(50)
-- DECLARE @RETVAL INT
-- DECLARE @MESSAGE NVARCHAR(50)
-- EXECUTE dbo.proc_Join @JOINID, @JOINPW, @JOINEMAIL, @RETVAL, @MESSAGE
-- GO

-- Create a new stored procedure called 'sp_ShowMyAvatars' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'sp_ShowMyAvatars'
)
DROP PROCEDURE dbo.sp_ShowMyAvatars
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.sp_ShowMyAvatars
    @UserId INT,
    @retval int out
AS
set NOCOUNT ON
DECLARE @cnts int;
set @cnts = (SELECT COUNT(*)
FROM dbo.Avatar as a
WHERE a.UserId = @UserId)
IF @cnts = 0 BEGIN
    set @retval = -1
    RETURN;
END
SELECT a.AvatarId, a.AvatarName , a.ClassType, a.AvatarLevel, al.AvatarField
FROM dbo.Avatar as a
    INNER JOIN dbo.AvatarLocation as al
    ON al.AvatarId = a.AvatarId
WHERE a.UserId = @UserId

set @retval = 0
GO
-- example to execute the stored procedure we just created
-- DECLARE @userid int
-- DECLARE @retval int
-- EXECUTE dbo.sp_ShowMyAvatars @userid, @retval
-- GO

-- Create a new stored procedure called 'SP_CreateAvatars' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_CreateAvatars'
)
DROP PROCEDURE dbo.SP_CreateAvatars
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_CreateAvatars
    @inClassType    TINYINT,
    @inAvatarName   [NVARCHAR](50),
    @inUserId       INT,
    @outRetCode     INT OUT
AS
SET NOCOUNT ON
DECLARE @sameNameCnt int
DECLARE @sameAvatarCnt int
DECLARE @createAvatarId int
SET @sameNameCnt = (SELECT COUNT(*)
FROM dbo.Avatar as a
WHERE a.AvatarName = @inAvatarName)
SET @sameAvatarCnt = (SELECT COUNT(*)
FROM dbo.Avatar as a
WHERE a.UserId = @inUserId)

IF @sameNameCnt <> 0 BEGIN
    SET @outRetCode = -2
    RETURN
END
IF @sameAvatarCnt >= 9 BEGIN
    SET @outRetCode = -3
    RETURN
END
BEGIN TRY
    BEGIN TRAN
    INSERT INTO dbo.Avatar
    (AvatarName, ClassType, UserId)
VALUES(@inAvatarName, @inClassType, @inUserId)
    set @createAvatarId = (select SCOPE_IDENTITY())

    INSERT INTO dbo.AvatarAbility
    (AvatarId, Str, Dex, Ints, Luck, Max_hp, Curr_Hp, Max_Mp, Curr_Mp)
VALUES(@createAvatarId, 10, 10, 10, 10, 50, 50, 50, 50)

    INSERT INTO dbo.AvatarLocation
    (AvatarId, AvatarField, Pos_X, Pos_Y)
VALUES
    (@createAvatarId, 1, 100, 100)
    SET @outRetCode = 1

    COMMIT TRAN
END TRY

BEGIN CATCH
    SET @outRetCode = -1
    ROLLBACK TRAN
END CATCH
GO
-- example to execute the stored procedure we just created
-- DECLARE @inClassType  TINYINT    
-- DECLARE @inAvatarName [NVARCHAR](50)    
-- DECLARE @inUserId     INT    
-- DECLARE @outRetCode   INT   
-- EXECUTE dbo.SP_CreateAvatars @inClassType, @inAvatarName, @inUserId, @outRetCode
-- GO


-- Create a new stored procedure called 'SP_EnnterIngame' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_EnnterIngame'
)
DROP PROCEDURE dbo.SP_EnnterIngame
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_EnnterIngame
    @inAvatarId INT,
    @outRetCode INT OUT,
    @outLoginTimeId INT OUT
AS
SET NOCOUNT ON
BEGIN TRY
BEGIN TRAN
    insert into dbo.Login_out_Info
    (AvatarId, LoginTime, LogoutTime)
values(@inAvatarId, GETDATE(), NULL)

    set @outLoginTimeId = (select SCOPE_IDENTITY())
COMMIT TRAN
SELECT
    a.AvatarName ,
    a.AvatarLevel,
    a.AvatarGold,
    a.AvatarCurExp,
    a.ClassType,
    aa.Str,
    aa.Dex,
    aa.Ints,
    aa.Luck,
    aa.Max_hp,
    aa.Max_Mp,
    aa.Curr_Hp,
    aa.Curr_Mp,
    al.AvatarField,
    al.Pos_X,
    al.Pos_Y,
    LE.NeedExp
FROM dbo.Avatar as a
    INNER JOIN dbo.AvatarAbility as aa
    ON a.AvatarId = aa.AvatarId
    inner JOIN dbo.AvatarLocation as al
    ON al.AvatarId = a.AvatarId
    INNER JOIN dbo.LevelUpExp as LE
    ON LE.LevelNO = a.AvatarLevel
WHERE a.AvatarId = @inAvatarId

    set @outRetCode = 1

END TRY
BEGIN CATCH
    set @outRetCode = -1
    ROLLBACK TRAN
END CATCH

GO
-- DECLARE @avatarid INT
-- -- example to execute the stored procedure we just created
-- EXECUTE dbo.SP_EnnterIngame @avatarid
-- GO

-- Create a new stored procedure called 'SP_ExitIngame' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_ExitIngame'
)
DROP PROCEDURE dbo.SP_ExitIngame
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_ExitIngame
    @inLoginTimeId INT,
    @outRetCode INT
-- add more stored procedure parameters here
AS
SET NOCOUNT ON
begin TRY
BEGIN TRAN
    UPDATE dbo.Login_out_Info set LoginTime = GETDATE()
    WHERE Login_out_InfoId = @inLoginTImeId
    SET @outRetCode = 1
COMMIT TRAN
END TRY
BEGIN CATCH
    SET @outRetCode = -1
    ROLLBACK TRAN
END CATCH
GO
-- example to execute the stored procedure we just created
-- EXECUTE dbo.SP_ExitIngame 
-- GO

-- Create a new stored procedure called 'SP_AvatarLocationUpdate' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_AvatarLocationUpdate'
)
DROP PROCEDURE dbo.SP_AvatarLocationUpdate
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_AvatarLocationUpdate
    @inAvatarId INT,
    @inPointx INT,
    @inPointY INT,
    @inFieldID INT,
    @outRetval INT OUT
-- add more stored procedure parameters here
AS
SET NOCOUNT ON
begin TRY
begin TRAN
update dbo.AvatarLocation 
    set Pos_X = @inPointx , Pos_Y = @inPointY, AvatarField = @inFieldID
    WHERE AvatarLocation.AvatarId = @inAvatarId
COMMIT TRAN
    set @outRetval = 0
end TRY
BEGIN CATCH
    set @outRetval = -2
end CATCH
    -- body of the stored procedure
    
GO

-- Create a new stored procedure called 'SP_AvatarLevelUp ' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_AvatarLevelUp'
)
DROP PROCEDURE dbo.SP_AvatarLevelUp
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_AvatarLevelUp
    @inAvatarId INT,
    @inCurrExp INT,
    @outLevel INT OUT,
    @outCurrExp INT OUT,
    @outNeedExp INT OUT,
    @outCurrHP INT OUT,
    @outCurrMP INT OUT,
    @outMaxHp INT OUT,
    @outMaxMP INT OUT,
    @outretcode INT OUT
AS
BEGIN TRY
BEGIN TRAN
SET NOCOUNT ON
DECLARE @NextLevelExp INT
set @NextLevelExp = (
    select lue.NeedExp
    from dbo.LevelUpExp as lue
    JOIN dbo.Avatar as avt
    on lue.LevelNO = avt.AvatarLevel + 1
    WHERE avt.AvatarId = @inAvatarId
)

    UPDATE dbo.Avatar
    set AvatarLevel = lue.LevelNO
    from dbo.LevelUpExp as lue
    WHERE Avatar.AvatarId = @inAvatarId and lue.LevelNO = AvatarLevel + 1

    UPDATE dbo.Avatar
    set AvatarCurExp = (@NextLevelExp - @inCurrExp)
    where Avatar.AvatarId = @inAvatarId

    UPDATE dbo.AvatarAbility
    set Max_hp = lue.HP
    from dbo.LevelUpExp as lue
    where AvatarAbility.AvatarId = @inAvatarId and lue.LevelNO = (select AvatarLevel + 1
    from dbo.Avatar
    where @inAvatarId = Avatar.AvatarId)

    UPDATE dbo.AvatarAbility
    set Max_Mp = lue.MP
    from dbo.LevelUpExp as lue
    where AvatarAbility.AvatarId = @inAvatarId and lue.LevelNO = (select AvatarLevel + 1
    from dbo.Avatar
    where @inAvatarId = Avatar.AvatarId)

    UPDATE dbo.AvatarAbility
    set Curr_Hp = lue.HP
    from dbo.LevelUpExp as lue
    where AvatarAbility.AvatarId = @inAvatarId and lue.LevelNO = (select AvatarLevel + 1
    from dbo.Avatar
    where @inAvatarId = Avatar.AvatarId)

    UPDATE dbo.AvatarAbility
    set Curr_Mp = lue.MP
    from dbo.LevelUpExp as lue
    where AvatarAbility.AvatarId = @inAvatarId and lue.LevelNO = (select AvatarLevel + 1
    from dbo.Avatar
    where @inAvatarId = Avatar.AvatarId)
    SELECT
    @outLevel = a.AvatarLevel,
    @outCurrExp = a.AvatarCurExp,
    @outMaxHp = aa.Max_hp,
    @outMaxMP =  aa.Max_Mp,
    @outCurrHP = aa.Curr_Hp,
    @outCurrMP = aa.Curr_Mp,
    @outNeedExp = LE.NeedExp
FROM dbo.Avatar as a
    INNER JOIN dbo.AvatarAbility as aa
    ON a.AvatarId = aa.AvatarId
    INNER JOIN dbo.LevelUpExp as LE
    ON LE.LevelNO = a.AvatarLevel
WHERE a.AvatarId = @inAvatarId
    set @outretcode = 1
COMMIT TRAN
END try
    begin CATCH
        ROLLBACK TRAN
        set @outretcode = -2
    end CATCH
GO

-- -- example to execute the stored procedure we just created
-- EXECUTE dbo.SP_AvatarLevelUp 1 /*value_for_param1*/, 2 /*value_for_param2*/
-- GO
-- example to execute the stored procedure we just created
-- EXECUTE dbo.SP_AvatarLocationUpdate 1 /*value_for_param1*/, 2 /*value_for_param2*/
-- GO



-- Create a new stored procedure called 'SP_UpdateNowExp' in schema 'dbo'
-- Drop the stored procedure if it already exists
IF EXISTS (
SELECT *
    FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA = N'dbo'
    AND SPECIFIC_NAME = N'SP_UpdateNowExp'
)
DROP PROCEDURE dbo.SP_UpdateNowExp
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE dbo.SP_UpdateNowExp
    @inAvatarId INT,
    @inNowExp INT
-- add more stored procedure parameters here
AS
set NOCOUNT ON
BEGIN TRAN
    UPDATE dbo.Avatar
    SET AvatarCurExp = @inNowExp
    WHERE AvatarId = @inAvatarId
commit TRAN
GO
-- example to execute the stored procedure we just created
EXECUTE dbo.SP_UpdateNowExp 1 /*value_for_param1*/, 2 /*value_for_param2*/
GO
/*
	1. 
골드 얻기


proc_GetGold()
	1. 
몬스터 잡기 (경험치 관련 -> 레벨업도 묶어서)


proc_
	1. 
인게임 나가기


proc_
아이템 드랍
경매장에 아이템을 등록
경매장에서 아이템을 구매
NPC에게 아이템을 구매
NPC에게 퀘스트를 받음
NPC에게 퀘스트를 완료

*/