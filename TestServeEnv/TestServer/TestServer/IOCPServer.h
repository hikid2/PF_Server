#pragma once

class Session;
class IOCPServer : public BaseServer, public Singleton<IOCPServer>
{
public:
	IOCPServer() : BaseServer() { mIocpHandle = NULL; }

	virtual bool Run();

	static DWORD WINAPI AcceptExecute(LPVOID inServerPtr);

	static void WINAPI WorkerExecute(LPVOID inServerPtr);

	handle_t & GetIOCPHandle() { return mIocpHandle; }

	SOCKET & GetListenSocket() { return mListenSocket; }
private:
	handle_t	mIocpHandle;
	SOCKET		mListenSocket; 
};