#include "stdafx.h"

Avatar::Avatar()
{
	ZeroMemory(&mAbility, sizeof(mAbility));
	mHorizontal = 0;
	mVertical = 1;
	mDBStoredAvatarID = -1;
	mName = L"";
	mLevel = 0;
	mClassType = 0;
	mCurrHP = 0;
	mCurrMP = 0;
	mMaxHP = 0;
	mMaxMP = 0;
	mCurrExp = 0;
	mNeedExp = 0;
	mNowField = 128;
	mX = 0;
	mY = 0;
	mVisibleDistance = 4;
	mHasGold = 0;
	mAbility.dex = 0;
	mAbility.ints = 0;
	mAbility.luck = 0;
	mAbility.str = 0;
}

Avatar::Avatar(const Avatar & ins)
	: mDBStoredAvatarID(ins.mDBStoredAvatarID), mName(ins.mName), mLevel(ins.mLevel), mClassType(ins.mClassType),
	mCurrHP(ins.mCurrHP),
	mCurrMP(ins.mCurrMP), mMaxHP(ins.mMaxHP), mMaxMP(ins.mMaxMP), mCurrExp(ins.mCurrExp), mNeedExp(ins.mNeedExp), mNowField(ins.mNowField),
	mX(ins.mX), mY(ins.mY), mVisibleDistance(ins.mVisibleDistance), mHasGold(ins.mHasGold), mAbility(ins.mAbility)
{
	mViewList = ins.mViewList;
}

void Avatar::Clear()
{
	mHorizontal = 0;
	mVertical = 1;
	mDBStoredAvatarID = -1;
	mName = L"";
	mLevel = 0;
	mClassType = 0;
	mCurrHP = 0;
	mCurrMP = 0;
	mMaxHP = 0;
	mMaxMP = 0;
	mCurrExp = 0;
	mNeedExp = 0;
	mNowField = 128;
	mX = 0;
	mY = 0;
	mVisibleDistance = 4;
	mHasGold = 0;
	mAbility.dex = 0;
	mAbility.ints = 0;
	mAbility.luck = 0;
	mAbility.str = 0;
}

void Avatar::SetAvatarPosition(int32_t inPx, int32_t inPy)
{
	mX = inPx;
	mY = inPy;
}

// TODO : 주석설명이 필요하다. 왜 이렇게 되었는가?(Race Condition 때문!)
void Avatar::AddGold(const int & inValue)
{
	while (true)
	{
		int prevGold = mHasGold;
		int NextGold = prevGold + inValue;
		if (NextGold < 0)
			NextGold = 0;
		if (CAS(&mHasGold, prevGold, NextGold) == true)
			return;
	}
}

void Avatar::SetAbility(const int & inStr, const int & inDex, const int & inInts, const int & inLuck)
{
	mAbility.dex = inDex;
	mAbility.ints = inInts;
	mAbility.luck = inLuck;
	mAbility.str = inStr;
}

// 초기화때만 콜이 되야한다. 이후에는 race condition발생가능!
void Avatar::SetCurrHP(const uint32_t inValue) {
	mCurrHP = inValue;
}

// TODO : 주석설명이 필요하다. 왜 이렇게 되었는가?(Race Condition 때문!)
void Avatar::AddNowHp(const int inValue)
{
	uint32_t NextHP = 0;
	while (true)
	{
		uint32_t prevHP = mCurrHP;
		int addValue = static_cast<int>(prevHP) + inValue;
		if (addValue < 1)
		{
			NextHP = 0;
		}
		else
		{
			NextHP = static_cast<uint32_t>(addValue);
		}
		if (CAS(&mCurrHP, prevHP, NextHP) == true)
			return;
	}
}

void Avatar::AddCurExp(const int64_t & inValue)
{
	while (true)
	{
		int64_t prevExp = mCurrExp;
		int64_t addValue = prevExp + inValue;
		if (CAS(&mCurrExp, prevExp, addValue) == true)
			return;
	}
}

void Avatar::GetAbility(int & outStr, int & outDex, int & outInts, int & outLuck)
{
	outStr = mAbility.str;
	outDex = mAbility.dex;
	outInts = mAbility.ints;
	outLuck = mAbility.luck;
}

int Avatar::GetAttackDamamge()
{
	float mainAbil = 0;
	float subAbil = 0;
	random_device rn;

	mt19937_64 rnd(rn());


	switch ((int)mClassType)
	{
	case Enum_AvatarClass::Warrior:
		mainAbil = static_cast<float>(mAbility.str);
		subAbil = static_cast<float>(mAbility.dex);
		break;
	case Enum_AvatarClass::Elf:
		mainAbil = static_cast<float>(mAbility.dex);
		subAbil = static_cast<float>(mAbility.dex);
		break;
	case Enum_AvatarClass::Wizerd:
		mainAbil = static_cast<float>(mAbility.ints);
		subAbil = static_cast<float>(mAbility.luck);
		break;
	}
	// ((주 스텟 * 4) + 부 스텟 * 1.25) * 공격력 / 10 = 최대 공격력
	float Maxdamage = (float)((mainAbil * 4) + subAbil * 1.25) * 10 / 10;
	float Mindamage = static_cast<float>(Maxdamage * 0.8);
	uniform_int_distribution<int> range((int)Mindamage, (int)Maxdamage);
	return range(rnd);
}

bool Avatar::IsFullHP()
{
	if (mCurrHP == mMaxHP)
		return true;
	return false;
}

void Avatar::SetRespawn()
{
	World::getInstance().DoWarpField(ENUM_WARPID::RESPAWN_STARTTOWN, mX, mY, mNowField);
	mCurrHP = mMaxHP;
	mCurrMP = mMaxMP;

}
void Avatar::WarpTo(const int & inPointx, const int & inPointy, const byte & inFieldID)
{
	mX = inPointx;
	mY = inPointy;
	mNowField = inFieldID;
}
