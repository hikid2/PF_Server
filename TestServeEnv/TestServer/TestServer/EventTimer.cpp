#include "stdafx.h"

EventTimer::EventTimer()
{
	Global::getInstance().mEventTimerThread = new thread(&EventTimer::Execute, this);
	std::thread::id this_id = Global::getInstance().mEventTimerThread->get_id();
	Log(L"** EventTimer Thread Created [Thread ID : 0x%08x]", this_id);
}

void EventTimer::Push(const int inObjectID, int inTarget, int inSec, int inEventType, int inOperation)
{
	Events event;
	event.operation = inOperation;
	event.event_id = inEventType;
	event.object_id = inObjectID;
	event.target_id = inTarget;
	event.wakeup_time = inSec + GetTickCount64();

	EventTimerQueue.push(event);
}

void EventTimer::Execute()
{
	while (!IsShutDonw)
	{
		Sleep(1);
		while (false == EventTimerQueue.empty()) {
			auto currentTick = GetTickCount64();
			Events events;
			if (!EventTimerQueue.try_pop(events))
				break;
			if (events.wakeup_time > currentTick) {
				EventTimerQueue.push(events);
				break;
			}
			OverlappedEvent_t * over = nullptr;
			RecycleOverlappedManager::getInstance().PopEventOverlap(over);
			over->overlapEx.operation = events.operation;
			over->result_value = events.event_id;
			over->target = events.target_id;
			atomic_thread_fence(memory_order_seq_cst);
			PostQueuedCompletionStatus(IOCPServer::getInstance().GetIOCPHandle(), 1, events.object_id, &(over->overlapEx.original_overlap));
		}
	}
}
