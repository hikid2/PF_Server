#pragma once
/*
	DB에서 매번 정보를 읽기보단..
	서버의 메모리에서 미리 데이터를
	올려놓는게 좋을것으로 보임.
*/

#define DropItemON 0
#ifndef DropItemON

typedef int32_t MonsterID_t;

typedef int32_t ItemCode_t;

enum ENUM_ItemType
{
	Weapontype,
	DefenseType,
	ConsumeType,
	OtherType,
	QuestType,
};
enum ENUM_ConsumeEffectType
{
	HP_heal = 1,
	MP_heal,
};
#pragma pack(push, 1)
struct ItemConst
{
	ENUM_ItemType mItemType;
	int mItemCode;
	wstring mName;
	int mSalePrice;
	bool mSaleAvailable;
};
struct WeaponConst : public ItemConst
{
	int		mMinPhyDamage;
	int		mMaxPhyDamage;
	int		mMinMagDamage;
	int		mMaxMagDamage;

	float	mMinCriticalProbability;
	float	mMinCriticalDamage;
	float	mMaxCriticalProbability;
	float	mMaxCriticalDamage;
	float	mAttackSpeed;

	byte	mReqClass;
	int		mReqLv;
};
struct DefenseConst : public ItemConst
{
	int		mMinPhyDefense;
	int		mMaxPhyDefense;
	byte	mReqClass;
	int		mReqLv;
};

struct ConsumeConst : public ItemConst
{
	int mEffectCode;
	int mEffectValue;
};

struct DropProbability
{
	ItemCode_t mItemCode;
	int			mProbability;
};
#pragma pack(pop)
class Item
{
public:
	Item() : mItemCode(-1), mName(), mSalePrice(0),mSaleAvailable(true) {}
	Item(int inItemCode, wstring inName, int inSalePrice, bool inSaleAvailable = true)
		: mItemCode(inItemCode), mName(inName), mSalePrice(inSalePrice), mSaleAvailable(inSaleAvailable)
	{

	}
	virtual ~Item(){}
	virtual void Use() = 0;
	void SetItemCode(const int inValue) { mItemCode = inValue; }
	void SetName(const wstring inValue) { mName = inValue; }
	void SetSalePrice(const int inValue) { mSalePrice = inValue; }
	void SetSaleAvailable(const bool inValue) { mSaleAvailable = inValue; }


	int mItemCode;
	wstring mName;
	int mSalePrice;
	bool mSaleAvailable;
};

class DefenseItem : public Item
{
public:
	DefenseItem() : Item() 
	{ 
		mEquipState = false;
		mDefenseValue = 0;
		mReqClass = Warrior;
		mReqLv = 1;
	}
	DefenseItem(DefenseConst * inDummy)
		: Item(inDummy->mItemCode, inDummy->mName, inDummy->mSalePrice) 
	{
		mEquipState = false;
		mDefenseValue = Global::getInstance().GetRamdom(inDummy->mMinPhyDefense, inDummy->mMaxPhyDefense);
		mReqClass = inDummy->mReqClass;
		mReqLv = inDummy->mReqLv;
		
	}
	virtual ~DefenseItem()
	{

	}
	virtual void Use()
	{
	}


	const bool & GetEquiptState() { return mEquipState; }
	const int & GetDefenseValue() { return mDefenseValue; }
	const byte & GetReqClass() { return mReqClass; }
	const int & GetReqLV() { return mReqLv; }

private:
	bool	mEquipState;
	int		mDefenseValue;
	byte	mReqClass;
	int		mReqLv;
};

class WeaponItem : public Item
{
public:
	WeaponItem() : Item()
	{
		mEquipState = false;
		mPhyDamage = 0;
		mMagDamage = 0;
		mCriticalProbability = 0;
		mCriticalDamage = 0;
		mAttackSpeed = 1;
		mReqClass = Enum_AvatarClass::Warrior;
		mReqLv = 1;
	}
	WeaponItem(WeaponConst* inDummy)
		: Item(inDummy->mItemCode, inDummy->mName, inDummy->mSalePrice)
	{
		mEquipState = false;
		mPhyDamage = Global::getInstance().GetRamdom(inDummy->mMinPhyDamage, inDummy->mMaxPhyDamage);
		mMagDamage = Global::getInstance().GetRamdom(inDummy->mMinMagDamage, inDummy->mMaxMagDamage);
		mCriticalProbability = Global::getInstance().GetRamdom(inDummy->mMinCriticalProbability, inDummy->mMaxCriticalProbability);
		mCriticalDamage = Global::getInstance().GetRamdom(inDummy->mMinCriticalDamage, inDummy->mMaxCriticalDamage);
		mAttackSpeed = inDummy->mAttackSpeed;
		mReqClass = inDummy->mReqClass;
		mReqLv = inDummy->mReqLv;
	}
	virtual void Use() {}
	virtual ~WeaponItem(){}

	const bool & GetEquipState() { return mEquipState; }
	const int & GetPhyDamage(){ return mPhyDamage; };
	const int & GetMagDamage(){ return mMagDamage; }
	const float & GetCriticalProbability(){ return mCriticalProbability; }
	const float & GetCriticalDamage(){ return mCriticalDamage; }
	const float & GetAttackSpeed() { return mAttackSpeed; }
	const byte & GetReqClass() { return mReqClass; }
	const int & GetReqLv() { return mReqLv; }

private:
	bool	mEquipState;
	int		mPhyDamage;
	int		mMagDamage;
	float	mCriticalProbability;
	float	mCriticalDamage;
	float	mAttackSpeed;
	byte	mReqClass;
	int		mReqLv;
};


class ConsumeItem : public Item
{
public:
	virtual void Use() {}

	bool mEquipState;
	int mMinDef;
	int mMaxDef;
};
class OtherItem : public Item
{
public:
	virtual void Use() {}
};
class QuestItem : public Item
{
public:
	virtual void Use() {}

	bool mEquipState;
};

class BagManager
{
public:

};

//class ItemFactory : public Singleton<ItemFactory>
//{
//public:
//	enum ENUM_ITEMTYPE
//	{
//		defenseType,
//		WeaponeType,
//		ConsumeType,
//		otherType,
//	};
//	Item * CreateConstItem(int inItemType)
//	{
//		switch (inItemType)
//		{
//		case ENUM_ITEMTYPE::defenseType:
//			return new DefenseItem();
//		case ENUM_ITEMTYPE::ConsumeType:
//			return new ConsumeItem();
//		case ENUM_ITEMTYPE::otherType:
//			return new OtherItem();
//		case ENUM_ITEMTYPE::WeaponeType:
//			return new WeaponItem();
//		default:
//			break;
//		}
//	}
//};

/// 읽기 전용클래스와 유저소유의 가변하는 데이터를 저장하는 것이 필요할듯? ㅇㅈ!
class ItemManager : public Singleton<ItemManager>
{
public:
	ItemManager()
	{
		Init();
	}
	void Init()
	{
		auto ReadItemData = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				int itemID = iter["ItemID"].GetInt();
				int itemtype = iter["ItemType"].GetInt();
				int saleprice = iter["SalePrice"].GetInt();

				string itemName = iter["ItemName"].GetString();
				wstring tmpname;
				tmpname.resize(itemName.size() + 1);
				StrConvA2W(&itemName.front(), &tmpname.front(), itemName.size() + 1);
				switch (itemtype)
				{
				case 1:
				case 2:
				case 3:
				{
					auto ptr = CreateItem<WeaponConst>();
					ptr->mItemType = ENUM_ItemType::Weapontype;
					ptr->mName = tmpname;
					ptr->mItemCode = itemID;
					ptr->mSalePrice = saleprice;
					ptr->mSaleAvailable = true;
					Insert(itemID, ptr);
				}
				break;
				case 4:
				case 5:
				case 6:
				case 7:
				{
					auto ptr = CreateItem<DefenseConst>();
					ptr->mItemType = ENUM_ItemType::DefenseType;
					ptr->mName = tmpname;
					ptr->mItemCode = itemID;
					ptr->mSalePrice = saleprice;
					ptr->mSaleAvailable = true;
					Insert(itemID, ptr);
				}
				case 8:
				{
					auto ptr = CreateItem<ConsumeConst>();
					ptr->mItemType = ENUM_ItemType::ConsumeType;
					ptr->mName = tmpname;
					ptr->mItemCode = itemID;
					ptr->mSalePrice = saleprice;
					ptr->mSaleAvailable = true;
					Insert(itemID, ptr);
				}
				case 9:
				{
					/*auto ptr = CreateItem<OtherItem>();
					Insert(itemID, ptr);
					mConstItemDataContainer[itemID] = ptr;*/
				}
				case 10:
				{
					/*auto ptr = CreateItem<OtherItem>();
					Insert(itemID, ptr);
					mConstItemDataContainer[itemID] = ptr;*/
				}
				}

			}
		};
		auto ReadConstWeapon = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				auto itemid = iter["ItemID"].GetInt();
				auto weapon = reinterpret_cast<WeaponConst*>(mConstItemDataContainer[itemid]);
				weapon->mMinPhyDamage = iter["MinPhyDamage"].GetInt();
				weapon->mMinMagDamage = iter["MinMagDamage"].GetInt();
				weapon->mMinCriticalProbability = iter["MinCriProbability"].GetFloat();
				weapon->mMinCriticalDamage = iter["MinCriDamage"].GetFloat();
				weapon->mAttackSpeed = iter["AttackSpeed"].GetFloat();
				weapon->mMaxPhyDamage = iter["MaxPhyDamage"].GetInt();
				weapon->mMaxMagDamage = iter["MaxMagDamage"].GetInt();
				weapon->mMaxCriticalProbability = iter["MaxCriProbability"].GetFloat();
				weapon->mMaxCriticalDamage = iter["MaxCriDamage"].GetFloat();
			}
		};

		auto ReadWearingItemInfo = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				auto itemid = iter["ItemID"].GetInt();
				switch (mConstItemDataContainer[itemid]->mItemType)
				{
				case ENUM_ItemType::Weapontype:
				{
					auto ptr = reinterpret_cast<WeaponConst*>(mConstItemDataContainer[itemid]);
					ptr->mReqClass = iter["ClassType"].GetInt();
					ptr->mReqLv = iter["ReqLevel"].GetInt();
				}
				break;
				case ENUM_ItemType::DefenseType:
				{
					auto ptr = reinterpret_cast<DefenseConst*>(mConstItemDataContainer[itemid]);
					ptr->mReqClass = iter["ClassType"].GetInt();
					ptr->mReqLv = iter["ReqLevel"].GetInt();
				}
				default:
					break;
				}

			}
		};

		auto ReadDefenseItemInfo = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				auto itemid = iter["ItemID"].GetInt();
				auto ptr = reinterpret_cast<DefenseConst*>(mConstItemDataContainer[itemid]);
				ptr->mMaxPhyDefense = iter["MaxPhyDefense"].GetInt();
				ptr->mMinPhyDefense = iter["MinPhyDefense"].GetInt();
			}

		};

		auto ReadConsumeItemInfo = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				auto itemid = iter["ConsumeItemId"].GetInt();
				auto ptr = reinterpret_cast<ConsumeConst*>(mConstItemDataContainer[itemid]);
				ptr->mEffectCode = iter["EffectCode"].GetInt();
				ptr->mEffectValue = iter["EffectValue"].GetInt();
			}
		};

		auto ReadDropItemInfo = [&](const Value & inValue)
		{
			for (auto & iter : inValue.GetArray())
			{
				DropProbability itemprob;
				ZeroMemory(&itemprob, sizeof(itemprob));
				itemprob.mItemCode = iter["ItemId"].GetInt();
				itemprob.mProbability = iter["Probability"].GetInt();

				auto monsterid = iter["MonsterId"].GetInt();

				mMonsterIDtoItemList[monsterid].push_back(itemprob);
			}
		};
		Document root;
		if (!loadConfig(&root, "ItemData"))
			return;
		auto & data = root["ItemData"];
		ReadItemData(data);

		auto & constWeaponData = root["ConstWeaponData"];
		ReadConstWeapon(constWeaponData);

		auto & wearingItemInfo = root["WearingItemInfo"];
		ReadWearingItemInfo(wearingItemInfo);

		auto & defenseItemData = root["DefenceItemAbility"];
		ReadDefenseItemInfo(defenseItemData);

		auto & consumeItemData = root["ConsumeItemAbility"];
		ReadConsumeItemInfo(consumeItemData);
	}
	virtual ~ItemManager()
	{
		for (auto &iter : mConstItemDataContainer)
		{
			SAFEDELETE(iter.second);
		}
		mConstItemDataContainer.clear();
	}


	/**
	* @brief
	*
	* @param inMonsterID : 몬스터 아이디
	*
	* @return true  : 드랍 OK
	* @return false : 드랍 NO
	*/
	bool GetDropItem(const MonsterID_t & inMonsterID, int & outItemID)
	{
		outItemID = -1;
		int weight = 0;
		int getrandom = Global::getInstance().GetRamdom(0, 100);
		for (auto iter : mMonsterIDtoItemList[inMonsterID])
		{
			getrandom -= iter.mProbability;
			if (getrandom < 1)
			{
				outItemID = iter.mItemCode;
				return true;
			}
		}
		return false;
	}
	bool CreateItem(const int & ItemId, Item *& outItemPtr)
	{
		auto WeaponItemCreate = [](WeaponConst * inDummy)->WeaponItem *
		{
			WeaponItem * ptr = new WeaponItem(inDummy);
			return ptr;
		};
		auto DefenseItemCreate = [](DefenseConst * inDummy)->DefenseItem *
		{
			DefenseItem * ptr = new DefenseItem(inDummy);
			return ptr;
		};
		/// TODO : Queset Consuem , OtherItem 작업해야한다.
		auto QuestItemCreate = []()
		{

		};
		auto ConsumeItemCreate = []()
		{

		};
		auto OthersItemCreate = []()
		{

		};

		switch (mConstItemDataContainer[ItemId]->mItemCode)
		{
		case ENUM_ItemType::Weapontype:
			
			outItemPtr = WeaponItemCreate(reinterpret_cast<WeaponConst*>(mConstItemDataContainer[ItemId]));
			break;
		case ENUM_ItemType::DefenseType:
			outItemPtr = DefenseItemCreate(reinterpret_cast<DefenseConst*>(mConstItemDataContainer[ItemId]));
			break;

		/*case ENUM_ItemType::OtherType:
			auto dummyData = reinterpret_cast<Otherconst*>(mConstItemDataContainer[ItemId]);
			OthersItemCreate();
			break;
		case ENUM_ItemType::QuestType:
			auto dummyData = reinterpret_cast<*>(mConstItemDataContainer[ItemId]);
			QuestItemCreate();
			break;*/
		default:
			return false;
		}
		return true;
	}
private:
	void Insert(int inKey, ItemConst * inValue)
	{
		mConstItemDataContainer[inKey] = inValue;

	}
	template<class T>
	T * CreateItem()
	{
		return new T();
	}
	unordered_map<int, ItemConst *> mConstItemDataContainer;
	unordered_map<MonsterID_t, list<DropProbability>> mMonsterIDtoItemList;
};
static ItemManager ins;
#endif // !DropItemON
