#pragma once

class DBTaskManager : public Singleton<DBTaskManager>
{
public:
	DBTaskManager();

	virtual ~DBTaskManager();

	virtual void Initialize();

	void Execute();

	void Push(DBEvent * inValue);
private:
	tbb::concurrent_queue<DBEvent *> mDBTaskQueue;
	Database						mDatabase;
};