#pragma once

#include <spin_rw_mutex.h>
class ClientProxy
{
public:
	ClientProxy();

	void Clear();

	const bool & IsConnected() { return mConnectionState; }

	void SetConnect(bool inValue) { mConnectionState = inValue; }

	void SetDBStoredUID(const int & inValue) { mDBStoredUserID = inValue; }

	const int & GetStoredUID() { return mDBStoredUserID; }

	void WriteProfile(sc_packet_Avatar_list & inPacket);

	void InsertProfile(int inAvatarID, DB_AvatarList_Profile && moveData);

	void ClearProfile();

	Avatar * GetSelectedAvatar() { return &mAvatar; }

	void SetAvatarInitialize( int avatarID,const wstring & inAvatarName, const int & inAvatarLv, const int & inAvatarGold, 
		const int64_t & inAvatarCurExp, const BYTE & inClassType, const int & inStr, const int & inDex, const int & inInts,
		const int & inLuck, const int & inMaxHp,const int & inMaxMp, const int & inCurrHp, const int & inCurrMp, 
		const int & inAvatarField,const int & inPointX, const int & inPointY, const int64_t & inNeedExp);

	const int GetSectorOnAvatar();

	const byte & GetAvatarField();

	void LevelUp(const int64_t & inGettingExp, const uint64_t & inSessionID);

	void SetNowExp(const int & inGettingExp, const uint64_t & inSessionID);

	const size_t CountToViewList(const int & inValue);

	void EraseToViewList(const int & inValue);

	void InsertToViewList(const int & inValue);

	void SetRespawn(const int & inClientProxyIndex);


	mutable tbb::spin_rw_mutex mtx;
private:
	unordered_map<int, DB_AvatarList_Profile> mAvatarListProfiles;
	int mDBStoredUserID;
	bool mConnectionState;
	Avatar mAvatar;
};