#pragma once
typedef enum
{
	open,
	close,
	None,
}NodeState;

class PathNode
{
public:

	PathNode() 
	{
		mParentPtr = nullptr;
		g = 0;
		f = 0;
		h = 0;
		mPointX = 0;
		mPointY = 0;
		state = NodeState::None;
		mBlock = false;
	}
	PathNode(const PathNode& other)
	{
		mParentPtr = other.mParentPtr;
		g = other.g;
		f = other.f;
		h = other.h;
		mPointX = other.mPointX;
		mPointY = other.mPointY;
		state = other.state;
	}
	PathNode& operator=(PathNode& other)
	{
		mParentPtr= other.mParentPtr;
		g = other.g;
		f = other.f;
		h = other.h;
		mPointX = other.mPointX;
		mPointY = other.mPointY;
		state = other.state;
		return *this;
	}
	void SetG(const int & inValue) { g = inValue; }
	void SetF() { 
		f= g + h;
	}
	void SetH(const int & inValue) { h = inValue; }
	void SetPointX(const int & inValue) { mPointX = inValue; }
	void SetPointY(const int & inValue) { mPointY = inValue; }
	void SetParentPtr(PathNode * inValuePtr) { mParentPtr = inValuePtr; }
	const int & GetG() { return g; }
	const int & GetF() { return f; }
	const int & GetH() { return h; }
	const int & GetPointX() { return mPointX; }
	const int & GetPointY() { return mPointY; }
	PathNode * GetParentPtr(){ return mParentPtr; }
	bool IsBlock() { return mBlock; }
	NodeState & GetState() { return state; }
	void SetState(NodeState inValue) { state = inValue; }
	void SetBlock(bool inValue) { mBlock = inValue; }
private:
	PathNode * mParentPtr;
	int g;
	int f;
	int h;
	int mPointX;
	int mPointY;
	bool mBlock;
	NodeState state;
};