#include "stdafx.h"

void Database::Initialize()
{
	setlocale(LC_ALL, "korean");

	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
	{
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
		{
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
			{
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);
			}
		}
	}
}

void Database::Connect()
{
	// Connect to data source  
	retcode = SQLConnect(hdbc, (SQLWCHAR*)L"PPMMO", SQL_NTS, (SQLWCHAR*)L"sa", SQL_NTS, (SQLWCHAR*)L"root1234", SQL_NTS);

	// Allocate statement handle  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {

	}
}

void Database::DisConnect()
{
	// if (hstmt)SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	if (hdbc)SQLDisconnect(hdbc);
	if (hdbc)SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
	if (henv)SQLFreeHandle(SQL_HANDLE_ENV, henv);
}

void Database::AllocStmt(SQLHSTMT & hstmt)
{
	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
}

void Database::FreeStmt(SQLHSTMT & hstmt)
{
	SQLFreeStmt(hstmt, SQL_UNBIND);
}
