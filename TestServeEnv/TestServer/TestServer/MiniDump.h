#pragma once
#include<DbgHelp.h>

typedef BOOL(WINAPI *MINIDUMPWRITEDUMP)( // Callback 함수의원형  
	HANDLE hProcess,
	DWORD dwPid,
	HANDLE hFile,
	MINIDUMP_TYPE DumpType,
	CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
	CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
	CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

class MiniDump
{
public:
	MiniDump();
	~MiniDump();
};
