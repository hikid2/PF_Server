ai_state_sleep = 0
ai_state_wakeup = 1
ai_state_roaming = 2
ai_state_trace = 3
ai_state_attack = 4
ai_state_dead = 5

myid = -1
spawn_x = 0
spawn_y = 0
my_state = ai_state_sleep
myobject = 0
target = -1

OP_EventTimer_TRACING = 16714
OP_EventTimer_ROAMING = 16715
OP_EventTimer_ATTACK = 16716
OP_EventTimer_SLEEP = 16717
OP_EventTimer_DEAD = 16718
OP_EventTimer_Respawn = 16719

--  시작 스테이지 몬스터 Data
M_Mushroom = {
    MID = 1,
    ImageID = 4,
    name = "mushroom",
    level = 1,
    MinDamage = 3,
    MaxDamage = 4,
    HP = 40,
    SpawnWorld = 1,
    SpawnBit = 2,
    MonsterType = 1,
    DropExp = 5,
    AttackSpeed = 2,
    MinDropGold = 2,
    MaxDropGold = 7
}
M_GreatMushroom = {
    MID = 2,
    ImageID = 4,
    name = "Greatmushroom",
    level = 3,
    MinDamage = 4,
    MaxDamage = 6,
    HP = 40,
    SpawnWorld = 1,
    SpawnBit = 2,
    MonsterType = 1,
    DropExp = 8,
    AttackSpeed = 2,
    MinDropGold = 7,
    MaxDropGold = 12
}

M_Greenslime = {
    MID = 3,
    ImageID = 5,
    name = "GreenSlime",
    level = 7,
    MinDamage = 12,
    MaxDamage = 16,
    HP = 100,
    SpawnWorld = 1,
    SpawnBit = 4,
    MonsterType = 1,
    DropExp = 16,
    AttackSpeed = 2,
    MinDropGold = 15,
    MaxDropGold = 20
}
M_GreatGreenslime = {
    MID = 4,
    ImageID = 5,
    name = "GreatGreenslime",
    level = 10,
    MinDamage = 16,
    MaxDamage = 21,
    HP = 100,
    SpawnWorld = 1,
    SpawnBit = 4,
    MonsterType = 1,
    DropExp = 20,
    AttackSpeed = 1.7,
    MinDropGold = 20,
    MaxDropGold = 25
}

M_Orangeslime = {
    MID = 5,
    ImageID = 6,
    name = "Orangeslime",
    level = 14,
    MinDamage = 22,
    MaxDamage = 26,
    HP = 200,
    SpawnWorld = 1,
    SpawnBit = 8,
    MonsterType = 2,
    DropExp = 42,
    AttackSpeed = 2,
    MinDropGold = 45,
    MaxDropGold = 50
}
M_GreatOrangeslime = {
    MID = 6,
    ImageID = 6,
    name = "GreatOrangeslime",
    level = 17,
    MinDamage = 27,
    MaxDamage = 37,
    HP = 200,
    SpawnWorld = 1,
    SpawnBit = 8,
    MonsterType = 2,
    DropExp = 55,
    AttackSpeed = 1.7,
    MinDropGold = 50,
    MaxDropGold = 55
}

M_Skyslime = {
    MID = 7,
    ImageID = 7,
    name = "Skyslime",
    level = 20,
    MinDamage = 40,
    MaxDamage = 56,
    HP = 400,
    SpawnWorld = 1,
    SpawnBit = 16,
    MonsterType = 2,
    DropExp = 82,
    AttackSpeed = 2,
    MinDropGold = 80,
    MaxDropGold = 85
}
M_GreatSkyslime = {
    MID = 8,
    ImageID = 7,
    name = "GreatSkyslime",
    level = 22,
    MinDamage = 65,
    MaxDamage = 91,
    HP = 400,
    SpawnWorld = 1,
    SpawnBit = 16,
    MonsterType = 2,
    DropExp = 120,
    AttackSpeed = 1.7,
    MinDropGold = 95,
    MaxDropGold = 100
}

-- 사막 스테이지 몬스터 Data
M_Redslime = {
    MID = 9,
    ImageID = 8,
    name = "Redslime",
    level = 27,
    MinDamage = 110,
    MaxDamage = 154,
    HP = 700,
    SpawnWorld = 2,
    SpawnBit = 2,
    MonsterType = 2,
    DropExp = 130,
    AttackSpeed = 1.8,
    MinDropGold = 150,
    MaxDropGold = 155
}
M_GreatRedslime = {
    MID = 10,
    ImageID = 8,
    name = "GreatRedslime",
    level = 29,
    MinDamage = 150,
    MaxDamage = 210,
    HP = 700,
    SpawnWorld = 2,
    SpawnBit = 2,
    MonsterType = 2,
    DropExp = 220,
    AttackSpeed = 1.5,
    MinDropGold = 175,
    MaxDropGold = 180
}

M_Skeleton = {
    MID = 11,
    ImageID = 9,
    name = "M_Skeleton",
    level = 33,
    MinDamage = 220,
    MaxDamage = 308,
    HP = 1000,
    SpawnWorld = 2,
    SpawnBit = 4,
    MonsterType = 2,
    DropExp = 350,
    AttackSpeed = 1.8,
    MinDropGold = 220,
    MaxDropGold = 225
}
M_GreatSkeleton = {
    MID = 12,
    ImageID = 9,
    name = "M_GreatSkeleton",
    level = 37,
    MinDamage = 250,
    MaxDamage = 350,
    HP = 1000,
    SpawnWorld = 2,
    SpawnBit = 8,
    MonsterType = 2,
    DropExp = 420,
    AttackSpeed = 1.5,
    MinDropGold = 260,
    MaxDropGold = 265
}

M_Goblin = {
    MID = 13,
    ImageID = 10,
    name = "M_Goblin",
    level = 43,
    MinDamage = 400,
    MaxDamage = 560,
    HP = 1300,
    SpawnWorld = 2,
    SpawnBit = 16,
    MonsterType = 2,
    DropExp = 790,
    AttackSpeed = 1.7,
    MinDropGold = 420,
    MaxDropGold = 425
}
M_GreatGoblin = {
    MID = 14,
    ImageID = 10,
    name = "M_GreatGoblin",
    level = 47,
    MinDamage = 560,
    MaxDamage = 784,
    HP = 1300,
    SpawnWorld = 2,
    SpawnBit = 16,
    MonsterType = 2,
    DropExp = 900,
    AttackSpeed = 1.4,
    MinDropGold = 500,
    MaxDropGold = 505
}

-- 던전 스테이지 몬스터 Data

M_Golem = {
    MID = 15,
    ImageID = 11,
    name = "Golem",
    level = 54,
    MinDamage = 900,
    MaxDamage = 1400,
    HP = 5000,
    SpawnWorld = 3,
    SpawnBit = 2,
    MonsterType = 2,
    DropExp = 1200,
    AttackSpeed = 2,
    MinDropGold = 1000,
    MaxDropGold = 1005
} -- 골램				2	2	54		2
M_GreatGolem = {
    MID = 16,
    ImageID = 11,
    name = "GreatGolem",
    level = 59,
    MinDamage = 1200,
    MaxDamage = 1800,
    HP = 10000,
    SpawnWorld = 3,
    SpawnBit = 2,
    MonsterType = 2,
    DropExp = 4000,
    AttackSpeed = 2,
    MinDropGold = 1200,
    MaxDropGold = 1205
} -- 강한 골램				2	2	59		2

M_Bat = {
    MID = 17,
    ImageID = 12,
    name = "Bat",
    level = 62,
    MinDamage = 1100,
    MaxDamage = 1400,
    HP = 12000,
    SpawnWorld = 3,
    SpawnBit = 4,
    MonsterType = 2,
    DropExp = 12000,
    AttackSpeed = 1.4,
    MinDropGold = 3000,
    MaxDropGold = 4000
} -- 박쥐괴물				3	2	62		1.4

M_GreatBat = {
    MID = 18,
    ImageID = 12,
    name = "GreatBat",
    level = 65,
    MinDamage = 1200,
    MaxDamage = 1800,
    HP = 12000,
    SpawnWorld = 3,
    SpawnBit = 8,
    MonsterType = 2,
    DropExp = 22000,
    AttackSpeed = 1.2,
    MinDropGold = 7000,
    MaxDropGold = 7500
} --강한 박쥐괴물				3	2	65		1.2
M_SkeletonHead = {
    MID = 19,
    ImageID = 13,
    name = "SkeletonHead",
    level = 70,
    MinDamage = 2200,
    MaxDamage = 3200,
    HP = 22000,
    SpawnWorld = 3,
    SpawnBit = 8,
    MonsterType = 2,
    DropExp = 70000,
    AttackSpeed = 1.5,
    MinDropGold = 12000,
    MaxDropGold = 14000
} --악마해골				3	2	70		1.5

M_BossHead = {
    MID = 20,
    ImageID = 13,
    name = "BossHead",
    level = 77,
    MinDamage = 2800,
    MaxDamage = 3400,
    HP = 22000,
    SpawnWorld = 3,
    SpawnBit = 16,
    MonsterType = 2,
    DropExp = 100000,
    AttackSpeed = 1,
    MinDropGold = 25000,
    MaxDropGold = 50000
} --강한 악마해골				3	2	77		1.5

myMon = {
    MID = 0,
    ImageID = 0,
    name = "",
    level = 0,
    MinDamage = 0,
    MaxDamage = 0,
    HP = 0,
    SpawnWorld = 0,
    SpawnBit = 0,
    MonsterType = 0,
    DropExp = 0,
    AttackSpeed = 0,
    MinDropGold = 0,
    MaxDropGold = 0
}

message = "hello world"
-- Initialize Monster name, level, exp, hp, att ....
function Get_MonsterDefineData(FieldNum, spawn_bit)
    math.randomseed(os.time())
    num = math.random(0, 2)
    if (FieldNum == 1) then
        if (spawn_bit == 2) then
            if (num == 2) then
                myMon = M_Mushroom
            else
                myMon = M_GreatMushroom
            end
        elseif (spawn_bit == 4) then
            if (num == 2) then
                myMon = M_Greenslime
            else
                myMon = M_GreatGreenslime
            end
        elseif (spawn_bit == 8) then
            if (num == 2) then
                myMon = M_Orangeslime
            else
                myMon = M_GreatOrangeslime
            end
        elseif (spawn_bit == 16) then
            if (num == 2) then
                myMon = M_Skyslime
            else
                myMon = M_GreatSkyslime
            end
        end
    elseif (FieldNum == 2) then
        if (spawn_bit == 2) then
            if (num == 2) then
                myMon = M_Redslime
            else
                myMon = M_GreatRedslime
            end
        elseif (spawn_bit == 4) then
            myMon = M_Skeleton
        elseif (spawn_bit == 8) then
            myMon = M_GreatSkeleton
        elseif (spawn_bit == 16) then
            if (num == 2) then
                myMon = M_GreatGoblin
            else
                myMon = M_Goblin
            end
        end
    elseif (FieldNum == 3) then
        if (spawn_bit == 2) then
            if (num == 2) then
                myMon = M_Golem
            else
                myMon = M_GreatGolem
            end
        elseif (spawn_bit == 4) then
            myMon = M_Bat
        elseif (spawn_bit == 8) then
            if (num == 2) then
                myMon = M_GreatBat
            else
                myMon = M_SkeletonHead
            end
        elseif (spawn_bit == 16) then
            myMon = M_BossHead
        end
    end
    return myMon.MID, myMon.level, myMon.MinDamage, myMon.MaxDamage, myMon.HP, myMon.MonsterType, myMon.DropExp, myMon.AttackSpeed, myMon.ImageID, myMon.MinDropGold ,myMon.MaxDropGold
end

-- set UID methods
function set_uid(x)
    myid = x
end

-- Set SpawnLocation and Fields?
function set_spawnlocation(x, y)
    spawn_x = x
    spawn_y = y
end

function PrintData()
    --print("id ", myid)
    --print("x ", spawn_x, "y ", spawn_y)
end
function event_EventTimer_Sleep()
    my_state = ai_state_sleep
    target = -1
    API_Active_false(myid)
end

function event_EventTimer_WakeUp(player)
    --print(debug.getinfo(1).currentline, "WakeUp_event Call")
    if (my_state == ai_state_sleep) then
        --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
        my_state = ai_state_wakeup
    else
        --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
        return
    end
    if (API_viewin_range(myid, player) == true) then
        --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
        -- 주변탐색을 했는데 사정거리에 있다.
        if (myMon.MonsterType == 1) then
            -- 1 : 평화주의자
            --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
            API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
        elseif (myMon.MonsterType == 2) then
            -- 2: 폭력주의자
            --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
            if (API_trace_range(myid, player, 4) == true) then
                -- 사정거리 안에 있으면? Trace
                if (target == -1) then
                    -- 논타겟 상황일시 타겟을 설정하고
                    target = player
                    --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
                end
                -- 추적하게 한다
                --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
                API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
            else
                -- 사정거리 밖이면? 로밍
                --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
                API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
            end
        end
    else
        -- 타켓은 사정거리에 없다.
        -- 주변에 날 움직이게 할 무언가가 있는가?
        if (true == API_search_Near_Avatars(myid, myMon.SpawnWorld)) then
            --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
            API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
        else
            --print(debug.getinfo(1).currentline, "event_EventTimer_WakeUp")
            API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
            target = -1
        end
    end
end

function event_roaming_move()
    --print(debug.getinfo(1).currentline, "event_roaming_move")
    retval_dead, killedID = API_Is_Dead(myid)
    if (retval_dead == true) then
        --print(debug.getinfo(1).currentline, "event_roaming_move")
        API_add_EventTimerEvent(OP_EventTimer_DEAD, 0, myid, killedID)
        return
    end
    if (my_state == ai_state_sleep) then
        --print(debug.getinfo(1).currentline, "event_roaming_move")
        -- 자는 상태에서는 여기로 바로 들어오는게 문제있다.
        return
    elseif (my_state == ai_state_attack) then
        --print(debug.getinfo(1).currentline, "event_roaming_move")
        return
    end
    my_state = ai_state_roaming
    -- 이동시키자
    retval, code = API_AI_Move_Roaming(myid)

    if (retval == false) then
        --print(debug.getinfo(1).currentline, "event_roaming_move")
        if (code == 1) then
            --print(debug.getinfo(1).currentline, "event_roaming_move")
            -- 충돌땜에 이동 실패
            if (myMon.MonsterType == 1) then
                --print(debug.getinfo(1).currentline, "event_roaming_move")
                woundedresult, targets = API_Is_Wounded(myid)
                if (woundedresult == true) then
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    -- 1 : 평화주의자 -> 로밍이 가능한가?
                    target = targets
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
                elseif (true == API_search_Near_Avatars(myid, myMon.SpawnWorld)) then
                    -- 가능하면 1번더,
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
                else
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
                    target = -1
                end
            else
                --print(debug.getinfo(1).currentline, "event_roaming_move")
                retval, id = API_Task_Search_Near_Avatar_Get(myid, myMon.SpawnWorld)

                if (retval == true) then
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    if (API_trace_range(myid, id, 4) == true) then
                        if (target == -1) then
                            -- 논타겟 상황일시 타겟을 설정하고
                            --print(debug.getinfo(1).currentline, "event_roaming_move")
                            target = id
                        end
                        --print(debug.getinfo(1).currentline, "event_roaming_move")
                        API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
                    else
                        --print(debug.getinfo(1).currentline, "event_roaming_move")
                        API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
                    end
                else
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
                end
            end
        elseif (code == 2) then
            -- 이동은 성공했지만 주변에 사람이 없다.
            --print(debug.getinfo(1).currentline, "event_roaming_move")
            API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
            target = -1
        end
    else
        -- 이동도 성공, 주변에 사람도 있다.
        if (myMon.MonsterType == 1) then
            --print(debug.getinfo(1).currentline, "event_roaming_move")
            woundedresult, targets = API_Is_Wounded(myid)
            if (woundedresult == true) then
                target = targets
                --print(debug.getinfo(1).currentline, "event_roaming_move")
                API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
            else
                -- 1 : 평화주의자 -> 이동
                --print(debug.getinfo(1).currentline, "event_roaming_move")
                API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
            end
        else
            --print(debug.getinfo(1).currentline, "event_roaming_move")
            retval, id = API_Task_Search_Near_Avatar_Get(myid, myMon.SpawnWorld)
            if (retval == true) then
                --print(debug.getinfo(1).currentline, "event_roaming_move")
                if (API_trace_range(myid, id, 4) == true) then
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    if (target == -1) then
                        --print(debug.getinfo(1).currentline, "event_roaming_move")
                        -- 논타겟 상황일시 타겟을 설정하고
                        target = id
                    end
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
                else
                    --print(debug.getinfo(1).currentline, "event_roaming_move")
                    API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
                end
            else
                --print(debug.getinfo(1).currentline, "event_roaming_move")
            end
        end
    end
end

function event_tracing_move(player)
    --print(debug.getinfo(1).currentline, "event_tracing_move")
    retval_dead, killedID = API_Is_Dead(myid)
    if (retval_dead == true) then
        --print(debug.getinfo(1).currentline, "event_tracing_move")
        API_add_EventTimerEvent(OP_EventTimer_DEAD, 0, myid, killedID)
        return
    end
    if (target == player) then
        --print(debug.getinfo(1).currentline, "event_tracing_move")
        my_state = ai_state_trace
        retcode, target = API_AI_Tracing(myid, target)
        if (retcode == 1) then
            --print(debug.getinfo(1).currentline, "event_tracing_move")
            -- const int nextTrace = 1;

            API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
        elseif (retcode == 2) then
            -- const int nextRoaming = 2;
            --print(debug.getinfo(1).currentline, "event_tracing_move")
            API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
        elseif (retcode == 3) then
            -- const int nextSleep = 3;
            --print(debug.getinfo(1).currentline, "event_tracing_move")
            API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
        elseif (retcode == 4) then
            -- const int nextAttack = 4;
            --print(debug.getinfo(1).currentline, "event_tracing_move")
            API_add_EventTimerEvent(OP_EventTimer_ATTACK, myMon.AttackSpeed * 1000, myid, target)
        end
    else
        --print(debug.getinfo(1).currentline, "event_tracing_move")
        --print("player id", player, "targets : ", target)
    end
end

function event_Attack_Method(inTarget)
    --print(debug.getinfo(1).currentline, "event_Attack_Method")
    retval_dead, killedID = API_Is_Dead(myid)
    if (retval_dead == true) then
        --print(debug.getinfo(1).currentline, "event_Attack_Method")
        API_add_EventTimerEvent(OP_EventTimer_DEAD, 0, myid, killedID)
        return
    end
    if (target == inTarget) then
        --print(debug.getinfo(1).currentline, "event_Attack_Method")
        retcode, target = API_AI_ATTACK_TARGET(myid, target)
        if (retcode == 1) then
            -- const int attack_target = 1;
            --print(debug.getinfo(1).currentline, "event_Attack_Method")
            API_add_EventTimerEvent(OP_EventTimer_ATTACK, myMon.AttackSpeed * 1000, myid, target)
        elseif (retcode == 2) then
            --print(debug.getinfo(1).currentline, "event_Attack_Method")
            -- const int trace_target = 2;
            API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
        elseif (retcode == 3) then
            -- NOt Found Target! Disconnected
            if (myMon.MonsterType == 1) then
                -- 평화주의자면?
                --print(debug.getinfo(1).currentline, "event_Attack_Method")
                if (true == API_search_Near_Avatars(myid, myMon.SpawnWorld)) then
                    -- 1. 주변에 뭐가 있으면 로밍
                    --print(debug.getinfo(1).currentline, "event_Attack_Method")
                    API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
                else
                    -- 2. 없으면  sleep
                    --print(debug.getinfo(1).currentline, "event_Attack_Method")
                    API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
                end
            else
                -- 3. 주변에 뭣도 없다. Sleep
                -- 폭력주의자면?
                --print(debug.getinfo(1).currentline, "event_Attack_Method")
                retval, id = API_Task_Search_Near_Avatar_Get(myid, myMon.SpawnWorld)
                if (retval == true) then
                    --print(debug.getinfo(1).currentline, "event_Attack_Method")
                    if (API_trace_range(myid, id, 4) == true) then
                        --print(debug.getinfo(1).currentline, "event_Attack_Method")
                        if (target == -1) then
                            --print(debug.getinfo(1).currentline, "event_Attack_Method")
                            -- 논타겟 상황일시 타겟을 설정하고
                            target = id
                        end
                        --print(debug.getinfo(1).currentline, "event_Attack_Method")
                        -- 1. 주변에 뭐가 있다 and 그게 내 시야안에 들어온다면 -> Trace
                        API_add_EventTimerEvent(OP_EventTimer_TRACING, 1200, myid, target)
                    else
                        --print(debug.getinfo(1).currentline, "event_Attack_Method")
                        -- 2. 주변에 뭐가 있다 그러나 그게 내 시야안에 안들어온다. -> roaming
                        API_add_EventTimerEvent(OP_EventTimer_ROAMING, 1200, myid, -1)
                    end
                else
                    --print(debug.getinfo(1).currentline, "event_Attack_Method")
                    API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
                end
            end
        end
    end
end

function setTarget(inTarget)
    target = inTarget
end

function event_Dead_Method(player)
    --print(debug.getinfo(1).currentline, "event_Dead_Method")
    API_Give_Exp(player, myMon.DropExp)
    API_Give_Gold(player, myid)

    target = -1

    my_state = ai_state_dead
    API_Dead_Execute(myid, false)
    --print(debug.getinfo(1).currentline, "event_Dead_Method")
    API_add_EventTimerEvent(OP_EventTimer_Respawn, 10000, myid, -1)
end
function event_Respawn_Method()
    API_Task_Respawn(myid, spawn_x, spawn_y)
    --print(debug.getinfo(1).currentline, "event_Respawn_Method")
    API_add_EventTimerEvent(OP_EventTimer_SLEEP, 0, myid, -1)
end
