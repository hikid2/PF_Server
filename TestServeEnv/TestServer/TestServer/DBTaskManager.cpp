#include "stdafx.h"

DBTaskManager::DBTaskManager() { Initialize(); }

DBTaskManager::~DBTaskManager()
{
	DBEvent * ptr = nullptr;
	while (!mDBTaskQueue.empty())
	{
		if (mDBTaskQueue.try_pop(ptr)) {
			delete ptr;
			ptr = nullptr;
		}
	}
}

void DBTaskManager::Initialize()
{
	
	Global::getInstance().mDBTaskThread = new thread(&DBTaskManager::Execute, this);
	std::thread::id this_id = Global::getInstance().mDBTaskThread->get_id();
	Log(L"** DBTaskManager Thread Created [Thread ID : 0x%08x]", this_id);
}

void DBTaskManager::Execute()
{
	mDatabase.Initialize();
	mDatabase.Connect();
	while (!IsShutDonw)
	{
		Sleep(1);
		if (!mDBTaskQueue.empty())
		{
			DBEvent * task = nullptr;
			if (!mDBTaskQueue.try_pop(task)) continue;
			mDatabase.AllocStmt(task->GetQuery().getstmt());
			int resultCode = -1;
			OverlappedDB_t * over = nullptr;
			if (task->IsNextDispose())
			{
				if (RecycleOverlappedManager::getInstance().PopDBOverlap(over))
				{
					if (task->Execute(over))
					{
						over->overlapEx.operation= OP_DB;
						over->target = task->GetUniqueKey();
						PostQueuedCompletionStatus(IOCPServer::getInstance().GetIOCPHandle(), 1, over->target, &(over->overlapEx.original_overlap));
					}
					else
					{
						RecycleOverlappedManager::getInstance().PushDBOverlap(over);
					}
				}
			}
			else
			{
				OverlappedDB_t * over = nullptr;
				task->Execute(over);
			}
			mDatabase.FreeStmt(task->GetQuery().getstmt());
			delete task;
		}
	}
	mDatabase.DisConnect();
}

void DBTaskManager::Push(DBEvent * inValue)
{
	mDBTaskQueue.push(inValue);
}
