#pragma once
#define SECTORX 20
#define SECTORY 20
#undef	SAFE_DELETE
#define SAFEDELETE(obj)						\
{												\
	if ((obj)) delete(obj);		    			\
    (obj) = 0L;									\
}

inline int GetSectorNo(int x, int y)
{
	return ((y / 10) * 20) + (x / 10);
}
inline int GetSectorIndex_X(int inSectorNo)
{
	return inSectorNo % 20;
}
inline int GetSectorIndex_Y(int inSectorNo)
{
	return inSectorNo / 20;
}
//-------------------------------------------------------------------//
//문자열 변환   멀티바이트  -> 유니코드  or 유니코드 ->  멀티바이트
inline void StrConvA2T(CHAR *src, TCHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
#endif
}

inline void StrConvT2A(TCHAR *src, CHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
#endif
}

inline void StrConvA2W(CHAR *src, WCHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
}
inline void StrConvW2A(WCHAR *src, CHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
}




inline bool loadConfig(Document * inRoot, const char * inFileName)
{
	string tempName = ".\\";
	tempName.append(inFileName);
	tempName.append(".json");
	ifstream ifs(tempName);
	IStreamWrapper isw(ifs);

	inRoot->ParseStream(isw);
	ifs.close();

	return true;
}

inline bool loadConfig(Document * root)
{
	//string readBuffer;
	//if (!ReadFromFile(".\\config.json", readBuffer)) {
	//	return false;
	//}
	//JsonReader_t reader;
	//if (!reader.parse(readBuffer, *root)) {
	//	printf("!! Json Parse Err. !!\n");
	//	return false;
	//}
	return true;
}

enum ENUM_FIELDINDEX
{
	StartTown = 1,
	Desert,
	Dungeon,
};

enum ENUM_FieldDataBit
{
	Collision_Bit = (1 << 0),
	Spawn_fir_Bit = (1 << 1),
	Spawn_sec_Bit = (1 << 2),
	Spawn_thr_Bit = (1 << 3),
	Spawn_for_Bit = (1 << 4),
};

enum ENUM_EventTimer_TASK_OPERATION
{
	OP_EventTimer_WAKEUP = 'AI',
	OP_EventTimer_TRACING,
	OP_EventTimer_ROAMING,
	OP_EventTimer_ATTACK,
	OP_EventTimer_SLEEP,
	OP_EventTimer_DEAD,
	OP_EventTimer_Respawn,

	OP_EventTimer_Respawn_Avatar,
	OP_EventTimer_Heal,
};

enum Enum_Operation
{
	OP_Send,
	OP_Recv,
	OP_DB,
	OP_EventTask,
};

struct Events
{
	int object_id;
	int target_id;
	uint64_t wakeup_time;
	int event_id;
	int operation;
};
extern void SetupModule();
extern bool IsShutDonw;


inline void error_display(const char *msg, int err_no)
{
	WCHAR *lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("%s", msg);
	wprintf(L"에러%s\n", lpMsgBuf);
	LocalFree(lpMsgBuf);
}
constexpr int WorkerThreadCount = 8;
class Global : public Singleton<Global>
{
public:
	void ShutdownServer();
	const int GetRamdom(const int & inMinValue, const int & inMaxValue);
	
	int ServerPort= 9100;
	array<thread *, WorkerThreadCount> workerThread;
	thread * accpetThread;
	thread * mEventTimerThread;
	thread * mDBTaskThread;
};

class Session;
extern void SendMovePlayer(const int & inTargetID, Session * inRecver);

extern void SendRemovePlayer(const int & inTargetID, Session * inRecever);

extern void SendEnterWorld(const int & inTargetID, Session * inRecever);

extern void SendPutPlayer(int inTargetID, Session * inRecever);

extern void SendUpdateHP(const int & inTargetID, Session * inRecever);