#include "stdafx.h"

/**
 * @brief Packet, DB의 작업에 따른 lambda를 컨테이너에 할당
 *
 */
void WorldContents::Initialize()
{
	/**
	 * @brief 로그인 요청을 받았을 경우 DB에 로그인정보를 담아 DBTaskManager에 전달한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_LOGIN_REQ] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_Login_req packet;
		packet.Read(inStream);

		auto events = new DB_Event_Login();
		events->SetId(packet.mId);
		events->SetPW(packet.mPw);
		events->SetUniqueID(inSessionPtr->GetSessionID());
		DBTaskManager::getInstance().Push(events);
	};
	/**
	 * @brief 회원가입 요청을 받았을 경우 DB에 회원가입 정보를 담아 DBTaskManager에 전달한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_JOIN_REQ] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_Joinreq packet;
		packet.Read(inStream);

		auto events = new DB_Event_Join();
		events->Setid(packet.mId);
		events->Setpw(packet.mPw);
		events->Setemail(packet.mEmail);
		events->SetUniqueID(inSessionPtr->GetSessionID());
		DBTaskManager::getInstance().Push(events);
	};

	/**
	 * @brief 아바타생성 요청을 받았을 경우 데이터를 담아 DBTaskManager에 전달한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_CREATE_AVATAR_REQ] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_create_avatar_req packet;
		uint64_t key = inSessionPtr->GetSessionID();
		int uid = ClientManager::getInstance().GetClient(key).GetStoredUID();
		packet.Read(inStream);

		auto events = new DB_Event_CreateAvatar();
		events->SetUserid(uid);
		events->SetAvatarClass(packet.mClassType);
		events->SetAvatarName(packet.mAvatarName);
		events->SetUniqueID(key);
		DBTaskManager::getInstance().Push(events);
	};

	/**
	 * @brief 월드입장 요청을 받았을 경우 월드에 들어가는 아바타의정보를 얻기위한 작업을 DBTaskManager에 요청한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_ENTERWORLD_REQ] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_enterworld_req packet;
		uint64_t key = inSessionPtr->GetSessionID();
		packet.Read(inStream);

		auto events = new DB_Evnet_EnterGame();
		events->SetAvatarId(packet.mAvatarId);
		events->SetUniqueID(key);
		DBTaskManager::getInstance().Push(events);
	};

	/**
	 * @brief 인게임 속 유저의 입력에 대한 작업을 진행한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_INPUT] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_input packet;
		packet.Read(inStream);
		int clientIndex = -1;
		if (!ClientManager::getInstance().InputExecute(packet.key, inSessionPtr, clientIndex))
		{
			return;
		}
		World::getInstance().NextViewUpdate(inSessionPtr, clientIndex);
	};

	/**
	 * @brief 채팅입력시 작업을 진행한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_CHAT_REQ] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_chat_req packet;
		packet.Read(inStream);
		int clientKey = -1;
		if (!ClientManager::getInstance().ConvertSessionIDToClientKey(inSessionPtr->GetSessionID(), clientKey))
			return;
		byte fieldID = ClientManager::getInstance()[clientKey].GetAvatarField();
		wchar_t tmp[100] = {};
		wsprintf(tmp, L"[ %s ] : %s", ClientManager::getInstance()[clientKey].GetSelectedAvatar()->GetName().c_str(), packet.mMessage.c_str());
		sc_packet_broadcast_chat broadcastPacket;
		broadcastPacket.mMessage = tmp;
		unordered_set<int> outList;
		World::getInstance().InsertViewAvatarToList(clientKey, outList);
		for (auto & iter : outList)
		{
			if (iter >= MaxUser)continue;
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
			{
				dumySesion->SendPacket(&broadcastPacket);
			}
		}
		inSessionPtr->SendPacket(&broadcastPacket);
	};

	/**
	 * @brief Hotspot을 방지하기 위해 시작시 아바타의 위치를 랜덤 워프 한다.
	 *
	 */
	mContentsKit[SC_PACKET::SC_enterworld_no_hot_spot] = [](ReadStream & inStream, Session * inSessionPtr)
	{
		cs_packet_enterworld_no_hot_spot packet;
		packet.Read(inStream);
		auto sessionID = inSessionPtr->GetSessionID();
		int tmpClientKey = -1;
		if (!ClientManager::getInstance().ConvertSessionIDToClientKey(sessionID, tmpClientKey)) {
			Log(L" [FNAME : %s || line = %d ] ** Error : Not found Client Proxy Key ", __FUNCTIONW__, __LINE__);
			return;
		}

		auto events = new DB_Evnet_EnterGame_with_NoHotspot();
		events->SetUniqueID(sessionID);
		events->SetAvatarId(packet.mAvatarId);
		events->SetWarpx(packet.mX);
		events->SetWarpy(packet.mY);
		events->SetFieldID(packet.mWarpField);
		DBTaskManager::getInstance().Push(events);
	};
	/**
	 * @brief 회원가입 성공 패킷을 전송
	 *
	 */
	mDBContentsKit[DB_JoinSucc] = [](const uint64_t& inSessionID, char * inReadBuffer)
	{
		sc_packet_join_succ packet;
		Session * sessionPtr = nullptr;
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 회원가입 실패 패킷을 전송
	 *
	 */
	mDBContentsKit[DB_JoinFail] = [](const uint64_t& inSessionID, char * inReadBuffer)
	{
		sc_packet_join_fail packet;
		Session * sessionPtr = nullptr;

		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 로그인 성공 패킷을 전송, 유저의 아바타 리스트조회를 위해 DB에 요청
	 *
	 */
	mDBContentsKit[DB_LoginSucc] = [](const uint64_t & inSessionID, char * inReadBuffer)
	{
		int uid = -1;
		memcpy_s(&uid, sizeof(uid), inReadBuffer, sizeof(uid));
		sc_packet_Login_succ packet;
		Session * sessionPtr = nullptr;
		ClientManager::getInstance().InsertTable(inSessionID, uid);
		int clientKey = -1;
		if (!ClientManager::getInstance().ConvertSessionIDToClientKey(inSessionID, clientKey))
			Log(L" ** mDBContentsKit[DB_LoginSucc]-> Convert ClientKey Error!!");
		// ClientManager::getInstance()[clientKey].SetConnect(true);
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
		auto events = new DB_Event_ShowAvatar();
		events->SetUid(uid);
		events->SetUniqueID(inSessionID);
		DBTaskManager::getInstance().Push(events);
	};

	/**
	 * @brief 로그인 실패 패킷을 전송
	 *
	 */
	mDBContentsKit[DB_LoginFail] = [](const uint64_t & inSessionID, char * inReadBuffer)
	{
		sc_packet_login_fail packet;
		Session * sessionPtr = nullptr;
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 아바타 리스트 데이터를 패킷으로 보냄
	 *
	 */
	mDBContentsKit[DB_ShowAvatarLists] = [](const uint64_t & inSessionID, char * inReadBuffer) {
		sc_packet_Avatar_list packet;
		Session * sessionPtr = nullptr;
		ClientManager::getInstance().GetClient(inSessionID).WriteProfile(packet);
		packet.listCnt = (int)packet.avatarListData.size();
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 아바타 생성 완료 패킷을 전송, 갱신된 아바타 리스트를 DB에 요청
	 *
	 */
	mDBContentsKit[DB_CreateAvatarSucc] = [](const uint64_t & inSessionID, char * inReadBuffer) {
		sc_packet_create_avatar_res packet;
		Session * sessionPtr = nullptr;
		packet.mResult = 1;
		packet.mMessage = L"아바타 생성 완료";
		int uid = ClientManager::getInstance().GetClient(inSessionID).GetStoredUID();
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
		auto events = new DB_Event_ShowAvatar();
		events->SetUid(uid);
		events->SetUniqueID(inSessionID);
		DBTaskManager::getInstance().Push(events);
	};

	/**
	 * @brief 아바타 생성 실패 패킷을 전송(1개의 아이디 당 아바타는 9개가 최대)
	 *
	 */
	mDBContentsKit[DB_CreateAvatarFail_FULLAVATARINDEX] = [](const uint64_t & inSessionID, char * inReadBuffer) {
		sc_packet_create_avatar_res packet;
		Session * sessionPtr = nullptr;
		packet.mResult = -100;
		packet.mMessage = L"아바타 갯수 초과[최대 9개]";
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 아바타 생성 실패 패킷 전송 (닉네임 중복 패킷)
	 *
	 */
	mDBContentsKit[DB_CreateAvatarFail_SAMEAVATARNAME] = [](const uint64_t & inSessionID, char * inReadBuffer) {
		sc_packet_create_avatar_res packet;
		Session * sessionPtr = nullptr;
		packet.mResult = -101;
		packet.mMessage = L"중복된 이름이 존재합니다.";
		if (SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
		{
			sessionPtr->SendPacket(&packet);
		}
	};

	/**
	 * @brief 월드입장 성공 시 유저에게 아바타의 전체 정보를 전달하는 패킷을 전송, 시야리스트 업데이트 진행
	 *
	 */
	mDBContentsKit[DB_EnterWorld_succ] = [](const uint64_t & inSessionID, char * inReadBuffer)
	{
		Session * sessionPtr = nullptr;
		if (!SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
			return;

		int clientKey = -1;
		if (!ClientManager::getInstance().ConvertSessionIDToClientKey(inSessionID, clientKey)) {
			Log(L"  **  DB_EnterWorld_succ -> ConvertSessionIDToClientKey Error !! **");
			// TODO : sessionID를 이용하여 clientKey를 찾을 수 없을 경우 연결을 끊어버리게 하자.
			return;
		}
		/// Log(L"  **  DB_EnterWorld_succ [sessionID : %d] [ proxyID : %d] !! **", inSessionID, clientKey);
		SendEnterWorld(clientKey, sessionPtr);
		ClientManager::getInstance()[clientKey].SetConnect(true);

		int sectorNo = ClientManager::getInstance()[clientKey].GetSectorOnAvatar();

		World::getInstance().InsertIDtoSector(clientKey, sectorNo);


		unordered_set<int> nearList;
		World::getInstance().InsertViewAllObjectToList(clientKey, nearList);

		for (auto & iter : nearList)
		{
			ClientManager::getInstance()[clientKey].InsertToViewList(iter);
			SendPutPlayer(iter, sessionPtr);
			if (NPCManager::getInstance().IsNPC(iter))
			{
				bool nonActive = false;
				if (!NPCManager::getInstance()[iter].isActive.compare_exchange_strong(nonActive, true))
					continue;
				EventTimer::getInstance().Push(iter, clientKey, 10, ENUM_EventTimer_TASK_OPERATION::OP_EventTimer_WAKEUP, OP_EventTask);
			}
			else if (ClientManager::getInstance()[iter].CountToViewList(clientKey) == 0)
			{
				/// 상대방이 날 안갖고 있으면?
				ClientManager::getInstance()[iter].InsertToViewList(clientKey);
				Session * dumySesion = nullptr;
				if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
				{
					//// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
					SendPutPlayer(clientKey, dumySesion);
				}
			}
		}
	};
	/**
	 * @brief 레벨업후 변경된 사항을 유저에게 전송
	 *
	 */
	mDBContentsKit[DB_LevelUp_succ] = [](const uint64_t & inSessionID, char * inReadBuffer)
	{
		int offset = 0;
		int level = 0;
		int nowExp = 0;
		int needExp = 0;
		int nowHP = 0;
		int nowMP = 0;
		int maxHP = 0;
		int maxMP = 0;
		memcpy_s(&level, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&nowExp, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&needExp, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&nowHP, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&nowMP, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&maxHP, sizeof(int), inReadBuffer + offset, sizeof(int));
		offset += sizeof(int);
		memcpy_s(&maxMP, sizeof(int), inReadBuffer + offset, sizeof(int));



		Session * sessionPtr = nullptr;
		if (!SessionManager::getInstance().GetSession(inSessionID, sessionPtr))
			return;



		int clientKey = -1;
		if (!ClientManager::getInstance().ConvertSessionIDToClientKey(inSessionID, clientKey)) {
			Log(L"  **  DB_EnterWorld_succ -> ConvertSessionIDToClientKey Error !! **");
			// TODO : sessionID를 이용하여 clientKey를 찾을 수 없을 경우 연결을 끊어버리게 하자.
			return;
		}

		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetCurrHP(nowHP);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetCurExp(nowExp);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetCurrMP(nowMP);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetLv(level);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetMaxHP(maxHP);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetMaxMP(maxMP);
		ClientManager::getInstance()[clientKey].GetSelectedAvatar()->SetNeedExp(needExp);
		sc_packet_notify_Levelup packet;
		packet.mLv = level;
		packet.mNowHP = nowHP;
		packet.mMaxHP = maxHP;
		packet.mMaxMP = maxMP;
		packet.mNeedExp = needExp;
		packet.mNowExp = nowExp;
		packet.mNowMP = nowMP;
		sessionPtr->SendPacket(&packet);
	};
}

/**
 * @brief Packet에 관련된 작업을 진행
 *
 * @param inContentsIndex : 컨텐츠 인덱스
 * @param inStream : 받은 데이터
 * @param inSessionPtr : 세션의 주소
 */
void WorldContents::Execute(int inContentsIndex, ReadStream & inStream, Session * inSessionPtr)
{
	if (mContentsKit.find(inContentsIndex) != mContentsKit.end()) {
		ContentsExecute run = mContentsKit[inContentsIndex];
		run(inStream, inSessionPtr);
	}
}

/**
 * @brief DB에 관련된 작업을 진행
 *
 * @param inSessionID : 세션의 아이디
 * @param inOverlapEx : 회수용 OverlapPtr
 */
void WorldContents::DB_Execute(const uint64_t & inSessionID, OverlappedDB_t * inOverlapEx)
{
	BYTE dbResult = 0;
	memcpy(&dbResult, inOverlapEx->transBuf, sizeof(dbResult));
	/// auto dbResult = inOverlapEx->result_value;
	auto readBuffer = reinterpret_cast<char *>(inOverlapEx->transBuf + sizeof(byte));
	if (mDBContentsKit.count(dbResult) == 1)
	{
		DBTask run = mDBContentsKit[dbResult];
		run(inSessionID, readBuffer);
	}
	RecycleOverlappedManager::getInstance().PushDBOverlap(inOverlapEx);
}
