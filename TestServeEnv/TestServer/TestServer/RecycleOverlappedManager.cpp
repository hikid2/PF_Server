#include "stdafx.h"

RecycleOverlappedManager::RecycleOverlappedManager() { Initialize(); }

RecycleOverlappedManager::~RecycleOverlappedManager()
{
	Release();
}


void RecycleOverlappedManager::Release()
{
	while (!mFreeDbOverlappedQueue.empty())
	{
		/// DB release
		OverlappedDB_t * valuePtr = nullptr;
		if (mFreeDbOverlappedQueue.try_pop(valuePtr))
		{
			delete valuePtr;
			valuePtr = nullptr;
		}
	}
	// event release
	while (!mFreeEventOverlappedQueue.empty())
	{
		OverlappedEvent_t * valuePtr = nullptr;
		if (mFreeEventOverlappedQueue.try_pop(valuePtr))
		{
			delete valuePtr;
			valuePtr = nullptr;
		}
	}
	// send release
	//while (!mFreeSendOverlappedQueue.empty())
	//{
	//	OverlappedSend_t * valuePtr = nullptr;
	//	if (mFreeSendOverlappedQueue.try_pop(valuePtr))
	//	{
	//		delete valuePtr;
	//		valuePtr = nullptr;
	//	}
	//}
}

//inline void RecycleOverlappedManager::PushSendOverlap(OverlappedSend_t * inValuePtr)
//{
//	memset(inValuePtr, NULL, sizeof(OverlappedSend_t));
//	mFreeSendOverlappedQueue.push(inValuePtr);
//}
//
//bool RecycleOverlappedManager::PopSendOverlap(OverlappedSend_t *& outValuePtr)
//{
//	while (true)
//	{
//		if (mFreeSendOverlappedQueue.try_pop(outValuePtr))
//			return true;
//		else
//			outValuePtr = new OverlappedSend_t();
//			return true;
//	}
//	return false;
//}

inline void RecycleOverlappedManager::PushDBOverlap(OverlappedDB_t * inValuePtr)
{
	memset(inValuePtr, NULL, sizeof(OverlappedDB_t));
	mFreeDbOverlappedQueue.push(inValuePtr);
}

bool RecycleOverlappedManager::PopDBOverlap(OverlappedDB_t *& outValuePtr)
{
	while (true)
	{
		if (mFreeDbOverlappedQueue.try_pop(outValuePtr))
			return true;
		else
		{
			wprintf(L"mFreeDBOverlappedExContainer.try_pop empty!");
			Sleep(1);
			break;
		}
		continue;
	}
	return false;

}

inline void RecycleOverlappedManager::PushEventOverlap(OverlappedEvent_t * inValuePtr)
{
	memset(inValuePtr, NULL, sizeof(OverlappedEvent_t));
	mFreeEventOverlappedQueue.push(inValuePtr);
}

bool RecycleOverlappedManager::PopEventOverlap(OverlappedEvent_t *& outValuePtr)
{
	while (true)
	{
		if (mFreeEventOverlappedQueue.try_pop(outValuePtr))
			return true;
		else
		{
			wprintf(L"mFreeEventOverlappedExContainer.try_pop empty!");
			Sleep(1);
		}
		continue;
	}
	return false;

}

inline void RecycleOverlappedManager::Initialize()
{
	/// new SendOverlapped
	//for (auto index = 0; index < MaxUser * 800; ++index) {
	//	auto tmpPtr = new OverlappedSend_t();
	//	PushSendOverlap(tmpPtr);
	//}
	/// new DBOverlapped
	for (auto index = 0; index < MaxUser; ++index) {
		auto tmpPtr = new OverlappedDB_t();
		PushDBOverlap(tmpPtr);
	}
	/// new EventOverlapped
	for (auto index = 0; index < MaxUser; ++index) {
		auto tmpPtr = new OverlappedEvent_t();
		PushEventOverlap(tmpPtr);
	}

	Log(L" ** RecycleOverlapped Initialize Success");
}

