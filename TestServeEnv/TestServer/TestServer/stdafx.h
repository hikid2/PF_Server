// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#include <stdio.h>
#include <tchar.h>
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "lua53")

extern "C" {
#include "lua.h" 
#include "lauxlib.h" 
#include "lualib.h"
}

#include<thread>
#include<iostream>
#include<mutex>
#include<WinSock2.h>
#include <WS2tcpip.h>
#include<mstcpip.h>
#include<atomic>
#include<array>
#include<string>
#include<unordered_set>
#include<fstream>
#include<queue>
#include<unordered_map>
#include<functional>
#pragma comment(lib,"odbc32.lib")
#pragma comment(lib,"tbb.lib")
#pragma comment(lib,"tbb_debug.lib")

#include <concurrent_queue.h>
#include<concurrent_priority_queue.h>
#include <concurrent_unordered_map.h>
#include <concurrent_unordered_set.h>
#include<concurrent_hash_map.h>
#include<parallel_for.h>
#include<blocked_range.h>

#include <sqlext.h>  
/// #include"json.h"
#include<map>

#define MAX_BUFF_SIZE 1024 * 3
#define MAX_SEND_SIZE 256
/// using namespace Json;

/// typedef Json::Value Document;
/// typedef Json::Reader JsonReader_t;


#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"   // FileReadStream
#include "rapidjson/encodedstream.h"    // EncodedInputStream
#include <rapidjson/istreamwrapper.h>
using namespace rapidjson;

using namespace std;
#define MaxUser 10000
#define NPCStart 10000
#define NPCEnd 16000

#include"Singleton.h"
#include"Global.h"
#include"MemoryPool.h"
#include"Sector.h"
#include"PathNode.h"
#include"Item.h"

//typedef struct PLAYER
//{
//	float x;
//	float y;
//	uint32_t mLevel;
//	wstring name;
//	uint32_t mCurrentHP;
//	uint32_t mCurrentMP;
//	uint32_t mCurrentExp;
//	uint32_t mMaxHP;
//	uint32_t mMaxMP;
//	uint32_t mNextExp;
//	byte	nowField;
//	int visibleDistance;
//	BYTE class_type;
//	int to_DB_define_UID;
//}Player;


//
//struct Overlap_ex {
//	WSAOVERLAPPED original_overlap;
//	int operation;
//	int target;
//	int result_value;
//	WSABUF wsabuf;
//	unsigned char iocp_buffer[MAX_BUFF_SIZE];
//};
#include"Clock.h"
#include"Log.h"


#include"OverlappedExtension.h"
#include"RecycleOverlappedManager.h"
#include"Server.h"

#include"IOCPServer.h"

#include"Stream.h"
#include"MiniDump.h"
class DB_AvatarList_Profile
{
public:
	DB_AvatarList_Profile()
	{
		avatar_id = -1;
		classType = 1;
		name = L"";
		level = 1;
		fieldID = 1;
	}
	DB_AvatarList_Profile(const DB_AvatarList_Profile & other) 
	{
		avatar_id = other.avatar_id;
		classType = other.classType;
		name = other.name;
		level = other.level;
		fieldID = other.fieldID;
	
	}

	DB_AvatarList_Profile(DB_AvatarList_Profile && other)
	{
		*this = move(other);
	}

	DB_AvatarList_Profile & operator=(DB_AvatarList_Profile && other)
	{
		if (this != &other)
		{
			avatar_id	= other.avatar_id;
			classType	= other.classType;
			name		= move(other.name);
			level		= other.level;
			fieldID		= other.fieldID;

			other.avatar_id = -1;
			other.classType = 0;
			other.level = 0;
			other.fieldID = 0;
		}
		return *this;
	}

	void Write(WriteStream & inStream)
	{
		inStream.Write(avatar_id);
		inStream.Write(classType);
		inStream.Write(name);
		inStream.Write(level);
		inStream.Write(fieldID);
	}

	void Read(ReadStream & inStream)
	{
		inStream.Read(&avatar_id);
		inStream.Read(&classType);
		inStream.Read(&name);
		inStream.Read(&level);
		inStream.Read(&fieldID);
	}

	int					avatar_id;
	BYTE				classType;
	wstring				name;
	int					level;
	int					fieldID;
};


#define Test 1

#ifndef Test


typedef struct CLIENT
{
	SOCKET mSocket;
	sockaddr_in Adder;
	bool isConnnected;
	Overlap_ex recv_overlepd;
	Player mPlayer;
	size_t packet_size;
	mutex mtx;
	unordered_set<int> mViewList;
	int previous_size;
	unsigned char packet_buff[MAX_BUFF_SIZE];
	map<int, DB_AvatarList_Profile> mAvatarLists;
}Client;

#endif // !1

#include"Packet.h"
#include"EventTimer.h"
#include"EventTask.h"
#include"Session.h"
#include"PacketManager.h"
#include"SessionManager.h"
#include"Contents.h"
#include"Avatar.h"
#include"ClientProxy.h"
#include"ClientManager.h"
#include"NPC.h"
#include"NPCManager.h"
#include"World.h"
#include"WorldContents.h"
#include"Query.h"
#include"DBEvent.h"
#include"Database.h"
#include"DBTaskManager.h"
enum Enum_AvatarClass : byte
{
	Warrior = 1,
	Wizerd,
	Elf,
};
enum AvatarClass : byte
{
	E_Warrior = 1,
	E_Wizerd,
	E_Elf,

	M_mushroom,
	M_SLIME_Green,
	M_SLIME_Orange,
	M_SLIME_SKY,

	M_SLIME_RED,
	M_LITTLE_SKELETON,
	M_LITTLE_Goblin,
	M_LITTLE_Golem,

	M_BAT,
	M_FIRESKELETONHEAD,
};


// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
