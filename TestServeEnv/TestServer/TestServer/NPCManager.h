#pragma once
class NPCManager : public Singleton<NPCManager>
{
public:
	NPCManager() { Initialize(); }

	virtual ~NPCManager() { 
		for(auto & iter : mNPCContainer)
			lua_close(iter.second.mLuastate);
	}

	void Initialize();

	int SetupLua(int inSpawnPointX, int inSpawnPointY, byte inFieldIIndex, BYTE inSpawnBit);

	NPC & operator[](int index)
	{
		return mNPCContainer[index];
	}

	bool IsNPC(int id);

	bool IsinDistance(int inClientIndex, int inNPCID);

	bool IsinDistance(int inClientIndex, int inNPCID, int inRange);


	void RandomMove(const int & inNPCID, int & outTmpx, int & outTmpy);

	void Hit(const int & inNPCId, const int & inTargetID, const int & inDamage);


private:
	unordered_map<int, NPC> mNPCContainer;
	tbb::concurrent_queue<int> mFreeNPCQueue;
};