#pragma once
class RecycleOverlappedManager : public Singleton<RecycleOverlappedManager>
{
public:
	RecycleOverlappedManager();

	virtual ~RecycleOverlappedManager();

	void Release();

	/// void PushSendOverlap(OverlappedSend_t * inValuePtr);
	
	/// bool PopSendOverlap(OverlappedSend_t *& outValuePtr);

	void PushDBOverlap(OverlappedDB_t * inValuePtr);

	bool PopDBOverlap(OverlappedDB_t *& outValuePtr);

	void PushEventOverlap(OverlappedEvent_t * inValuePtr);

	bool PopEventOverlap(OverlappedEvent_t *& outValuePtr);

private:
	void Initialize();


	/// tbb::concurrent_queue<OverlappedSend_t*> mFreeSendOverlappedQueue;

	tbb::concurrent_queue<OverlappedDB_t*> mFreeDbOverlappedQueue;

	tbb::concurrent_queue<OverlappedEvent_t*> mFreeEventOverlappedQueue;
};