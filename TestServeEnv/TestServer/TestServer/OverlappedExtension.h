#pragma once
typedef struct OverlappedEx
{
	WSAOVERLAPPED original_overlap;
	int operation;
}OverlappedEx_t;

typedef struct OverlappedDB
{
	OverlappedEx_t overlapEx;
	int target;
	unsigned char transBuf[80];
}OverlappedDB_t;

typedef struct SendSturuct : public CMemoryPool<SendSturuct,MaxUser * 300>
{
	OverlappedEx_t overlapEx;
	WSABUF wsabuf;
	unsigned char iocp_buffer[MAX_SEND_SIZE];
}OverlappedSend_t;

typedef struct RecvSturct
{
	OverlappedEx_t overlapEx;
	WSABUF wsabuf;
	unsigned char iocp_buffer[MAX_BUFF_SIZE];
}OverlappedRecv_t;


typedef struct EventStruct
{
	OverlappedEx_t overlapEx;
	int target;
	int result_value;
}OverlappedEvent_t;

// class OverlappedExtensions
//{
//public:
//	OverlappedExtensions();
//	WSAOVERLAPPED original_overlap;
//	int operation;
//	// OverlappedEx_t overlaps;
//	int target;
//	int result_value;
//	WSABUF wsabuf;
//	unsigned char iocp_buffer[MAX_BUFF_SIZE];
//
//	WSAOVERLAPPED & GetOverlapped() { return original_overlap; }
//
//	void SetOperationType(const Enum_Operation inValue) { operation = inValue; }
//
//	const int & GetOperationType() { return operation; }
//
//	void Clear();
//};