#include "stdafx.h"

void SetupModule()
{
	RecycleOverlappedManager::getInstance();
	EventTimer::getInstance();
	DBTaskManager::getInstance();
	NPCManager::getInstance();
	World::getInstance();
	WorldContents::getInstance();
}
bool IsShutDonw = false;
void Global::ShutdownServer()
{
	IsShutDonw = true;
	for(auto i =0; i < WorkerThreadCount; ++i)
		PostQueuedCompletionStatus(IOCPServer::getInstance().GetIOCPHandle(), NULL, 0, nullptr);
	for (auto i = 0; i < WorkerThreadCount; ++i)
		workerThread[i]->join();
	closesocket(IOCPServer::getInstance().GetListenSocket());
	accpetThread->join();
	mEventTimerThread->join();
	mDBTaskThread->join();
	for (auto i = 0; i < WorkerThreadCount; ++i)
		delete workerThread[i];
	delete accpetThread;
	delete mEventTimerThread;
	delete mDBTaskThread;
}

const int Global::GetRamdom(const int & inMinValue, const int & inMaxValue)
{
	random_device rn;

	mt19937_64 rnd(rn());
	uniform_int_distribution<int> range(inMinValue, inMaxValue);
	return range(rnd);
}

void SendMovePlayer(const int & inTargetID, Session * inRecever)
{
	sc_packet_move packet;
	packet.avatarindex = inTargetID;
	if (NPCManager::getInstance().IsNPC(inTargetID))
	{
		packet.mPointx = NPCManager::getInstance()[inTargetID].mPointX;
		packet.mPointy = NPCManager::getInstance()[inTargetID].mPointY;
		packet.mHorizontal = NPCManager::getInstance()[inTargetID].mHorizontal;
		packet.mVertical = NPCManager::getInstance()[inTargetID].mVertical;
	}
	else
	{
		auto avt = ClientManager::getInstance()[inTargetID].GetSelectedAvatar();
		packet.mPointx = avt->GetAvatarPositionX();
		packet.mPointy = avt->GetAvatarPositionY();
		packet.mHorizontal = avt->GetHorizontal();
		packet.mVertical = avt->GetVertical();
	}

	inRecever->SendPacket(&packet);
}

void SendRemovePlayer(const int & inTargetID, Session * inRecever)
{
	sc_packet_remove_player packet;
	packet.id = inTargetID;
	inRecever->SendPacket(&packet);
}

void SendEnterWorld(const int & inTargetID, Session * inRecever)
{
	sc_packet_enterworld_Succ packet;
	auto avt = ClientManager::getInstance()[inTargetID].GetSelectedAvatar();
	wcscpy_s(packet.mName, avt->GetName().c_str());
	packet.mClassType = avt->GetClassType();
	avt->GetAbility(packet.str, packet.dex, packet.ints, packet.luck);


	packet.mCurrExp = avt->GetCurExp();
	packet.mCurrHP = avt->GetCurrHP();
	packet.mCurrMP = avt->GetCurrMP();
	packet.mHasGold = avt->GetGold();
	packet.mLevel = avt->GetLv();
	packet.mMaxHP = avt->GetMaxHP();
	packet.mMaxMP = avt->GetMaxMP();
	packet.mNeedExp = avt->GetNeedExp();
	packet.mNowField = avt->GetNowField();
	packet.mObjectID = inTargetID;
	packet.mX = avt->GetAvatarPositionX();
	packet.mY = avt->GetAvatarPositionY();
	packet.mHorizontal = avt->GetHorizontal();
	packet.mVertial = avt->GetVertical();
	inRecever->SendPacket(&packet);
}

void SendPutPlayer(int inTargetID, Session * inRecever)
{
	sc_packet_put_player packet;
	packet.avatarindex = inTargetID;
	if (!NPCManager::getInstance().IsNPC(inTargetID))
	{
		auto avt = ClientManager::getInstance()[inTargetID].GetSelectedAvatar();
		packet.isNPC = false;
		packet.mName = avt->GetName();
		packet.avatarindex = inTargetID;
		packet.mClasstype = avt->GetClassType();
		packet.mCurrHP = avt->GetCurrHP();
		packet.mMaxHP = avt->GetMaxHP();
		packet.mCurrMP = avt->GetCurrMP();
		packet.mMaxMP = avt->GetMaxMP();
		packet.mPosX = avt->GetAvatarPositionX();
		packet.mPosY = avt->GetAvatarPositionY();
		packet.mLv = avt->GetLv();
		packet.mHorizontal = avt->GetHorizontal();
		packet.mVertial = avt->GetVertical();
		/// Log(L" ** SendPut Packet avatar [target : %d]", targetID);
	}
	else
	{
		packet.isNPC = true;
		packet.mName = NPCManager::getInstance()[inTargetID].mName;
		packet.avatarindex = inTargetID;
		packet.mClasstype = NPCManager::getInstance()[inTargetID].mImageType;
		packet.mCurrHP = NPCManager::getInstance()[inTargetID].GetNowHp();
		packet.mMaxHP = NPCManager::getInstance()[inTargetID].mMaxHP;
		packet.mCurrMP = 0;
		packet.mMaxMP = 0;
		packet.mPosX = NPCManager::getInstance()[inTargetID].mPointX;
		packet.mPosY = NPCManager::getInstance()[inTargetID].mPointY;
		packet.mLv = NPCManager::getInstance()[inTargetID].mLevel;
		packet.mHorizontal = NPCManager::getInstance()[inTargetID].mHorizontal;
		packet.mVertial = NPCManager::getInstance()[inTargetID].mVertical;
		///Log(L" ** SendPut Packet NPC [target : %d]", targetID);
	}

	inRecever->SendPacket(&packet);
}

void SendUpdateHP(const int & inTargetID, Session * inRecever)
{
	auto hp = NPCManager::getInstance()[inTargetID].GetNowHp();
	sc_packet_update_avatarhp packet;
	packet.hp = hp;
	packet.id = inTargetID;

	inRecever->SendPacket(&packet);
}