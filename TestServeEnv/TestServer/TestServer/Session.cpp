#include "stdafx.h"

void Session::SendPacket(Packet * inPacket)
{
	WriteStream stream;
	inPacket->Write(stream);
	stream.WriteSize();
	OverlappedSend_t * overlapEx = new OverlappedSend_t();

	overlapEx->overlapEx.operation = Enum_Operation::OP_Send;
	overlapEx->wsabuf.buf = reinterpret_cast<CHAR *>(overlapEx->iocp_buffer);
	overlapEx->wsabuf.len = stream.GetOffset();
	/// Log(L"Send Packet Size : %d", stream.GetOffset());
	memcpy(overlapEx->iocp_buffer, stream.GetData(), stream.GetOffset());
	int ret = WSASend(mSocket, &overlapEx->wsabuf, 1, NULL, 0,
		&overlapEx->overlapEx.original_overlap, NULL);
	if (0 != ret) {
		int err = WSAGetLastError();
		if (WSAEWOULDBLOCK != err && ERROR_IO_PENDING != err)
		{
			error_display("sendPacket error : ", err);
			delete overlapEx;
			SessionManager::getInstance().CloseSession(this->mSessionID);
		}
		/// while (true);
	}



}

void Session::OnSend(OverlappedSend_t * inOverlappedEx)
{

	// overlappedExtention 재활용 하기
	delete (OverlappedSend_t*)(inOverlappedEx);
	/// RecycleOverlappedManager::getInstance().PushSendOverlap(inOverlappedEx);
}

void Session::OnRecv(DWORD & inRecvSize)
{
	unsigned char *buf_ptr = mRecvOverlapEx.iocp_buffer;
	int remained = inRecvSize;
	while (0 < remained)
	{
		if (0 == mPacketSize)
			memcpy_s(&mPacketSize, sizeof(byte), &buf_ptr[0], sizeof(byte));
		int required = mPacketSize - mPreviousSize;
		if (remained >= required)
		{
			memcpy(packet_buff + mPreviousSize, buf_ptr, required);

			ReadStream stream;
			stream.set((char *)packet_buff + sizeof(byte), mPacketSize);

			PacketAnalyze::getInstance().Analyze(this, stream);

			buf_ptr += required;
			remained -= required;
			mPacketSize = 0;
			mPreviousSize = 0;
		}
		else
		{
			memcpy(packet_buff + mPreviousSize, buf_ptr, remained);
			mPreviousSize += remained;
			remained = 0;
		}
	}
	DWORD flags = 0;
	int retval = WSARecv(mSocket, &mRecvOverlapEx.wsabuf, 1, NULL, &flags,
		&mRecvOverlapEx.overlapEx.original_overlap, NULL);
	if (retval == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		if (err != WSAEWOULDBLOCK && err != ERROR_IO_PENDING)
		{
			error_display("wsaRecvError : ", err);
		}
	}
}

int Session::RecvStandBy()
{
	DWORD flags = 0;
	int ret = WSARecv(mSocket, &mRecvOverlapEx.wsabuf, 1, NULL,
		&flags, &(mRecvOverlapEx.overlapEx.original_overlap), NULL);
	return ret;
}

void Session::CloseSession()
{
	closesocket(mSocket);
}

void Session::Initialize()
{
	ZeroMemory(&mRecvOverlapEx.overlapEx.original_overlap, sizeof(mRecvOverlapEx.overlapEx.original_overlap));
	mRecvOverlapEx.overlapEx.operation = Enum_Operation::OP_Recv;
	mRecvOverlapEx.wsabuf.buf = reinterpret_cast<char *>(mRecvOverlapEx.iocp_buffer);
	mRecvOverlapEx.wsabuf.len = sizeof(mRecvOverlapEx.iocp_buffer);
	mPacketSize = 0;
	mPreviousSize = 0;
}
