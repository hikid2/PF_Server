#pragma once
class BaseServer
{
public:
	BaseServer();
	virtual ~BaseServer();
	virtual bool Run() = 0;
private:
	WSADATA mWsadata;
};