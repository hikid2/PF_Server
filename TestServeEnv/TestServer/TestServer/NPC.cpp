#include "stdafx.h"

void NPC::initialize(const byte & inNPCType, const int32_t & inNPCID, 
	const int32_t & inHP, const int32_t & inMinDamage, 
	const int32_t & inMaxDamage, const int32_t & inTendency, 
	const int32_t & inLevel, const int & DropExp, 
	const float & AttackSpeed, const byte & imageType, 
	const int & inDropMinGold, const int & inDropMaxGold)
{
	mImageType = imageType;
	mNPCType = inNPCType;
	mNPCID = inNPCID;
	mCurrentHP = inHP;
	mMaxHP = inHP;
	mMinDamage = inMinDamage;
	mMaxDamage = inMaxDamage;
	mTendency = inTendency;
	mLevel = inLevel;
	mExp = DropExp;
	mAttackSpeed = AttackSpeed;
	mAlive = true;
	mDropMinGold = inDropMinGold;
	mDropMaxGold = inDropMaxGold;
	mName = npcConverter.Execute(mNPCType);
}

void NPC::SetPointXY(int32_t inPointX, int32_t inPointY) {
	mPointX = inPointX;
	mPointY = inPointY;
}

void NPC::IsWound(bool & retval, int & clientID)
{
	retval = mWound;
	clientID = mWoundedAvatarID;

	mWound = false;
	mWoundedAvatarID = -1;
}

const int32_t NPC::GetDropGold() const
{
	return Global::getInstance().GetRamdom(mDropMinGold, mDropMaxGold);
}

const int32_t NPC::GetObjectDamage() const
{
	return Global::getInstance().GetRamdom(mMinDamage, mMaxDamage);
}

void NPC::AddNowHp(const int32_t & inValue)
{
	while (true)
	{
		int32_t prevHP = mCurrentHP;
		int32_t nextHP = prevHP + inValue;
		if (nextHP < 0)
			nextHP = 0;
		if (CAS(&mCurrentHP, prevHP, nextHP) == true)
			return;
	}
}

void NPC::Initialize_Respawn(const int & inPointx, const int & inPointy)
{
	mPointX = inPointx;
	mPointY = inPointy;

	mCurrentHP = mMaxHP;
	mAlive = true;
}

inline const wchar_t * NPCTypeConverter::Execute(const byte & inNPCType)
{
	switch (inNPCType)
	{
	case 1:
		return L"���� ����";
	case 2:
		return L"���� ���� ����";
	case 3:
		return L"��� ������";
	case 4:
		return L"���� ��� ������";
	case 5:
		return L"��Ȳ ������";
	case 6:
		return L"���� ��Ȳ������";
	case 7:
		return L"�ϴû� ������";
	case 8:
		return L"���� �ϴû� ������";
	case 9:
		return L"���� ������";
	case 10:
		return L"���� ���� ������";
	case 11:
		return L"�ذ�";
	case 12:
		return L"���� �ذ�";
	case 13:
		return L"������";
	case 14:
		return L"���� ������";
	case 15:
		return L"��";
	case 16:
		return L"���� ��";
	case 17:
		return L"���㱫��";
	case 18:
		return L"�� ���� ���㱫��";
	case 19:
		return L"������ �ذ�";
	case 20:
		return L"������ �ذ�";
	}
	return L"";
}
