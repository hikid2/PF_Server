#pragma once

const int MaxUserCount = 8000;

struct SessionClientCompare
{
	static size_t hash(const uint64_t & x)
	{
		size_t h = 0;
		h = x % 50120;
		return h;
	}

	static bool equal(const uint64_t & inLeft, const uint64_t & inRight)
	{
		return inLeft == inRight;
	}
};

struct ClientSessionCompare
{
	static size_t hash(const uint64_t & x)
	{
		size_t h = 0;
		h = x % 5012;
		return h;
	}

	static bool equal(const uint64_t & inLeft, const uint64_t & inRight)
	{
		return inLeft == inRight;
	}
};

////// typedef concurrent_hash_map	///////

typedef tbb::concurrent_hash_map<uint64_t, int, SessionClientCompare> SCMappingTable_t;
typedef tbb::concurrent_hash_map<int, uint64_t, ClientSessionCompare> CSMappingTable_t;


class ClientManager : public Singleton<ClientManager>
{
public:

	ClientManager();
	
	virtual ~ClientManager() {}

	ClientProxy & operator[](int inClientIndex) { return mClientContainer[inClientIndex]; }

	bool InsertTable(const uint64_t & inKey, const int & inUid);

	bool ConvertSessionIDToClientKey(const uint64_t & inSessionID, int & outClientKey);

	void DisConnectClient(const int & inClientKey);

	ClientProxy & GetClient(const uint64_t & inSessionID);

	ClientProxy & GetClient(Session * inSessionPtr);

	const int GetClientIndex(const uint64_t & inSessionID);

	Avatar * GetAvatar(Session * inSessionPtr);

	const bool & IsConnected(const int inNo);

	bool IsinDistance(int inOrigin, int inTarget);

	bool BetweenNPCtoAvatarIsinDistance(int inNPC, int inAvt);

	bool InputExecute(int inKeyData, Session * inSessionPtr, int & outClientIndex);

	uint64_t GetSessionID(int inClientID);

	bool IsSameField(int inFirstID, int inSecondsID);

	/// void BetweenNPCtoAvatarIsInRange(const byte & inFieldID, const int & NPCID, unordered_set<int> & outList);

	/// void BetweenAvatarToAvatarIsInRange(const int & inAvatarID, unordered_set<int> & outList);

	/// void BetweenNPCtoAvatarIsInRangeAndSendHp(const byte & inFieldID, const int & NPCID);

	
private:
	
	SCMappingTable_t mSessionClientMappingTable;
	CSMappingTable_t mClientSessionMappingTable;


	tbb::concurrent_queue<int>				mFreeClientIndex;
	array<ClientProxy, MaxUserCount>		mClientContainer;
};

