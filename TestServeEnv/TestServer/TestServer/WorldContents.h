#pragma once

class WorldContents : public Contents, public Singleton<WorldContents>
{
public:
	WorldContents() { Initialize(); }

	typedef void(*DBTask)(const uint64_t &, char *);

	virtual void Initialize();

	virtual void Execute(int inContentsIndex, ReadStream & inStream, Session * inSessionPtr);

	void DB_Execute(const uint64_t & inSessionID, OverlappedDB_t * inDBResult);
private:
	unordered_map<int, ContentsExecute> mContentsKit;
	unordered_map<int, DBTask> 			mDBContentsKit;
};
