#include "stdafx.h"

ClientProxy::ClientProxy() : mDBStoredUserID(-1)
{
	Clear();
}

inline void ClientProxy::Clear()
{
	SetConnect(false);
	mDBStoredUserID = -1;
	mAvatarListProfiles.clear();
	mAvatar.Clear();
}

void ClientProxy::WriteProfile(sc_packet_Avatar_list & inPacket)
{
	for (auto & iter : mAvatarListProfiles)
	{
		inPacket.avatarListData.push_back(iter.second);
	}
}

void ClientProxy::InsertProfile(int inAvatarID, DB_AvatarList_Profile && moveData)
{
	mAvatarListProfiles.emplace(make_pair(inAvatarID, move(moveData)));
}

void ClientProxy::ClearProfile()
{
	mAvatarListProfiles.clear();
}

void ClientProxy::SetAvatarInitialize(int avatarID, const wstring & inAvatarName, const int & inAvatarLv, const int & inAvatarGold, const int64_t & inAvatarCurExp, const BYTE & inClassType, const int & inStr, const int & inDex, const int & inInts, const int & inLuck, const int & inMaxHp, const int & inMaxMp, const int & inCurrHp, const int & inCurrMp, const int & inAvatarField, const int & inPointX, const int & inPointY, const int64_t & inNeedExp)
{
	mAvatar.SetDBStoredAvatarID(avatarID);
	mAvatar.SetName(inAvatarName);
	mAvatar.SetNeedExp(inNeedExp);
	mAvatar.SetCurExp(inAvatarCurExp);
	mAvatar.SetGold(inAvatarGold);
	mAvatar.SetCurrHP(inCurrHp);
	mAvatar.SetCurrMP(inCurrMp);
	mAvatar.SetMaxHP(inMaxHp);
	mAvatar.SetMaxMP(inMaxMp);
	mAvatar.SetNowField(inAvatarField);
	mAvatar.SetClassType(inClassType);
	mAvatar.SetAbility(inStr, inDex, inInts, inLuck);
	mAvatar.SetAvatarPosition(inPointX, inPointY);
	mAvatar.SetLv(inAvatarLv);
}

const int ClientProxy::GetSectorOnAvatar()
{
	auto x = mAvatar.GetAvatarPositionX();
	auto y = mAvatar.GetAvatarPositionY();
	int sectorNo = GetSectorNo(x, y);
	return sectorNo;
}

const byte & ClientProxy::GetAvatarField() { return mAvatar.GetNowField(); }

void ClientProxy::LevelUp(const int64_t & inGettingExp, const uint64_t & inSessionID)
{
	auto events = new DB_Event_LevelUp();
	events->SetUniqueID(inSessionID);
	events->SetAvatarID(mAvatar.GetDBStoredAvatarID());
	events->SetNowExp(inGettingExp);
	DBTaskManager::getInstance().Push(events);
}

void ClientProxy::SetNowExp(const int & inGettingExp, const uint64_t & inSessionID)
{
	this->mAvatar.SetCurExp(inGettingExp);
	auto events = new DB_Event_UpdateNowExp();
	events->SetUniqueID(inSessionID);
	events->SetAvatarID(mAvatar.GetDBStoredAvatarID());
	events->SetNowExp(inGettingExp);
	DBTaskManager::getInstance().Push(events);
}

const size_t ClientProxy::CountToViewList(const int & inValue)
{
	tbb::spin_rw_mutex::scoped_lock lock(mtx, false);
	auto retval = mAvatar.mViewList.count(inValue);
	return retval;
}

void ClientProxy::EraseToViewList(const int & inValue)
{
	tbb::spin_rw_mutex::scoped_lock lock(mtx, true);
	mAvatar.mViewList.erase(inValue);
}

void ClientProxy::InsertToViewList(const int & inValue)
{
	tbb::spin_rw_mutex::scoped_lock lock(mtx, true);
	mAvatar.mViewList.insert(inValue);
}

void ClientProxy::SetRespawn(const int & inClientProxyIndex)
{
	int prevSectorNO = GetSectorNo(mAvatar.GetAvatarPositionX(), mAvatar.GetAvatarPositionY());
	mAvatar.SetRespawn();

	auto x = mAvatar.GetAvatarPositionX();
	auto y = mAvatar.GetAvatarPositionY();

	auto field = mAvatar.GetNowField();
	auto dbStoredAvatarId = mAvatar.GetDBStoredAvatarID();
	auto sessionID = ClientManager::getInstance().GetSessionID(inClientProxyIndex);

	auto events = new DB_Event_UpdateAvatarLocation();
	events->SetLocalX(x);
	events->SetLocalY(y);
	events->SetFieldID(field);
	events->SetAvatarID(dbStoredAvatarId);
	events->SetUniqueID(sessionID);
	DBTaskManager::getInstance().Push(events);

	// Log(L" ** Respawn Avatar [proxy ID : %d   ]||  [x = %d | y = %d]", inClientProxyIndex, x, y);
	
	int sectorNo = GetSectorNo(x, y);
	/// UpdateSector(sectorNo);
	World::getInstance().UpdateIDSector(inClientProxyIndex, prevSectorNO, sectorNo);
	sc_packet_respawn_avatar packet;
	packet.mNowHP = mAvatar.GetCurrHP();
	packet.mNowMP = mAvatar.GetCurrMP();
	packet.mPointx = x;
	packet.mPointy = y;
	packet.mWarpFieldId = mAvatar.GetNowField();
	Session * dummySessionPtr = nullptr;
	if (SessionManager::getInstance().GetSession(sessionID, dummySessionPtr))
	{
		dummySessionPtr->SendPacket(&packet);
	}

}
