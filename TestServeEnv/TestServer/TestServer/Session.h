#pragma once
class Session
{
public:
	Session() : mSocket(NULL) { Initialize(); }

	~Session() {}

	void SendPacket(Packet * inPacket);

	void OnSend(OverlappedSend_t * inOverlappedEx);

	void OnRecv(DWORD & inRecvSize);

	int RecvStandBy();

	const uint64_t & GetSessionID() { return mSessionID; }

	void SetSessionID(const uint64_t & inSessionID) { mSessionID = inSessionID; }

	void SetSocket(SOCKET & inSocket) { mSocket = inSocket; }

	void SetAddr(const sockaddr_in & inAdder) { mAdder = inAdder; }

	void CloseSession();

	void Initialize();
private:
	OverlappedRecv_t mRecvOverlapEx;
	uint64_t mSessionID;
	SOCKET	mSocket;
	sockaddr_in mAdder;
	uint32_t mPacketSize;
	int mPreviousSize;
	unsigned char packet_buff[MAX_BUFF_SIZE];
};
