#pragma once
#include <chrono>
#include <ctime>
using namespace std::chrono;

#define CLOCK					Clock::getInstance()
#define NOW_TICK				CLOCK.systemTick
#define NOW_TICKTP				CLOCK.systemTickTP
#define NOW_STRING				CLOCK.nowTime

#define TICK_MIN                (60)
#define TICK_HOUR               (TICK_MIN * 60)
#define TICK_DAY                (TICK_HOUR * 24)

#define TICK_TO_MIN(x)          (x / TICK_MIN)
#define MIN_TO_TICK(x)          (x * TICK_MIN)

#define TICK_TO_HOUR(x)         (x / TICK_HOUR)        
#define HOUR_TO_TICK(x)         (x * TICK_HOUR)

#define TICK_TO_DAY(x)          (x / TICK_DAY)
#define DAY_TO_TICK(x)          (x * TICK_DAY)
typedef time_t tick_t;


typedef enum {
	DAY_SUNDAY = 0,
	DAY_MONDAY = 1,        
	DAY_TUESDAY = 2,       
	DAY_WEDNESDAY = 3,     
	DAY_THURSDAY = 4,
	DAY_FRIDAY = 5,
	DAY_SATURDAY = 6,
}DayOfTheWeek;

#define DATETIME_FORMAT         L"D%Y-%m-%dT%H:%M:%S"
#define DATE_FORMAT             L"%Y-%m-%d"
#define TIME_FORMAT             L"%H:%M:%S"
#define DB_TIME_FORMAT          L"%4d-%2d-%2d %2d:%2d:%2d"
#define snwprintf(dst, format, ...)    _snwprintf_s(dst.data(), dst.size(), _TRUNCATE, format, __VA_ARGS__)

using namespace std::chrono;
typedef system_clock::time_point timePoint;

namespace
{
	high_resolution_clock::time_point sStartTime;
}

class Clock : public Singleton<Clock>
{
	tick_t	serverStartTick_;

	float	mDeltaTime;
	UINT64	mdeltaTick;
	double	mLasfFrameStartTime;
	float	mFrameStartTimef;
	double	mPerfCountDuration;
public:
	void Update()
	{
		double currentTime = GetTime();

		mDeltaTime = (float)(currentTime - mLasfFrameStartTime);

		mLasfFrameStartTime = currentTime;
		mFrameStartTimef = static_cast< float > (mLasfFrameStartTime);

	}
	double GetTime() const
	{
		auto now = high_resolution_clock::now();
		auto ms = duration_cast< milliseconds >(now - sStartTime).count();
		//a little uncool to then convert into a double just to go back, but oh well.
		return static_cast< double >(ms) / 1000;
	}
	float GetTimef() const
	{
		return static_cast< float >(GetTime());
	}

	wstring	tickToStr(tick_t tick, const WCHAR *fmt = DATETIME_FORMAT);

public:
	Clock();
	~Clock();

	tick_t	serverStartTick();
	tick_t	systemTick();
	tick_t	strToTick(wstring str, const WCHAR *fmt = DB_TIME_FORMAT);

	wstring	nowTime(const WCHAR *fmt = DATETIME_FORMAT);
	wstring	nowTimeWithMilliSec(const WCHAR *fmt = DATETIME_FORMAT);

	wstring today();
	wstring tomorrow();
	wstring yesterday();

	DayOfTheWeek todayOfTheWeek();
	INT64 systemTickTP();
};


