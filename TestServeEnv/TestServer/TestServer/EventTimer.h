#pragma once

struct compares
{
	bool operator()(Events left, Events right)
	{
		return left.wakeup_time > right.wakeup_time;
	}
};

class EventTimer : public Singleton<EventTimer>
{
public:
	EventTimer();

	void Push(const int inObjectID, int inTarget, int inSec, int inEventType, int inOperation);

	void Execute();

	function<bool(Events left, Events right)> compareFunc() { return [](Events left, Events right)->bool {return left.wakeup_time > right.wakeup_time; }; }
private:
	tbb::concurrent_priority_queue < Events, compares> EventTimerQueue;
};