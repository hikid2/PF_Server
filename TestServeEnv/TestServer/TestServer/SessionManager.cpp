#include "stdafx.h"
uint64_t SessionManager::mStaticCount = 0;
bool SessionManager::CreateNewSession(SOCKET & inSocket, Session *& outSessionPtr)
{
	if (mStaticCount <= 0xFFFFFFFFFFFFFFFE) {
		outSessionPtr = new Session();
		const uint64_t newSessionID = mStaticCount;
		++mStaticCount;

		outSessionPtr->SetSessionID(newSessionID);
		outSessionPtr->SetSocket(inSocket);
		SessionContainer_t::accessor acs;
		if (mSessionContainer.insert(acs, newSessionID))
		{
			acs->second = outSessionPtr;
			return true;
		}
	}
	return false;
}

bool SessionManager::GetSession(const uint64_t & inSessionID, Session *& outSessionPtr)
{
	SessionContainer_t::const_accessor acs;
	if (mSessionContainer.find(acs, inSessionID))
	{
		outSessionPtr = acs->second;
		return true;
	}
	return false;
}


void SessionManager::CloseSession(const uint64_t & inValue)
{
	/// Socket Close
	Session * outSessionPtr = nullptr;
	if (!GetSession(inValue, outSessionPtr))
		return;
	outSessionPtr->CloseSession();
	/*if (mSessionContainer[inValue] != nullptr)
	{
		delete mSessionContainer[inValue];
		mSessionContainer[inValue] = nullptr;
	}*/
	int dummyKey = -1;
	if (ClientManager::getInstance().ConvertSessionIDToClientKey(inValue, dummyKey))
	{
		if(dummyKey != -1)
			ClientManager::getInstance().DisConnectClient(dummyKey);
	}
}
