#include"stdafx.h"
#include"IOCPServer.h"
/**
 * @brief 
 * 
 * @return true : IOCP를 위한 기초작업 성공
 * @return false : IOCP를 위한 기초작업 실패
 */
bool IOCPServer::Run()
{
	mIocpHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, WorkerThreadCount);
	if (mIocpHandle == NULL)
	{
		Log(L"CreateIoCompletionPort method Error");
		IsShutDonw = true;
		return false;
	}
	mListenSocket = WSASocket(AF_INET, SOCK_STREAM, NULL, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (mListenSocket == SOCKET_ERROR)
	{
		Log(L"WsaSocket(listenSocket) Error");
		return false;
	}

	SOCKADDR_IN serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(Global::getInstance().ServerPort);
	
	int options = 1;
	int retval = setsockopt(mListenSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&options, (int)sizeof(options));
	if (retval != 0) {
		Log(L"setsockopt Method Failed...");
		return false;
	}

	retval = ::bind(mListenSocket, (const sockaddr*)&serverAddr, sizeof(serverAddr));
	if (retval == SOCKET_ERROR) {
		Log(L"Bing Method Failed...");
		IsShutDonw = true;
		return false;
	}
	int backLog = 20;

	retval = ::listen(mListenSocket, backLog);
	if (retval == SOCKET_ERROR) {
		Log(L"Listen Method Failed...");
		IsShutDonw = true;
		return false;
	}
	Global::getInstance().accpetThread = new thread(&IOCPServer::AcceptExecute, this);
	auto this_id = Global::getInstance().accpetThread->get_id();
	Log(L"** IOCPServer::AcceptExecute Thread Created [Thread ID : 0x%08x]", this_id);
	for (auto & iter : Global::getInstance().workerThread)
	{
		iter = new thread(&IOCPServer::WorkerExecute, this);
		this_id = iter->get_id();
		Log(L"** IOCPServer::WorkerExecute Thread Created [Thread ID : 0x%08x]", this_id);
	}

	return true;
}

/**
 * @brief Accept Task Execute
 * 
 * @param inServerPtr 
 * @return DWORD 
 */
DWORD IOCPServer::AcceptExecute(LPVOID inServerPtr)
{
	IOCPServer * ownServer = (IOCPServer*)inServerPtr;

	while (IsShutDonw == false)
	{
		SOCKET newClientSocket = INVALID_SOCKET;

		sockaddr_in ClientAdder;

		static int adderLen = sizeof(ClientAdder);

		newClientSocket = WSAAccept(ownServer->GetListenSocket(), (sockaddr*)&ClientAdder, &adderLen, NULL, 0);

		if (newClientSocket == INVALID_SOCKET)
		{
			int error_no = WSAGetLastError();
			error_display("Accept:WSARecv", error_no);
		}
		///> 테스트용... 네이글 알고리즘을 잠시 꺼두자.
		/*int opt_val = 1;
		setsockopt(newClientSocket, IPPROTO_TCP, TCP_NODELAY, (const char *)&opt_val, sizeof(opt_val));*/
		Session * newSessionPtr = nullptr;

		if (!SessionManager::getInstance().CreateNewSession(newClientSocket, newSessionPtr))
		{
			Log(L"  **   Warning !! SessionID Count overflow!! do shutdown GameServer !!  **");
			closesocket(newClientSocket);
			IsShutDonw = true;
			break;
		}
		atomic_thread_fence(memory_order_seq_cst);
		CreateIoCompletionPort(reinterpret_cast<HANDLE>(newClientSocket),
			ownServer->GetIOCPHandle(), newSessionPtr->GetSessionID(), 0);

		newSessionPtr->SetAddr(ClientAdder);

		DWORD flags = 0;

		int retval = newSessionPtr->RecvStandBy();

		if (0 != retval)
		{
			int error_no = WSAGetLastError();
			if (WSA_IO_PENDING != error_no && error_no != WSAEWOULDBLOCK) {
				error_display("Accept:WSARecv", error_no);
			}
		}
		/// Log(L"  ** Accept Session ** [SessionID: %d ] ", newSessionPtr->GetSessionID());
	}
	return 0;
}
/**
 * @brief WorkerThread Method
 * 
 * @param inServerPtr : this Pointer
 */
void IOCPServer::WorkerExecute(LPVOID inServerPtr)
{
	IOCPServer * iocp = (IOCPServer *)inServerPtr;
	while (!IsShutDonw)
	{
		DWORD iosize = 0;
		uint64_t key = 0;
		OverlappedEx *my_overlap = nullptr;
		Session * sessionPtr = nullptr;
		BOOL result = GetQueuedCompletionStatus(iocp->GetIOCPHandle(),
			&iosize, (PULONG_PTR)&key, reinterpret_cast<LPOVERLAPPED *>(&my_overlap), INFINITE);
		if (FALSE == result) {
			if (my_overlap == nullptr)
			{
				Log(L"[  ** WorkerExecute -> Can not Get data from Completion Port **  ]");
				break;
			}
			else
			{
				int error_no = WSAGetLastError();
				if ((error_no == ERROR_NETNAME_DELETED) || (error_no == ERROR_OPERATION_ABORTED)) {
					// 서버에서 강제 종료.
					error_display("GQCS:", WSAGetLastError());
					SessionManager::getInstance().CloseSession(key);
					continue;
				}
				error_display("GQCS:", WSAGetLastError());
				Log(L"[  ** WorkerExecute -> invalid ClientClose  Do CloseSocket() **  ] [SessionID : %d]", key);
				
			}
		}
		else if (0 == iosize)
		{
			///> PostqueuedCompletionStatus -> 특별한(실행 종료) 이벤트를 전달.
			if (key == 0 && my_overlap == nullptr)
			{
				break;
			}
			Log(L"[  ** WorkerExecute -> ClientClose  Do CloseSocket() **  ] [SessionID : %d]", key);
			SessionManager::getInstance().CloseSession(key);

			sc_packet_remove_player packet;
			packet.id = key;

			unordered_set<int> outList;
			World::getInstance().InsertViewAvatarToList(key, outList);
			for (auto & iter : outList)
			{
				if (iter >= MaxUser)
					continue;
				if (ClientManager::getInstance()[iter].CountToViewList(key) != 0)
				{
					Session * dummySession = nullptr;
					if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dummySession))
					{
						dummySession->SendPacket(&packet);
					}
				}
			}
			continue;
		}
		int operation = my_overlap->operation;
		if (operation == OP_Send)
		{
			Session * sessionPtr = nullptr;
			if (SessionManager::getInstance().GetSession(key, sessionPtr))
			{
				///> Send 처리
				sessionPtr->OnSend(reinterpret_cast<OverlappedSend_t*>(my_overlap));
			}
			else
			{
				Log(L"key [ %d ]!!!!!!!!!!!!!!!!!!!!!!!",key);
			}
			continue;
		}
		else if (operation == OP_Recv)
		{
			Session * sessionPtr = nullptr;
			if (SessionManager::getInstance().GetSession(key, sessionPtr))
			{
				///> Recv Task
				sessionPtr->OnRecv(iosize);
			}
			continue;
		}
		else if (operation == OP_DB)
		{
			///> DB Task 
			WorldContents::getInstance().DB_Execute(key, reinterpret_cast<OverlappedDB_t*>(my_overlap));
		}
		else if (operation == OP_EventTask)
		{
			///> Event Task 
			auto overlap_event = reinterpret_cast<OverlappedEvent_t*>(my_overlap);
			EventTask::getInstance().Execute(overlap_event->result_value, key, overlap_event);
		}
	}
	return;
}
