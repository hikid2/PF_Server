#include "stdafx.h"

/**
 * @brief Construct a new Client Manager:: Client Manager object
 *
 */
ClientManager::ClientManager()
{
	for (auto index = 0; index < MaxUserCount; ++index)
		mFreeClientIndex.push(index);
}

/**
 * @brief 세션과 ClientProxy의 인덱스를 묶어주는 mSessionClientMappingTable에 데이터(SessionID)를 저장
 *
 * @param inKey : SessionID
 * @param inUid : DB에 저장된 UID
 * @return true : Insert 성공
 * @return false : insert 실패
 */
bool ClientManager::InsertTable(const uint64_t & inKey, const int & inUid)
{
#ifndef text
	while (!mFreeClientIndex.empty())
	{
		int dumyIndex = -1;
		if (mFreeClientIndex.try_pop(dumyIndex))
		{
			{
				// tbb::concurrent_hash_map<uint64_t, int>::accessor acr;
				SCMappingTable_t::accessor acr;
				if (mSessionClientMappingTable.insert(acr, inKey))
				{
					acr->second = dumyIndex;
					acr.release();
				}
			}

			{
				/// tbb::concurrent_hash_map<int, uint64_t>::accessor acr;
				CSMappingTable_t::accessor acr;
				if (mClientSessionMappingTable.insert(acr, dumyIndex))
				{
					acr->second = inKey;
					acr.release();
				}
			}
			mClientContainer[dumyIndex].SetDBStoredUID(inUid);
			return true;
		}
	}
	return false;
#else 
	while (!mFreeClientIndex.empty())
	{
		int dumyIndex = -1;
		if (mFreeClientIndex.try_pop(dumyIndex))
		{
			mSessionClientMappingTable[inKey] = dumyIndex;
			mClientSessionMappingTable[dumyIndex] = inKey;
			mClientContainer[dumyIndex].SetDBStoredUID(inUid);
			return true;
		}
	}
	return false;
#endif
}

bool ClientManager::ConvertSessionIDToClientKey(const uint64_t & inSessionID, int & outClientKey)
{
#ifndef text

	/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor acr;
	SCMappingTable_t::const_accessor acr;
	if (mSessionClientMappingTable.find(acr, inSessionID))
	{
		outClientKey = acr->second;

		acr.release();
		return true;

	}
	return false;
#else 
	if (mSessionClientMappingTable.find(inSessionID) != mSessionClientMappingTable.end())
	{
		outClientKey = mSessionClientMappingTable[inSessionID];
		return true;
	}
	return false;
#endif
}


void ClientManager::DisConnectClient(const int & inClientKey)
{
#ifndef text
	uint64_t tmpkey = 0;
	{
		// tbb::concurrent_hash_map<int, uint64_t>::const_accessor a;
		CSMappingTable_t::const_accessor acs;
		if (mClientSessionMappingTable.find(acs, inClientKey))
		{
			tmpkey = acs->second;
			acs.release();
		}
	}
	{
		tbb::concurrent_hash_map<int, uint64_t>::accessor acs;
		if (mClientSessionMappingTable.erase(inClientKey))
		{

		}

	}
	{
		tbb::concurrent_hash_map<uint64_t, int>::accessor a;
		if (mSessionClientMappingTable.erase(tmpkey))
		{

		}

	}
	auto sectorNO = mClientContainer[inClientKey].GetSectorOnAvatar();
	World::getInstance().EraseSector(inClientKey, sectorNO);
	mClientContainer[inClientKey].Clear();
	mFreeClientIndex.push(inClientKey);

#else 
	auto tmpKey = mClientSessionMappingTable[inClientKey];
	mClientSessionMappingTable[inClientKey] = 0xFFFFFFFFFFFFFFFF;
	mSessionClientMappingTable[tmpKey] = -1;
	mClientContainer[inClientKey].Clear();
	mFreeClientIndex.push(inClientKey);
#endif
}

/**
 * @brief 세션ID를 통해 ClientProxy를 얻는함수
 *
 * @param inSessionID
 * @return ClientProxy&
 */
ClientProxy & ClientManager::GetClient(const uint64_t & inSessionID)
{
	{
		/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor a;
		SCMappingTable_t::const_accessor acs;
		if (mSessionClientMappingTable.find(acs, inSessionID))
		{
			return mClientContainer[acs->second];
			acs.release();
		}
	}
}

/**
 * @brief 세션포인터를 통해 ClientProxy를 얻는 함수
 *
 * @param inSessionPtr
 * @return ClientProxy&
 */
ClientProxy & ClientManager::GetClient(Session * inSessionPtr)
{

	{
		auto inSessionID = inSessionPtr->GetSessionID();
		/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor a;
		SCMappingTable_t::const_accessor acs;
		if (mSessionClientMappingTable.find(acs, inSessionID))
		{
			return mClientContainer[acs->second];
			acs.release();
		}
	}

}

/**
 * @brief 세션ID를 통해 ClientProxyIndex를 얻는 함수
 *
 * @param inSessionID
 * @return const int
 */
const int ClientManager::GetClientIndex(const uint64_t & inSessionID)
{
	{
		/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor a;
		SCMappingTable_t::const_accessor acs;
		if (mSessionClientMappingTable.find(acs, inSessionID))
		{
			return acs->second;
			acs.release();
		}
		return -1;
	}

}

/**
 * @brief 세션주소를 통하여 ClientProxy안에 있는 사용자의 Avatar &를 얻는 함수
 *
 * @param inSessionPtr
 * @return Avatar&
 */
Avatar * ClientManager::GetAvatar(Session * inSessionPtr)
{
	{
		auto inSessionID = inSessionPtr->GetSessionID();
		/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor a;
		SCMappingTable_t::const_accessor acs;
		if (mSessionClientMappingTable.find(acs, inSessionID))
		{
			return mClientContainer[acs->second].GetSelectedAvatar();
			acs.release();
		}
	}

}

/**
 * @brief ClientProxy의 Index를 통해 연결유무를 확인하는 함수
 *
 * @param inNo : ClientProxy Index
 * @return true : 연결중
 * @return false : 연결끊김
 */
const bool & ClientManager::IsConnected(const int inNo)
{
	return mClientContainer[inNo].IsConnected();
}

/**
 * @brief origin Index의 ClientProxy->Avatar와 target Index ClientProxy->Avatar아바타 사이의 시야(origin의 시야 기준) 거리를 확인
 *
 * @param inOrigin : ClientIndex
 * @param inTarget : ClientIndex
 * @return true : target이 origin의 시야 안에 있다
 * @return false : target이 origin의 시야 안에 없다.
 */
bool ClientManager::IsinDistance(int inOrigin, int inTarget)
{
	auto origin = mClientContainer[inOrigin].GetSelectedAvatar();
	auto target = mClientContainer[inTarget].GetSelectedAvatar();

	int dist = (origin->GetAvatarPositionX() - target->GetAvatarPositionX())
		*(origin->GetAvatarPositionX() - target->GetAvatarPositionX())
		+ (origin->GetAvatarPositionY() - target->GetAvatarPositionY())
		* (origin->GetAvatarPositionY() - target->GetAvatarPositionY());

	if (dist <= target->GetVisibleDistance() * target->GetVisibleDistance())
	{
		/// Log(L"[org : %d][target : %d] %d <= %d", inOrigin, inTarget, dist, origin->GetVisibleDistance() * origin->GetVisibleDistance());
		return true;
	}
	return false;
}

/**
 * @brief NPC와 Avatar 사이의 시야 거리를 확인(아바타의 시야 거리 기준)
 *
 * @param inNPC : NPC INDEX
 * @param inAvt : ClientIndex
 * @return true  : Avatar의 시야거리 안에 있다.
 * @return false : Avatar의 시야 거리 안에 없다.
 */
bool ClientManager::BetweenNPCtoAvatarIsinDistance(int inNPC, int inAvt)
{
	auto npcX = NPCManager::getInstance()[inNPC].mPointX;
	auto npcY = NPCManager::getInstance()[inNPC].mPointY;
	auto target = mClientContainer[inAvt].GetSelectedAvatar();

	int dist = (npcX - target->GetAvatarPositionX())
		*(npcX - target->GetAvatarPositionX())
		+ (npcY - target->GetAvatarPositionY())
		* (npcY - target->GetAvatarPositionY());
	if (dist <= target->GetVisibleDistance() * target->GetVisibleDistance())
	{
		/// Log(L"[NPC : %d][avt : %d] %d <= %d", inNPC, inAvt, dist, target->GetVisibleDistance() * target->GetVisibleDistance());
		return true;
	}

	return false;
}

/**
 * @brief INPUT Packet에 대한 처리를 담당한다.
 *
 * @param inKeyData : 입력 데이터
 * @param inSessionPtr : Packet을 보낸 세션의 주소
 * @param outClientIndex : 입력을 Session의 ClientProxy Index를 인자를 통해 반환
 */
bool ClientManager::InputExecute(int inKeyData, Session * inSessionPtr, int & outClientIndex)
{
	auto Attack = [](const int inClientIndex)
	{
		auto dummyhorizontal = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->GetHorizontal();
		auto dummyvertical = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->GetVertical();
		auto targetPointx = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->GetAvatarPositionX();
		auto targetPointy = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->GetAvatarPositionY();
		/// 범위기반 SpinrwMutex 건다. mViewList의 Read 작업때문.[Write로 건다]
		auto damage = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->GetAttackDamamge();
		unordered_set<int> woundedNPC;
		{
			tbb::spin_rw_mutex::scoped_lock lock(ClientManager::getInstance()[inClientIndex].mtx, false);
			for (auto & iter : ClientManager::getInstance()[inClientIndex].GetSelectedAvatar()->mViewList)
			{
				/// 같은 시야공간일 경우 필드가 달라질 이유가 없기때문에 필드검사는 하지 않는다.
				/// NPC가 아닐 경우 재끼자.
				if (!NPCManager::getInstance().IsNPC(iter)) continue;
				if ((NPCManager::getInstance()[iter].mPointX == targetPointx + 1
					&& NPCManager::getInstance()[iter].mPointY == targetPointy) ||
					(NPCManager::getInstance()[iter].mPointX == targetPointx - 1
						&& NPCManager::getInstance()[iter].mPointY == targetPointy) ||
						(NPCManager::getInstance()[iter].mPointX == targetPointx
							&& NPCManager::getInstance()[iter].mPointY == targetPointy + 1) ||
							(NPCManager::getInstance()[iter].mPointX == targetPointx
								&& NPCManager::getInstance()[iter].mPointY == targetPointy - 1))
				{
					NPCManager::getInstance().Hit(iter, inClientIndex, damage);
					woundedNPC.insert(iter);
				}
			}
		}
		for (auto & iter : woundedNPC)
		{
			unordered_set<int> nearList;
			World::getInstance().InsertViewAvatarToList(iter, nearList);
			for (auto & iter2 : nearList)
			{
				Session * dummySession = nullptr;
				if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter2), dummySession))
				{
					SendUpdateHP(iter, dummySession);
				}
			}
		}		
	};
	{
		/// tbb::concurrent_hash_map<uint64_t, int>::const_accessor a;
		SCMappingTable_t::const_accessor acs;
		if (mSessionClientMappingTable.find(acs, inSessionPtr->GetSessionID()))
		{
			outClientIndex = acs->second;
			acs.release();
		}
		else
		{
			return false;
		}
	}

	auto outAvatar = mClientContainer[outClientIndex].GetSelectedAvatar();

	int32_t x = outAvatar->GetAvatarPositionX();
	int32_t y = outAvatar->GetAvatarPositionY();
	byte fieldID = outAvatar->GetNowField();

	int prevSectorNO = GetSectorNo(x, y);

	int dbStoredAvatarID = outAvatar->GetDBStoredAvatarID();

	if (inKeyData & 1 << 0)
	{
		y -= 1;
		outAvatar->SetHorizontal(0);
		outAvatar->SetVertical(-1);
	}
	else if (inKeyData & 1 << 1)
	{
		y += 1;
		outAvatar->SetHorizontal(0);
		outAvatar->SetVertical(1);
	}
	else if (inKeyData & 1 << 2)
	{
		x -= 1;
		outAvatar->SetHorizontal(-1);
		outAvatar->SetVertical(0);
	}
	else if (inKeyData & 1 << 3)
	{
		x += 1;
		outAvatar->SetHorizontal(1);
		outAvatar->SetVertical(0);
	}
	else if (inKeyData & 1 << 4)
	{
		Attack(outClientIndex);
		return false;
	}

	if (x < 0) x = 0;
	else if (x >= 200) x = 199;
	if (y < 0) y = 0;
	else if (y >= 200) y = 199;

	/// 이동할 곳이 워프가 가능한 공간인지 확인, 공간이 맞다면 워프 작업을 진행
	ENUM_WARPID tmpWarpData = ENUM_WARPID::WARPID_NOT_YET;
	if (World::getInstance().IsWarpZone(x, y, fieldID, tmpWarpData))
	{

		/// 2 유저의 필드를 옮김
		byte fieldData = 0;
		World::getInstance().DoWarpField(tmpWarpData, x, y, fieldData);

		outAvatar->SetNowField(fieldData);

		outAvatar->SetAvatarPosition(x, y);

		int sectorNo = GetSectorNo(x, y);
		World::getInstance().UpdateIDSector(
			outClientIndex,
			prevSectorNO,
			sectorNo);

		/// 3 나에게 필드이동 패킷을 전송
		sc_packet_wrap_field packet;
		packet.mAvatarID = outClientIndex;
		packet.mWarpField = fieldData;
		packet.mPointx = x;
		packet.mPointy = y;
		inSessionPtr->SendPacket(&packet);
		fieldID = fieldData;

	}
	else
	{
		// 지형과 충돌체크 확인 후 충돌이면 쉬게한다.
		if (World::getInstance().IsCollisionToTerrain(x, y, fieldID))
		{

			/// cout << "Collision!" << endl;
			return false;
		}
		// Sector Update
		int sectorNo = GetSectorNo(x, y);
		if (prevSectorNO != sectorNo)
			World::getInstance().UpdateIDSector(outClientIndex, prevSectorNO, sectorNo);

		// UpdatePointXY
		outAvatar->SetAvatarPosition(x, y);
		/// Log(L"** avatar move : %d | x = %d y : %d ", outClientIndex, x, y);
		/// PacketManager::getInstance().SendExecute(SC_PACKET::SC_MOVE_PLAYER, outClientIndex, inSessionPtr);
		SendMovePlayer(outClientIndex, inSessionPtr);
	}
	//auto events = new DB_Event_UpdateAvatarLocation();
	//events->SetLocalX(x);
	//events->SetLocalY(y);
	//events->SetFieldID(fieldID);
	//events->SetAvatarID(dbStoredAvatarID);
	//events->SetUniqueID(inSessionPtr->GetSessionID());
	//DBTaskManager::getInstance().Push(events);
	return true;
}

uint64_t ClientManager::GetSessionID(int inClientID)
{
	{
		///  tbb::concurrent_hash_map<int, uint64_t>::const_accessor a;
		CSMappingTable_t::const_accessor acs;


		if (mClientSessionMappingTable.find(acs, inClientID))
		{
			return acs->second;
			acs.release();
		}
		return 0xFFFFFFFFFFFFFFFF;
	}

}

bool ClientManager::IsSameField(int inFirstID, int inSecondsID)
{
	if (mClientContainer[inFirstID].GetAvatarField() == mClientContainer[inSecondsID].GetAvatarField())
		return true;
	return false;
}
