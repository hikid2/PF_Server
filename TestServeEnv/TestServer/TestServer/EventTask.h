#pragma once


class EventTask : public Singleton<EventTask >
{
public:
	EventTask() { Initialize(); }

	typedef void (*AITaskFunc)(int32_t, OverlappedEvent_t *);

	virtual void Initialize();

	void error(lua_State *L, const char *fmt, ...);

	virtual void Execute(int inOperation, int inNPCKey, OverlappedEvent_t * inOverlapEx);

	unordered_map<int32_t, AITaskFunc> mEventTaskContainer;








	static int API_Task_View_In_Range(lua_State * L);

	static int API_Task_AddTimer_Event(lua_State * L);

	static int API_Task_Trace_Is_In_Range(lua_State * L);

	static int API_Task_Roaming(lua_State * L);

	static int API_Task_Search_Near_Avatar(lua_State *L);

	static int API_Task_Search_Near_Avatar_Get(lua_State *L);

	static int API_Active_false(lua_State * L);

	static int API_Task_Trace(lua_State * L);

	static int API_Task_Attack(lua_State * L);

	static int API_Task_Dead_Execute(lua_State * L);

	static int API_Task_Respawn(lua_State * L);

	static int API_Tast_Give_Exp(lua_State * L);

	static int API_Get_Attacked_Data(lua_State * L);

	static int API_Is_Dead(lua_State * L);
	
	static int API_Get_Gold(lua_State * L);
};