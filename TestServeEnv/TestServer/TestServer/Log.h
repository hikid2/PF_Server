#pragma once
#include<fstream>
#define Log(arg, ...) CLog::getInstance().Loging(arg, __VA_ARGS__);


class CLog : public Singleton<CLog>
{
public:
	CLog() {
		wstring name = L"LogFile.log";
		mFile.open(name, std::ios::out | std::ios::trunc);
		if (mFile.fail()) {
			printf("! File Error \n");
		}
		if (mFile.bad()) {
			printf("! LogFile error .... file open Failed.\n");
		}
	}
	virtual ~CLog()
	{
		mFile.close();
		mFile.clear();

		wstring beforeName = L"LogFile.log";
		wstring closeFilename = {};
		closeFilename += CLOCK.nowTime(L"_%Y%m%d-%H%M%S.log");
		// LogFile name ���� �Լ�
		_wrename(beforeName.c_str(), closeFilename.c_str());
	}

	void Loging(const wchar_t * inPtr, ...)
	{

		std::array<wchar_t, 1024> buffer = {};
		wstring temp = {};
		va_list vs;
		temp += CLOCK.nowTime(L"[%Y.%m.%d %H:%M:%S]");
		va_start(vs, inPtr);
		vswprintf_s(buffer.data(), buffer.size(),inPtr, vs);
		temp += buffer.data();
		temp += L"\n";
		this->StoreDataLog(temp.c_str());
		va_end(vs);
	}
	void StoreDataLog(const wchar_t * inPtr)
	{
		wprintf(L"%s", inPtr);
		mFile << inPtr;
		mFile.flush();
	}
private:
	wfstream mFile;
};