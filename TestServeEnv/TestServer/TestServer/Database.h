#pragma once
class Database
{
public:
	void Initialize();
	void Connect();
	void DisConnect();
	void AllocStmt(SQLHSTMT &hstmt);
	void FreeStmt(SQLHSTMT &hstmt);
private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLRETURN retcode;
};