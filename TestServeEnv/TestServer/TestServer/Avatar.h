#pragma once
#pragma pack(push, 1)
struct Ability
{
	int str;
	int dex;
	int ints;
	int luck;
};
#pragma pack(pop)
class Avatar
{
public:
	Avatar();
	~Avatar() {}
	Avatar(const Avatar & ins);
	void Clear();

	const int32_t & GetAvatarPositionX() { return mX; }

	const int32_t & GetAvatarPositionY() { return mY; }

	void SetAvatarPosition(int32_t inPx, int32_t inPy);

	const byte & GetClassType() { return mClassType; }

	const byte & GetNowField() { return mNowField; }

	void SetNowField(const byte & inValue) { mNowField = inValue; }

	const int & GetVisibleDistance() { return mVisibleDistance; }

	void SetLv(const int & inLV) { mLevel = inLV; }

	void SetGold(const int & inGold) { mHasGold = inGold; }

	void AddGold(const int & inValue);

	void SetAbility(const int & inStr, const int & inDex, const int & inInts, const int & inLuck);

	void SetClassType(const byte & inValue) { mClassType = inValue; }

	void SetCurrHP(const uint32_t inValue);

	void AddNowHp(const int inValue);


	void SetCurrMP(const int & inValue) { mCurrMP= inValue; }

	void SetMaxHP(const int & inValue) { mMaxHP = inValue;}

	void SetMaxMP(const int & inValue) { mMaxMP = inValue;}

	void SetCurExp(const int64_t & inValue) { mCurrExp = inValue; }

	void AddCurExp(const int64_t & inValue);

	void SetNeedExp(const int64_t & inValue) { mNeedExp = inValue; }

	void SetName(const wstring & inValue) { mName = inValue; }

	void GetAbility(int & outStr, int & outDex, int & outInts, int & outLuck);
	const int & GetLv() { return mLevel; }

	const int & GetGold() { return mHasGold; }

	const volatile uint32_t & GetCurrHP() { return mCurrHP; }

	const uint32_t & GetCurrMP() { return mCurrMP; }

	const uint32_t & GetMaxHP() { return mMaxHP; }

	const uint32_t & GetMaxMP() { return mMaxMP; }

	const int64_t & GetCurExp() { return mCurrExp; }

	const int64_t & GetNeedExp() { return mNeedExp; }

	const wstring & GetName() { return mName; }

	void SetDBStoredAvatarID(int inValue) { mDBStoredAvatarID = inValue; }

	const int & GetDBStoredAvatarID() { return mDBStoredAvatarID; }

	unordered_set<int> mViewList;
	//*> �¿�
	void SetHorizontal(int inValue) { mHorizontal = inValue; }

	const int & GetHorizontal() { return mHorizontal; }
	//*> ����
	void SetVertical(int inValue) { mVertical = inValue; }

	const int & GetVertical() { return mVertical; }

	int GetAttackDamamge();

	template<class T>
	bool CAS(volatile uint32_t * next, T old_value, T new_value)
	{
		return atomic_compare_exchange_strong(reinterpret_cast< volatile atomic_uint32_t *>(next), &old_value, new_value);
	}

	template<class T>
	bool CAS(volatile int * next, T old_value, T new_value)
	{
		return atomic_compare_exchange_strong(reinterpret_cast<volatile atomic_int *>(next), &old_value, new_value);
	}

	template<class T>
	bool CAS(volatile int64_t * next, T old_value, T new_value)
	{
		return atomic_compare_exchange_strong(reinterpret_cast<volatile atomic_int64_t *>(next), &old_value, new_value);
	}

	bool IsFullHP();

	void SetRespawn();

	void WarpTo(const int & inPointx, const int & inPointy, const byte & inFieldID);

private:
	int					mDBStoredAvatarID;
	wstring				mName;
	int					mLevel;
	byte				mClassType;
	volatile uint32_t	mCurrHP;
	uint32_t			mCurrMP;
	uint32_t			mMaxHP;
	uint32_t			mMaxMP;
	int64_t				mCurrExp;
	int64_t				mNeedExp;
	byte				mNowField;
	int32_t				mX;
	int32_t				mY;
	int					mVisibleDistance = 5;
	int					mHasGold;
	Ability				mAbility;
	int					mVertical;
	int					mHorizontal;
};

