#include "stdafx.h"

BaseServer::BaseServer()
{
	_wsetlocale(LC_ALL, L"korean");
	if (WSAStartup(MAKEWORD(2, 2), &mWsadata) != 0)
	{
		Log(L"**   WSAStartup Error !!");
		IsShutDonw = true;
		return;
	}
}

BaseServer::~BaseServer() 
{
	WSACleanup();
}
