#include "stdafx.h"
//
//MiniDump::MiniDump()
//{
//	::SetUnhandledExceptionFilter(exceptionFilter);
//	cout << " ** Mini Dump Filter Setting Succ !" << endl;
//}
//
//LONG WINAPI MiniDump::exceptionFilter(_EXCEPTION_POINTERS * exceptionInfo)
//{
//	Global::getInstance().ShutdownServer();
//	_CrtMemDumpAllObjectsSince(NULL);
//
//	HMODULE dumpDLL = nullptr;
//	dumpDLL = ::LoadLibrary(L"DBGHELP.DLL");
//	if (!dumpDLL) {
//		cout << "DBGHELP.DLL not Load" << endl;
//		return 0;
//	}
//	wstring dumpPatch;
//	dumpPatch += L"dumpFile.dmp";
//
//	HANDLE file = ::CreateFile(dumpPatch.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
//	if (file == INVALID_HANDLE_VALUE) {
//		cout << "! dump file not making" << endl;
//		return 0;
//	}
//	::CloseHandle(file);
//
//	return EXCEPTION_CONTINUE_SEARCH;
//}
//
//static MiniDump minidump;

LPTOP_LEVEL_EXCEPTION_FILTER PreviousExceptionFilter = NULL;

LONG WINAPI UnHandledExceptionFilter(struct _EXCEPTION_POINTERS *exceptionInfo) {
	HMODULE DllHandle = NULL;
	printf("dump call");
	// Windows 2000 이전에는따로DBGHELP를배포해서설정해주어야한다.  
	DllHandle = LoadLibrary(_T("DBGHELP.DLL"));

	if (DllHandle) {
		MINIDUMPWRITEDUMP Dump = (MINIDUMPWRITEDUMP)GetProcAddress(DllHandle, "MiniDumpWriteDump");

		if (Dump) {
			TCHAR  DumpPath[MAX_PATH] = { 0, };    SYSTEMTIME SystemTime;

			GetLocalTime(&SystemTime);

			_sntprintf_s(DumpPath, MAX_PATH, _T("%d-%d-%d %d_%d_%d.dmp"), SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond);
			HANDLE FileHandle = CreateFile(DumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

			if (FileHandle != INVALID_HANDLE_VALUE) {
				_MINIDUMP_EXCEPTION_INFORMATION MiniDumpExceptionInfo;          MiniDumpExceptionInfo.ThreadId = GetCurrentThreadId();     MiniDumpExceptionInfo.ExceptionPointers = exceptionInfo;     MiniDumpExceptionInfo.ClientPointers = NULL;

				BOOL Success = Dump(GetCurrentProcess(), GetCurrentProcessId(), FileHandle, MiniDumpNormal, &MiniDumpExceptionInfo, NULL, NULL);
				if (Success) {
					CloseHandle(FileHandle);

					return EXCEPTION_EXECUTE_HANDLER;
				}
			}

			CloseHandle(FileHandle);
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}

MiniDump::MiniDump()
{
	SetErrorMode(SEM_FAILCRITICALERRORS);

	PreviousExceptionFilter = SetUnhandledExceptionFilter(UnHandledExceptionFilter);
}
MiniDump::~MiniDump()
{
	SetUnhandledExceptionFilter(PreviousExceptionFilter);
}

static MiniDump dumps;

