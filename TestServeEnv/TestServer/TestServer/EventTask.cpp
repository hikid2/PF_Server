#include "stdafx.h"

void EventTask::Initialize()
{
	mEventTaskContainer[OP_EventTimer_WAKEUP] = [](int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		int target = inOverlapEx->target;
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_EventTimer_WakeUp");
		lua_pushinteger(NPCManager::getInstance()[inNpcKey].mLuastate, target);
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 1, 0, 0);
		/*{
			error(NPCManager::getInstance()[inNpcKey].mLuastate, "error running function ‘XXX’: %s\n", lua_tostring(NPCManager::getInstance()[inNpcKey].mLuastate, -1));
		}*/
	};

	mEventTaskContainer[OP_EventTimer_TRACING] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		int target = inOverlapEx->target;
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_tracing_move");
		lua_pushinteger(NPCManager::getInstance()[inNpcKey].mLuastate, target);
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 1, 0, 0);
		/*{
			error(NPCManager::getInstance()[inNpcKey].mLuastate, "error running function ‘XXX’: %s\n", lua_tostring(NPCManager::getInstance()[inNpcKey].mLuastate, -1));
		}*/
	};

	mEventTaskContainer[OP_EventTimer_ROAMING] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_roaming_move");
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 0, 0, 0);
		/*{
			error(NPCManager::getInstance()[inNpcKey].mLuastate, "error running function ‘XXX’: %s\n", lua_tostring(NPCManager::getInstance()[inNpcKey].mLuastate, -1));
		}*/
	};

	mEventTaskContainer[OP_EventTimer_ATTACK] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		/// cout << "OP_AI_ATTACK " << endl;
		int target = inOverlapEx->target;
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_Attack_Method");
		lua_pushinteger(NPCManager::getInstance()[inNpcKey].mLuastate, target);
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 1, 0, 0);
	};

	mEventTaskContainer[OP_EventTimer_SLEEP] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_EventTimer_Sleep");
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 0, 0, 0);
	};

	mEventTaskContainer[OP_EventTimer_DEAD] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_Dead_Method");
		int target = inOverlapEx->target;
		lua_pushinteger(NPCManager::getInstance()[inNpcKey].mLuastate, target);
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 1, 0, 0);
	};

	mEventTaskContainer[OP_EventTimer_Respawn] = [](const int inNpcKey, OverlappedEvent_t * inOverlapEx) {
		lua_getglobal(NPCManager::getInstance()[inNpcKey].mLuastate, "event_Respawn_Method");
		lua_pcall(NPCManager::getInstance()[inNpcKey].mLuastate, 0, 0, 0);
	};

	mEventTaskContainer[OP_EventTimer_Respawn_Avatar] = [](const int inAvatarID, OverlappedEvent_t * inOverlapEx) {

		Session * dummySesion = nullptr;
		ClientManager::getInstance()[inAvatarID].SetRespawn(inAvatarID);

		if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(inAvatarID), dummySesion))
		{
			World::getInstance().NextViewUpdate(dummySesion, inAvatarID);
		}
	};
}

inline void EventTask::error(lua_State * L, const char * fmt, ...)
{
	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	lua_close(L);
	exit(EXIT_FAILURE);
}

void EventTask::Execute(int inOperation, int inNPCKey, OverlappedEvent_t * inOverlapEx)
{
	if (inOverlapEx == nullptr)
	{
		Log(L"1?!?");
		return;
	}
	if (mEventTaskContainer.count(inOperation) == 0)
		return;
	AITaskFunc TaskExec = mEventTaskContainer[inOperation];
	TaskExec(inNPCKey, inOverlapEx);
	RecycleOverlappedManager::getInstance().PushEventOverlap(inOverlapEx);
}

int EventTask::API_Task_View_In_Range(lua_State * L)
{
	int aikey = (int)lua_tointeger(L, -2);
	int player_id = (int)lua_tointeger(L, -1);
	bool retval = NPCManager::getInstance().IsinDistance(player_id, aikey);
	lua_pop(L, 2);
	lua_pushboolean(L, retval);
	return 1;
}

int EventTask::API_Task_AddTimer_Event(lua_State * L)
{
	int event_type = (int)lua_tointeger(L, -4);
	int tick = (int)lua_tointeger(L, -3);
	int myId = (int)lua_tointeger(L, -2);
	int target = (int)lua_tointeger(L, -1);
	EventTimer::getInstance().Push(myId,
		target,
		tick,
		event_type,
		OP_EventTask);
	lua_pop(L, 4);
	return 0;
}

int EventTask::API_Task_Trace_Is_In_Range(lua_State * L)
{
	int aikey = (int)lua_tointeger(L, -3);
	int player_id = (int)lua_tointeger(L, -2);
	int distance = (int)lua_tointeger(L, -1);
	bool retval = NPCManager::getInstance().IsinDistance(player_id, aikey, distance);
	lua_pop(L, 3);
	lua_pushboolean(L, retval);
	return 1;
}

int EventTask::API_Task_Roaming(lua_State * L)
{

	int npcKey = (int)lua_tointeger(L, -1);
	lua_pop(L, 1);
	uint64_t now = GetTickCount64();
	int x = NPCManager::getInstance()[npcKey].mPointX;
	int y = NPCManager::getInstance()[npcKey].mPointY;
	int prevSectorNO = GetSectorNo(x, y);
	byte fieldNo = NPCManager::getInstance()[npcKey].mInFieldNo;

	NPCManager::getInstance().RandomMove(npcKey, x, y);
	if (World::getInstance().IsCollisionToTerrain(x, y, fieldNo)) {

		lua_pushboolean(L, false);
		lua_pushinteger(L, 1);
		return 2;
	}

	/// 이동전 주변 lIst체크
	unordered_set<int> viewList;
	World::getInstance().InsertViewAvatarToList(npcKey, viewList);

	NPCManager::getInstance()[npcKey].mPointX = x;
	NPCManager::getInstance()[npcKey].mPointY = y;
	int sectorNO = GetSectorNo(x, y);
	World::getInstance().UpdateIDSector(npcKey, prevSectorNO, sectorNO);
	unordered_set<int> newList;

	// 이동 후 주변 리스트 체크
	World::getInstance().InsertViewAvatarToList(npcKey, newList);
	// 이전 시야를 가지고 주변을 보자
	for (auto & cl : viewList)
	{
		// 이전에 있었으나 이후 시야에서 사라진 플레이어는?
		if (newList.count(cl) == 0)
		{
			ClientManager::getInstance()[cl].EraseToViewList(npcKey);
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(cl), dumySesion))
			{
				SendRemovePlayer(npcKey, dumySesion);
			}
		}
		else {
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(cl), dumySesion))
			{
				SendMovePlayer(npcKey, dumySesion);
			}
		}
	}
	for (auto & cl : newList)
	{
		/// 새로운 뷰에도 있고 이전에도 있었다면 ->이미 위에서 이동을 했으므로 패스
		if (viewList.count(cl) != 0)
			continue;
		Session * dummySesion = nullptr;
		if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(cl), dummySesion))
		{
			/// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
			SendPutPlayer(npcKey, dummySesion);
		}
	}
	NPCManager::getInstance()[npcKey].mLastEventTick = now;
	if (newList.empty()) {
		lua_pushboolean(L, false);
		lua_pushinteger(L, 2);
		return 2;
	}

	lua_pushboolean(L, true);
	lua_pushinteger(L, 0);
	return 2;
}

int EventTask::API_Task_Search_Near_Avatar(lua_State * L)
{
	int aiKey = (int)lua_tointeger(L, -2);
	byte worldID = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);
	unordered_set<int> newList;
	World::getInstance().InsertViewAvatarToList(aiKey, newList);

	bool retval = !newList.empty();
	lua_pushboolean(L, retval);
	return 1;
}

int EventTask::API_Task_Search_Near_Avatar_Get(lua_State * L)
{
	int aiKey = (int)lua_tointeger(L, -2);
	byte worldID = (byte)lua_tointeger(L, -1);
	lua_pop(L, 2);
	unordered_set<int> newList;
	World::getInstance().InsertViewAvatarToList(aiKey, newList);
	bool retval = !newList.empty();
	int numbers = -1;
	if (retval) {
		numbers = *newList.begin();
	}
	lua_pushboolean(L, retval);
	lua_pushinteger(L, numbers);
	return 2;
}

int EventTask::API_Active_false(lua_State * L)
{
	int aikey = (int)lua_tointeger(L, -1);
	lua_pop(L, 1);
	bool val = true;
	NPCManager::getInstance()[aikey].isActive.compare_exchange_strong(val, false);
	return 0;
}

int EventTask::API_Task_Trace(lua_State * L)
{
	uint64_t nowTime = GetTickCount64();
	const int nextTrace = 1;
	const int nextRoaming = 2;
	const int nextSleep = 3;
	const int nextAttack = 4;

	int npcID = (int)lua_tointeger(L, -2);
	int avatarID = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);


	byte fieldID = NPCManager::getInstance()[npcID].mInFieldNo;

	// 타겟이 죽은 사람인지 확인할 필요가 있다.
	if (ClientManager::getInstance()[avatarID].GetSelectedAvatar()->GetCurrHP() <= 0)
	{
		// 다른 타겟을 찾게끔 해야한다.
		unordered_set<int> viewList;
		World::getInstance().InsertViewAvatarToList(npcID, viewList);
		if (viewList.empty())
		{
			lua_pushinteger(L, nextSleep);
			lua_pushinteger(L, -1);
			return 2;
		}
		lua_pushinteger(L, nextRoaming);
		lua_pushinteger(L, -1);
		return 2;
	}

	POINT start = { ClientManager::getInstance()[avatarID].GetSelectedAvatar()->GetAvatarPositionX(), ClientManager::getInstance()[avatarID].GetSelectedAvatar()->GetAvatarPositionY() };
	POINT target = { NPCManager::getInstance()[npcID].mPointX, NPCManager::getInstance()[npcID].mPointY };

	POINT nowMovePoint = { 0,0 };
	POINT nextStepPoint = { 0,0 };

	/// 직선 경로가 없다면 -> A*가동
	if (!World::getInstance().IsOneLinePath(start, target, fieldID, nowMovePoint))
	{
		/// A*로도 길을 못찾으면?
		if (!World::getInstance().TestAstar(fieldID, start, target, nowMovePoint, nextStepPoint))
		{
			lua_pushinteger(L, nextRoaming);
			lua_pushinteger(L, -1);
			return 2;
		}
	}

	unordered_set<int> viewList;
	World::getInstance().InsertViewAvatarToList(npcID, viewList);
	NPCManager::getInstance()[npcID].mHorizontal = nowMovePoint.x - NPCManager::getInstance()[npcID].mPointX;
	NPCManager::getInstance()[npcID].mVertical = nowMovePoint.y - NPCManager::getInstance()[npcID].mPointY;
	NPCManager::getInstance()[npcID].mPointX = nowMovePoint.x;
	NPCManager::getInstance()[npcID].mPointY = nowMovePoint.y;
	int prevSectorNO = GetSectorNo(target.x, target.y);

	int sectorNO = GetSectorNo(NPCManager::getInstance()[npcID].mPointX, NPCManager::getInstance()[npcID].mPointY);

	World::getInstance().UpdateIDSector(npcID, prevSectorNO, sectorNO);


	unordered_set<int> newList;
	World::getInstance().InsertViewAvatarToList(npcID, newList);
	for (auto & iter : viewList)
	{
		if (newList.count(iter) > 0)
		{
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
			{
				// SendMovePacket(npcID, dumySesion);
				/// PacketManager::getInstance().SendExecute(SC_PACKET::SC_MOVE_PLAYER, npcID, dumySesion);
				SendMovePlayer(npcID, dumySesion);
			}
		}
		else
		{
			ClientManager::getInstance()[iter].EraseToViewList(npcID);
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
			{
				/// PacketManager::getInstance().SendExecute(SC_PACKET::SC_REMOVE_PLAYER, npcID, dumySesion);
				SendRemovePlayer(npcID, dumySesion);
			}
		}
	}
	for (auto & iter : newList)
	{
		if (viewList.count(iter) > 0) continue;
		Session * dumySesion = nullptr;
		if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
		{
			// SendPutPacket(npcID, dumySesion);
			ClientManager::getInstance()[iter].InsertToViewList(npcID);
			/// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
			SendPutPlayer(npcID, dumySesion);
		}
	}
	NPCManager::getInstance()[npcID].mLastEventTick = nowTime;
	if (newList.count(avatarID) > 0)
	{
		if ((abs(NPCManager::getInstance()[npcID].mPointX - ClientManager::getInstance()[avatarID].GetSelectedAvatar()->GetAvatarPositionX()) +
			abs(NPCManager::getInstance()[npcID].mPointY - ClientManager::getInstance()[avatarID].GetSelectedAvatar()->GetAvatarPositionY())) == 1)
		{
			// 나와 상대방의 거리가 1일 경우
			lua_pushinteger(L, nextAttack);
			lua_pushinteger(L, avatarID);
			return 2;
		}
		lua_pushinteger(L, nextTrace);
		lua_pushinteger(L, avatarID);
		return 2;
	}
	else {
		if (newList.empty())
		{
			lua_pushinteger(L, nextSleep);
			lua_pushinteger(L, -1);
			return 2;
		}
		for (auto & iter : newList)
		{
			if (!NPCManager::getInstance().IsinDistance(iter, npcID, 4))
				continue;
			else
			{
				lua_pushinteger(L, nextTrace);
				lua_pushinteger(L, iter);
				return 2;
			}
		}
		lua_pushinteger(L, nextRoaming);
		lua_pushinteger(L, -1);
		return 2;
	}
}


/*
1. AI가 플레이어의 체력을 깎는다.
2. 플레이어의 상태가 죽은 상태인지 확인한다.
3. 이후 이벤트를 설정한다.
3-1 플레이어가 아직 살아있고, 공격범위에 존재하는 경우
-> 플레이어에게 공격을 가한다 1초 뒤

3-2 플레이어가 아직 살아있고, 공격범위에 존재하진 않는다
-> 플레이어에게 접근(Astar)를 이용
3-3 플레이어가 죽은 상태일 경우
-> 몬스터의 상태에 따라 반응이 달라진다.
3-3-1 평소 평화적인 몬스터일 경우
- ViewList안에 플레이어가 존재한다
- ViewList안에 플레이어가 존재하지 않는다.
3-3-2 평소에 성격나쁜 몬스터일 경우
- ViewList안에 플레이어가 존재한다
- ViewList안에 플레이어가 존재하지 않는다.
*/
int EventTask::API_Task_Attack(lua_State * L)
{
	const int attack_target = 1;
	const int trace_target = 2;
	const int NotFoundTarget = 3;
	int object = (int)lua_tointeger(L, -2);
	int target = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);

	if ((ClientManager::getInstance()[target].IsConnected() == false) ||
		(ClientManager::getInstance()[target].GetSelectedAvatar()->GetCurrHP() <= 0)) {
		// 1. 타겟이 로그아웃 || 이미 죽어있다면?
		lua_getglobal(L, "setTarget");
		lua_pushinteger(L, -1);
		lua_pcall(L, 1, 0, 0);
		lua_pushinteger(L, NotFoundTarget);
		lua_pushinteger(L, -1);
		return 2;
	}
	// 2. 타겟이 죽었다 ?? 안죽었는데 죽을수도 있잖아...
	// int hp = ClientManager::getInstance()[target].GetSelectedAvatar().GetCurrHP();
	int damage = -NPCManager::getInstance()[object].GetObjectDamage();

	ClientManager::getInstance()[target].GetSelectedAvatar()->AddNowHp(damage);
	uint32_t hp = ClientManager::getInstance()[target].GetSelectedAvatar()->GetCurrHP();
	if (hp == 0)
	{
		EventTimer::getInstance().Push(target, 0, 5000, ENUM_EventTimer_TASK_OPERATION::OP_EventTimer_Respawn_Avatar, OP_EventTask);
		sc_packet_update_avatarhp packet;
		packet.hp = 0;
		packet.id = target;
		Session * dumySesion = nullptr;
		if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(target), dumySesion))
		{
			dumySesion->SendPacket(&packet);
		}
		lua_getglobal(L, "setTarget");
		lua_pushinteger(L, -1);
		lua_pcall(L, 1, 0, 0);
		lua_pushinteger(L, NotFoundTarget);
		lua_pushinteger(L, -1);
		return 2;
	}
	else
	{
		sc_packet_update_avatarhp packet;
		packet.hp = hp;
		packet.id = target;
		Session * dumySesion = nullptr;
		if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(target), dumySesion))
		{
			dumySesion->SendPacket(&packet);
		}
		if (abs(ClientManager::getInstance()[target].GetSelectedAvatar()->GetAvatarPositionX() - NPCManager::getInstance()[object].mPointX) +
			abs(ClientManager::getInstance()[target].GetSelectedAvatar()->GetAvatarPositionY() - NPCManager::getInstance()[object].mPointY) == 1)
		{
			// 3-1 플레이어가 아직 살아있고, 공격범위에 존재하는 경우
			lua_pushinteger(L, attack_target);
			lua_pushinteger(L, target);
			return 2;
		}
		else
		{
			//3-2 플레이어가 아직 살아있고, 공격범위에 존재하진 않는다
			// -> 플레이어에게 접근(Astar)를 이용
			lua_pushinteger(L, trace_target);
			lua_pushinteger(L, target);
			return 2;
		}
	}
}

int EventTask::API_Task_Dead_Execute(lua_State * L)
{
	int npcID = (int)lua_tointeger(L, -2);/**< NPCID */
	bool isAlive = lua_toboolean(L, -1); /**< 생존상태 변수*/
	lua_pop(L, 2);
	/// TODO : 몬스터가 죽으면 아이템 줘야되지 않을까?
#ifndef DropItemON
	int tmpItemCode = -1;
	if (ItemManager::getInstance().GetDropItem(NPCManager::getInstance()[npcID].mNPCType, tmpItemCode))
	{
		/// ITEM Create Method
		Item * tmpItemPtr = nullptr;
		if (!ItemManager::getInstance().CreateItem(tmpItemCode, tmpItemPtr))
		{
			Log(L"** Error -- [File : %s || LINE : %d] CreateItem return false **", __FILEW__, __LINE__);
			/// TODO : 에러 처리
			while (true) {}
		}

}

#endif // !DropItemON

	NPCManager::getInstance()[npcID].SetAlive(isAlive);
	/// 몬스터가 죽었을 경우, 주변 avatar에게 아바타 삭제를 요청
	if (!isAlive)
	{
		byte fieldID = NPCManager::getInstance()[npcID].mInFieldNo;
		unordered_set<int> viewList;
		/// NPC 주변 AvatarID를 조사
		World::getInstance().InsertViewAvatarToList(npcID, viewList);
		/// ViewList안에 있는 Client들에게 removePacket Send
		for (auto & iter : viewList)
		{
			ClientManager::getInstance()[iter].CountToViewList(npcID);
			if (ClientManager::getInstance()[iter].GetSelectedAvatar()->mViewList.count(npcID) > 0)
			{
				ClientManager::getInstance()[iter].EraseToViewList(npcID);
				Session * dumySesion = nullptr;
				if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
				{
					SendRemovePlayer(npcID, dumySesion);
					continue;
				}
			}
		}
		/// 죽은 NPC를 Sector에서 삭제
		int sectorNO = NPCManager::getInstance()[npcID].GetSectorNO();
		World::getInstance().EraseSector(npcID, sectorNO);
	}
	return 0;
}

int EventTask::API_Task_Respawn(lua_State * L)
{
	int npcID = (int)lua_tointeger(L, -3);
	int pointx = (int)lua_tointeger(L, -2);
	int pointy = (int)lua_tointeger(L, -1);
	lua_pop(L, 3);

	NPCManager::getInstance()[npcID].Initialize_Respawn(pointx, pointy);

	int sectorNO = GetSectorNo(pointx, pointy);
	World::getInstance().InsertIDtoSector(npcID, sectorNO);

	byte fieldID = NPCManager::getInstance()[npcID].mInFieldNo;
	unordered_set<int> viewList;
	World::getInstance().InsertViewAvatarToList(npcID, viewList);
	for (auto & iter : viewList)
	{
		if (ClientManager::getInstance()[iter].CountToViewList(npcID) == 0)
		{
			ClientManager::getInstance()[iter].EraseToViewList(npcID);
			Session * dumySesion = nullptr;
			if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
			{
				/// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
				SendPutPlayer(npcID, dumySesion);

				continue;
			}
		}
	}
	return 0;
}

int EventTask::API_Tast_Give_Exp(lua_State * L)
{
	int clientProxyIndex = (int)lua_tointeger(L, -2); /**< 클라이언트 아바타 INDEX*/
	int monExp = (int)lua_tointeger(L, -1);	/**< 몬스터가 주는 경험치 */

	auto prevExp = ClientManager::getInstance()[clientProxyIndex].GetSelectedAvatar()->GetCurExp(); /**< 아바타의 현재 경험치*/
	auto needExp = ClientManager::getInstance()[clientProxyIndex].GetSelectedAvatar()->GetNeedExp(); /**< 아바타의 레벨업을 위한 필요경험치 */
	uint64_t sessionID = ClientManager::getInstance().GetSessionID(clientProxyIndex);
	/// 현 경험치에서 몬스터 경험치를 합친 총 경험치
	auto gettingExp = prevExp + monExp;
	/// 몬스터에게 얻은 경험치가 레벨업을 위한 경험치보다 높을 경우 레벨업을 진행해야한다.
	if (gettingExp > needExp)
	{
		ClientManager::getInstance()[clientProxyIndex].LevelUp(gettingExp, sessionID);
	}
	/// 레벨업의 조건엔 부합하지 않다면 경험치를 올리고 수정된 사항을 패킷으로 보낸다.
	else
	{
		ClientManager::getInstance()[clientProxyIndex].SetNowExp(gettingExp, sessionID);
		sc_packet_update_avatar_exp packet;
		packet.mNowExp = gettingExp;
		Session * dumySesion = nullptr;
		if (SessionManager::getInstance().GetSession(sessionID, dumySesion))
		{
			dumySesion->SendPacket(&packet);
		}
	}
	lua_pop(L, 2);
	return 0;
}

int EventTask::API_Get_Attacked_Data(lua_State * L)
{
	int npcID = (int)lua_tointeger(L, -1);
	lua_pop(L, 1);
	bool retval = false;
	int clientID = -1;
	NPCManager::getInstance()[npcID].IsWound(retval, clientID);
	lua_pushboolean(L, retval);
	lua_pushinteger(L, clientID);
	return 2;
}

int EventTask::API_Is_Dead(lua_State * L)
{
	bool retval = false;
	int npcID = (int)lua_tointeger(L, -1);
	int woundedid = -1;
	lua_pop(L, 1);

	auto hp = NPCManager::getInstance()[npcID].GetNowHp();
	if (hp == 0)
	{
		retval = true;
		woundedid = NPCManager::getInstance()[npcID].mWoundedAvatarID;
		NPCManager::getInstance()[npcID].mWoundedAvatarID = 0;
	}
	lua_pushboolean(L, retval);
	lua_pushinteger(L, woundedid);
	return 2;
}

int EventTask::API_Get_Gold(lua_State * L)
{
	int clientProxyIndex = (int)lua_tointeger(L, -2);
	int npcID = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);
	auto dropGold = NPCManager::getInstance()[npcID].GetDropGold();
	ClientManager::getInstance()[clientProxyIndex].GetSelectedAvatar()->AddGold(dropGold);

	sc_packet_update_avatar_Gold packet;
	packet.mNowGold = ClientManager::getInstance()[clientProxyIndex].GetSelectedAvatar()->GetGold();;
	Session * dumySesion = nullptr;
	if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(clientProxyIndex), dumySesion))
	{
		dumySesion->SendPacket(&packet);
	}
	return 0;
}
