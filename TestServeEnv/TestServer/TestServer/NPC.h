#pragma once
#include<random>
class NPCTypeConverter
{
public:
	const wchar_t * Execute(const byte & inNPCType);
};

static NPCTypeConverter npcConverter;

class NPC
{
public:
	NPC() : isActive(false) { mHorizontal = 0; mVertical = 1; mAlive = true; 	mWound = false; mWoundedAvatarID = -1; mLuastate = nullptr;  }
	void initialize(const byte & inNPCType, const int32_t & inNPCID, const int32_t & inHP, 
		const int32_t & inMinDamage, const int32_t & inMaxDamage, const int32_t & inTendency, 
		const int32_t & inLevel, const int & DropExp, const float & AttackSpeed, const byte & imageType,
		const int & inDropMinGold,const int & inDropMaxGold);

	void SetPointXY(int32_t inPointX, int32_t inPointY);


	void SetField(const byte & inValue) { mInFieldNo = inValue; }
	//*> �¿�
	void SetHorizontal(int inValue) { mHorizontal = inValue; }

	const int & GetHorizontal() { return mHorizontal; }
	//*> ����
	void SetVertical(int inValue) { mVertical = inValue; }

	const int & GetVertical() { return mVertical; }

	void SetAlive(bool inValue) { mAlive = inValue; }

	const bool & IsAlive() { return mAlive; }

	void IsWound(bool & retval, int & clientID);

	bool mWound;
	volatile int mWoundedAvatarID;

	const int32_t GetDropGold()const;

	const int32_t GetObjectDamage()const;

	template<class T>
	bool CAS(volatile int32_t * next, T old_value, T new_value)
	{
		return atomic_compare_exchange_strong(reinterpret_cast<volatile atomic_int *>(next), &old_value, new_value);
	}
	const int GetSectorNO()
	{
		int no = GetSectorNo(mPointX, mPointY);
		return no;
	}
	void AddNowHp(const int32_t & inValue);

	const volatile int32_t & GetNowHp(){return mCurrentHP;}

	void Initialize_Respawn(const int & inPointx, const int & inPointy);

	byte	mImageType;
	byte	mInFieldNo;
	byte	 mNPCType;
	int32_t mNPCID;
	wstring  mName;
	int32_t  mMaxHP;
	int32_t  mMinDamage;
	int32_t  mMaxDamage;
	int32_t  mPointX;
	int32_t  mPointY;
	uint64_t mLastEventTick;
	int32_t  mDropMinGold;
	int32_t	 mDropMaxGold;
	lua_State * mLuastate;
	byte	mTendency;
	float	mAttackSpeed;
	// volatile bool isActive;
	atomic<bool> isActive;
	bool mAlive = true;
	int mLevel;
	int mExp;
	int			mVertical;
	int			mHorizontal;
private:
	volatile int32_t  mCurrentHP;
};

