#include "stdafx.h"

bool DB_Event_Login::Execute(OverlappedDB_t * inOverlapEx)
{
	mQuery.SetQuery(L"call proc_Login(?, ?, ?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;

	/// in Bind param to ID
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)id.c_str(), id.size(), &cbParm);
	/// in Bind param to PW
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)pw.c_str(), pw.size(), &cbParm);
	/// out Bind param to 결과
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &result, 0, &cbParm);
	/// out Bind param to 디비에 저장된 UID
	retval = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &uid, 0, &cbParm);


	SQLRETURN sqlResult = SQLExecDirect(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	/// 만약 DB에서 에러가 발생할 시 에러체크 
	if (sqlResult == -1)
	{
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
	}

	/// 결과과 성공적으로 이루어지면 뒷 작업을 workerThread로 전달
	if (result == 1)
	{
		BYTE result = DB_LoginSucc;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));

		offset += sizeof(result);

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&uid, sizeof(uid));
	}
	else
	{
		// LoginFail
		BYTE result = DB_LoginFail;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));
	}
	return true;
}

bool DB_Event_ShowAvatar::Execute(OverlappedDB_t * inOverlapEx)
{
	int clientKey = -1;
	/// SessionID -> ClientProxy Index 얻는 과정, 얻지 못한다면 ClientProxy value값이 저장이 안된경우 -> 강제 종료 or 로그아웃으로 간주
	if (!ClientManager::getInstance().ConvertSessionIDToClientKey(this->GetUniqueKey(), clientKey)) {
		Log(L"** DB_Event_ShowAvatar -> Not Found Client Key");
		return false;
	}
	/// 기존의 프로필을 제거
	ClientManager::getInstance()[clientKey].ClearProfile();

	mQuery.SetQuery(L"call sp_ShowMyAvatars(?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	int retcode = -2;
	/// in Bind Param 디비에 저장되어있는 UID
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &uid, 0, &cbParm);
	/// out Bind param SP의 결과
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);

	SQLRETURN sqlResult = SQLExecDirect(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);


	/// retcode = -1 -> 아직 아바타가 없다는 의미
	if (retcode == -1)
	{
		BYTE result = DB_ShowAvatarLists;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));
		return true;
	}

	int					avatarId = -1;
	BYTE				classType = -1;
	array<wchar_t, 50> name;
	int					level = -1;
	int					fieldID = -1;

	SQLLEN len = 0;
	SQLRETURN retsql = NULL;
	/// BindCol DB에 저장된 (PK) AvatarID
	retsql = SQLBindCol(mQuery.getstmt(), 1, SQL_C_LONG, &avatarId, sizeof(avatarId), &len);
	/// BindCol DB에 저장된 아바타 이름
	retsql = SQLBindCol(mQuery.getstmt(), 2, SQL_C_WCHAR, &name.front(), name.size() * sizeof(WCHAR), &len);
	/// BindCol 아바의 직업
	retsql = SQLBindCol(mQuery.getstmt(), 3, SQL_C_TINYINT, &classType, sizeof(classType), &len);
	/// BindCol 아바타의 레벨
	retsql = SQLBindCol(mQuery.getstmt(), 4, SQL_C_LONG, &level, sizeof(level), &len);
	/// BindCol 아바타의 현재 필드정보
	retsql = SQLBindCol(mQuery.getstmt(), 5, SQL_C_LONG, &fieldID, sizeof(fieldID), &len);

	while (true)
	{
		retsql = SQLFetch(mQuery.getstmt());
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, retsql);
		if (retsql == SQL_NO_DATA) {
			BYTE result = DB_ShowAvatarLists;
			int offset = 0;
			memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
				&result, sizeof(result));
			return true;
		}

		DB_AvatarList_Profile data;
		data.avatar_id = avatarId;
		data.classType = classType;
		wstring names = name.data();
		data.name = names;
		data.level = level;
		data.fieldID = fieldID;

		ClientManager::getInstance()[clientKey].InsertProfile(avatarId, move(data));
	}

}

bool DB_Evnet_EnterGame::Execute(OverlappedDB_t * inOverlapEx)
{
	mRetcode = -1;
	int clientKey = -1;

	/// SessionID -> ClientProxy Index 얻는 과정 얻지 못한다면 ClientProxy value값이 저장이 안된경우 -> 강제 종료 or 로그아웃으로 간주
	if (!ClientManager::getInstance().ConvertSessionIDToClientKey(GetUniqueKey(), clientKey)) {
		Log(L"** DB_Event_ShowAvatar -> Not Found Client Key");
		return false;
	}

	mQuery.SetQuery(L"call SP_EnnterIngame(?, ?, ?)");

	SQLLEN cbParm = SQL_NTS;

	SQLRETURN sqlResult;
	/// in Bind Param 디비에 저장되어있는 avatarID
	sqlResult = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mAvatarId, 0, &cbParm);
	/// out Bind Param 프로시져 실행 결과
	sqlResult = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mRetcode, 0, &cbParm);
	/// out Bind Param 로그인 성공시 로그인시간을 갖는 LogID -> TODO : 서버에서 갖고 있는뒤 유저가 나갈때 기록해야 한다.
	sqlResult = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mloginLogID, 0, &cbParm);

	sqlResult = SQLExecDirect(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);

	SQLLEN len = 0;


	wstring avatarName;
	avatarName.resize(50);
	sqlResult = SQLBindCol(mQuery.getstmt(), 1, SQL_C_WCHAR, &avatarName.front(), avatarName.capacity() * sizeof(wchar_t), &len);
	int avatarlevel = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 2, SQL_C_LONG, &avatarlevel, sizeof(avatarlevel), &len);

	int avatarGold = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 3, SQL_C_LONG, &avatarGold, sizeof(avatarGold), &len);

	int64_t avatarCurExp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 4, SQL_C_SBIGINT, &avatarCurExp, sizeof(int), &len);
	byte ClassType = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 5, SQL_C_TINYINT, &ClassType, sizeof(byte), &len);
	int Str = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 6, SQL_C_LONG, &Str, sizeof(int), &len);
	int Dex = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 7, SQL_C_LONG, &Dex, sizeof(int), &len);
	int Ints = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 8, SQL_C_LONG, &Ints, sizeof(int), &len);
	int Luck = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 9, SQL_C_LONG, &Luck, sizeof(int), &len);
	int Maxhp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 10, SQL_C_LONG, &Maxhp, sizeof(int), &len);
	int MaxMp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 11, SQL_C_LONG, &MaxMp, sizeof(int), &len);
	int CurrHp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 12, SQL_C_LONG, &CurrHp, sizeof(int), &len);
	int CurrMp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 13, SQL_C_LONG, &CurrMp, sizeof(int), &len);
	int AvatarField = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 14, SQL_C_LONG, &AvatarField, sizeof(int), &len);
	int pointX = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 15, SQL_C_LONG, &pointX, sizeof(int), &len);
	int pointY = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 16, SQL_C_LONG, &pointY, sizeof(int), &len);
	int64_t needExp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 17, SQL_C_SBIGINT, &needExp, sizeof(int), &len);

	while (true)
	{
		sqlResult = SQLFetch(mQuery.getstmt());
		if (sqlResult == -1) {
			HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
			return false;
		}
		if (sqlResult == SQL_NO_DATA) {
			BYTE result = DB_EnterWorld_succ;
			int offset = 0;

			memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
				&result, sizeof(result));
			return true;
		}
		ClientManager::getInstance()[clientKey].SetAvatarInitialize(mAvatarId, avatarName, avatarlevel, avatarGold, avatarCurExp, ClassType, Str, Dex, Ints, Luck, Maxhp, MaxMp, CurrHp, CurrMp, AvatarField, pointX, pointY, needExp);
	}
}

bool DB_Event_CreateAvatar::Execute(OverlappedDB_t * inOverlapEx)
{
	retcode = -1;

	mQuery.SetQuery(L"call SP_CreateAvatars(?, ?, ?, ?)");
	SQLLEN cbParm = SQL_NTS;
	SQLRETURN sqlResult = NULL;
	sqlResult = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_TINYINT, SQL_TINYINT, 0, 0, &avatar_class, 0, &cbParm);

	sqlResult = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)avatar_name.c_str(), 0, &cbParm);

	sqlResult = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &user_id, 0, &cbParm);

	sqlResult = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	sqlResult = SQLExecute(mQuery.getstmt());

	if (retcode == -2) {
		wcout << L"같은 이름의 아바타가 존재합니다." << endl;
		BYTE result = DB_CreateAvatarFail_SAMEAVATARNAME;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));

	}
	else if (retcode == -3)
	{
		wcout << L"최대 9개까지 캐릭터 생성이 가능합니다." << endl;
		BYTE result = DB_CreateAvatarFail_FULLAVATARINDEX;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));

	}

	else if (retcode == 1)
	{
		wcout << L"캐릭터 생성 성공" << endl;
		BYTE result = DB_CreateAvatarSucc;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));
	}
	return true;
}

bool DB_Event_LevelUp::Execute(OverlappedDB_t * inOverlapEx)
{
	int retcode = -1;
	mQuery.SetQuery(L"call SP_AvatarLevelUp(?, ?, ?, ?, ?, ?, ? ,?, ?,?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	int level = 0;
	int nowExp = 0;
	int needExp = 0;
	int nowHP = 0;
	int nowMP = 0;
	int maxHP = 0;
	int maxMP = 0;

	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mAvatarid, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mNowExp, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &level, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &nowExp, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 5, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &needExp, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 6, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &nowHP, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 7, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &nowMP, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 8, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &maxHP, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 9, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &maxMP, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 10, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	SQLRETURN sqlResult = SQLExecute(mQuery.getstmt());
	if (sqlResult == -1)
	{
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
	}
	if (retcode == -2)
	{
		Log(L"** DB_Event_LevelUp Execute -> SQL Rollback !");
		/// inOverlapEx->result_value = DB_LevelUp_Fail;
		BYTE result = DB_LevelUp_Fail;
		int offset = 0;

		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));
		return true;
	}
	/// inOverlapEx->result_value = DB_LevelUp_succ;

	int offset = 0;
	BYTE result = DB_LevelUp_succ;
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
		&result, sizeof(result));

	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &level, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &nowExp, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &needExp, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &nowHP, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &nowMP, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &maxHP, sizeof(level));
	offset += sizeof(int);
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf), &maxMP, sizeof(level));

	return true;
}

bool DB_Event_UpdateNowExp::Execute(OverlappedDB_t * inOverlapEx)
{
	int retcode = -1;
	mQuery.SetQuery(L"call SP_UpdateNowExp(?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mAvatarid, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mNowExp, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	SQLRETURN sqlResult = SQLExecute(mQuery.getstmt());
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
	return false;
}

bool DB_Event_UpdateAvatarLocation::Execute(OverlappedDB_t * inOverlapEx)
{
	retcode = -1;
	mQuery.SetQuery(L"call SP_AvatarLocationUpdate(?, ?, ?, ?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &avatar_id, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &local_x, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &local_y, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &fieldID, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 5, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 100, 0, &retcode, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	SQLRETURN sqlResult = SQLExecute(mQuery.getstmt());
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
	if (retcode == 0)
	{
		return true;
	}
	return true;
}

bool DB_Event_LeaveGame::Execute(OverlappedDB_t * inOverlapEx)
{
	retcode = -1;
	mQuery.SetQuery(L"? = call proc_leave_ingame(?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &avatar_id, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &loginLog_ID, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	SQLRETURN ret = SQLExecute(mQuery.getstmt());
	HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, ret);
	if (retcode == 0)
		cout << "Logout succ!" << endl;
	if (retcode == -1)
		cout << "Logout fail!" << endl;
	return true;
}

bool DB_Event_Join::Execute(OverlappedDB_t * inOverlapEx)
{
	mQuery.SetQuery(L"call proc_Join(?, ?, ?, ?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	wchar_t message[50];
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)id.c_str(), id.length() * sizeof(wchar_t), &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)pw.c_str(), pw.length() * sizeof(wchar_t), &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_INPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, (SQLPOINTER)email.c_str(), email.length() * sizeof(wchar_t), &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 5, SQL_PARAM_OUTPUT, SQL_C_WCHAR, SQL_WVARCHAR, 50, 0, message, sizeof(message), &cbParm);

	SQLRETURN ret = SQLExecDirect(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	if (retcode == -1) {
		/// inOverlapEx->result_value = DB_JoinFail;
		int offset = 0;
		BYTE result = DB_JoinFail;
		memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
			&result, sizeof(result));
		return true;
	}
	/// inOverlapEx->result_value = DB_JoinSucc;

	int offset = 0;
	BYTE result = DB_JoinSucc;
	memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
		&result, sizeof(result));

	return true;
}

bool DB_Evnet_EnterGame_with_NoHotspot::Execute(OverlappedDB_t * inOverlapEx)
{
	int retcode = -1;
	mQuery.SetQuery(L"call SP_AvatarLocationUpdate(?, ?, ?, ?, ?)");
	SQLLEN cbParm = SQL_NTS;
	int retval = 0;
	retval = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mAvatarId, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mWarpx, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mWarpy, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 4, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mFieldID, 0, &cbParm);
	retval = SQLBindParameter(mQuery.getstmt(), 5, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 100, 0, &retcode, 0, &cbParm);
	SQLLEN len = 0;
	SQLPrepare(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	SQLRETURN sqlResult = SQLExecute(mQuery.getstmt());
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);

	///////////////////////////////////////////////////////////////////////////////
	retcode = -1;
	int clientKey = -1;

	/// SessionID -> ClientProxy Index 얻는 과정 얻지 못한다면 ClientProxy value값이 저장이 안된경우 -> 강제 종료 or 로그아웃으로 간주
	if (!ClientManager::getInstance().ConvertSessionIDToClientKey(GetUniqueKey(), clientKey)) {
		Log(L"** DB_Event_ShowAvatar -> Not Found Client Key");
		return false;
	}

	mQuery.Clear();
	mQuery.SetQuery(L"call SP_EnnterIngame(?, ?, ?)");

	cbParm = SQL_NTS;

	/// in Bind Param 디비에 저장되어있는 avatarID
	sqlResult = SQLBindParameter(mQuery.getstmt(), 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mAvatarId, 0, &cbParm);
	/// out Bind Param 프로시져 실행 결과
	sqlResult = SQLBindParameter(mQuery.getstmt(), 2, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &retcode, 0, &cbParm);
	/// out Bind Param 로그인 성공시 로그인시간을 갖는 LogID -> TODO : 서버에서 갖고 있는뒤 유저가 나갈때 기록해야 한다.
	sqlResult = SQLBindParameter(mQuery.getstmt(), 3, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &mloginLogID, 0, &cbParm);

	sqlResult = SQLExecDirect(mQuery.getstmt(), (SQLWCHAR *)mQuery.GetQuery(), SQL_NTS);
	if (sqlResult == -1)
		HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);

	len = 0;


	wstring avatarName;
	avatarName.resize(20);
	sqlResult = SQLBindCol(mQuery.getstmt(), 1, SQL_C_WCHAR, &avatarName.front(), avatarName.capacity() * sizeof(wchar_t), &len);
	int avatarlevel = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 2, SQL_C_LONG, &avatarlevel, sizeof(avatarlevel), &len);

	int avatarGold = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 3, SQL_C_LONG, &avatarGold, sizeof(avatarGold), &len);

	int64_t avatarCurExp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 4, SQL_C_SBIGINT, &avatarCurExp, sizeof(int), &len);
	byte ClassType = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 5, SQL_C_TINYINT, &ClassType, sizeof(byte), &len);
	int Str = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 6, SQL_C_LONG, &Str, sizeof(int), &len);
	int Dex = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 7, SQL_C_LONG, &Dex, sizeof(int), &len);
	int Ints = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 8, SQL_C_LONG, &Ints, sizeof(int), &len);
	int Luck = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 9, SQL_C_LONG, &Luck, sizeof(int), &len);
	int Maxhp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 10, SQL_C_LONG, &Maxhp, sizeof(int), &len);
	int MaxMp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 11, SQL_C_LONG, &MaxMp, sizeof(int), &len);
	int CurrHp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 12, SQL_C_LONG, &CurrHp, sizeof(int), &len);
	int CurrMp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 13, SQL_C_LONG, &CurrMp, sizeof(int), &len);
	int AvatarField = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 14, SQL_C_LONG, &AvatarField, sizeof(int), &len);
	int pointX = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 15, SQL_C_LONG, &pointX, sizeof(int), &len);
	int pointY = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 16, SQL_C_LONG, &pointY, sizeof(int), &len);
	int64_t needExp = 0;
	sqlResult = SQLBindCol(mQuery.getstmt(), 17, SQL_C_SBIGINT, &needExp, sizeof(int), &len);

	while (true)
	{
		sqlResult = SQLFetch(mQuery.getstmt());
		if (sqlResult == -1) {
			HandleDiagnosticRecord(mQuery.getstmt(), SQL_HANDLE_STMT, sqlResult);
			return false;
		}
		if (sqlResult == SQL_NO_DATA) {
			/// inOverlapEx->result_value = DB_EnterWorld_succ;
			int offset = 0;
			BYTE result = DB_EnterWorld_succ;
			memcpy_s(inOverlapEx->transBuf + offset, sizeof(inOverlapEx->transBuf),
				&result, sizeof(result));
			return true;
		}
		ClientManager::getInstance()[clientKey].SetAvatarInitialize(mAvatarId, avatarName, avatarlevel, avatarGold, avatarCurExp, ClassType, Str, Dex, Ints, Luck, Maxhp, MaxMp, CurrHp, CurrMp, AvatarField, pointX, pointY, needExp);
	}


	if (retcode == 0)
	{
		return true;
	}
	return true;
}
