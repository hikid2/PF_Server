#pragma once
inline void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode) {
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];
	if (RetCode == SQL_INVALID_HANDLE) {
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	} while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage, (SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT *)NULL) == SQL_SUCCESS) {
		// Hide data truncated.. 
		if (wcsncmp(wszState, L"01004", 5)) {
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}

enum Enum_DB_Result
{
	DB_LoginSucc,
	DB_LoginFail,
	DB_JoinSucc,
	DB_JoinFail,
	DB_ShowAvatarLists,
	DB_CreateAvatarSucc,
	DB_CreateAvatarFail_SAMEAVATARNAME,
	DB_CreateAvatarFail_FULLAVATARINDEX,
	DB_EnterWorld_succ,
	DB_LevelUp_succ,
	DB_LevelUp_Fail,
};

class DBEvent
{
public:
	DBEvent(){}

	virtual ~DBEvent() {}

	virtual bool Execute(OverlappedDB_t * inOverlapEx) = 0;

	virtual bool IsNextDispose() = 0;

	const uint64_t & GetUniqueKey() { return mUniqueKey; }


	void SetUniqueID(const uint64_t & inValue) { mUniqueKey = inValue; }
	Query & GetQuery() { return mQuery; }

protected:
	uint64_t mUniqueKey;	///*> 유저의 경우는 SessionID AI의 경우는 NCP Key
	Query mQuery;
};



class DB_Event_Login : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);

	virtual bool IsNextDispose() { return true; }

	void SetId(const wstring & inValue) { id = inValue; }

	void SetPW(const wstring & inValue) { pw = inValue; }

private:
	wstring id;
	wstring pw;
	int result;
	int uid;
};


class DB_Event_ShowAvatar : public DBEvent
{
public:

	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	virtual bool IsNextDispose() { return true; }
	void SetUid(const int inValue) { uid = inValue; }
private:
	int uid;

};

class DB_Event_Join : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	void Setid(const wstring & inValue) { id = inValue; }
	void Setpw(const wstring & inValue) { pw = inValue; }
	void Setemail(const wstring & inValue) { email = inValue; }
	virtual bool IsNextDispose() { return true; }
private:
	wstring id;
	wstring pw;
	wstring email;
	int retcode;

};

class DB_Evnet_EnterGame_with_NoHotspot : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	virtual bool IsNextDispose() { return true; }
	void SetAvatarId(const int & inValue) { mAvatarId = inValue; }
	void SetWarpx(const int & inValue) { mWarpx = inValue; }
	void SetWarpy(const int & inValue) { mWarpy = inValue; }
	void SetFieldID(const int & inValue) { mFieldID = inValue; }
private:
	int mloginLogID;
	int mRetcode;
	int mAvatarId;
	int mLoginLogID;
	int mWarpx;
	int mWarpy;
	int mFieldID;
};


class DB_Evnet_EnterGame : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	void SetAvatarId(const int inValue) { mAvatarId = inValue; }
	virtual bool IsNextDispose() { return true; }
private:
	int mRetcode;
	int mAvatarId;
	int mloginLogID;
};

class DB_Event_LeaveGame : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	void SetAvatarId(const int inValue) { avatar_id = inValue; }
	void SetLoginLogID(const int inValue) { loginLog_ID = inValue; }
	virtual bool IsNextDispose() { return false; }
private:
	int retcode;
	int avatar_id;
	int loginLog_ID;
};

class DB_Event_CreateAvatar : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	void SetUserid(const int inValue) { user_id = inValue; }
	void SetAvatarClass(const byte inValue) { avatar_class = inValue; }
	void SetAvatarName(const wstring & inValue) { avatar_name = inValue; }
	virtual bool IsNextDispose() { return true; }
private:
	int retcode;
	int user_id;
	byte avatar_class;
	wstring avatar_name;
};

class DB_Event_UpdateAvatarLocation : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	void SetAvatarID(int inValue) { avatar_id = inValue; }
	void SetFieldID(int inValue) { fieldID = inValue; }
	void SetLocalX(int inValue) { local_x = inValue; }
	void SetLocalY(int inValue) { local_y = inValue; }
	virtual bool IsNextDispose() { return false; }
private:
	int retcode;
	int avatar_id;
	int local_x;
	int local_y;
	int fieldID;
};

class DB_Event_LevelUp : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	virtual bool IsNextDispose() { return true; }
	void SetNowExp(const int & inValue) { mNowExp = inValue; }
	void SetAvatarID(const int & inValue) { mAvatarid = inValue; }
private:
	int mRetcode = -1;
	int mAvatarid;
	int mNowExp;
};

class DB_Event_UpdateNowExp : public DBEvent
{
public:
	virtual bool Execute(OverlappedDB_t * inOverlapEx);
	virtual bool IsNextDispose() { return false; }
	void SetNowExp(const int & inValue) { mNowExp = inValue; }
	void SetAvatarID(const int & inValue) { mAvatarid = inValue; }
private:
	int mAvatarid;
	int mNowExp;
};