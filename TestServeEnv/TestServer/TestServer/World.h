#pragma once
#include<set>
enum ENUM_WARPID :  BYTE
{
	RESPAWN_STARTTOWN,	///*> 죽었을 시 마을로 돌아가는  OR 강제적으로 마을에 가야할 경우
	STARTFIELD_DESERT,	///*> 시작의 마을 -> 사막 필드로 가야할 때
	DESERT_DUNGEON,		///*> 사막필드 -> 던전 입구
	DESERT_STARTFIELD,	///*> 사막필드 -> 시작의마을 필드로 이동
	DUNGEON_DESERT,		///*> 던전 -> 사막으로 이동
	WARPID_NOT_YET,
};

struct SectorCompare
{
	static size_t hash(const uint64_t & x)
	{
		size_t h = 0;
		h = x % 12400;
		return h;
	}

	static bool equal(const uint64_t & inLeft, const uint64_t & inRight)
	{
		return inLeft == inRight;
	}
};


typedef PathNode * PathNodePtr;

struct FValueComapre
{
	inline bool operator()(PathNodePtr left, PathNodePtr right)
	{
		return (left->GetF() > right->GetF());
	}
};

struct PathValueCompare
{
	inline bool operator()(PathNodePtr left, PathNodePtr right)
	{
		return (left->GetPointX() + left->GetPointY() * 10) < (right->GetPointX() + right->GetPointY() * 10);
	}
};
class World : public Singleton<World>
{
	
public:
	World() { initialize(); }

	virtual ~World() {}

	void initialize();

	bool IsCollisionToTerrain(const int32_t & inPositionX, const int32_t & inPositionY, const BYTE & inFiled);

	/**
	 * @brief 직선상 이동이 가능한지 확인.
	 * 
	 * @param inStart 
	 * @param inTarget 
	 * @param inFiled 
	 * @param nowMovePoint 
	 * @return true 
	 * @return false 
	 */
	bool IsOneLinePath(const POINT & inStart, const POINT & inTarget, const BYTE & inFiled, POINT & nowMovePoint);

	bool UpdateIDSector(const int & inObjectID, const int & inPrevSectorNO, const int & inNextSectorNO);

	bool EraseSector(const int & inTargetID, const int & inSectorNO);

	int MaxMinRange(int target, int num_min = 0, int num_max = 100);

	void NextViewUpdate(Session * inSessionPtr, int clientIndex);

	void InsertIDtoSector(const int & inClientIndex, const int & inSectorNO);

	bool TestAstar(const byte & inFieldID, const POINT & inStart, const POINT & inTarget, POINT & outPoint, POINT & outNextPoint);

	bool IsWarpZone(const int & inPointx, const int & inPointy, const byte & inFieldID, ENUM_WARPID & outFieldID);

	void DoWarpField(const byte & inWarpID, int & outPointx, int & outPointy, byte & outFieldID);

	void InsertViewAllObjectToList(const int & inOrigin, unordered_set<int> & outList);
	void InsertViewAvatarToList(const int & inOrigin, unordered_set<int>& outList);

private:
	void InsertNearSectorNO(const int & inPointX, const int & inPointY, unordered_set<int> & inSectorNOList)
	{
		int no = -1;
		for (auto i = 0; i < 4; ++i)
		{
			switch (i)
			{
			case 0:
				no = GetSectorNo(inPointX - 5, inPointY - 5); break;
			case 1:
				no = GetSectorNo(inPointX + 5, inPointY + 5); break;
			case 2:
				no = GetSectorNo(inPointX - 5, inPointY + 5); break;
			case 3:
				no = GetSectorNo(inPointX + 5, inPointY - 5); break;
			}
			if (no < 0 || no >= 400)
				continue;
			inSectorNOList.insert(no);
		}
	}
	void InsertCollisions(array<array<BYTE, 200>, 200> & inField, const Value & inJsonValue);

	void SpawnMonster(array<array<BYTE, 200>, 200> & inField, int inFieldIIndex);
	
	int Getheuristics(PathNode * tmp, const int targetx, const int targety);

	int Getheuristics(PathNode * originPtr, PathNode *  targetPtr);

	array<array<BYTE, 200>, 200> mStartTown;

	array<array<BYTE, 200>, 200> mDesert;

	array<array<BYTE, 200>, 200> mDungeon;

	typedef array<array<BYTE, 200>, 200> Field_t;

	Sector mSectorArray[20][20];

	void InsertAllObjectFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList);
	void InsertAvatarFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList);
	void InsertNPCFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList);

};