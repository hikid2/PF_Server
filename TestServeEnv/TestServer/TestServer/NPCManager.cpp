#include "stdafx.h"

/**
 * @brief
 *
 */
void NPCManager::Initialize()
{
	for (int no = NPCStart; no < NPCEnd; ++no)
		mFreeNPCQueue.push(no);
	Log(L"  ** Free NPC INDEX Push Task Success !");
}

/**
 * @brief
 *
 * @param inSpawnPointX
 * @param inSpawnPointY
 * @param inFieldIIndex
 * @param inSpawnBit
 * @return int
 */
int NPCManager::SetupLua(int inSpawnPointX, int inSpawnPointY, byte inFieldIIndex, BYTE inSpawnBit)
{
	int freeIndex = -1;

	while (true) {
		if (mFreeNPCQueue.try_pop(freeIndex))
		{
			mNPCContainer[freeIndex].SetField(inFieldIIndex);
			mNPCContainer[freeIndex].mLuastate = luaL_newstate();

			luaL_openlibs(mNPCContainer[freeIndex].mLuastate);
			auto error = luaL_loadfile(mNPCContainer[freeIndex].mLuastate, "monster.lua");
			lua_pcall(mNPCContainer[freeIndex].mLuastate, 0, 0, 0);


			lua_getglobal(mNPCContainer[freeIndex].mLuastate, "Get_MonsterDefineData");
			lua_pushnumber(mNPCContainer[freeIndex].mLuastate, inFieldIIndex);
			lua_pushnumber(mNPCContainer[freeIndex].mLuastate, inSpawnBit);
			lua_pcall(mNPCContainer[freeIndex].mLuastate, 2, 11, 0);
			// return myMon.MID, myMon.level, myMon.MinDamage, myMon.MaxDamage, myMon.HP, myMon.MonsterType, myMon.DropExp, myMon.AttackSpeed
			int typeID = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -11);
			int level = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -10);
			int minDamage = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -9);
			int maxDamage = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -8);
			int hp = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -7);
			int tendency = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -6);
			int dropExp = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -5);
			float attackSpeed = lua_tonumber(mNPCContainer[freeIndex].mLuastate, -4);
			int imageType = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -3);

			int dropMinGold = lua_tonumber(mNPCContainer[freeIndex].mLuastate, -2);
			int  dropMaxGold = lua_tointeger(mNPCContainer[freeIndex].mLuastate, -1);

			mNPCContainer[freeIndex].initialize(typeID, freeIndex,
				hp, minDamage, maxDamage, tendency, level, dropExp,
				attackSpeed, imageType, dropMinGold, dropMaxGold);
			lua_pop(mNPCContainer[freeIndex].mLuastate, 9);

			lua_getglobal(mNPCContainer[freeIndex].mLuastate, "set_spawnlocation");
			lua_pushnumber(mNPCContainer[freeIndex].mLuastate, inSpawnPointX);
			lua_pushnumber(mNPCContainer[freeIndex].mLuastate, inSpawnPointY);
			lua_pcall(mNPCContainer[freeIndex].mLuastate, 2, 0, 0);
			lua_pop(mNPCContainer[freeIndex].mLuastate, 2);
			mNPCContainer[freeIndex].SetPointXY(inSpawnPointX, inSpawnPointY);
			/// mNPCContainer[freeIndex].SetActive(false);

			lua_getglobal(mNPCContainer[freeIndex].mLuastate, "set_uid");
			lua_pushnumber(mNPCContainer[freeIndex].mLuastate, freeIndex);
			lua_pcall(mNPCContainer[freeIndex].mLuastate, 1, 0, 0);
			lua_pop(mNPCContainer[freeIndex].mLuastate, 1);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_viewin_range", &EventTask::API_Task_View_In_Range);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_add_EventTimerEvent", &EventTask::API_Task_AddTimer_Event);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_trace_range", &EventTask::API_Task_Trace_Is_In_Range);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_search_Near_Avatars", &EventTask::API_Task_Search_Near_Avatar);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Task_Search_Near_Avatar_Get", &EventTask::API_Task_Search_Near_Avatar_Get);

			lua_register(mNPCContainer[freeIndex].mLuastate, "API_AI_Move_Roaming", &EventTask::API_Task_Roaming);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Active_false", &EventTask::API_Active_false);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_AI_Tracing", &EventTask::API_Task_Trace);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_AI_ATTACK_TARGET", &EventTask::API_Task_Attack);

			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Dead_Execute", &EventTask::API_Task_Dead_Execute);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Task_Respawn", &EventTask::API_Task_Respawn);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Give_Exp", &EventTask::API_Tast_Give_Exp);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Is_Wounded", &EventTask::API_Get_Attacked_Data);
			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Is_Dead", &EventTask::API_Is_Dead);

			lua_register(mNPCContainer[freeIndex].mLuastate, "API_Give_Gold", &EventTask::API_Get_Gold);
			/// lua_register(mNPCContainer[freeIndex].mLuastate, "API_Is_Dead", &EventTask::API_Is_Dead);

			return freeIndex;
		}
	}
	return freeIndex;
}

/**
 * @brief
 *
 * @param id
 * @return true
 * @return false
 */
bool NPCManager::IsNPC(int id)
{
	if (NPCStart <= id && NPCEnd > id)
		return true;
	return false;

}

/**
 * @brief
 *
 * @param inClientIndex
 * @param inNPCID
 * @return true
 * @return false
 */
bool NPCManager::IsinDistance(int inClientIndex, int inNPCID)
{
	auto avater = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar();
	auto & npc = mNPCContainer[inNPCID];

	int dist = (avater->GetAvatarPositionX() - npc.mPointX)
		*(avater->GetAvatarPositionX() - npc.mPointX)
		+ (avater->GetAvatarPositionY() - npc.mPointY)
		* (avater->GetAvatarPositionY() - npc.mPointY);

	return dist <= avater->GetVisibleDistance() * avater->GetVisibleDistance();
}

/**
 * @brief
 *
 * @param inClientIndex
 * @param inNPCID
 * @param inRange
 * @return true
 * @return false
 */
bool NPCManager::IsinDistance(int inClientIndex, int inNPCID, int inRange)
{
	auto avater = ClientManager::getInstance()[inClientIndex].GetSelectedAvatar();
	auto & npc = mNPCContainer[inNPCID];

	int dist = (avater->GetAvatarPositionX() - npc.mPointX)
		*(avater->GetAvatarPositionX() - npc.mPointX)
		+ (avater->GetAvatarPositionY() - npc.mPointY)
		* (avater->GetAvatarPositionY() - npc.mPointY);

	return dist <= inRange * inRange;
}

void NPCManager::RandomMove(const int & inNPCID, int & outTmpx, int & outTmpy)
{
	random_device rn;

	mt19937_64 rnd(rn());
	uniform_int_distribution<int> range(0, 3);
	int dir = range(rnd);

	if (dir == 0)
	{
		outTmpy -= 1;
		mNPCContainer[inNPCID].mHorizontal = 0;
		mNPCContainer[inNPCID].mVertical = -1;
	}
	if (dir == 1)
	{
		outTmpy += 1;
		mNPCContainer[inNPCID].mHorizontal = 0;
		mNPCContainer[inNPCID].mVertical = 1;
	}
	if (dir == 2)
	{
		outTmpx -= 1;
		mNPCContainer[inNPCID].mHorizontal = -1;
		mNPCContainer[inNPCID].mVertical = 0;
	}
	if (dir == 3)
	{
		outTmpx += 1;
		mNPCContainer[inNPCID].mHorizontal = 1;
		mNPCContainer[inNPCID].mVertical = 0;
	}
	if (outTmpx < 0) outTmpx = 0;
	else if (outTmpx >= 200) outTmpx = 199;
	if (outTmpy < 0) outTmpy = 0;
	else if (outTmpy >= 200) outTmpy = 199;
}

/// TODO : 
void NPCManager::Hit(const int & inNPCId, const int & inTargetID, const int & inDamage)
{
	mNPCContainer[inNPCId].AddNowHp(-inDamage);
	auto hp = mNPCContainer[inNPCId].GetNowHp();
	if (hp < 1)
	{
		mNPCContainer[inNPCId].mWoundedAvatarID = inTargetID;
		/// Log(L"** NPC Hit -> Dead NPC ID : %d , HP ; %d  ", inNPCId, hp);
		return;
	}
	mNPCContainer[inNPCId].mWound = true;
	mNPCContainer[inNPCId].mWoundedAvatarID = inTargetID;
	/// 공격은 내가 했지만... 체력상태는 시야에 보이는 인원모두에게 보여줘야 하지 않을까?

	/// Log(L"** NPC Hit -> NPC ID : %d , HP ; %d  ", inNPCId, hp);
}
