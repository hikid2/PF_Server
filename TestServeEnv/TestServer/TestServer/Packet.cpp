#include "stdafx.h"

void PacketAnalyze::Analyze(Session * sessionPtr, ReadStream & stream)
{
	wstring packetName = L"[Packet Type : ";
	byte packetType = 0;
	stream.Read(&packetType);
	switch (packetType)
	{
	case SC_LOGIN_OK:
		packetName += L"SC_LOGIN_OK";
		break;
	case SC_LOGIN_FAIL:
		packetName += L"SC_LOGIN_FAIL";
		break;
	case SC_INPUT:
		packetName += L"SC_INPUT";
		break;
	case SC_PUT_PLAYER:
		packetName += L"SC_PUT_PLAYER";
		break;
	case SC_REMOVE_PLAYER:
		packetName += L"SC_REMOVE_PLAYER";
		break;
	case SC_MOVE_PLAYER:
		packetName += L"SC_MOVE_PLAYER";
		break;
	case SC_LOGIN_REQ:
		packetName += L"SC_LOGIN_REQ";
		break;
	case SC_JOIN_REQ:
		packetName += L"SC_JOIN_REQ";
		break;
	case SC_JOIN_SUCC:
		packetName += L"SC_JOIN_SUCC";
		break;
	case SC_JOIN_FAIL:
		packetName += L"SC_JOIN_FAIL";
		break;
	case SC_AVATAR_LIST:
		packetName += L"SC_AVATAR_LIST";
		break;
	case SC_LOGIN_SUCC:
		packetName += L"SC_LOGIN_SUCC";
		break;
	case SC_CREATE_AVATAR_REQ:
		packetName += L"SC_CREATE_AVATAR_REQ";
		break;
	case SC_CREATE_AVATAR_RES:
		packetName += L"SC_CREATE_AVATAR_RES";
		break;
	case SC_ENTERWORLD_REQ:
		packetName += L"SC_ENTERWORLD_REQ";
		break;
	case SC_CHAT_REQ:
		packetName += L"SC_CHAT_REQ";
		break;
	case SC_enterworld_no_hot_spot:
		packetName += L"SC_enterworld_no_hot_spot";
		break;
	default:
		Log(L" ** Invalid PacketType Detected [Recv PacketType : %d ]", packetType);
		return;
	}
	packetName += L"]";
	/// Log(L" ** Recv Packet [Recv SessionID -> %I64d] %s", sessionPtr->GetSessionID(), packetName.c_str());
	WorldContents::getInstance().Execute((int)packetType, stream, sessionPtr);
}
