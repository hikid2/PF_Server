#include"stdafx.h"
#include"Stream.h"


void WriteStream::Clear()
{
	ZeroMemory(mStream.data(), mStream.size());
	mOffset = sizeof(byte);
}

void WriteStream::Write(const bool & inData)
{
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &inData, 1);
	mOffset += 1;
}

void WriteStream::Write(const string & inData)
{
	size_t len = 0;
	len = inData.length();
	if (len == 0) return;
	Write(len);
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, inData.data(), len);
	mOffset += len;
}

void WriteStream::Write(const wstring & inData)
{
	size_t len = 0;
	len = inData.length() + 1;
	if (inData.length() == 0) return;
	Write(len);
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, inData.data(), len * sizeof(WCHAR));
	mOffset += len * sizeof(WCHAR);
}
void WriteStream::Write(const wchar_t * inData, const int length)
{
	memcpy_s(mStream.data()+ mOffset, mStream.size() - mOffset, inData , length * sizeof(wchar_t));
	mOffset += length * sizeof(wchar_t);
}