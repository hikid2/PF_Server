#pragma once
#include<functional>
class Contents
{
public:
	typedef function<void(ReadStream &, Session *)> ContentsExecute;
	virtual void Initialize() = 0;
	virtual void Execute(int inContentsIndex, ReadStream & inStream, Session * inSessionPtr) = 0;
};