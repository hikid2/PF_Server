#include "stdafx.h"

void Sector::InsertToAvatar(const int & inObjectID)
{
	tbb::spin_rw_mutex::scoped_lock lock{ lock1};
	atomic_thread_fence(memory_order_seq_cst);
	mAvatarSector.insert(inObjectID);
}

void Sector::EraseToAvatar(const int & inObjectID) 
{
	tbb::spin_rw_mutex::scoped_lock lock{ lock1};
	atomic_thread_fence(memory_order_seq_cst);
	mAvatarSector.unsafe_erase(inObjectID);
}

void Sector::InsertToNPC(const int & inObjectID)
{
	tbb::spin_rw_mutex::scoped_lock lock{ lock2 };
	atomic_thread_fence(memory_order_seq_cst);
	mNPCSector.insert(inObjectID);
}

void Sector::EraseToNPC(const int & inObjectID)
{
	tbb::spin_rw_mutex::scoped_lock lock{ lock2 };
	atomic_thread_fence(memory_order_seq_cst);
	mNPCSector.unsafe_erase(inObjectID);
}

void Sector::RegisterAvatarInRange(const int & inOriginID, unordered_set<int>& outList)
{
	/// 현 섹터에서 아바타를 대상으로 작업을 진행 
	/// 아바타가 아바타를 찾는상황
	if (inOriginID >= 0 && inOriginID < MaxUser)
	{
		tbb::spin_rw_mutex::scoped_lock lock{ lock1 , false};
		atomic_thread_fence(memory_order_seq_cst);
		for (const auto & iter : mAvatarSector)
		{
			int id = iter;
			if (!ClientManager::getInstance().IsConnected(id))continue;
			if (inOriginID == id) continue;
			if (!ClientManager::getInstance().IsSameField(inOriginID, id))continue;
			if (!ClientManager::getInstance().IsinDistance(inOriginID, id)) continue;
			if (ClientManager::getInstance()[iter].GetSelectedAvatar()->GetCurrHP() <= 0)continue;
			outList.insert(iter);
		}
	}
	/// NPC가 아바타를 찾는 상황
	else if (NPCStart <= inOriginID && inOriginID < NPCEnd)
	{
		/// ExecutionAroundScope lockHelper(avatarPointLock, avatarAroundLock);
		tbb::spin_rw_mutex::scoped_lock lock{ lock1 , false };
		tbb::concurrent_unordered_set<int>::iterator iter;
		atomic_thread_fence(memory_order_seq_cst);
		for (const auto & iter : mAvatarSector)
		{
			if (!ClientManager::getInstance().IsConnected(iter))continue;
			if (ClientManager::getInstance()[iter].GetAvatarField() !=
				NPCManager::getInstance()[inOriginID].mInFieldNo)continue;
			if (!NPCManager::getInstance().IsinDistance(iter, inOriginID)) continue;
			if (ClientManager::getInstance()[iter].GetSelectedAvatar()->GetCurrHP() <= 0)continue;
			outList.insert(iter);
		}
	}

}
/// NPC를 찾는 상황 
// 아바타가 NPC를 찾는건 필요하지만 NPC가 NPC를 찾는건 의미가 없다
void Sector::RegisterNPCInRange(const int & inOriginID, unordered_set<int>& outList)
{
	if (inOriginID >= 0 && inOriginID < MaxUser)
	{
		/// ExecutionAroundScope lockHelper(npcPointLock, npcAroundLock);
		tbb::spin_rw_mutex::scoped_lock lock{ lock2 ,false};
		tbb::concurrent_unordered_set<int>::iterator iter;
		atomic_thread_fence(memory_order_seq_cst);
		for (iter = mNPCSector.begin(); iter != mNPCSector.end(); ++iter)
		{
			if (NPCManager::getInstance()[*iter].mAlive == false) continue;
			if (ClientManager::getInstance()[inOriginID].GetAvatarField()
				!= NPCManager::getInstance()[*iter].mInFieldNo) continue;
			if (!NPCManager::getInstance().IsinDistance(inOriginID, *iter)) continue;
			outList.insert(*iter);
		}
	}
}
