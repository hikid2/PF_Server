// TestServer.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"

//#ifdef _DEBUG
//#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
//// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
//// allocations to be of _CLIENT_BLOCK type
//#else
//#define DBG_NEW new
//#endif
void ServerStart()
{
	
	if (!IOCPServer::getInstance().Run())
	{
		Log(L"[Error : ]IOCPServer.Run Method return false")
		return;
	}
}

int main()
{
	SetupModule();
	thread IOCPThread { ServerStart };

	IOCPThread.join();
	while (false == IsShutDonw)
	{
		Sleep(1);
	}
	getchar();
	Global::getInstance().ShutdownServer();
	/// _CrtDumpMemoryLeaks();
	return 0;
}
