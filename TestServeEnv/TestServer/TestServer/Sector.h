#pragma once
/*
	인근 섹터를 찾는다.
*/

struct NPCCompare
{
	static size_t hash(const int & x)
	{
		int h = 0;
		h = x % 20000;
		return h;
	}

	static bool equal(const int & inLeft, const int & inRight)
	{
		return inLeft == inRight;
	}
};

struct AvatarCompare
{
	static int hash(const int & x)
	{
		size_t h = 0;
		h = x % 10000;
		return h;
	}

	static bool equal(const int & inLeft, const int & inRight)
	{
		return inLeft == inRight;
	}
};

class Sector
{
public:
	Sector() 
		///: avatarPointLock(), avatarAroundLock(), npcPointLock(), npcAroundLock()
	{}

	//typedef tbb::concurrent_hash_map<int, bool, AvatarCompare> AsectorList_t;
	//typedef tbb::concurrent_hash_map<int, bool, NPCCompare> NsectorList_t;

	//AsectorList_t mAvatarSector;
	//NsectorList_t mNPCSector;
	tbb::spin_rw_mutex lock1;
	tbb::spin_rw_mutex lock2;

	tbb::concurrent_unordered_set<int> mAvatarSector;
	tbb::concurrent_unordered_set<int> mNPCSector;

	//ExecutionOrderLock avatarPointLock;
	//ExecutionOrderLock avatarAroundLock;
	//ExecutionOrderLock npcPointLock;
	//ExecutionOrderLock npcAroundLock;


	void InsertToAvatar(const int & inObjectID);
	void EraseToAvatar(const int & inObjectID);
	void InsertToNPC(const int & inObjectID);
	void EraseToNPC(const int & inObjectID);

	void RegisterAvatarInRange(const int & inTarget, unordered_set<int>& outList);

	void RegisterNPCInRange(const int & inTarget, unordered_set<int>& outList);
};
