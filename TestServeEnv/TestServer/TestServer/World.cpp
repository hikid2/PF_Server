#include "stdafx.h"

/**
 * @brief 월드 정보 Read, 몬스터 소환작업 진행
 *
 */
void World::initialize()
{
	Document root;
	if (!loadConfig(&root, "StartTown"))
		return;
	const Value & layers = root["layers"];
	InsertCollisions(mStartTown, layers);
	Log(L"**  Read to Collision Data from Json FIle [ StartTown ]");

	if (!loadConfig(&root, "desertField"))
		return;
	const Value & layers2 = root["layers"];
	InsertCollisions(mDesert, layers2);
	Log(L"**  Read to Collision Data from Json FIle [ desertField ]");

	if (!loadConfig(&root, "Instance"))
		return;
	const Value & layers3 = root["layers"];
	InsertCollisions(mDungeon, layers);
	Log(L"**  Read to Collision Data from Json FIle [ Instance ]");

	SpawnMonster(mStartTown, ENUM_FIELDINDEX::StartTown);
	SpawnMonster(mDesert, ENUM_FIELDINDEX::Desert);
	SpawnMonster(mDungeon, ENUM_FIELDINDEX::Dungeon);
	root.RemoveAllMembers();
}

/**
 * @brief 좌표의 충돌정보를 확인하여 반환한다.
 *
 * @param inPositionX : X좌표
 * @param inPositionY : Y좌표
 * @param inFiled : 필드정보
 * @return true : 충돌정보 있음
 * @return false : 충돌 정보 없음
 */
bool World::IsCollisionToTerrain(const int32_t & inPositionX, const int32_t & inPositionY, const BYTE & inFiled)
{
	switch (inFiled)
	{
	case ENUM_FIELDINDEX::StartTown:
		if ((mStartTown[inPositionY][inPositionX] & Collision_Bit) != Collision_Bit) return false;
		break;
	case ENUM_FIELDINDEX::Desert:
		if ((mDesert[inPositionY][inPositionX] & Collision_Bit) != Collision_Bit) return false;
		break;
	case ENUM_FIELDINDEX::Dungeon:
		if ((mDungeon[inPositionY][inPositionX] & Collision_Bit) != Collision_Bit) return false;
		break;
	}
	return true;
}



/**
* @brief 직선상 이동이 가능한지 확인.
*
* @param inStart
* @param inTarget
* @param inFiled
* @param nowMovePoint
* @return true
* @return false
*/
bool World::IsOneLinePath(const POINT & inStart, const POINT & inTarget, const BYTE & inFiled, POINT & nowMovePoint)
{
	bool isOK = true;
	if ((inStart.x - inTarget.x) == 0)
	{
		int s = inStart.y > inTarget.y ? inTarget.y : inStart.y;
		int e = inStart.y > inTarget.y ? inStart.y : inTarget.y;

		for (int i = s; i < e; ++i)
		{
			if (!IsCollisionToTerrain(inStart.x, i, inFiled))
			{
				continue;
			}
			isOK = false;
			break;
		}
		if (isOK == true)
		{
			nowMovePoint.y = inStart.y > inTarget.y ? inTarget.y + 1 : inTarget.y - 1;
			nowMovePoint.x = inStart.x;
			return true;
		}
	}
	if ((inStart.y - inTarget.y) == 0)
	{
		int s = inStart.x > inTarget.x ? inTarget.x : inStart.x;
		int e = inStart.x > inTarget.x ? inStart.x : inTarget.x;
		for (int i = s; i < e; ++i)
		{
			if (World::getInstance().IsCollisionToTerrain(inStart.x, i, inFiled))
			{
				break;
			}
			isOK = false;
			break;
		}
		if (isOK == true)
		{
			nowMovePoint.x = inStart.x > inTarget.x ? inTarget.x + 1 : inTarget.x - 1;
			nowMovePoint.y = inStart.y;
			return true;
		}
	}
	return false;
}

/**
* @brief 아바타의 섹터정보를 업데이트 함
*
* @param inClientIndex 아바타를소유중인 프록시의 인덱스
* @param inSectorNO 변경할 섹터번호
* @return true 기존섹터번호를 정상적으로 변경하였다.
* @return false 변경이 실패햐였다.(프록시의 인덱스가 존재하지 않음)
*/
bool World::UpdateIDSector(const int & inObjectID, const int & inPrevSectorNO, const int & inNextSectorNO)
{
	if (inPrevSectorNO == inNextSectorNO)
		return false;

	int prevIndexX = GetSectorIndex_X(inPrevSectorNO);
	int prevIndexY = GetSectorIndex_Y(inPrevSectorNO);
	int nextIndexX = GetSectorIndex_X(inNextSectorNO);
	int nextIndexY = GetSectorIndex_Y(inNextSectorNO);
	if (NPCManager::getInstance().IsNPC(inObjectID))
	{
		mSectorArray[prevIndexY][prevIndexX].EraseToNPC(inObjectID);

		mSectorArray[nextIndexY][nextIndexX].InsertToNPC(inObjectID);
	}
	else
	{
		mSectorArray[prevIndexY][prevIndexX].EraseToAvatar(inObjectID);

		mSectorArray[nextIndexY][nextIndexX].InsertToAvatar(inObjectID);

	}
	return true;
}


bool World::EraseSector(const int & inObjectID, const int & inSectorNO)
{
	int indexX = GetSectorIndex_X(inSectorNO);
	int indexY = GetSectorIndex_Y(inSectorNO);
	if (NPCManager::getInstance().IsNPC(inObjectID))
	{
		mSectorArray[indexY][indexX].EraseToNPC(inObjectID);
	}
	else
	{
		mSectorArray[indexY][indexX].EraseToAvatar(inObjectID);
	}
	return false;
}

int World::MaxMinRange(int target, int num_min, int num_max)
{
	return (max(min(num_max, target), num_min));
}

/**
 * @brief 시야 업데이트 및 패킷전송 작업
 *
 * @param inSessionPtr
 * @param clientIndex
 */
void World::NextViewUpdate(Session * inSessionPtr, int clientIndex)
{
	unordered_set<int> newList;
	InsertViewAllObjectToList(clientIndex, newList);

	for (auto & iter : newList)
	{
		// 나의 viewList에 대상이 없다면?

		if (ClientManager::getInstance()[clientIndex].CountToViewList(iter) == 0)
		{
			// 나의 view List에 넣는다.
			ClientManager::getInstance()[clientIndex].InsertToViewList(iter);
			/// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
			SendPutPlayer(iter, inSessionPtr);
			if (NPCManager::getInstance().IsNPC(iter))
			{
				// 상대가 NPC라면
				bool nonActive = false;
				if (NPCManager::getInstance()[iter].isActive.compare_exchange_strong(nonActive, true))
				{
					auto now = 1000;
					EventTimer::getInstance().Push(iter, clientIndex, now, OP_EventTimer_WAKEUP, OP_EventTask);
					atomic_thread_fence(memory_order_seq_cst);
				}
				continue;
			}
			// 상대가 NPC가 아니라면?
			else
			{
				auto otherAvatar = ClientManager::getInstance()[iter].GetSelectedAvatar();
				// 넣은 유저는 날 안갖고 있다면?
				if (ClientManager::getInstance()[iter].CountToViewList(clientIndex) == 0)
				{
					ClientManager::getInstance()[iter].InsertToViewList(clientIndex);

					Session * dumySesion = nullptr;
					if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
					{
						SendPutPlayer(clientIndex, dumySesion);
					}
				}
				else
				{
					// 이미 날 갖고 있으니 이동 정보를 상대방에게 보내야 하지 않을까?
					Session * dumySesion = nullptr;
					if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
					{
						SendMovePlayer(clientIndex, dumySesion);

					}
				}
			}
		}
		else
		{
			// 나의 viewList에 대상이 있다면?
			// 상대방이 NPC라면?
			if (NPCManager::getInstance().IsNPC(iter))continue;
			// 상대방도 날 들고 있는가??
			auto otherAvatar = ClientManager::getInstance()[iter].GetSelectedAvatar();
			if (ClientManager::getInstance()[iter].CountToViewList(clientIndex) == 0)
			{
				// 없다면 ?
				ClientManager::getInstance()[iter].InsertToViewList(clientIndex);
				Session * dumySesion = nullptr;
				if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
				{
					// SendPacket(PutPlayer(상대방에게, 내 정보를));
					//// Log(L"[FILE : %s | LINE : %d] SC_PUT_PLAYER", __FUNCTIONW__, __LINE__);
					SendPutPlayer(clientIndex, dumySesion);

				}
			}
			else
			{
				// 갖고 있다면?
				Session * dumySesion = nullptr;
				if (SessionManager::getInstance().GetSession(ClientManager::getInstance().GetSessionID(iter), dumySesion))
				{
					// SendPacket(MovePlayer(상대방에게, 내 위치를));
					SendMovePlayer(clientIndex, dumySesion);

				}
			}
		}
	}

	// 뷰리스트에서 나가는 객체 처리
	auto avatar = ClientManager::getInstance()[clientIndex].GetSelectedAvatar();
	vector <int> remove_list;
	{
		tbb::spin_rw_mutex::scoped_lock lock(ClientManager::getInstance()[clientIndex].mtx, false);
		for (auto & i : avatar->mViewList)
		{
			// 내 리스트에 아직 있다. pass
			if (0 != newList.count(i)) continue;
			remove_list.push_back(i);
		}
	}
	for (auto & i : remove_list)
	{
		tbb::spin_rw_mutex::scoped_lock lock(ClientManager::getInstance()[clientIndex].mtx, true);
		avatar->mViewList.erase(i);
	}
	for (auto & i : remove_list)
		SendRemovePlayer(i, inSessionPtr);


	for (auto & iter : remove_list) 
	{
		if (NPCManager::getInstance().IsNPC(iter)) continue;
		if (0 != ClientManager::getInstance()[iter].CountToViewList(clientIndex)) 
		{
			ClientManager::getInstance()[iter].EraseToViewList(clientIndex);
			if (ClientManager::getInstance()[iter].IsConnected())
			{
				Session * dumySesion = nullptr;
				uint64_t sessionid = ClientManager::getInstance().GetSessionID(iter);
				if (SessionManager::getInstance().GetSession(sessionid, dumySesion))
				{
					SendRemovePlayer(clientIndex, dumySesion);

				}
			}
		}
	}
}

/**
 * @brief 타겟의 아이디와 섹터번호를 insert
 *
 * @param inTargetID : 타겟 아이디
 * @param inSectorNO : 섹터번호
 */
void World::InsertIDtoSector(const int & inTargetID, const int & inSectorNO)
{
	int index_x = GetSectorIndex_X(inSectorNO);
	int index_y = GetSectorIndex_Y(inSectorNO);
	if (!NPCManager::getInstance().IsNPC(inTargetID))
		mSectorArray[index_y][index_x].InsertToAvatar(inTargetID);
	else
		mSectorArray[index_y][index_x].InsertToNPC(inTargetID);
}

/**
* @brief A* 알고리즘을 이용하여 경로를 검색
*
* @param inFieldID : 충돌체크 필드
* @param inStart : 시작점(플레이어 기준으로 몬스터 좌표를 찾는다)
* @param inTarget : 목표(몬스터 좌표를 넣는다)
* @param outPoint : 이번에 이동해야할 좌표
* @param outNextPoint : 다음 스텝에 이동할 좌표
*/
bool World::TestAstar(const byte & inFieldID, const POINT & inStart, const POINT & inTarget, POINT & outPoint, POINT & outNextPoint)
{
	array<array<PathNode, 11>, 11> fields = {};
	Field_t * fieldPtr = nullptr;
	if (inFieldID == StartTown) fieldPtr = &mStartTown;
	else if (inFieldID == Desert) fieldPtr = &mDesert;
	else if (inFieldID == Dungeon) fieldPtr = &mDungeon;

	for (int y = 0; y < 11; ++y)
	{
		for (int x = 0; x < 11; ++x)
		{
			if ((MaxMinRange(inStart.y - 5 + y, 0, 199) == inStart.y - 5 + y) &&
				(MaxMinRange(inStart.x - 5 + x, 0, 199) == inStart.x - 5 + x))
			{
				if ((*fieldPtr)[inStart.y - 5 + y][inStart.x - 5 + x] == Collision_Bit)
				{
					fields[y][x].SetBlock(true);
				}
			}
			fields[y][x].SetPointX(x);
			fields[y][x].SetPointY(y);
		}
	}
	long delx = inStart.x - inTarget.x; // 5   7 =  -2
	long dely = inStart.y - inTarget.y; // 8   3 =  5
	if (MaxMinRange(delx, -5, 5) != delx)return false;
	if (MaxMinRange(dely, -5, 5) != dely)return false;
	PathNode * startPoint = &fields[5][5];
	PathNode * TargetPoint = &fields[5 - dely][5 - delx];

	startPoint->SetParentPtr(nullptr);
	startPoint->SetG(0);
	startPoint->SetH(Getheuristics(startPoint, TargetPoint));
	startPoint->SetF();

	priority_queue<PathNodePtr, vector<PathNodePtr>, FValueComapre> openList[2]; ///> 열린 목록																			  
	int openindex = 0;
	set<PathNodePtr, PathValueCompare> closeList;
	openList[0].push(startPoint);

	while (!openList[openindex].empty())
	{
		auto current = openList[0].top();

		openList[openindex].pop();
		if (current->GetH() == 0)
		{
			outPoint.x = inStart.x - (5 - current->GetParentPtr()->GetPointX());
			outPoint.y = inStart.y - (5 - current->GetParentPtr()->GetPointY());
			if (current->GetParentPtr()->GetParentPtr() != nullptr)
			{
				outNextPoint.x = inStart.x - (5 - current->GetParentPtr()->GetParentPtr()->GetPointX());
				outNextPoint.y = inStart.y - (5 - current->GetParentPtr()->GetParentPtr()->GetPointY());
			}
			// 경로를 반환하고
			// 경로 탐색 종료!
			return true;
		}
		current->SetState(NodeState::close);
		closeList.insert(current);

		vector<PathNode *> nearnode;
		if (MaxMinRange(current->GetPointY() + 1, 0, 10) == current->GetPointY() + 1)
		{
			nearnode.push_back(&(fields[current->GetPointY() + 1][current->GetPointX()]));
		}
		if (MaxMinRange(current->GetPointY() - 1, 0, 10) == current->GetPointY() - 1)
		{
			nearnode.push_back(&(fields[current->GetPointY() - 1][current->GetPointX()]));
		}
		if (MaxMinRange(current->GetPointX() + 1, 0, 10) == current->GetPointX() + 1)
		{
			nearnode.push_back(&(fields[current->GetPointY()][current->GetPointX() + 1]));
		}
		if (MaxMinRange(current->GetPointX() - 1, 0, 10) == current->GetPointX() - 1)
		{
			nearnode.push_back(&(fields[current->GetPointY()][current->GetPointX() - 1]));
		}
		for (auto & iter : nearnode)
		{
			/// 벽이면 패스한다.
			if (iter->IsBlock())
				continue;
			NodeState nowState = iter->GetState();
			/// 이미 닫혀있다면 패스한다.
			if (nowState == NodeState::close)
				continue;
			/// G를 넣고
			int newG = current->GetG() + 1;
			/// 열린목록에 있다면
			if (nowState == 0)
			{
				/// 이전 G < 새로운 G : 효율성이 없으므로 패스
				if (iter->GetG() < newG)
					continue;
				/// 이전 G > 새로운 G : 재설정해주고 queue를 다시 뺏다가 넣어준다.
				else
				{
					iter->SetG(newG);
					iter->SetF();
					iter->SetParentPtr(current);
					while (!openList[openindex].empty())
					{
						auto ptr = openList[openindex].top();
						openList[1 - openindex].push(ptr);
						openList[openindex].pop();
					}
					openindex = 1 - openindex;
					continue;
				}
			}
			/// 열리지도 닫히지도 않은거니 설정해주자.
			iter->SetH(Getheuristics(iter, TargetPoint));
			iter->SetG(newG);
			iter->SetF();
			iter->SetParentPtr(current);
			openList[openindex].push(iter);
		}
	}
	return false;
}
/**
 * @brief 워프를 할 수 있는 위치인지 확인
 *
 * @param inPointx : 좌표
 * @param inPointy : 좌표
 * @param inFieldID : 필드아이디
 * @param outFieldID : 이동할 워프 아이디
 * @return true : 워프 가능한 위치
 * @return false : 워프 불가 위치
 */
bool World::IsWarpZone(const int & inPointx, const int & inPointy, const byte & inFieldID, ENUM_WARPID & outFieldID)
{
	switch (inFieldID)
	{
	case ENUM_FIELDINDEX::StartTown:
		if (inPointy != 199)
			break;
		if (inPointx == 169 || inPointx == 170 || inPointx == 171)
		{
			outFieldID = ENUM_WARPID::STARTFIELD_DESERT;
			return true;
		}
	case ENUM_FIELDINDEX::Dungeon:
		if (127 == inPointx && inPointy == 3)
		{
			outFieldID = ENUM_WARPID::DUNGEON_DESERT;
			return true;
		}
	case ENUM_FIELDINDEX::Desert:
		// 2가지 케이스
		// 사막-> 던전
		if (80 <= inPointy && inPointy <= 82 && 48 <= inPointx && inPointx <= 50)
		{
			outFieldID = ENUM_WARPID::DESERT_DUNGEON;
			return true;
		}
		// 사막에서 -> 시작필드갈때
		else if (inPointx == 91 && inPointy == 0)
		{
			outFieldID = ENUM_WARPID::DESERT_STARTFIELD;
			return true;
		}
	default:
		break;
	}
	return false;
}

/**
 * @brief 워프될 필드의 좌표를 전달
 *
 * @param inWarpID : 워프아이디
 * @param outPointx : 반한될 x좌표
 * @param outPointy : 반환될 y좌표
 * @param outFieldID : 실제 이동 필드아이디
 */
void World::DoWarpField(const byte & inWarpID, int & outPointx, int & outPointy, byte & outFieldID)
{
	switch (inWarpID)
	{
	case ENUM_WARPID::DESERT_DUNGEON:
		outPointx = Global::getInstance().GetRamdom(125, 130);
		outPointy = Global::getInstance().GetRamdom(4, 5);
		outFieldID = Dungeon;
		break;
	case ENUM_WARPID::DESERT_STARTFIELD:
		outPointx = Global::getInstance().GetRamdom(169, 171);
		outPointy = 197;
		outFieldID = StartTown;
		break;
	case ENUM_WARPID::DUNGEON_DESERT:
		outPointx = Global::getInstance().GetRamdom(48, 50);
		outPointy = Global::getInstance().GetRamdom(84, 85);
		outFieldID = Desert;
		break;
	case ENUM_WARPID::RESPAWN_STARTTOWN:
		outFieldID = StartTown;
		outPointx = Global::getInstance().GetRamdom(106, 115);
		outPointy = Global::getInstance().GetRamdom(81, 84);
		break;
	case ENUM_WARPID::STARTFIELD_DESERT:
		outPointx = Global::getInstance().GetRamdom(86, 90);
		outPointy = Global::getInstance().GetRamdom(1, 5);
		outFieldID = Desert;
		break;
	}
}

/**
 * @brief 충돌점보를 읽어 필드에 저장
 *
 * @param inField : 필드 배열
 * @param inJsonValue : Json데이터
 */
inline void World::InsertCollisions(array<array<BYTE, 200>, 200>& inField, const Value & inJsonValue)
{
	auto & CollisionJson = inJsonValue[0]["objects"];


	uint32_t collisionSize = CollisionJson.Size();
	for (uint32_t index = 0; index < collisionSize; ++index)
	{
		int x = CollisionJson[index]["x"].GetFloat() / 32;
		int y = CollisionJson[index]["y"].GetFloat() / 32;
		inField[y][x] |= Collision_Bit;
	}
	auto & spawnFir = inJsonValue[1]["objects"];
	auto spawn_firstCnt = spawnFir.Size();
	for (uint32_t index = 0; index < spawn_firstCnt; ++index)
	{
		int x = spawnFir[index]["x"].GetFloat() / 32;
		int y = spawnFir[index]["y"].GetFloat() / 32;
		inField[y][x] |= Spawn_fir_Bit;
	}
	auto & spawnSec = inJsonValue[2]["objects"];
	auto spawn_SecCnt = spawnSec.Size();
	for (uint32_t index = 0; index < spawn_SecCnt; ++index)
	{
		int x = spawnSec[index]["x"].GetFloat() / 32;
		int y = spawnSec[index]["y"].GetFloat() / 32;
		inField[y][x] |= Spawn_sec_Bit;
	}

	auto &  spawnThr = inJsonValue[3]["objects"];
	auto spawn_thrCnt = spawnThr.Size();
	for (uint32_t index = 0; index < spawn_thrCnt; ++index)
	{
		int x = spawnThr[index]["x"].GetFloat() / 32;
		int y = spawnThr[index]["y"].GetFloat() / 32;
		inField[y][x] |= Spawn_thr_Bit;
	}
	auto & spawnfor = inJsonValue[4]["objects"];
	auto spawn_forCnt = spawnfor.Size();
	for (uint32_t index = 0; index < spawn_forCnt; ++index)
	{
		int x = spawnfor[index]["x"].GetFloat() / 32;
		int y = spawnfor[index]["y"].GetFloat() / 32;
		inField[y][x] |= Spawn_for_Bit;
	}

}

/**
 * @brief 몬스터 스폰장소를 읽은 뒤 해당 잘소로 몬스터를 생성
 *
 * @param inField 필드배열
 * @param inFieldIIndex 필드 아이디
 */
inline void World::SpawnMonster(array<array<BYTE, 200>, 200>& inField, int inFieldIIndex) {
	for (int y = 0; y < 200; ++y)
	{
		for (int x = 0; x < 200; ++x)
		{
			if (inField[y][x] == 0) continue;
			if (inField[y][x] & ENUM_FieldDataBit::Collision_Bit) continue;
			int sectorNO = GetSectorNo(x, y);
			if (inField[y][x] & Spawn_fir_Bit)
			{
				int npcID = NPCManager::getInstance().SetupLua(x, y, inFieldIIndex, Spawn_fir_Bit);
				InsertIDtoSector(npcID, sectorNO);
				continue;
			}
			if (inField[y][x] & Spawn_sec_Bit)
			{
				int npcID = NPCManager::getInstance().SetupLua(x, y, inFieldIIndex, Spawn_sec_Bit);
				InsertIDtoSector(npcID, sectorNO);
				continue;
			}
			if (inField[y][x] & Spawn_thr_Bit)
			{
				int npcID = NPCManager::getInstance().SetupLua(x, y, inFieldIIndex, Spawn_thr_Bit);
				InsertIDtoSector(npcID, sectorNO);
				continue;
			}
			if (inField[y][x] & Spawn_for_Bit)
			{
				int npcID = NPCManager::getInstance().SetupLua(x, y, inFieldIIndex, Spawn_for_Bit);
				InsertIDtoSector(npcID, sectorNO);
				continue;
			}
		}
	}
}

/**
 * @brief 휴리스틱 값을 반환
 *
 * @param tmp
 * @param targetx
 * @param targety
 * @return int
 */
int World::Getheuristics(PathNode * tmp, const int targetx, const int targety)
{
	return abs(targetx - tmp->GetPointX()) + abs(targety - tmp->GetPointY());
}

/**
 * @brief 휴리스틱 값을 반환
 *
 * @param originPtr
 * @param targetPtr
 * @return int
 */
int World::Getheuristics(PathNode * originPtr, PathNode *  targetPtr)
{
	return abs(targetPtr->GetPointX() - originPtr->GetPointX()) + abs(targetPtr->GetPointY() - originPtr->GetPointY());
}

void World::InsertAllObjectFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList)
{
	int index_x = GetSectorIndex_X(sectorNO);
	int index_y = GetSectorIndex_Y(sectorNO);
	mSectorArray[index_y][index_x].RegisterAvatarInRange(inOriginID, outList);
	mSectorArray[index_y][index_x].RegisterNPCInRange(inOriginID, outList);
}

void World::InsertAvatarFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList)
{
	int index_x = GetSectorIndex_X(sectorNO);
	int index_y = GetSectorIndex_Y(sectorNO);
	mSectorArray[index_y][index_x].RegisterAvatarInRange(inOriginID, outList);
}

void World::InsertNPCFromSector(const int & sectorNO, const int & inOriginID, unordered_set<int>& outList)
{
	int index_x = GetSectorIndex_X(sectorNO);
	int index_y = GetSectorIndex_Y(sectorNO);
	mSectorArray[index_y][index_x].RegisterNPCInRange(inOriginID, outList);
}


void World::InsertViewAllObjectToList(const int & inOriginID, unordered_set<int>& outList)
{
	int x = -1;  int y = -1;
	if (NPCManager::getInstance().IsNPC(inOriginID))
	{
		x = NPCManager::getInstance()[inOriginID].mPointX;
		y = NPCManager::getInstance()[inOriginID].mPointY;
	}
	else
	{
		x = ClientManager::getInstance()[inOriginID].GetSelectedAvatar()->GetAvatarPositionX();
		y = ClientManager::getInstance()[inOriginID].GetSelectedAvatar()->GetAvatarPositionY();
	}
	unordered_set<int> nearSectorList;

	InsertNearSectorNO(x, y, nearSectorList);

	for (auto & iter : nearSectorList)
	{
		InsertAllObjectFromSector(iter, inOriginID, outList);
	}
}

void World::InsertViewAvatarToList(const int & inOriginID, unordered_set<int>& outList)
{
	int x = -1;  int y = -1;
	if (NPCManager::getInstance().IsNPC(inOriginID))
	{
		x = NPCManager::getInstance()[inOriginID].mPointX;
		y = NPCManager::getInstance()[inOriginID].mPointY;
	}
	else
	{
		x = ClientManager::getInstance()[inOriginID].GetSelectedAvatar()->GetAvatarPositionX();
		y = ClientManager::getInstance()[inOriginID].GetSelectedAvatar()->GetAvatarPositionY();
	}
	unordered_set<int> nearSectorList;
	InsertNearSectorNO(x, y, nearSectorList);
	for (auto & iter : nearSectorList)
	{
		InsertAvatarFromSector(iter, inOriginID, outList);
	}
}