#pragma once

class Query
{
public:
	Query() {}
	virtual ~Query() {}
	virtual void SetQuery(const wchar_t * inQuery)
	{
		mQueryText += L"{";
		mQueryText += inQuery;
		mQueryText += L"}";
	}
	virtual void Clear()
	{
		mQueryText.clear();
	}
	const wchar_t * GetQuery() { return mQueryText.c_str(); }
	SQLHSTMT & getstmt() { return mQueryStmt; }
protected:
	SQLHSTMT	mQueryStmt;
	wstring		mQueryText;
};