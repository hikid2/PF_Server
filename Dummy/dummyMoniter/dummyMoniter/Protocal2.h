#pragma once
#pragma pack(push, 1)

struct Avatar_Profile
{
	int					avatar_id;
	BYTE				classType;
	wchar_t				name[20];
	int					level;
	int					fieldID;
};
struct Psc_packet_Avatar_list
{
	int					listCnt;
	Avatar_Profile		profileList[9];
};

struct Psc_packet_broadcast_chat
{
	wstring mMessage;
};
struct Psc_packet_put_dummyAvatars
{
	bool		isNPC;
	wchar_t		mName[20];
	int			avatarindex;
	int			mPosX;
	int			mPosY;
	BYTE		mClasstype;
	uint32_t	mCurrHP;
	uint32_t	mMaxHP;
	uint32_t	mCurrMP;
	uint32_t	mMaxMP;
	int			mLv;
	int			mHorizontal;
	int			mVertial;
};
struct Psc_packet_enterworld_Succ
{
	int			mObjectID;
	wchar_t		mName[20];
	int			mLevel;
	BYTE		mClassType;
	uint32_t	mCurrHP;
	uint32_t	mCurrMP;
	uint32_t	mMaxHP;
	uint32_t	mMaxMP;
	int64_t		mCurrExp;
	int64_t		mNeedExp;
	int			mNowField;
	int32_t		mX;
	int32_t		mY;
	int			mHasGold;
	int			str;
	int			dex;
	int			ints;
	int			luck;
	int			mHorizontal;
	int			mVertial;
};
struct Psc_packet_move
{
	int		avatarindex;
	int32_t mPointx;
	int32_t mPointy;
	int32_t mHorizontal;
	int32_t mVertical;
};
struct Psc_packet_update_avatarhp 
{
	int id;
	int hp;
};

struct Psc_packet_respawn_avatar
{
	int32_t mPointx;
	int32_t mPointy;
	BYTE	mWarpFieldId;
	uint32_t mNowHP;
	uint32_t mNowMP;
};
#pragma pack(pop)

void SendBroadCastChat()
{

}