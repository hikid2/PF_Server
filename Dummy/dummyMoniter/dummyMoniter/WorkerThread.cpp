#include "stdafx.h"

DWORD WINAPI WorkerThread(LPVOID para)
{
	DWORD	iosize;
	DWORD	flag = 0;
	ULONG	comp_key;
	OverlappedExtension_t *over_ptr = NULL;
	char *buf = NULL;
	int remained_io_data_size = 0;
	int	result = 0;
	uint32_t offset = 0;
	bool iocpretval = true;
	while (true) {
		iocpretval = GetQueuedCompletionStatus(g_iocp, &iosize, (PULONG_PTR)&comp_key, (LPOVERLAPPED *)&over_ptr, INFINITE);
		if (FALSE == iocpretval) {
			if (over_ptr == nullptr)
			{
				int retval = WSAGetLastError();
				error_display("getqueued Error1", retval);
				return -1;
				break;
			}
			else
			{
				int retval = WSAGetLastError();
				if ((retval == ERROR_NETNAME_DELETED) || (retval == ERROR_OPERATION_ABORTED)) {
					error_display("getqueued Error2", retval);
					closesocket(dummyAvatars[comp_key].GetSocket());
				}
				error_display("getqueued Error3", retval);
			}
		}
		if (0 == iosize) {
			int retval = WSAGetLastError();
			error_display("getqueued Error", retval);
			closesocket(dummyAvatars[comp_key].GetSocket());
			//	Process_disconnect(comp_key);
			break;
		}
		switch (over_ptr->mOperation) {
		case IO_RECV:
			/// iosize
			remained_io_data_size = iosize;
			///recvbuffer 두고
			buf = dummyAvatars[comp_key].recvBuffer;
			
			while (remained_io_data_size > 0) {
				offset = 0;
				if (0 == dummyAvatars[comp_key].mPacketSize) {
					memcpy_s(&dummyAvatars[comp_key].mPacketSize, sizeof(byte), buf, sizeof(byte));
					offset += sizeof(byte);
				}
				/// mPreviousSize 이전의 사이즈
				int remained_packet_data_size = dummyAvatars[comp_key].mPacketSize - dummyAvatars[comp_key].mPreviousSize;

				if (remained_packet_data_size <= remained_io_data_size) {

					memcpy_s(dummyAvatars[comp_key].mUncompletePacket + dummyAvatars[comp_key].mPreviousSize, 
						MAX_BUFF_SIZE - dummyAvatars[comp_key].mPreviousSize,
						buf + offset,
						remained_packet_data_size - offset);
					/*ReadStream stream;
					stream.set((char *)dummyAvatars[comp_key].mUncompletePacket, 
						dummyAvatars[comp_key].mPacketSize);*/
					byte packetType = dummyAvatars[comp_key].mUncompletePacket[0];

					/*stream.Read(&packetType);*/
					ProcessExecute(packetType, comp_key, (char*)&dummyAvatars[comp_key].mUncompletePacket[1]);
					dummyAvatars[comp_key].mPacketSize = 0;
					dummyAvatars[comp_key].mPreviousSize = 0;
					buf += remained_packet_data_size;
					remained_io_data_size -= remained_packet_data_size;
				}
				else {
					memcpy_s(dummyAvatars[comp_key].mUncompletePacket+ dummyAvatars[comp_key].mPreviousSize,
						MAX_BUFF_SIZE - dummyAvatars[comp_key].mPreviousSize,
						buf,
						remained_io_data_size);
					dummyAvatars[comp_key].mPreviousSize += remained_io_data_size;
					break;
				}
			}
			result = WSARecv(dummyAvatars[comp_key].GetSocket(), &dummyAvatars[comp_key].mWsaBuf, 1,
				NULL, &flag, reinterpret_cast<LPWSAOVERLAPPED>(&dummyAvatars[comp_key].mOverlappeds), NULL);

			if (result == SOCKET_ERROR) {
				int errcode = WSAGetLastError();
				if (errcode != WSA_IO_PENDING && errcode != WSAEWOULDBLOCK) {
					printf("WSARecv() error!!\n");
					return FALSE;
				}
			}
			break;

		case IO_SEND:
			delete over_ptr;
			//printf("Send Complete on User:%d\n",comp_key);
			break;
		default:
			printf("IO type error!!\n");
			break;

		}

	} // while()

	return 0;
}
