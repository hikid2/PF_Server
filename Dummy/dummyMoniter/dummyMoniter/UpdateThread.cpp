#include"stdafx.h"
bool IsCollisionToTerrain(int i, int inKeyData)
{
	if (inKeyData & 1 << 4)
		return true;
	int y = dummyAvatars[i].y;
	int x = dummyAvatars[i].x;
	if (inKeyData & 1 << 0)
	{
		y -= 1;
	}
	else if (inKeyData & 1 << 1)
	{
		y += 1;
	}
	else if (inKeyData & 1 << 2)
	{
		x -= 1;
	}
	else if (inKeyData & 1 << 3)
	{
		x += 1;
	}
	if (x < 0) x = 0;
	else if (x >= 200) x = 199;
	if (y < 0) y = 0;
	else if (y >= 200) y = 199;
	switch (objectList2[dummyAvatars[i].objectID].field)
	{
	case Start_Town:
		if ((startTown[y][x] & collision_bit) != collision_bit) 
			return false;
		break;
	case Desert_Field:
		if ((Desert[y][x] & collision_bit) != collision_bit) 
			return false;
		break;
	case Ins_Dan:
		if ((Dungeon[y][x] & collision_bit) != collision_bit) 
			return false;
		break;
	}
	return true;
}
DWORD WINAPI UpdateThread(LPVOID para)
{
	while (!isShutdown)
	{
		Sleep(1000);
		for (auto i = 0; i < TEST_MAX_USER; ++i)
		{
			if (dummyAvatars[i].mInGameWorldState && dummyAvatars[i].mIsAlive)
			{
				sc_packet_input packet;
				int inputs = GetRandom(0, 3);
				int data = 1 << inputs;
				if (!IsCollisionToTerrain(i, data))
				{
					packet.key = data;
					dummyAvatars[i].SendPacket(&packet);
					/// printf("%d User Send Move !\n", i);
				}

			}
		}


		tbb::concurrent_unordered_map<int, DrawDisPlayInfo>::iterator iter;
		for (iter = objectList2.begin(); iter != objectList2.end(); ++iter)
		{
			if (iter->second.field == NowDisplayField)
			{
				InterlockedIncrement(&displayArray[iter->second.y][iter->second.x]);
			}
		}
		for (int i = 0; i < 200; ++i)
		{
			for (int j = 0; j < 200; ++j)
			{
				displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::White);
				displayBox[i][j].inActive = false;
			}
		}
		int count = 0;
		for (int i = 0; i < 200; ++i)
		{
			for (int j = 0; j < 200; ++j)
			{
				if (displayArray[i][j] == object_bit)
				{
					displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::SkyBlue);
					displayBox[i][j].inActive = true;
					count++;
				}
				else if (NowDisplayField == Start_Town)
				{
					if (startTown[i][j] == collision_bit) {
						displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::Black);
						displayBox[i][j].inActive = true;

					}
				}
				else if (NowDisplayField == Desert_Field)
				{
					if (Desert[i][j] == collision_bit)
					{
						displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::Black);
						displayBox[i][j].inActive = true;
					}
				}
				else if (NowDisplayField == Ins_Dan)
				{
					if (Dungeon[i][j] == collision_bit)
					{
						displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::Black);
						displayBox[i][j].inActive = true;
					}
				}
				else
					displayBox[i][j].inColor = D2D1::ColorF(D2D1::ColorF::LightGreen, 0, 8);
			}
		}
		for (int i = 0; i < 200; ++i)
		{
			for (int j = 0; j < 200; ++j)
			{
				displayArray[i][j] = 0;
			}
		}
	}
	return 0;
}