// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:

//
#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif
const int IOBufferSize = 8192;
const int MAX_BUFF_SIZE = 8192;
#pragma warning(suppress : 4996)
#pragma comment(lib, "D2D1.lib")
#pragma comment(lib, "dwrite.lib")
#pragma comment(lib, "Windowscodecs.lib")


#include<DirectXCollision.h>
#include<DirectXMath.h>
using namespace DirectX;
#include<D3D10_1.h>
#include <iostream>
#include<string>
#include <list>
#include <map>
#include <algorithm>  
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <process.h>
#include <WinSock2.h>
#include <WinBase.h>
#include <Windows.h>
#include <time.h>
#include <WS2tcpip.h>
#include<d2d1.h>
#include <tchar.h>
#include<thread>
#include<mutex>
#pragma comment(lib, "ws2_32.lib")
using namespace std;
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"   // FileReadStream
#include "rapidjson/encodedstream.h"    // EncodedInputStream
#include <rapidjson/istreamwrapper.h>
using namespace rapidjson;
#pragma comment(lib,"tbb.lib")
#pragma comment(lib,"tbb_debug.lib")
#include <concurrent_unordered_map.h>
#include"DirectGraphics.h"

#include"Stream.h"
#include"StructDefine.h"
#include"Protocol.h"
#include"Protocal2.h"
#include"DummyAvatar.h"


#include"Global.h"



// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.

/**
	부하 테스트가 가능한 클라이언트를 만들어보자
	기능 
	 - 자동 회원가입 , 로그인, 자동 캐릭터 생성, 게임 안으로 들어가기,
	 - 일정 시간마다 이동 패킷 및 공격 하기!
	 - 실제 유저는 시각으로 보는 지형에 대한 판단이 되는 부분이니 비슷한 시뮬레이션을 위해
	   지형정보를 클라이언트가 갖고 이동하게끔 함.
	 - 네트워트는 IOCP를 이용해서 구현!
	 - 리소스를 이용한 렌더링은 제외, 전체 맵의 상태를 알수 있는 전체맵 현황을 볼수 있게 하면 좋을듯 하다.
	 키입력은 1초에 1번씩 전송

	 - 1차 테스트는 1000명

	 - 2차 테스트는 2000명

	 - 3차 테스트는 5000명

	 - 4차 테스트는 8000명
*/