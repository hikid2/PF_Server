#pragma once

enum Enum_AvatarClass : byte
{
	Warrior = 1,
	Wizerd,
	Elf,
};

typedef struct OVERLAPPEDEXTENSION
{
	WSAOVERLAPPED	mMyoverlap;
	DWORD			mOperation;

}OverlappedExtension_t;

typedef struct {
	OverlappedExtension_t	mOverlapSend;
	WSABUF			mSendWSABuf;
	CHAR			mSendBuffer[MAX_BUFF_SIZE];
}SendInfo_t;

typedef struct DrawDisPlayInfo
{
	
	int32_t x;
	int32_t y;
	byte	classtype;
	int		field;
	DrawDisPlayInfo()
	{
		x = 0;
		y = 0;
		classtype = 0;
		field = 0;
	}
}DrawDisPlayInfo;

typedef struct
{
	D2D1_COLOR_F inColor;
	D2D1_RECT_F inRect;
	bool		inActive = false;
}Box;