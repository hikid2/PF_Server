#pragma once
class DummyAvatar
{
public:
	DummyAvatar();
	bool Connect(int index);
	bool DisConnect();

	void SendPacket(Packet * inPacket);
	const SOCKET & GetSocket();
	void OnRecv(const int & comp_key);

	int x;
	int y;
	int objectID;
	bool					mLoginState;
	bool					mInGameWorldState;
	bool					mConnectState;
	bool					mIsAlive;


	SOCKET					mSocket;
	OverlappedExtension_t	mOverlappeds;

	uint32_t						mPreviousSize;
	int						mPacketSize;

	WSABUF					mWsaBuf;
	char					recvBuffer[MAX_BUFF_SIZE];
	char					mUncompletePacket[MAX_BUFF_SIZE];
};