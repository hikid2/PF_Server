#pragma once

#include<DWrite.h>
#include<string>


class DirectGraphics
{
public:
	DirectGraphics() {}
	virtual ~DirectGraphics();
	bool Initialize(HWND & inWindowsHandle);

	void BeginDraw() { mRenderTarget->BeginDraw(); }
	void EndDraw() {
		HRESULT hr = mRenderTarget->EndDraw();
	}

	void ClearScreen(float r, float g, float b);
	void DrawCircle(float x, float y, float radius, float r, float g, float b, float a);
	void DrawRectangle();
	void DrawRectangle(const D2D1::ColorF inColor, const D2D1_RECT_F rect)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(inColor, &brush);

		// mRenderTarget->DrawRectangle(rect, brush, 5.0f, 0);
		mRenderTarget->FillRectangle(rect, brush);
		brush->Release();
	}
	HRESULT initialized_Text()
	{
		HRESULT hr;
		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,
			__uuidof(mWriteFactory), (IUnknown**)&mWriteFactory);
		if (FAILED(hr))
			return hr;
		hr = mWriteFactory->CreateTextFormat(
			L"��������",
			0,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			15,
			L"ko",
			&mWriteTextFormat
		);
		return hr;
	}

	void DrawTextW(wstring inText, D2D1_RECT_F rect)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1, 1, 1, 1), &brush);
		mRenderTarget->DrawTextW(inText.c_str(), (uint32_t)inText.length(), mWriteTextFormat, rect, brush);

		brush->Release();
	}
	void DrawTextW(wstring inText, D2D1_RECT_F rect, float alpha)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1, 1, 1, alpha), &brush);
		mRenderTarget->DrawTextW(inText.c_str(), (uint32_t)inText.length(), mWriteTextFormat, rect, brush);

		brush->Release();
	}
	void DrawChatText(const wstring & inStr, const D2D1_RECT_F & inPosition, const D2D1_COLOR_F inColor)
	{
		ID2D1SolidColorBrush * brush;
		mRenderTarget->CreateSolidColorBrush(inColor, &brush);
		mRenderTarget->DrawTextW(inStr.c_str(), (uint32_t)inStr.length(), mWriteTextFormat,
			inPosition, brush);
		brush->Release();
	}

	void DrawFillRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor);


	void ConstTextDraw(const wstring & inStr, D2D1_RECT_F inRect, int inFontSize, DWRITE_TEXT_ALIGNMENT inAlignment = DWRITE_TEXT_ALIGNMENT_LEADING)
	{
		wstring datss = inStr;
		IDWriteTextLayout *textLayout = 0;
		ID2D1SolidColorBrush * brush;

		HRESULT hr = mWriteFactory->CreateTextLayout(
			datss.c_str(),
			datss.length(),
			mWriteTextFormat,
			inRect.right - inRect.left,
			inRect.bottom - inRect.top,
			&textLayout
		);
		D2D1_POINT_2F startPoint;
		startPoint.x = inRect.left;
		startPoint.y = inRect.top;
		hr = mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &brush);
		DWRITE_TEXT_RANGE range;
		range.startPosition = 0;
		range.length = datss.length();
		hr = textLayout->SetTextAlignment(inAlignment);
		hr = textLayout->SetFontSize((float)inFontSize, range);
		mRenderTarget->DrawTextLayout(startPoint, textLayout, brush, D2D1_DRAW_TEXT_OPTIONS_NO_SNAP);
		// mRenderTarget->DrawTextW(inStr.c_str(), inStr.length(),			mWriteTextFormat, inRect,brush);
		brush->Release();
		textLayout->Release();
	}

	void ConstTextDraw(const wstring & inStr, D2D1_RECT_F inRect, int inFontSize, const D2D1_COLOR_F inColor, DWRITE_TEXT_ALIGNMENT inAlignment = DWRITE_TEXT_ALIGNMENT_LEADING)
	{
		wstring datss = inStr;
		IDWriteTextLayout *textLayout = 0;
		ID2D1SolidColorBrush * brush;

		HRESULT hr = mWriteFactory->CreateTextLayout(
			datss.c_str(),
			datss.length(),
			mWriteTextFormat,
			inRect.right - inRect.left,
			inRect.bottom - inRect.top,
			&textLayout
		);
		D2D1_POINT_2F startPoint;
		startPoint.x = inRect.left;
		startPoint.y = inRect.top;
		hr = mRenderTarget->CreateSolidColorBrush(inColor, &brush);
		DWRITE_TEXT_RANGE range;
		range.startPosition = 0;
		range.length = datss.length();
		hr = textLayout->SetTextAlignment(inAlignment);
		hr = textLayout->SetFontSize((float)inFontSize, range);
		mRenderTarget->DrawTextLayout(startPoint, textLayout, brush, D2D1_DRAW_TEXT_OPTIONS_NO_SNAP);
		// mRenderTarget->DrawTextW(inStr.c_str(), inStr.length(),			mWriteTextFormat, inRect,brush);
		brush->Release();
		textLayout->Release();
	}
	ID2D1RenderTarget * GetRenderTarget() { return mRenderTarget; }

	void DrawRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect);

	void DrawFillRectangle(const D2D1_COLOR_F & inColor, const D2D1_RECT_F & inRect);
	void DrawRoundedRectangle(const D2D1_ROUNDED_RECT rect, const D2D1_COLOR_F inColor);
	void GameDraw();
private:
	ID2D1Factory * mFactory;
	IDWriteFactory * mWriteFactory = nullptr;
	IDWriteTextFormat * mWriteTextFormat = nullptr;
	ID2D1HwndRenderTarget * mRenderTarget;
};

