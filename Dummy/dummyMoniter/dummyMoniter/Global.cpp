#include"stdafx.h"
DirectGraphics graphics;
HANDLE g_iocp;
sockaddr_in clntAdr;
WSADATA wsadata;
char ip[20] = "127.0.0.1";
int g_threadCount;
DummyAvatar dummyAvatars[TEST_MAX_USER];
HWND win_hWnd;
int horizontal = 0;
int vertical = 0;


/// map<int, DrawDisPlayInfo> objectList;
tbb::concurrent_unordered_map<int, DrawDisPlayInfo> objectList2;

array<array<BYTE, 200>, 200> startTown;
array<array<BYTE, 200>, 200> Desert;
array<array<BYTE, 200>, 200> Dungeon;

int NowDisplayField = Start_Town;
Box displayBox[200][200];
LONG displayArray[200][200];

const BYTE object_bit = 1 << 0;
const BYTE collision_bit = 1 << 1;
bool isShutdown = false;

void ProcessExecute(const BYTE & inPacketType, const int & inSessionID, ReadStream & inData);
const int GetRamdom(const int & inMinValue, const int & inMaxValue);
void InitializeWorldData();

void error_display(const char *msg, int err_no)
{
	WCHAR *lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("%s", msg);
	wprintf(L"����%s\n", lpMsgBuf);
	LocalFree(lpMsgBuf);
}