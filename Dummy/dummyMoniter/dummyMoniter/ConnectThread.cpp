#include"stdafx.h"

DWORD WINAPI ConnectThread(LPVOID para)
{
	DWORD flag = 0;
	for (auto i = 0; i < TEST_MAX_USER; ++i)
	{
		Sleep(1);
		if (dummyAvatars[i].Connect(i))
		{
			/*cs_packet_Joinreq packet;
			wchar_t dummyID[50] = L"";
			wsprintf(dummyID, L"dummy-%d", i);
			wchar_t dummyPW[50] = L"";
			wsprintf(dummyPW, L"%d", i);
			wchar_t dummyEmail[50] = L"";
			wsprintf(dummyEmail, L"%d", i);
			packet.mId = dummyID;
			packet.mPw = dummyPW;
			packet.mEmail = dummyEmail;
			dummyAvatars[i].SendPacket(&packet);*/

			cs_packet_Login_req packet;
			wchar_t dummyID[50] = L"";
			wsprintf(dummyID, L"dummy-%d", i);
			wchar_t dummyPW[50] = L"";
			wsprintf(dummyPW, L"%d", i);
			packet.mID = dummyID;
			packet.mPW = dummyPW;
			dummyAvatars[i].SendPacket(&packet);


			ZeroMemory(&dummyAvatars[i].mOverlappeds, sizeof(OverlappedExtension_t));

			dummyAvatars[i].mOverlappeds.mOperation = IO_RECV;
			dummyAvatars[i].mPreviousSize = 0;
			dummyAvatars[i].mPacketSize = 0;

			dummyAvatars[i].mWsaBuf.buf = dummyAvatars[i].recvBuffer;
			dummyAvatars[i].mWsaBuf.len = IOBufferSize;

			int result = WSARecv(dummyAvatars[i].GetSocket(), &dummyAvatars[i].mWsaBuf, 1,
				NULL, &flag, reinterpret_cast<LPWSAOVERLAPPED>(&dummyAvatars[i].mOverlappeds), NULL);

			if (result == SOCKET_ERROR) {
				int err = WSAGetLastError();
				if (err != WSA_IO_PENDING && err != WSAEWOULDBLOCK) {
					error_display("recvError : ", err);
					return FALSE;
				}
			}
		}
	}
	return 0;
}
