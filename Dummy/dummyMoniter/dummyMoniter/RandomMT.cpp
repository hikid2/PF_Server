#include"stdafx.h"
#include<random>
#include<fstream>
using namespace std;
inline bool loadConfig(Document * inRoot, const char * inFileName)
{
	string tempName = ".\\";
	tempName.append(inFileName);
	tempName.append(".json");
	ifstream ifs(tempName);
	IStreamWrapper isw(ifs);

	inRoot->ParseStream(isw);
	ifs.close();

	return true;
}
const int GetRandom(const int & inMinValue, const int & inMaxValue)
{
	random_device rn;

	mt19937_64 rnd(rn());
	uniform_int_distribution<int> range(inMinValue, inMaxValue);
	return range(rnd);
}



void InsertCollisions(array<array<BYTE, 200>, 200>& inField, const Value & inJsonValue)
{
	auto & CollisionJson = inJsonValue[0]["objects"];
	uint32_t collisionSize = CollisionJson.Size();
	for (uint32_t index = 0; index < collisionSize; ++index)
	{
		int x = CollisionJson[index]["x"].GetFloat() / 32;
		int y = CollisionJson[index]["y"].GetFloat() / 32;
		inField[y][x] |= collision_bit;
	}
}

void InitializeWorldData()
{
	Document root;
	if (!loadConfig(&root, "StartTown"))
		return;
	const Value & layers = root["layers"];
	InsertCollisions(startTown, layers);
	wprintf(L"**  Read to Collision Data from Json FIle [ StartTown ]\n");

	if (!loadConfig(&root, "desertField"))
		return;
	const Value & layers2 = root["layers"];
	InsertCollisions(Desert, layers2);
	wprintf(L"**  Read to Collision Data from Json FIle [ desertField ]\n");

	if (!loadConfig(&root, "Instance"))
		return;
	const Value & layers3 = root["layers"];
	InsertCollisions(Dungeon, layers);
	wprintf(L"**  Read to Collision Data from Json FIle [ Instance ]\n");
}

