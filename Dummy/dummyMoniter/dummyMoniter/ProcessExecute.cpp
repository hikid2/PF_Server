#include"stdafx.h"
enum ENUM_FIELDINDEX
{
	enum_StartTown = 1,
	enum_Desert,
	enum_Dungeon,
};
bool IsCollisionToTerrain(int x, int y, int field)
{
	switch (field)
	{
	case Start_Town:
		if ((startTown[y][x] & collision_bit) != collision_bit) return false;
		break;
	case Desert_Field:
		if ((Desert[y][x] & collision_bit) != collision_bit) return false;
		break;
	case Ins_Dan:
		if ((Dungeon[y][x] & collision_bit) != collision_bit) return false;
		break;
	}
	return true;
}
void ProcessJoinSucc(const int & inSessionID, ReadStream & inData)
{
	cs_packet_Login_req packet;
	wchar_t dummyID[50] = L"";
	wsprintf(dummyID, L"dummy-%d", inSessionID);
	wchar_t dummyPW[50] = L"";
	wsprintf(dummyPW, L"%d", inSessionID);
	packet.mID = dummyID;
	packet.mPW = dummyPW;
	dummyAvatars[inSessionID].SendPacket(&packet);
}


void ProcessCreateAvatarRes(const int & inSessionID, ReadStream & inData)
{
	sc_packet_create_avatar_res packet;
	packet.Read(inData);
	wprintf(L"%s", packet.mMessage.c_str());
}

void ProcessGetAvatarList(const int & inSessionID, ReadStream & inData)
{
	sc_packet_Avatar_list packet;
	packet.Read(inData);
	if (!packet.avatarListData.empty())
	{
#ifndef NOHOTSPOT
		cs_packet_enterworld_no_hot_spot nohotpacket;
		nohotpacket.mAvatarId = packet.avatarListData[0].avatar_id;
		int x = GetRandom(5, 195);
		int y = GetRandom(5, 195);
		/// int fields = GetRandom(1, 2);
		int fields = 1;
		while (IsCollisionToTerrain(x, y, fields))
		{
			 x = GetRandom(5, 195);
			 y = GetRandom(5, 195);
		}
		nohotpacket.mWarpField = fields;
		nohotpacket.mX = x;
		nohotpacket.mY = y;
		dummyAvatars[inSessionID].SendPacket(&nohotpacket);
#else
		cs_packet_enterworld_req enterPacket;
		enterPacket.mAvatarId = packet.avatarListData[0].avatar_id;
		dummyAvatars[inSessionID].SendPacket(&enterPacket);

#endif // !NOHOTSPOT
	}
}
void ProcessEnterWorldSucc(const int & inSessionID, ReadStream & inData)
{
	sc_packet_enterworld_Succ packet;
	packet.Read(inData);
	objectList2[packet.mObjectID].x = packet.mX;
	objectList2[packet.mObjectID].y = packet.mY;
	objectList2[packet.mObjectID].classtype = packet.mClassType;
	objectList2[packet.mObjectID].field = packet.mNowField;
	dummyAvatars[inSessionID].objectID = packet.mObjectID;
	dummyAvatars[inSessionID].x = packet.mX;
	dummyAvatars[inSessionID].y = packet.mY;
	dummyAvatars[inSessionID].mInGameWorldState= true;
}


void ProcessEnterWorldSucc(const int & inSessionID, char inDataPtr[])
{
	Psc_packet_enterworld_Succ * packet = reinterpret_cast<Psc_packet_enterworld_Succ*>(inDataPtr);
	objectList2[packet->mObjectID].x = packet->mX;
	objectList2[packet->mObjectID].y = packet->mY;
	objectList2[packet->mObjectID].classtype = packet->mClassType;
	objectList2[packet->mObjectID].field = packet->mNowField;
	dummyAvatars[inSessionID].objectID = packet->mObjectID;
	dummyAvatars[inSessionID].x = packet->mX;
	dummyAvatars[inSessionID].y = packet->mY;
	cout << inSessionID << "is Enter World!" << endl;
	dummyAvatars[inSessionID].mInGameWorldState = true;
}


void ProcessMove(const int & inSessionID, ReadStream & inData)
{
	sc_packet_move packet;
	packet.Read(inData);
	if (objectList2.find(packet.avatarindex) != objectList2.end())
	{
		objectList2[packet.avatarindex].x = packet.mPointx;
		objectList2[packet.avatarindex].y = packet.mPointy;
	}
	if (packet.avatarindex == dummyAvatars[inSessionID].objectID)
	{
		dummyAvatars[inSessionID].x = packet.mPointx;
		dummyAvatars[inSessionID].y = packet.mPointy;
		/// printf("%d index move [ %d ] [ %d ]\n", packet.avatarindex, packet.mPointx, packet.mPointy);
	} 
}

void ProcessMove(const int & inSessionID, char * inDataPtr)
{
	Psc_packet_move * packet = reinterpret_cast<Psc_packet_move *>(inDataPtr);

	if (objectList2.find(packet->avatarindex) != objectList2.end())
	{
		objectList2[packet->avatarindex].x = packet->mPointx;
		objectList2[packet->avatarindex].y = packet->mPointy;
		if (objectList2[packet->avatarindex].y > 200)
		{
			cout << "error" << endl;
		}
	}
	/*if (packet.avatarindex == dummyAvatars[inSessionID].objectID)
	{
		dummyAvatars[inSessionID].x = packet.mPointx;
		dummyAvatars[inSessionID].y = packet.mPointy;
		/// printf("%d index move [ %d ] [ %d ]\n", packet.avatarindex, packet.mPointx, packet.mPointy);
	}*/
}

void ProcessPutObject(const int & inSessionID, ReadStream & inData)
{
	/*sc_packet_put_dummyAvatars packet;
	packet.avatarindex;
	objectList[packet.avatarindex].x =  packet.mPosX;
	objectList[packet.avatarindex].y = packet.mPosY;*/
}


void ProcessRespawn(const int & inSessionID, char * inData)
{
	Psc_packet_respawn_avatar * packet = reinterpret_cast<Psc_packet_respawn_avatar*>(inData);
	objectList2[dummyAvatars[inSessionID].objectID].field = packet->mWarpFieldId;
	objectList2[dummyAvatars[inSessionID].objectID].x = packet->mPointx;
	objectList2[dummyAvatars[inSessionID].objectID].y = packet->mPointy;
	dummyAvatars[inSessionID].mIsAlive = true;
	dummyAvatars[inSessionID].x = packet->mPointx;
	dummyAvatars[inSessionID].y = packet->mPointy;
	/// printf("respawn objectid : %d | x = %d | y = %d\n", dummyAvatars[inSessionID].objectID, packet.mPointx, packet.mPointy);

}

void ProcessUpdateHP(const int & inSessionID, char * inData)
{
	Psc_packet_update_avatarhp * packet = reinterpret_cast<Psc_packet_update_avatarhp*>(inData);
	
	if (packet->hp == 0)
		dummyAvatars[inSessionID].mIsAlive = false;
	else
		dummyAvatars[inSessionID].mIsAlive = true;
}

void ProcessLoginSucc(const int & inSessionID, char * inData)
{
	dummyAvatars[inSessionID].mLoginState = true;
}

void ProcessGetAvatarList(const int & inSessionID, char * inData)
{
	Psc_packet_Avatar_list * packet = reinterpret_cast<Psc_packet_Avatar_list * >(inData);
	int count = packet->listCnt;

	if (count != 0)
	{
#ifndef NOHOTSPOT
		cs_packet_enterworld_no_hot_spot nohotpacket;
		nohotpacket.mAvatarId = packet->profileList[0].avatar_id;
		int x = GetRandom(5, 195);
		int y = GetRandom(5, 195);
		/// int fields = GetRandom(1, 2);
		int fields = 1;
		while (IsCollisionToTerrain(x, y, fields))
		{
			x = GetRandom(5, 195);
			y = GetRandom(5, 195);
		}
		nohotpacket.mWarpField = fields;
		nohotpacket.mX = x;
		nohotpacket.mY = y;
		dummyAvatars[inSessionID].SendPacket(&nohotpacket);
#else
		cs_packet_enterworld_req enterPacket;
		enterPacket.mAvatarId = packet.avatarListData[0].avatar_id;
		dummyAvatars[inSessionID].SendPacket(&enterPacket);

#endif // !NOHOTSPOT
	}
}
////////////////////////////////////////////////////////////////




void ProcessUpdateHP(const int & inSessionID, ReadStream & inData)
{
	sc_packet_update_avatarhp packet;
	packet.Read(inData);
	if (packet.hp == 0)
		dummyAvatars[inSessionID].mIsAlive = false;
	else
		dummyAvatars[inSessionID].mIsAlive = true;
}

void ProcessRespawn(const int & inSessionID, ReadStream & inData)
{
	sc_packet_respawn_avatar packet;
	packet.Read(inData);
	objectList2[dummyAvatars[inSessionID].objectID].field = packet.mWarpFieldId;
	objectList2[dummyAvatars[inSessionID].objectID].x = packet.mPointx;
	objectList2[dummyAvatars[inSessionID].objectID].y = packet.mPointy;
	dummyAvatars[inSessionID].mIsAlive = true;
	dummyAvatars[inSessionID].x = packet.mPointx;
	dummyAvatars[inSessionID].y = packet.mPointy;
	/// printf("respawn objectid : %d | x = %d | y = %d\n", dummyAvatars[inSessionID].objectID, packet.mPointx, packet.mPointy);

}

void ProcessLoginSucc(const int & inSessionID, ReadStream & inData)
{
	/*cs_packet_create_avatar_req packet;
	wchar_t dummyID[50] = L"";
	wsprintf(dummyID, L"dmBot%d", inSessionID);
	packet.mAvatarName = dummyID;
	packet.mClassType = GetRamdom(1,3);
	dummyAvatars[inSessionID].SendPacket(&packet);*/
	dummyAvatars[inSessionID].mLoginState = true;
}
////////////////////////////////////////////////////////////////
void ProcessExecute(const BYTE & inPacketType, const int & inSessionID, char * inData)
{
	switch (inPacketType)
	{
	case PacketType::SC_JOIN_SUCC:
		break;
	case PacketType::SC_JOIN_FAIL:
		break;
	case PacketType::SC_LOGIN_SUCC:
		ProcessLoginSucc(inSessionID, inData);
		break;
	case PacketType::SC_LOGIN_FAIL:
		break;
	case PacketType::SC_PUT_dummyAvatars:
		/// ProcessPutObject(inSessionID, inData);
		break;
	case PacketType::SC_ENTERWORLD_Succ:
		ProcessEnterWorldSucc(inSessionID, inData);
		break;
	case PacketType::SC_CREATE_AVATAR_RES:
		break;
	case PacketType::SC_AVATAR_LIST:
		ProcessGetAvatarList(inSessionID, inData);
		break;
	case PacketType::SC_MOVE_dummyAvatars:
		ProcessMove(inSessionID, inData);
		break;
	case PacketType::SC_Respawn_Avatar:
		ProcessRespawn(inSessionID, inData);
		break;
	case PacketType::SC_UPDATE_AVATARHP:
		ProcessUpdateHP(inSessionID, inData);
		break;
	default:
		break;
	}
}
void ProcessExecute(const BYTE & inPacketType, const int & inSessionID, ReadStream & inData)
{
	switch (inPacketType)
	{
	case PacketType::SC_JOIN_SUCC:
		ProcessJoinSucc(inSessionID, inData);
		break;
	case PacketType::SC_JOIN_FAIL:
		break;
	case PacketType::SC_LOGIN_SUCC:
		ProcessLoginSucc(inSessionID, inData);
		break;
	case PacketType::SC_LOGIN_FAIL:
		break;
	case PacketType::SC_PUT_dummyAvatars:
		ProcessPutObject(inSessionID, inData);
		break;
	case PacketType::SC_ENTERWORLD_Succ:
		ProcessEnterWorldSucc(inSessionID, inData);
		break;
	case PacketType::SC_CREATE_AVATAR_RES:
		ProcessCreateAvatarRes(inSessionID, inData);
		break;
	case PacketType::SC_AVATAR_LIST:
		ProcessGetAvatarList(inSessionID, inData);
		break;
	case PacketType::SC_MOVE_dummyAvatars:
		ProcessMove(inSessionID, inData);
		break;
	case PacketType::SC_Respawn_Avatar:
		ProcessRespawn(inSessionID, inData);
		break;
	case PacketType::SC_UPDATE_AVATARHP:
		ProcessUpdateHP(inSessionID, inData);
		break;
	default:
		break;
	} 

}