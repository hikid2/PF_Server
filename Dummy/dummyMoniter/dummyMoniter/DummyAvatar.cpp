#include "stdafx.h"

DummyAvatar::DummyAvatar() {
	mConnectState = false;
	mSocket = NULL;
	mLoginState = false;
	mInGameWorldState = false;
	mConnectState = false;
	mIsAlive = true;
}

bool DummyAvatar::Connect(int index)
{
	mSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	int opt_val = 1;
	setsockopt(mSocket, IPPROTO_TCP, TCP_NODELAY, (const char *)&opt_val, sizeof(opt_val));
	HANDLE iocp = CreateIoCompletionPort((HANDLE)mSocket, g_iocp, index, 0);
	int retval = 0;
	retval = WSAConnect(mSocket, (SOCKADDR*)&clntAdr, sizeof(clntAdr), NULL, NULL, NULL, NULL);
	if (retval == SOCKET_ERROR)
	{
		retval = WSAGetLastError();
		printf("WSAConnect() Error!!\n");
		return false;
	}
	mConnectState = true;
	return true;
}

bool DummyAvatar::DisConnect()
{
	if (mSocket)
		closesocket(mSocket);
	mConnectState = false;
	return true;
}
/*
void Session::SendPacket(Packet * inPacket)
{
WriteStream streamData;
inPacket->Write(streamData);
streamData.WriteSize();
uint32_t packetSize = streamData.GetOffset();
size_t sendOffset = 0;
while (true)
{
int retval = Send(streamData, packetSize, sendOffset);
if (retval == SOCKET_ERROR) {
if (WSAGetLastError() != WSAEWOULDBLOCK) {
NLog(L"Send Error");
closesocket(mSocket);
return;
}
mIoData[LWRITE].WriteData(streamData, packetSize);
break;
}
if (retval < packetSize) {
sendOffset = retval;
packetSize -= retval;
continue;
}
break;
}
}

*/
void DummyAvatar::SendPacket(Packet * inPacket)
{
	WriteStream streamData;
	inPacket->Write(streamData);
	streamData.WriteSize();
	uint32_t packetSize = streamData.GetOffset();
	SendInfo_t * sendData = new SendInfo_t();
	memcpy_s(sendData->mSendBuffer, MAX_BUFF_SIZE,streamData.GetData(), packetSize);
	sendData->mSendWSABuf.buf = sendData->mSendBuffer;
	sendData->mSendWSABuf.len = packetSize;
	sendData->mOverlapSend.mOperation = IO_SEND;
	size_t sendOffset = 0;

	int retval = ::WSASend(mSocket, &sendData->mSendWSABuf, 1, NULL, 0, reinterpret_cast<LPWSAOVERLAPPED>(&sendData->mOverlapSend), NULL);
	if (retval == SOCKET_ERROR) {
		int errorCode = WSAGetLastError();
		if (errorCode != WSAEWOULDBLOCK && errorCode != WSA_IO_PENDING) {
			error_display("sendError", errorCode);
			closesocket(mSocket);
			return;
		}
	}

}

const SOCKET & DummyAvatar::GetSocket() { return mSocket; }

void DummyAvatar::OnRecv(const int & comp_key)
{

	
}
