#pragma once
#define PORT 9100
#define TEST_MAX_USER 4500

#define IO_SEND 1
#define IO_RECV 2

extern array<array<BYTE, 200>, 200> startTown;
extern array<array<BYTE, 200>, 200> Desert;
extern array<array<BYTE, 200>, 200> Dungeon;
extern void error_display(const char *msg, int err_no);
extern bool isShutdown;
extern LONG displayArray[200][200];
extern Box displayBox[200][200];
extern int NowDisplayField;
extern thread threadPtr;
extern const BYTE collision_bit;
extern const BYTE object_bit;
extern int horizontal;
extern int vertical;
/// extern map<int, DrawDisPlayInfo> objectList;
extern tbb::concurrent_unordered_map<int, DrawDisPlayInfo> objectList2;
extern WSADATA wsadata;
extern HWND win_hWnd;
extern DirectGraphics graphics;
extern DummyAvatar dummyAvatars[TEST_MAX_USER];
extern sockaddr_in clntAdr;
extern char ip[20];
extern HANDLE g_iocp;
extern int g_threadCount;
extern int GetPhysicalProcessorsNum(void);
extern DWORD WINAPI ConnectThread(LPVOID para);
extern DWORD WINAPI WorkerThread(LPVOID para);
extern DWORD WINAPI UpdateThread(LPVOID para);
// extern void ProcessExecute(const BYTE & inPacketType);
extern void InitializeWorldData();
extern void ProcessExecute(const BYTE & inPacketType, const int & inSessionID, ReadStream & inData);
extern void ProcessExecute(const BYTE & inPacketType, const int & inSessionID, char * inData);
extern BOOL CALLBACK InputServerIpDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
extern const int GetRandom(const int & inMinValue, const int & inMaxValue);