#pragma once
#include<vector>
class Packet
{
public:
	virtual BYTE GetPacketType() = 0;
	virtual void Write(WriteStream & inStream) { }
	virtual void Read(ReadStream & inStream) { }
};
enum WorldEnum
{
	Start_Town = 1,
	Desert_Field,
	Ins_Dan,
	Notyet,
};

class DB_AvatarList_Profile
{
public:
	DB_AvatarList_Profile() {}
	DB_AvatarList_Profile(const DB_AvatarList_Profile & other)
	{
		avatar_id = other.avatar_id;
		classType = other.classType;
		name = other.name;
		level = other.level;
		fieldID = other.fieldID;

	}
	DB_AvatarList_Profile(DB_AvatarList_Profile && other)
	{
		*this = move(other);
	}
	DB_AvatarList_Profile & operator=(DB_AvatarList_Profile && other)
	{
		if (this != &other)
		{
			avatar_id = other.avatar_id;
			classType = other.classType;
			name = move(other.name);
			level = other.level;
			fieldID = other.fieldID;

			other.avatar_id = -1;
			other.classType = 0;
			other.level = 0;
			other.fieldID = 0;
		}
		return *this;
	}
	void Write(WriteStream & inStream)
	{
		inStream.Write(avatar_id);
		inStream.Write(classType);
		inStream.Write(name);
		inStream.Write(level);
		inStream.Write(fieldID);
	}
	void Read(ReadStream & inStream)
	{
		inStream.Read(&avatar_id);
		inStream.Read(&classType);
		inStream.Read(&name);
		inStream.Read(&level);
		inStream.Read(&fieldID);
	}
	void Read(unsigned char buf[])
	{
		avatar_id = (int)buf;
		buf += sizeof(int);

		classType = (BYTE)buf;
		buf += sizeof(BYTE);

		
		size_t wstringLen = 0;
		wstringLen = (size_t)buf;
		buf += sizeof(size_t);

		wchar_t * read_wstring = (wchar_t *)buf;
		name.append(read_wstring, wstringLen);
		buf += sizeof(wchar_t) * wstringLen;
		level = (int)buf;
		buf += sizeof(int);
		fieldID = (int)buf;
		buf += sizeof(fieldID);
	}
	int					avatar_id;
	BYTE				classType;
	wstring				name;
	int					level;
	int					fieldID;
};

enum PacketType
{
	SC_LOGIN_OK,
	SC_LOGIN_FAIL,
	SC_INPUT,
	SC_PUT_dummyAvatars,
	SC_REMOVE_dummyAvatars,
	SC_MOVE_dummyAvatars,
	SC_LOGIN_REQ,
	SC_JOIN_REQ,
	SC_JOIN_SUCC,
	SC_JOIN_FAIL,
	SC_LOGIN_SUCC,
	SC_AVATAR_LIST,
	SC_CREATE_AVATAR_REQ,
	SC_CREATE_AVATAR_RES,
	SC_ENTERWORLD_REQ,
	SC_ENTERWORLD_Succ,
	SC_UPDATE_AVATARHP,
	SC_CHAT_REQ,
	SC_Notify_LevelUp,
	SC_UPDATE_Avatar_Exp,
	SC_Broad_Chat,
	SC_Warp_Field,
	SC_Respawn_Avatar,
	SC_Update_Gold,
	SC_enterworld_no_hot_spot,
};

#pragma pack(push, 1)
struct sc_packet_login_ok
{
	uint32_t size;
	BYTE type;
	int id;
	int x;
	int y;
	BYTE class_type;
	uint32_t curr_hp;
	uint32_t max_hp;
	uint32_t curr_mp;
	uint32_t max_mp;
	uint32_t curr_exp;
	uint32_t next_exp;
	uint32_t level;
};

#pragma pack(pop)

class sc_packet_remove_dummyAvatars : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_REMOVE_dummyAvatars;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(id);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&id);
	}
	int id;
};

class sc_packet_broadcast_chat : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_Broad_Chat;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mMessage);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mMessage);
	}
	wstring mMessage;
};

class sc_packet_update_avatar_exp : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_UPDATE_Avatar_Exp;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mNowExp);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mNowExp);
	}
	int mNowExp;
};

class sc_packet_move : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_MOVE_dummyAvatars;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(avatarindex);
		inStream.Write(mPointx);
		inStream.Write(mPointy);
		inStream.Write(mHorizontal);
		inStream.Write(mVertical);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&avatarindex);
		inStream.Read(&mPointx);
		inStream.Read(&mPointy);
		inStream.Read(&mHorizontal);
		inStream.Read(&mVertical);
	}
	int		avatarindex;
	int32_t mPointx;
	int32_t mPointy;
	int32_t mHorizontal;
	int32_t mVertical;
};

class sc_packet_notify_Levelup : public Packet
{
public:
	virtual BYTE GetPacketType() {
		return SC_Notify_LevelUp;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mLv);
		inStream.Write(mNowExp);
		inStream.Write(mNeedExp);
		inStream.Write(mNowHP);
		inStream.Write(mNowMP);
		inStream.Write(mMaxHP);
		inStream.Write(mMaxMP);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mLv);
		inStream.Read(&mNowExp);
		inStream.Read(&mNeedExp);
		inStream.Read(&mNowHP);
		inStream.Read(&mNowMP);
		inStream.Read(&mMaxHP);
		inStream.Read(&mMaxMP);
	}
	int mLv;
	int mNowExp;
	int mNeedExp;
	int mNowHP;
	int mNowMP;
	int mMaxHP;
	int mMaxMP;
};
class sc_packet_input : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_INPUT;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(key);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&key);
	}
	int key;
};


class sc_packet_enterworld_Succ : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_ENTERWORLD_Succ;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mObjectID);
		inStream.Write(mName);
		inStream.Write(mLevel);
		inStream.Write(mClassType);
		inStream.Write(mCurrHP);
		inStream.Write(mCurrMP);
		inStream.Write(mMaxHP);
		inStream.Write(mMaxMP);
		inStream.Write(mCurrExp);
		inStream.Write(mNeedExp);
		inStream.Write(mNowField);
		inStream.Write(mX);
		inStream.Write(mY);
		inStream.Write(mHasGold);
		inStream.Write(str);
		inStream.Write(dex);
		inStream.Write(ints);
		inStream.Write(luck);
		inStream.Write(mHorizontal);
		inStream.Write(mVertial);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mObjectID);
		inStream.Read(&mName);
		inStream.Read(&mLevel);
		inStream.Read(&mClassType);
		inStream.Read(&mCurrHP);
		inStream.Read(&mCurrMP);
		inStream.Read(&mMaxHP);
		inStream.Read(&mMaxMP);
		inStream.Read(&mCurrExp);
		inStream.Read(&mNeedExp);
		inStream.Read(&mNowField);
		inStream.Read(&mX);
		inStream.Read(&mY);
		inStream.Read(&mHasGold);
		inStream.Read(&str);
		inStream.Read(&dex);
		inStream.Read(&ints);
		inStream.Read(&luck);
		inStream.Read(&mHorizontal);
		inStream.Read(&mVertial);
	}
	int			mObjectID;
	wstring		mName;
	int			mLevel;
	BYTE		mClassType;
	uint32_t	mCurrHP;
	uint32_t	mCurrMP;
	uint32_t	mMaxHP;
	uint32_t	mMaxMP;
	int64_t		mCurrExp;
	int64_t		mNeedExp;
	int		mNowField;
	int32_t		mX;
	int32_t		mY;
	int			mHasGold;
	int			str;
	int			dex;
	int			ints;
	int			luck;
	int			mHorizontal;
	int			mVertial;
};

class sc_packet_put_dummyAvatars : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_PUT_dummyAvatars;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(isNPC);
		inStream.Write(mName);
		inStream.Write(avatarindex);
		inStream.Write(mPosX);
		inStream.Write(mPosY);
		inStream.Write(mClasstype);
		inStream.Write(mCurrHP);
		inStream.Write(mMaxHP);
		inStream.Write(mCurrMP);
		inStream.Write(mMaxMP);
		inStream.Write(mLv);
		inStream.Write(mHorizontal);
		inStream.Write(mVertial);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&isNPC);
		inStream.Read(&mName);
		inStream.Read(&avatarindex);
		inStream.Read(&mPosX);
		inStream.Read(&mPosY);
		inStream.Read(&mClasstype);
		inStream.Read(&mCurrHP);
		inStream.Read(&mMaxHP);
		inStream.Read(&mCurrMP);
		inStream.Read(&mMaxMP);
		inStream.Read(&mLv);
		inStream.Read(&mHorizontal);
		inStream.Read(&mVertial);

	}
	bool		isNPC;
	wstring		mName;
	int			avatarindex;
	int			mPosX;
	int			mPosY;
	BYTE		mClasstype;
	uint32_t	mCurrHP;
	uint32_t	mMaxHP;
	uint32_t	mCurrMP;
	uint32_t	mMaxMP;
	int			mLv;
	int			mHorizontal;
	int			mVertial;
};


class sc_packet_join_succ : public Packet
{
	virtual BYTE GetPacketType()
	{
		return SC_JOIN_SUCC;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
	}
	virtual void Read(ReadStream & inStream)
	{
	}
};

class sc_packet_Login_succ : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_LOGIN_SUCC;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
	}
	virtual void Read(ReadStream & inStream)
	{
	}
};

class sc_packet_login_fail : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_LOGIN_FAIL;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
	}
	virtual void Read(ReadStream & inStream)
	{
	}
};

class sc_packet_join_fail : public Packet
{
	virtual BYTE GetPacketType()
	{
		return SC_JOIN_FAIL;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
	}
	virtual void Read(ReadStream & inStream)
	{
	}
};

class sc_packet_update_avatarhp : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_UPDATE_AVATARHP;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(id);
		inStream.Write(hp);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&id);
		inStream.Read(&hp);
	}
	int id;
	int hp;
};

class sc_packet_create_avatar_res : public Packet
{
public:
	virtual BYTE GetPacketType() { return SC_CREATE_AVATAR_RES; }
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mResult);
		inStream.Write(mMessage);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mResult);
		inStream.Read(&mMessage);
	}
	int mResult;
	wstring mMessage;
};

class sc_packet_wrap_field : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_Warp_Field;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mAvatarID);
		inStream.Write(mWarpField);
		inStream.Write(mPointx);
		inStream.Write(mPointy);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mAvatarID);
		inStream.Read(&mWarpField);
		inStream.Read(&mPointx);
		inStream.Read(&mPointy);
	}
	int mAvatarID;
	BYTE mWarpField;
	int mPointx;
	int mPointy;
};

class sc_packet_respawn_avatar : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_Respawn_Avatar;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mPointx);
		inStream.Write(mPointy);
		inStream.Write(mWarpFieldId);
		inStream.Write(mNowHP);
		inStream.Write(mNowMP);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mPointx);
		inStream.Read(&mPointy);
		inStream.Read(&mWarpFieldId);
		inStream.Read(&mNowHP);
		inStream.Read(&mNowMP);
	}
	int32_t mPointx;
	int32_t mPointy;
	BYTE	mWarpFieldId;
	uint32_t mNowHP;
	uint32_t mNowMP;
};

class cs_packet_Joinreq : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_JOIN_REQ;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mId);
		inStream.Write(mPw);
		inStream.Write(mEmail);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mId);
		inStream.Read(&mPw);
		inStream.Read(&mEmail);
	}
	wstring mId;
	wstring mPw;
	wstring mEmail;
};

class sc_packet_update_avatar_Gold : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_Update_Gold;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mNowGold);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mNowGold);
	}
	int mNowGold;
};

class cs_packet_Login_req : public Packet
{
public:
	virtual BYTE GetPacketType() { return SC_LOGIN_REQ; }

	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mID);
		inStream.Write(mPW);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mID);
		inStream.Read(&mPW);
	}
	wstring mID;
	wstring mPW;
};

class cs_packet_create_avatar_req : public Packet
{
public:
	virtual BYTE GetPacketType() { return SC_CREATE_AVATAR_REQ; }

	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mAvatarName);
		inStream.Write(mClassType);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mAvatarName);
		inStream.Read(&mClassType);

	}
	wstring mAvatarName;
	BYTE	mClassType;
};

class cs_packet_chat_req : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_CHAT_REQ;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mMessage);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mMessage);
	}
	wstring mMessage;
};

class cs_packet_enterworld_req : public Packet
{
public:
	virtual BYTE GetPacketType() { return SC_ENTERWORLD_REQ; }
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mAvatarId);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mAvatarId);
	}
	int mAvatarId;
};

class cs_packet_enterworld_no_hot_spot : public Packet
{
public:
	virtual BYTE GetPacketType() { return SC_enterworld_no_hot_spot; }
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(mAvatarId);
		inStream.Write(mX);
		inStream.Write(mY);
		inStream.Write(mWarpField);
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&mAvatarId);
		inStream.Read(&mX);
		inStream.Read(&mY);
		inStream.Read(&mWarpField);
	}
	int mAvatarId;
	int mX;
	int mY;
	BYTE mWarpField;
};
class sc_packet_Avatar_list : public Packet
{
public:
	virtual BYTE GetPacketType()
	{
		return SC_AVATAR_LIST;
	}
	virtual void Write(WriteStream & inStream)
	{
		inStream.Write(GetPacketType());
		inStream.Write(listCnt);
		for (auto iter : avatarListData)
		{
			iter.Write(inStream);
		}
	}
	virtual void Read(ReadStream & inStream)
	{
		inStream.Read(&listCnt);
		for (auto index = 0; index < listCnt; ++index)
		{
			if (index > 9)
				break;
			DB_AvatarList_Profile data;
			data.Read(inStream);
			avatarListData.emplace_back(std::move(data));
		}
	}
	int listCnt;
	vector<DB_AvatarList_Profile> avatarListData;
};
